# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building LibTool as part of this project.
#

# Temporary installation directory for libtool.
set( _libtoolInstallDir "${CMAKE_BINARY_DIR}/Externals/LibTool/install" )

# Build libtool.
ExternalProject_Add( LibTool
   PREFIX "${CMAKE_BINARY_DIR}/Externals/LibTool"
   INSTALL_DIR "${_libtoolInstallDir}"
   URL "http://ftpmirror.gnu.org/libtool/libtool-2.4.6.tar.gz"
   URL_MD5 "addf44b646ddb4e3919805aa88fa7c5e"
   CONFIGURE_COMMAND <SOURCE_DIR>/configure --prefix=<INSTALL_DIR>
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( LibTool buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory <INSTALL_DIR>/
   ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   COMMENT "Installing LibTool into the build area"
   DEPENDEES install )

# Install the OpenCL C headers along with the project.
install( DIRECTORY ${_libtoolInstallDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS )
