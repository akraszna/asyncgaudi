# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# File configuring environment (an) variable(s) needed by POCL.
#

# Set up the variable responsible for the environment variable setting.
set( POCLENVIRONMENT_ENVIRONMENT
   FORCESET OPENCL_VENDOR_PATH "\${AsyncGaudi_DIR}/etc/OpenCL/vendors"
   FORCESET OCL_ICD_VENDORS    "\${AsyncGaudi_DIR}/etc/OpenCL/vendors" )

# Declare the module found.
set( POCLENVIRONMENT_FOUND TRUE )
