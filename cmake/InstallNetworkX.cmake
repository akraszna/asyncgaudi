# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Configuration for installing the NetworkX python package as part of the
# project.
#

# This installation needs python and pip.
find_package( PythonInterp 2.7 REQUIRED )
find_package( pip REQUIRED )

# Directories/files used in the installation.
set( _buildDir "${CMAKE_BINARY_DIR}/Externals/NetworkX/install" )
set( _stampFile
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/NetworkX.stamp" )

# Set up the installation of the NetworkX module.
add_custom_command( OUTPUT "${_stampFile}"
   COMMAND ${CMAKE_COMMAND} -E env --unset=SHELL PYTHONUSERBASE=${_buildDir}
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh
   ${PIP_EXECUTABLE} install --disable-pip-version-check --no-cache-dir
   --user "networkx==2.2"
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}
   ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   COMMAND ${CMAKE_COMMAND} -E touch "${_stampFile}"
   WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}"
   COMMENT "Building/installing NetworkX...")

# Set up a target that installs NetworkX.
add_custom_target( NetworkX ALL DEPENDS "${_stampFile}" )

# Set up the installation of NetworkX with the project.
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Set up the runtime environment for the installed python package(s).
configure_file(
   "${CMAKE_SOURCE_DIR}/cmake/PythonEnvironmentConfig.cmake.in"
   "${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PythonEnvironmentConfig.cmake"
   @ONLY )
set( PythonEnvironment_DIR "${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}"
   CACHE PATH "Location of PythonEnvironmentConfig.cmake" )
find_package( PythonEnvironment REQUIRED )
