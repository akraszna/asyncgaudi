# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building Gaudi as part of this project.
#

# Find/set up python, as we need to know which version of python Gaudi
# should be pointed at...
find_package( PythonInterp REQUIRED )
find_package( PythonLibs REQUIRED )

# Set up the configuration file that will be used by the Gaudi build.
configure_file( "${CMAKE_SOURCE_DIR}/cmake/AsyncGaudiExternalsConfig.cmake.in"
   "${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/AsyncGaudiExternalsConfig.cmake"
   @ONLY )

# Directories used by the Gaudi build.
set( _installDir "${CMAKE_BINARY_DIR}/Externals/Gaudi/install" )
set( _stampDir   "${CMAKE_BINARY_DIR}/Externals/Gaudi/src/Gaudi-stamp" )

# Set up the build of Gaudi.
ExternalProject_Add( Gaudi
   PREFIX ${CMAKE_BINARY_DIR}/Externals/Gaudi
   INSTALL_DIR ${_installDir}
   SOURCE_DIR ${CMAKE_SOURCE_DIR}/Externals/Gaudi
   STAMP_DIR ${_stampDir}
   DOWNLOAD_COMMAND ""
   CMAKE_CACHE_ARGS
   -DAsyncGaudiExternals_DIR:PATH=${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}
   -DGAUDI_ATLAS:BOOL=TRUE -DGAUDI_ATLAS_BASE_PROJECT:STRING=AsyncGaudiExternals
   -DGAUDI_USE_STD_FILESYSTEM:BOOL=FALSE
   -DCMAKE_INSTALL_PREFIX:PATH=<INSTALL_DIR>
   -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
   -DATLAS_FORCE_PYTHON2:BOOL=${ATLAS_FORCE_PYTHON2}
   LOG_CONFIGURE 1
   BUILD_BYPRODUCTS
   "${_installDir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}GaudiPluginService${CMAKE_SHARED_LIBRARY_SUFFIX}"
   "${_installDir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}GaudiKernel${CMAKE_SHARED_LIBRARY_SUFFIX}" )
ExternalProject_Add_Step( Gaudi buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory <INSTALL_DIR>/
   ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   COMMENT "Installing Gaudi into the build area"
   DEPENDEES install )

# Set up a target that allow a convenient incremental build of Gaudi. It's
# necessary because the "Gaudi" target set up by ExternalProject_Add(...) does
# not recognise changes in the Gaudi submodule's source code. After "building"
# this target, you need to perform a "normal" build of the project to do the
# incremental build of Gaudi.
add_custom_target( GaudiRebuild
   COMMAND ${CMAKE_COMMAND} -E remove -f "${_stampDir}/Gaudi-configure"
   COMMENT "Forcing the re-configuration/build of Gaudi" )

# Install Gaudi together with the main project.
install( DIRECTORY ${_installDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS )

# Set up where Gaudi would be picked up from during the build.
set( GAUDI_ROOT
   "$<BUILD_INTERFACE:${_installDir}>:$<INSTALL_INTERFACE:\${CUDAGaudi_DIR}>" )
set( GAUDI_INSTALLAREA "${_installDir}"
   CACHE PATH "Directory to pick up Gaudi from" )
set( GAUDI_INCLUDE_DIR "${GAUDI_INSTALLAREA}/include"
   CACHE PATH "Directory to pick up the Gaudi headers from" )

# Forcefully create the include directory already now.
file( MAKE_DIRECTORY "${GAUDI_INCLUDE_DIR}" )

# Set up compilation flags for the main project, that have to do with Gaudi.
add_definitions( -DGAUDI_V20_COMPAT=1 )

# Clean up.
unset( _installDir )
unset( _stampDir )
