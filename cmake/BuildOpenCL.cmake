#  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building a vendor independent version of the OpenCL headers
# and ICD library.
#

# Tell the user what's happening.
message( STATUS
   "Providing OpenCL headers and an ICD library as part of the project" )

# Location of the OpenCL C headers.
set( _openclCHeadersDir "${CMAKE_SOURCE_DIR}/Externals/OpenCL-Headers" )

# Install the OpenCL C headers along with the project.
install( DIRECTORY ${_openclCHeadersDir}/CL
   DESTINATION ${CMAKE_INSTALL_INCLUDEDIR} USE_SOURCE_PERMISSIONS )

# Temporary installation directory for the OpenCL C++ header(s).
set( _openclCppHeadersInstallDir
   "${CMAKE_BINARY_DIR}/Externals/OpenCL/cpp-headers/install" )

# Build the OpenCL C++ wrapper header(s).
ExternalProject_Add( OpenCLCppHeaders
   PREFIX "${CMAKE_BINARY_DIR}/Externals/OpenCL/cpp-headers"
   INSTALL_DIR "${_openclCppHeadersInstallDir}"
   URL "https://github.com/KhronosGroup/OpenCL-CLHPP/archive/v2.0.10.tar.gz"
   URL_MD5 "7ccdc2416bce676f9a4dcb3a8a4caeb7"
   CMAKE_CACHE_ARGS -DCMAKE_INSTALL_PREFIX:PATH=<INSTALL_DIR>/include
   -DOPENCL_INCLUDE_DIR:PATH=${_openclCHeadersDir}
   -DBUILD_DOCS:BOOL=FALSE -DBUILD_EXAMPLES:BOOL=FALSE
   -DBUILD_TESTS:BOOL=FALSE
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( OpenCLCppHeaders buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory <INSTALL_DIR>/
   ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   COMMENT "Installing the OpenCL C++ headers into the build area"
   DEPENDEES install )

# Install the OpenCL C++ header(s) along with the project.
install( DIRECTORY ${_openclCppHeadersInstallDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS )

# Temporary installation directory for ocl-icd.
set( _oclInstallDir "${CMAKE_BINARY_DIR}/Externals/OpenCL/ocl-icd/install" )

# Build ocl-icd.
ExternalProject_Add( OpenCLICD
   DEPENDS LibTool
   PREFIX "${CMAKE_BINARY_DIR}/Externals/OpenCL/ocl-icd"
   INSTALL_DIR "${_oclInstallDir}"
   URL "https://github.com/OCL-dev/ocl-icd/archive/v2.2.12.tar.gz"
   URL_MD5 "47035a0b597fe334be16a653c7c7951f"
   CONFIGURE_COMMAND
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh
   libtoolize
   COMMAND
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh
   <SOURCE_DIR>/bootstrap
   COMMAND ${CMAKE_COMMAND} -E env
   CPPFLAGS=-I${_openclCHeadersDir}
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh
   <SOURCE_DIR>/configure --prefix=<INSTALL_DIR>
   BUILD_IN_SOURCE 1
   LOG_CONFIGURE 1
   BUILD_BYPRODUCTS
   ${_oclInstallDir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}OpenCL${CMAKE_SHARED_LIBRARY_SUFFIX} )
ExternalProject_Add_Step( OpenCLICD buildinstall
   COMMAND ${CMAKE_COMMAND} -E remove
   <INSTALL_DIR>/lib/${CMAKE_SHARED_LIBRARY_PREFIX}OpenCL.la
   COMMAND ${CMAKE_COMMAND} -E copy_directory <INSTALL_DIR>/
   ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   COMMENT "Installing ocl-icd into the build area"
   DEPENDEES install )

# Install ocl-icd along with the project.
install( DIRECTORY ${_oclInstallDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS )

# Set up the variables that will be used in the build to use OpenCL.
set( OpenCL_INCLUDE_DIRS $<BUILD_INTERFACE:${_openclCHeadersDir}>
   $<BUILD_INTERFACE:${_openclCppHeadersInstallDir}/include>
   $<INSTALL_INTERFACE:include> )
set( OpenCL_LIBRARIES
   $<BUILD_INTERFACE:${_oclInstallDir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}OpenCL${CMAKE_SHARED_LIBRARY_SUFFIX}>
   $<INSTALL_INTERFACE:lib/${CMAKE_SHARED_LIBRARY_PREFIX}OpenCL${CMAKE_SHARED_LIBRARY_SUFFIX}> )
