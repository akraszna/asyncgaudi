# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building the Portable Computing Language
# library / OpenCL device as part of this project.
#

# Tell the user what's happening.
message( STATUS "Providing POCL as part of the project" )

# Temporary installation directory for LLVM / Clang.
set( _llvmInstallDir "${CMAKE_BINARY_DIR}/Externals/POCL/llvm/install" )

# Build LLVM / Clang.
ExternalProject_Add( LLVM
   PREFIX "${CMAKE_BINARY_DIR}/Externals/POCL/llvm"
   INSTALL_DIR "${_llvmInstallDir}"
   URL "https://github.com/llvm/llvm-project/archive/llvmorg-8.0.0.tar.gz"
   URL_MD5 "56d611480b48aee351c13e113f7722e6"
   SOURCE_SUBDIR "llvm"
   CMAKE_CACHE_ARGS -DCMAKE_INSTALL_PREFIX:PATH=<INSTALL_DIR>
   -DCMAKE_BUILD_TYPE:STRING=Release
   -DLLVM_ENABLE_PROJECTS:STRING=clang
   -DBUILD_SHARED_LIBS:BOOL=OFF
   LOG_CONFIGURE 1 )

# Temporary installation directory for HWLoc.
set( _hwlocInstallDir "${CMAKE_BINARY_DIR}/Externals/POCL/hwloc/install" )

# Build HWLoc.
ExternalProject_Add( HWLoc
   PREFIX "${CMAKE_BINARY_DIR}/Externals/POCL/hwloc"
   INSTALL_DIR "${_hwlocInstallDir}"
   URL "https://download.open-mpi.org/release/hwloc/v2.0/hwloc-2.0.4.tar.bz2"
   URL_MD5 "c4e43c41abbd76765273b7c91269cac7"
   CONFIGURE_COMMAND <SOURCE_DIR>/configure --prefix=<INSTALL_DIR>
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( HWLoc buildinstall
   COMMAND ${CMAKE_COMMAND} -E remove
   <INSTALL_DIR>/lib/${CMAKE_SHARED_LIBRARY_PREFIX}hwloc.la
   COMMAND ${CMAKE_COMMAND} -E copy_directory <INSTALL_DIR>/
   ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   COMMENT "Installing HWLoc into the build area"
   DEPENDEES install )

# Install HWLoc along with the project.
install( DIRECTORY ${_hwlocInstallDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS )

# Temporary installation directory for POCL.
set( _poclInstallDir "${CMAKE_BINARY_DIR}/Externals/POCL/pocl/install" )

# CMake prefix path to be used for the POCL build.
set( _poclPrefixPath ${_llvmInstallDir} ${_libtoolInstallDir}
   ${_hwlocInstallDir} )
if( ATLAS_BUILD_OPENCL )
   list( APPEND _poclPrefixPath ${_oclInstallDir} )
endif()

# Build POCL itself.
ExternalProject_Add( POCL
   DEPENDS LLVM LibTool HWLoc
   PREFIX "${CMAKE_BINARY_DIR}/Externals/POCL/pocl"
   INSTALL_DIR "${_poclInstallDir}"
   URL "https://github.com/pocl/pocl/archive/v1.3.tar.gz"
   URL_MD5 "eab5b76466ea6fb3fa0ec1a60812dcc2"
   CMAKE_CACHE_ARGS -DCMAKE_INSTALL_PREFIX:PATH=<INSTALL_DIR>
   -DCMAKE_BUILD_TYPE:STRING=Release
   -DCMAKE_PREFIX_PATH:STRING=${_poclPrefixPath}
   -DKERNELLIB_HOST_CPU_VARIANTS:STRING=distro
   -DPOCL_ICD_ABSOLUTE_PATH:BOOL=FALSE
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( POCL buildinstall
   COMMAND ${CMAKE_COMMAND} -E remove_directory <INSTALL_DIR>/lib/pkgconfig
   COMMAND ${CMAKE_COMMAND} -E copy_directory <INSTALL_DIR>/
   ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   COMMENT "Installing POCL into the build area"
   DEPENDEES install )
if( ATLAS_BUILD_OPENCL )
   add_dependencies( POCL OpenCLICD )
endif()

# Install POCL along with the project.
install( DIRECTORY ${_poclInstallDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS )

# Set up the correct runtime environment for POCL.
set( POCLEnvironment_DIR ${CMAKE_SOURCE_DIR}/cmake
   CACHE PATH "Directory holding POCLEnvironmentConfig.cmake" )
mark_as_advanced( POCLEnvironment_DIR )
find_package( POCLEnvironment REQUIRED )
