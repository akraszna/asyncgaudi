# Asynchronous Gaudi Development

This repository is meant to collect all the GPU code that I developed on top
of [Gaudi](https://gitlab.cern.ch/gaudi/Gaudi). It collects code from
[CUDATests](https://gitlab.cern.ch/akraszna/CUDATests),
[CUDAGaudi](https://gitlab.cern.ch/akraszna/CUDAGaudi),
[CUDATBBScheduler](https://gitlab.cern.ch/akraszna/CUDATBBScheduler),
[OpenCLTests](https://gitlab.cern.ch/akraszna/OpenCLTests) and
[OpenACCTests](https://gitlab.cern.ch/akraszna/OpenACCTests).

## Code Structure

The repository builds itself in a pretty novel way. It builds a
**single** project out of packages/code picked up from the
[atlasexternals](https://gitlab.cern.ch/atlas/atlasexternals),
[Gaudi](https://gitlab.cern.ch/gaudi/Gaudi) and
[athena](https://gitlab.cern.ch/atlas/athena) repositories,
and adding packages/code of its own on top. Instead of building 3 separate
projects with all that code.

This design comes with a few upsides, and a few downsides. The major
upside is of course that building the code is a lot simpler. One has
to only build/install/setup a single project. But since none of the
aforementioned repositories were designed with this use case in mind,
the code is unfortunately not the most robust like this.

First off, [atlasexternals](https://gitlab.cern.ch/atlas/atlasexternals)
is only used to pick up the `AtlasCMake` and `AtlasLCG` code from it.
No external software package is built using that repository in this
project.

To build [Gaudi](https://gitlab.cern.ch/gaudi/Gaudi), a few tricks were
used to build it using
[ExternalProject_Add](https://cmake.org/cmake/help/latest/module/ExternalProject.html).
All of which can be found in [cmake/BuildGaudi.cmake](cmake/BuildGaudi.cmake).
In short, we generate a CMake file that emulates an "external project"
that Gaudi would pick up for its build. (Called `AsyncGaudiExternals`.)
The code then configures the build of Gaudi using that helper file,
and installs it into the build directory. It then sets up a number
of cache/normal variables that force
[FindGaudi.cmake](https://gitlab.cern.ch/atlas/atlasexternals/blob/master/Build/AtlasCMake/modules/FindGaudi.cmake)
to set up the "imported targets" for Gaudi from inside the build
directory. And finally, after setting up all the "normal" packages
from [athena](https://gitlab.cern.ch/atlas/athena) and this repository,
we tell a number of (library/executable/dictionary) targets to
explicitly wait for the build of Gaudi before they would run.
(This is done in the main [CMakeLists.txt](CMakeLists.txt) file.)

As stated before, the packages from
[athena](https://gitlab.cern.ch/atlas/athena) and from this repository
are handled as usual, built using `atlas_project(...)`.

## Submodules

The "ATLAS submodules" are set up to point at the heads of the following
branches:
   - [akraszna/atlasexternals/CUDATests-2.0.49-20190917](https://gitlab.cern.ch/akraszna/atlasexternals/tree/CUDATests-2.0.49-20190917);
   - [akraszna/AtlasGaudi/AsyncTests-master-20191015](https://gitlab.cern.ch/akraszna/AtlasGaudi/tree/AsyncTests-master-20191015);
   - [akraszna/athena/CUDATests-master-20190421](https://gitlab.cern.ch/akraszna/athena/tree/CUDATests-master-20190421).

## Build Instructions

The repository comes with a CI configuration
([.gitlab-ci.yml](.gitlab-ci.yml)) that is able to perform the
full build/test/packaging of the code, so you may want to look at that.

### Building on CentOS 7

On an "lxplus-like" machine you can build the code by:
  - Setting up GCC 8. Possibly with:

```
source /afs/cern.ch/work/k/krasznaa/public/gcc/8.3.0/x86_64-centos7/setup.sh
```

  - Optionally setting up CUDA. Possibly with:

```
source /afs/cern.ch/work/k/krasznaa/public/cuda/10.1/x86_64-centos7/setup.sh
```

  - Configuring the build against [LCG_95](http://lcginfo.cern.ch/release/95/)
    like:

```
cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DLCG_VERSION_NUMBER=95 ../AsyncGaudi/
```

Note that you also need a recent version of CMake for the build. You can
pick one up for instance from:

```
/afs/cern.ch/work/k/krasznaa/public/cmake/
```

### Building on Ubuntu 18.04

When building the code on a different OS, like
[Ubuntu 18.04](http://releases.ubuntu.com/18.04/), you need to make sure
that you have all the following available:
  - [GCC](https://gcc.gnu.org/) >=8. Unfortunately GCC 7 no longer builds Gaudi
    successfully;
  - [ROOT 6](https://root.cern.ch/);
  - [TBB](https://www.threadingbuildingblocks.org/);
  - [Boost](https://www.boost.org/).

As long as these can all be found by CMake in your environment, you can
set up the build of the project simply with:

```
cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo ../AsyncGaudi/
```

### (Re-)Building Gaudi

The project by default only builds Gaudi once. Once that first build succeeded,
it doesn't detect code changes in the Gaudi submodule. (The first setup I had
was the other extreme. When it was downloading Gaudi from Git, it
re-built/installed Gaudi on every incremental build of the main project.
Leading to a **lot** of wasted CPU time.)

When making a change in the Gaudi code, it can be (incrementally) re-built
by:

```
cmake --build . -- GaudiRebuild
cmake --build .
```

Where the first command just "primes" the build directory for a Gaudi re-build,
while the second command actually does it.

## Visual Studio Code Usage

If you want to use [Visual Studio Code](https://code.visualstudio.com/) to
edit the project, you should set up the/a build of the project just beside
the source directory, in a directory called `build/`. As is the ATLAS
recommendation.

This ensures that the [c_cpp_properties.json](.vscode/c_cpp_properties.json)
configuration saved with the project would pick up the cmake-generated
`compile_commands.json` file correctly.
