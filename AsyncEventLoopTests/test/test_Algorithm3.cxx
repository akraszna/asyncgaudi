// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "Macros.h"

// Project include(s).
#include "AsyncEventLoop/Job.h"

int main() {

   // Instantiate the job object.
   ASync::Job job( 2 );

   // Configure it.
   SC_CHECK( job.configure() );

   // Add the test algorithms to the job.
   SC_CHECK( job.addAlgorithm( "TestAlgorithm2Writer", "WriterAlg",
                               { { "OutputKey"  , "Particles" } } ) );
   SC_CHECK( job.addAlgorithm( "TestAlgorithm3", "ReaderAlg",
                               { { "InputKey"   , "Particles" } } ) );

   // And now run it.
   SC_CHECK( job.run() );

   // Return gracefully:
   return 0;
}
