// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCEVENTLOOPTESTS_MACROS_H
#define ASYNCEVENTLOOPTESTS_MACROS_H

// System include(s).
#include <iostream>

/// Helper macro checking status codes inside main(...)
#define SC_CHECK( EXP )                                           \
   do {                                                           \
      const auto sc = EXP;                                        \
      if( ! sc.isSuccess() ) {                                    \
         std::cout << "Failed to execute: " << #EXP << std::endl; \
         return 1;                                                \
      }                                                           \
   } while( false )

#endif // ASYNCEVENTLOOPTESTS_MACROS_H
