# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

# Import the algorithm sequence object.
from AsyncEventLoop.AlgSequence import AlgSequence
algSeq = AlgSequence()

# Add the test algorithm to the list of algorithms.
from AsyncEventLoopTests.AsyncEventLoopTestsConf import TestAlgorithm1
algSeq += TestAlgorithm1( 'TestAlgorithm1' )
