# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

# Import the algorithm sequence object.
from AsyncEventLoop.AlgSequence import AlgSequence
algSeq = AlgSequence()

# Add the test algorithms to the list of algorithms.
from AsyncEventLoopTests.AsyncEventLoopTestsConf import *
algSeq += TestAlgorithm2Writer( 'WriterAlg', OutputKey = 'Particles' )
algSeq += TestAlgorithm2Reader( 'ReaderAlg', InputKey = 'Particles' )
algSeq.ReaderAlg.OutputLevel = 2
