// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "TestAlgorithm3.h"

// Project include(s).
#include "AsyncBaseComps/ReadHandle.h"

// Athena include(s).
#include "AthContainers/AuxElement.h"
#include "AthContainers/DataVector.h"

// System include(s).
#include <cmath>

DECLARE_COMPONENT( TestAlgorithm3 )

/// Helper macro for checking assertions in a "Gaudi way"
#define SC_ASSERT( EXP )                                                 \
   do {                                                                  \
      const bool result = EXP;                                           \
      if( result == false ) {                                            \
         ATH_MSG_ERROR( "Failed assertion: " #EXP );                     \
         return StatusCode::FAILURE;                                     \
      }                                                                  \
   } while( false )

StatusCode TestAlgorithm3::initialize() {

   // Tell the user what will happen.
   ATH_MSG_INFO( "Will read: " << m_inputKey );

   // Return gracefully.
   return StatusCode::SUCCESS;
}

StatusCode
TestAlgorithm3::mainExecute( const EventContext& ctx,
                             ASync::AlgTaskPtr_t postExecTask ) const {

   // Tell the user where we are.
   ATH_MSG_DEBUG( "in mainExecute(...)" );

   // Check the container.
   ATH_CHECK( checkContainer( ctx ) );
   // Enqueue the post-exec task right away.
   ATH_CHECK( enqueue( std::move( postExecTask ) ) );
   // Return gracefully.
   return StatusCode::SUCCESS;
}

StatusCode TestAlgorithm3::postExecute( const EventContext& ctx ) const {

   // Tell the user where we are.
   ATH_MSG_DEBUG( "in postExecute(...)" );

   // Check the container.
   ATH_CHECK( checkContainer( ctx ) );
   // Return gracefully.
   return StatusCode::SUCCESS;
}

StatusCode TestAlgorithm3::checkContainer( const EventContext& ctx ) const {

   // Create the read handle.
   ASync::ReadHandle< DataVector< SG::AuxElement > > container( m_inputKey );

   // Check the properties of the container.
   SC_ASSERT( container->size() == 10 );
   for( std::size_t i = 0; i < 10; ++i ) {
      const SG::AuxElement* e = container->at( i );
      static const SG::AuxElement::ConstAccessor< float >
         etaAcc( "eta" ), phiAcc( "phi" ), ptAcc( "pt" );
      SC_ASSERT( std::abs( etaAcc( *e ) - ( ctx.evt() + 0.2f * i ) ) < 0.001 );
      SC_ASSERT( std::abs( phiAcc( *e ) -
                           ( ctx.evt() + -2.0f + 0.4f * i ) ) < 0.001 );
      SC_ASSERT( std::abs( ptAcc( *e ) -
                           ( ctx.evt() + 10000.0f + 1000.0f * i ) ) < 0.001 );
   }

   // Return gracefully.
   return StatusCode::SUCCESS;
}
