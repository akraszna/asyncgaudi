// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCEVENTLOOPTESTS_TESTALGORITHM3_H
#define ASYNCEVENTLOOPTESTS_TESTALGORITHM3_H

// Project include(s).
#include "AsyncBaseComps/Algorithm.h"
#include "AsyncBaseComps/ReadHandleKey.h"

/// Algorithm consuming a @c DataVector container asynchronously
///
/// This is to test the asynchronous execution using @c ASync::SchedulerSvc.
///
/// @author Attila Krasznahorky <Attila.Krasznahorkay@cern.ch>
///
class TestAlgorithm3 : public ASync::Algorithm {

public:
   /// Inherit all constructor(s).
   using ASync::Algorithm::Algorithm;

   /// @name Basic algorithm function(s).
   /// @{

   /// Function initialising the algorithm
   virtual StatusCode initialize() override;

   /// @}

   /// @name Asynchronous algorithm function(s).
   /// @{

   /// Offload the calculation to a device
   virtual StatusCode
   mainExecute( const EventContext& ctx,
                ASync::AlgTaskPtr_t postExecTask ) const override;
   /// Run a post-execution after the offloaded calculation
   virtual StatusCode postExecute( const EventContext& ctx ) const override;

   /// Declare that the algorithm is asynchronous
   virtual bool isAsynchronous() const override {
      return true;
   }

   /// @}

private:
   /// Function validating that the object in the store is what we expect
   StatusCode checkContainer( const EventContext& ctx ) const;

   /// Key to read the container with
   Gaudi::Property< ASync::ReadHandleKey > m_inputKey{ this, "InputKey",
      ASync::ReadHandleKey( "TestParticles", *this ),
      "Key to write a container with" };

}; // class TestAlgorithm3

#endif // ASYNCEVENTLOOPTESTS_TESTALGORITHM3_H
