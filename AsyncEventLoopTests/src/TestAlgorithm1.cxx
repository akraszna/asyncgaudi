// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "TestAlgorithm1.h"

DECLARE_COMPONENT( TestAlgorithm1 )

StatusCode TestAlgorithm1::initialize() {

   // Tell the user what's happening.
   ATH_MSG_INFO( "Initialising the algorithm" );

   // Return gracefully.
   return StatusCode::SUCCESS;
}

StatusCode TestAlgorithm1::execute( const EventContext& ) const {

   // Tell the user what's happening.
   ATH_MSG_INFO( "Executing the algorithm" );

   // Return gracefully.
   return StatusCode::SUCCESS;
}

StatusCode TestAlgorithm1::finalize() {

   // Tell the user what's happening.
   ATH_MSG_INFO( "Finalising the algorithm" );

   // Return gracefully.
   return StatusCode::SUCCESS;
}
