// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCEVENTLOOPTESTS_TESTALGORITHM2READER_H
#define ASYNCEVENTLOOPTESTS_TESTALGORITHM2READER_H

// Project include(s).
#include "AsyncBaseComps/Algorithm.h"
#include "AsyncBaseComps/ReadHandleKey.h"

/// Algorithm consuming a @c DataVector container
///
/// This is to test the event store, and the data handle infrastructure.
///
/// @author Attila Krasznahorky <Attila.Krasznahorkay@cern.ch>
///
class TestAlgorithm2Reader : public ASync::Algorithm {

public:
   /// Inherit all constructor(s).
   using ASync::Algorithm::Algorithm;

   /// @name Basic algorithm function(s).
   /// @{

   /// Function initialising the algorithm
   virtual StatusCode initialize() override;
   /// Function executing the algorithm
   virtual StatusCode execute( const EventContext& ) const override;

   /// @}

private:
   /// Key to read the container with
   Gaudi::Property< ASync::ReadHandleKey > m_inputKey{ this, "InputKey",
      ASync::ReadHandleKey( "TestParticles", *this ),
      "Key to write a container with" };

}; // class TestAlgorithm2Writer

#endif // ASYNCEVENTLOOPTESTS_TESTALGORITHM2READER_H
