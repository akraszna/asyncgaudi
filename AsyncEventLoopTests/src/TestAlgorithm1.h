// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCEVENTLOOPTESTS_TESTALGORITHM1_H
#define ASYNCEVENTLOOPTESTS_TESTALGORITHM1_H

// Project include(s).
#include "AsyncBaseComps/Algorithm.h"

/// Algorithm just printing that it is executing
///
/// This is the simplest test algorithm. Just printing, possibly in multiple
/// threads, that it is executing itself.
///
/// @author Attila Krasznahorky <Attila.Krasznahorkay@cern.ch>
///
class TestAlgorithm1 : public ASync::Algorithm {

public:
   /// Inherit all constructor(s).
   using ASync::Algorithm::Algorithm;

   /// @name Basic algorithm function(s).
   /// @{

   /// Function initialising the algorithm
   virtual StatusCode initialize() override;
   /// Function executing the algorithm
   virtual StatusCode execute( const EventContext& ) const override;
   /// Function finalising the algorithm
   virtual StatusCode finalize() override;

   /// @}

}; // class TestAlgorithm1

#endif // ASYNCEVENTLOOPTESTS_TESTALGORITHM1_H
