// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "TestAlgorithm2Writer.h"

// Project include(s).
#include "AsyncBaseComps/WriteHandle.h"

// Athena include(s).
#include "AthContainers/AuxElement.h"
#include "AthContainers/DataVector.h"
#include "AthContainers/AuxStoreInternal.h"

// System include(s).
#include <memory>

DECLARE_COMPONENT( TestAlgorithm2Writer )

StatusCode TestAlgorithm2Writer::initialize() {

   // Tell the user what will happen.
   ATH_MSG_INFO( "Will write: " << m_outputKey );

   // Return gracefully.
   return StatusCode::SUCCESS;
}

StatusCode TestAlgorithm2Writer::execute( const EventContext& ctx ) const {

   // Create the write handle.
   ASync::WriteHandle< DataVector< SG::AuxElement > > writer( m_outputKey );

   // Create the container.
   auto particles = std::make_unique< DataVector< SG::AuxElement > >();
   auto aux = std::make_unique< SG::AuxStoreInternal >();
   particles->setStore( aux.get() );

   // Add elements to it.
   for( int i = 0; i < 10; ++i ) {
      // Create a new element.
      SG::AuxElement* p = new SG::AuxElement();
      particles->push_back( p );
      // Set some variables on it.
      static const SG::AuxElement::Accessor< float > etaAcc( "eta" );
      etaAcc( *p ) = ctx.evt() + 0.2f * i;
      static const SG::AuxElement::Accessor< float > phiAcc( "phi" );
      phiAcc( *p ) = ctx.evt() + -2.0f + 0.4f * i;
      static const SG::AuxElement::Accessor< float > ptAcc( "pt" );
      ptAcc( *p ) = ctx.evt() + 10000.0f + 1000.0f * i;
   }

   // Record the container into the event store.
   ATH_CHECK( writer.put( std::move( particles ), std::move( aux ) ) );

   // Return gracefully.
   return StatusCode::SUCCESS;
}
