# (Unit) Tests for core asynchronous event loop code

This package collects some code that tests jobs set up using
`ASync::Job`, with the help of some simple test algorithms. All the code
of the package is just meant to be used by the tests defined in the package.
