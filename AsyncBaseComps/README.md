# Base package for component libraries

This package builds the `AsyncBaseCompsLib` library, which can be used as a
base package for all component libraries that implement components to be used
in asynchronous Gaudi (test) jobs.
