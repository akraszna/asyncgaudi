// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AsyncBaseComps/DataHandleKey.h"

// Gaudi include(s).
#include <GaudiKernel/IDataHandleHolder.h>

namespace ASync {

   DataHandleKey::DataHandleKey( const std::string& key,
                                 Gaudi::DataHandle::Mode mode,
                                 IDataHandleHolder* owner )
   : Gaudi::DataHandle( DataObjID( key ), mode, owner ),
     m_dataStore( "ASync::WhiteBoard", "ASync::DataHandleKey" ) {

      // Declare the handle to its owner:
      if( owner ) {
         owner->declare( *this );
      }
   }

   DataHandleKey::DataHandleKey( const DataHandleKey& parent )
   : Gaudi::DataHandle( parent.fullKey(), parent.mode(), parent.owner() ),
     m_dataStore( "ASync::WhiteBoard", "ASync::DataHandleKey" ) {

      // Declare the handle to its owner:
      if( owner() ) {
         owner()->declare( *this );
      }
   }

   DataHandleKey::~DataHandleKey() {

      // The handle is about to be deleted, make the data handle holder forget
      // about it.
      if( owner() ) {
         owner()->renounce( *this );
      }
   }

   DataHandleKey& DataHandleKey::operator= ( const DataHandleKey& rhs ) {

      // Check if anything needs to be done.
      if( this == &rhs ) {
         return *this;
      }

      // Copy the key. The mode can't be changed on an existing object, so don't
      // bother with that.
      setKey( rhs.fullKey() );

      // If we have an owner, let's renounce / re-declare ourselves.
      if( owner() && rhs.owner() ) {
         owner()->renounce( *this );
      } else if( owner() ) {
         owner()->renounce( *this );
         owner()->declare( *this );
      }

      // If the rhs object has an owner, let's declare ourselves to it.
      if( rhs.owner() ) {
         setOwner( rhs.owner() );
         owner()->declare( *this );
      }

      // Return this object:
      return *this;
   }

   StatusCode DataHandleKey::initialize() {

      // Make sure that the data store is available.
      return m_dataStore.retrieve();
   }

   DataHandleKey& DataHandleKey::operator=( const std::string& name ) {

      Gaudi::DataHandle::updateKey( name );
      return *this;
   }

   ServiceHandle< IDataStore >& DataHandleKey::dataStore() const {

      return m_dataStore;
   }

} // namespace ASync

namespace std {

   ostream& operator<< ( ostream& out, const ASync::DataHandleKey& key ) {
      out << "'" << key.objKey() << "'";
      return out;
   }

} // namespace std

namespace Gaudi {

   namespace Parsers {

      StatusCode parse( ASync::DataHandleKey& key, const std::string& name ) {

         // Rely on Gaudi's built-in function to parse the property into a
         // string with no quotation marks:
         std::string prop = "";
         StatusCode sc = Gaudi::Parsers::parse( prop, name );
         if( sc.isSuccess() ) {
            // If successful set it as our data key:
            key = prop;
         }
         return sc;
      }

   } // namespace Parsers

} // namespace Gaudi
