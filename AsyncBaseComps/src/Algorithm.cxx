// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AsyncBaseComps/Algorithm.h"

namespace ASync {

   StatusCode Algorithm::mainExecute( const EventContext&,
                                      AlgTaskPtr_t ) const {

      return StatusCode::SUCCESS;
   }

   StatusCode Algorithm::postExecute( const EventContext& ) const {

      return StatusCode::SUCCESS;
   }

   StatusCode Algorithm::execute( const EventContext& ) const {

      return StatusCode::SUCCESS;
   }

   ServiceHandle< IDataStore >& Algorithm::evtStore() const {

      return m_evtStore;
   }

   StatusCode Algorithm::enqueue( AlgTaskPtr_t postExecTask ) {

      // Enqueue the task using another helper function.
      return ASync::enqueue( std::move( postExecTask ) );
   }

   bool Algorithm::isAsynchronous() const {

      return m_isAsynchronous;
   }

} // namespace ASync
