// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AsyncBaseComps/AlgTaskPtr.h"

// System include(s).
#include <stdexcept>

namespace ASync {

   namespace details {

      void TaskDeleter::operator()( tbb::task* task ) {

         // Only do anything if we didn't receive a nullptr.
         if( task == nullptr ) {
            return;
         }

         // Destroy the task object using TBB's dedicated function.
         tbb::task::destroy( *task );

         // And then fail loudly...
         throw std::runtime_error( "Task was not properly scheduled" );
      }

   } // namespace details

   StatusCode enqueue( AlgTaskPtr_t postExecTask ) {

      // Make sure that the task is valid.
      if( ! postExecTask ) {
         return StatusCode::FAILURE;
      }

      // Enqueue the task into TBB.
      tbb::task::enqueue( *( postExecTask.release() )
#if __TBB_TASK_PRIORITY
                           , tbb::priority_high
#endif // __TBB_TASK_PRIORITY
                           );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

} // namespace ASync
