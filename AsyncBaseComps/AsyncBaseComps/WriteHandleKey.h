// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCBASECOMPS_WRITEHANDLEKEY_H
#define ASYNCBASECOMPS_WRITEHANDLEKEY_H

// Local include(s).
#include "AsyncBaseComps/DataHandleKey.h"

namespace ASync {

   /// Object describing a written output of an algorithm
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class WriteHandleKey : public DataHandleKey {

   public:
      /// Constructor to be used in user-level code
      WriteHandleKey( const std::string& key, IDataHandleHolder& owner )
      : DataHandleKey( key, Writer, &owner ) {}

      /// Default constructor, needed for property handling
      WriteHandleKey()
      : DataHandleKey( "", Writer, nullptr ) {}

      // Use the base class's copy constructor and assignment operator
      using DataHandleKey::DataHandleKey;
      using DataHandleKey::operator=;

   }; // class WriteHandleKey

} // namespace ASync

#endif // ASYNCBASECOMPS_WRITEHANDLEKEY_H
