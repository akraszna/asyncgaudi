// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCBASECOMPS_WRITEHANDLEKEYARRAY_H
#define ASYNCBASECOMPS_WRITEHANDLEKEYARRAY_H

// Local include(s).
#include "AsyncBaseComps/WriteHandleKey.h"
#include "AsyncBaseComps/DataHandleKeyArray.h"

namespace ASync {

   /// Array of @c WriteHandleKey objects
   ///
   /// Such an object can be used to declare a variable number of output
   /// objects produced by an algorithm.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class WriteHandleKeyArray : public DataHandleKeyArray< WriteHandleKey > {

   public:
      /// Constructor to be used in user-level code
      WriteHandleKeyArray( IDataHandleHolder& owner )
      : DataHandleKeyArray< WriteHandleKey >( &owner ) {}

      /// Default constructor, needed for property handling
      WriteHandleKeyArray()
      : DataHandleKeyArray< WriteHandleKey >( nullptr ) {}

      // Use the base class's assignment operator
      using DataHandleKeyArray< WriteHandleKey >::operator=;

   }; // class WriteHandleKeyArray

} // namespace ASync

#endif // ASYNCBASECOMPS_WRITEHANDLEKEYARRAY_H
