// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCBASECOMPS_READHANDLE_H
#define ASYNCBASECOMPS_READHANDLE_H

// Local include(s).
#include "AsyncBaseComps/ReadHandleKey.h"

// Gaudi include(s).
#include <GaudiKernel/StatusCode.h>

// System include(s).
#include <stdexcept>

namespace ASync {

   /// Data read handle
   ///
   /// This is a ridiculously simplified version of StoreGate's read handle.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   template< class T >
   class ReadHandle {

   public:
      /// Type for the returned pointer
      typedef const T* const_pointer_t;
      /// Type for the returned reference
      typedef const T& const_reference_t;
   
      /// Constructor with a ReadHandleKey
      explicit ReadHandle( const ReadHandleKey& key );
      /// Copy-constructor
      ReadHandle( const ReadHandle& parent );
      
      /// Assignment operator
      ReadHandle& operator= ( const ReadHandle& rhs );

      /// Return a constant pointer to the object in question
      const_pointer_t get();
      /// Return a constant pointer to the object in question
      const_pointer_t operator->();
      /// Return a constant reference to the object in question
      const_reference_t operator*();

   private:
      /// The function loading the object from the store
      void loadPtr();

      /// Pointer to the read handle key
      const ReadHandleKey* m_key;

      /// The cached pointer
      const_pointer_t m_ptr;

   }; // class ReadHandle

   /// @name Implementation for the template functions
   /// @{

   template< class T >
   ReadHandle< T >::ReadHandle( const ReadHandleKey& key )
   : m_key( &key ), m_ptr( nullptr ) {

   }

   template< class T >
   ReadHandle< T >::ReadHandle( const ReadHandle& parent )
   : m_key( parent.m_key ), m_ptr( parent.m_ptr ) {

   }

   template< class T >
   ReadHandle< T >& ReadHandle< T >::operator= ( const ReadHandle& rhs ) {

      // Check if anything needs to be done.
      if( this == &rhs ) {
         return *this;
      }

      // Copy the members of the handle.
      m_key = rhs.m_key;
      m_ptr = rhs.m_ptr;

      // Return this object.
      return *this;
   }

   template< class T >
   typename ReadHandle< T >::const_pointer_t ReadHandle< T >::get() {

      // Load the pointer if necessary.
      if( m_ptr == nullptr ) {
         loadPtr();
      }

      // Now return it.
      return m_ptr;
   }

   template< class T >
   typename ReadHandle< T >::const_pointer_t ReadHandle< T >::operator->() {

      // Leave the heavy lifting to the get() function.
      return get();
   }

   template< class T >
   typename ReadHandle< T >::const_reference_t ReadHandle< T >::operator*() {

      // Load the pointer if necessary.
      if( m_ptr == nullptr ) {
         loadPtr();
      }

      // If the pointer is still not available, that's a problem...
      if( m_ptr == nullptr ) {
         throw std::runtime_error( "Couldn't load object: " +
                                   m_key->objKey() );
      }

      // Return the reference:
      return *m_ptr;
   }

   template< class T >
   void ReadHandle< T >::loadPtr() {

      // If the pointer is already available, return right away:
      if( m_ptr ) {
         return;
      }

      // Attempt to load the object:
      m_key->dataStore()->retrieve( m_ptr, m_key->objKey() ).ignore();

      // Return gracefully:
      return;
   }

   /// @}

} // namespace ASync

#endif // ASYNCBASECOMPS_READHANDLE_H
