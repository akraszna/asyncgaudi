// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCBASECOMPS_DATAHANDLEKEYARRAY_H
#define ASYNCBASECOMPS_DATAHANDLEKEYARRAY_H

// Gaudi include(s).
#include <GaudiKernel/StatusCode.h>

// System include(s).
#include <iostream>
#include <string>
#include <vector>

// Forward declaration(s).
class IDataHandleHolder;

namespace ASync {

   /// Base class for the data handle key arrays
   ///
   /// Inspired quite a bit by the StoreGate implementation of its similar
   /// code.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   template< class T >
   class DataHandleKeyArray : public std::vector< T > {

   public:
      /// Type of the data handles in the array
      typedef T handle_t;

      /// Constructor with all necessary argument(s)
      DataHandleKeyArray( IDataHandleHolder* owner )
         : std::vector< T >(), m_owner( owner ) {}

      /// Assignment operator
      DataHandleKeyArray< T >&
      operator= ( const DataHandleKeyArray< T >& rhs ) {

         // Check if anything needs to be done.
         if( &rhs == this ) {
            return *this;
         }

         // Clear the container as the first thing.
         this->clear();

         // Perform the copy. In a not completely trivial way...
         for( const T& key : rhs ) {
            if( m_owner ) {
               this->emplace_back( "", *m_owner );
            } else {
               this->emplace_back();
            }
            this->back() = key;
         }

         // Return a reference to the updated object.
         return *this;
      }

      /// The owner of the array
      IDataHandleHolder* owner() const { return m_owner; }

   private:
      /// Owner of the array
      IDataHandleHolder* m_owner = nullptr;

   }; // class DataHandleKeyArray

} // namespace ASync

namespace std {

   /// Output operator. Needed by Gaudi's property management.
   template< class T >
   ostream& operator<< ( ostream& out,
                         const ASync::DataHandleKeyArray< T >& keys ) {

      out << "[";
      for( std::size_t i = 0; i < keys.size(); ++i ) {
         out << keys[ i ];
         if( i + 1 < keys.size() ) {
            out << ",";
         }
      }
      out << "]";
      return out;
   }

} // namespace std

namespace Gaudi {

   namespace Parsers {

      /// Function parsing a string property into a data handle key array object
      template< class T >
      StatusCode parse( ASync::DataHandleKeyArray< T >& keys,
                        const std::string& prop ) {

         // Rely on Gaudi's built-in function to parse the property into a
         // string vector.
         std::vector< std::string > key_names;
         StatusCode sc = Gaudi::Parsers::parse( key_names, prop );
         if( sc.isSuccess() ) {
            // If successful create the correct objects for the array.
            keys.clear();
            for( const std::string& key : key_names ) {
               if( keys.owner() ) {
                  keys.emplace_back( "", *( keys.owner() ) );
               } else {
                  keys.emplace_back();
               }
               keys.back() = key;
            }
         }
         return sc;
      }

   } // namespace Parsers

} // namespace Gaudi

#endif // ASYNCBASECOMPS_DATAHANDLEKEYARRAY_H
