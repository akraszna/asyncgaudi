// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCBASECOMPS_READHANDLEKEY_H
#define ASYNCBASECOMPS_READHANDLEKEY_H

// Local include(s).
#include "AsyncBaseComps/DataHandleKey.h"

namespace ASync {

   /// Object describing a read dependency for an algorithm
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class ReadHandleKey : public DataHandleKey {

   public:
      /// Constructor to be used in user-level code
      ReadHandleKey( const std::string& key, IDataHandleHolder& owner )
      : DataHandleKey( key, Reader, &owner ) {}

      /// Default constructor, needed for property handling
      ReadHandleKey()
      : DataHandleKey( "", Reader, nullptr ) {}

      // Use the base class's copy constructor and assignment operator
      using DataHandleKey::DataHandleKey;
      using DataHandleKey::operator=;

   }; // class ReadHandleKey

} // namespace ASync

#endif // ASYNCBASECOMPS_READHANDLEKEY_H
