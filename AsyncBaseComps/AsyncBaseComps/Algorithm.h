// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCBASECOMPS_ALGORITHM_H
#define ASYNCBASECOMPS_ALGORITHM_H

// Local include(s).
#include "AsyncBaseComps/MessagingMacros.h"
#include "AsyncBaseComps/CheckMacros.h"
#include "AsyncBaseComps/AlgTaskPtr.h"

// Project include(s).
#include "AsyncEventLoopInterfaces/IDataStore.h"

// Gaudi include(s).
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/Property.h>
#include <GaudiKernel/ServiceHandle.h>
#include <GaudiKernel/IHiveWhiteBoard.h>

namespace ASync {

   /// Custom layer above @c Gaudi::Algorithm
   ///
   /// This layer describes how an "asynchronous" algorithm should behave.
   /// Algorithm classes inheriting from this base can choose to either execute
   /// "synchronously" (just as @c Gaudi::Algorithm does), or "asynchronously",
   /// when a different "execute interface" is used on them.
   ///
   /// In all cases @c ASync::Algorithm implementations must be reentrant, as
   /// @c ASync::SchedulerSvc only handles reentrant (and partly also
   /// clonable...) algorithms correctly.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class Algorithm : public Gaudi::Algorithm {

   public:
      /// Inherit the @c Gaudi::Algorithm constructor(s).
      using Gaudi::Algorithm::Algorithm;

      /// @name Custom execute interface for asynchronous calculations
      ///
      /// This "execution interface" is used on the algorithm when
      /// @c ASync::Algorithm::isAsynchronous() returns @c true for the
      /// algorithm object.
      ///
      /// @{

      /// Execute the algorithm on the host or an asynchronous device
      ///
      /// The algorithm is provided with a pre-prepared task, which, when
      /// executed, calls the @c postExecute function of the algorithm.
      /// The function must take care of either enqueing the task (using
      /// @c ASync::Algorithm::enqueue), or passing the task to some service
      /// that will enqueue the task once the asynchronous calculation set up
      /// by this function has finished.
      ///
      /// @param ctx The current event's context
      /// @param postExecTask Task executing the @c postExecute function
      ///                     of this algorithm
      /// @return The usual @c StatusCode values
      ///
      virtual StatusCode mainExecute( const EventContext& ctx,
                                      AlgTaskPtr_t postExecTask ) const;

      /// Run a post-execution step on the algorithm
      ///
      /// This step is meant mainly to collect data from an asynchronous
      /// calculation, and save it into the event store.
      ///
      /// @param ctx The current event's context
      /// @return The usual @c StatusCode values
      ///
      virtual StatusCode postExecute( const EventContext& ctx ) const;

      /// @}

      /// @name Execute interface for synchronous calculations
      /// @{

      /// Implementation of the base class's @c execute function
      ///
      /// @param ctx The current event's context
      /// @return The usual @c StatusCode values
      ///
      virtual StatusCode execute( const EventContext& ctx ) const override;

      /// @}

      /// @name Data store / White-board accessing function(s).
      /// @{

      /// Provide the event store handle to the users
      ServiceHandle< IDataStore >& evtStore() const;

      /// @}

      /// @name Function(s) used to orchestrate the (asynchronous) execution
      /// @{

      /// Enqueue the post-execute task in the TBB scheduler
      static StatusCode enqueue( AlgTaskPtr_t postExecTask );

      /// Check if the algorithm is set up to execute itself asynchronously
      virtual bool isAsynchronous() const;

      /// @}

   private:
      /// Async algorithms can only be reentrant (to simplify the code)
      virtual unsigned int cardinality() const override { return 0; }

      /// @name Algorithm properties
      /// @{

      Gaudi::Property< bool > m_isAsynchronous {
         this, "IsAsynchronous", false,
         "Whether the algorithm will/can be executed asynchronously" };

      /// @}

      /// Event store provided to all algorithms
      mutable ServiceHandle< IDataStore > m_evtStore{ this, "EvtStore",
         "ASync::WhiteBoard" };

   }; // class Algorithm

} // namespace ASync

#endif // ASYNCBASECOMPS_ALGORITHM_H
