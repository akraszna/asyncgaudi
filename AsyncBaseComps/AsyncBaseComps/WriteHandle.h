// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCBASECOMPS_WRITEHANDLE_H
#define ASYNCBASECOMPS_WRITEHANDLE_H

// Local include(s).
#include "AsyncBaseComps/WriteHandleKey.h"

// Gaudi include(s).
#include <GaudiKernel/StatusCode.h>

// System include(s).
#include <memory>

namespace ASync {

   /// Data write handle
   ///
   /// This is a ridiculously simplified version of StoreGate's write handle.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   template< class T >
   class WriteHandle {

   public:
      /// Constructor with a WriteHandleKey
      explicit WriteHandle( const WriteHandleKey& key );
      /// Copy-constructor
      WriteHandle( const WriteHandle& parent );

      /// Assignment operator
      WriteHandle& operator= ( const WriteHandle& rhs );

      /// Function recording a new object into the data store
      StatusCode put( std::unique_ptr< T > obj );
      /// Overloaded function recording a new object with its AUX container
      template< class AUX >
      StatusCode put( std::unique_ptr< T > obj, std::unique_ptr< AUX > aux );

   private:
      /// Pointer to the write handle key
      const WriteHandleKey* m_key;

   }; // class WriteHandle

   /// @name Implementation for the template functions
   /// @{

   template< class T >
   WriteHandle< T >::WriteHandle( const WriteHandleKey& key )
   : m_key( &key ) {

   }

   template< class T >
   WriteHandle< T >::WriteHandle( const WriteHandle& parent )
   : m_key( parent.m_key ) {

   }

   template< class T >
   WriteHandle< T >& WriteHandle< T >::operator= ( const WriteHandle& rhs ) {

      // Check if anything needs to be done.
      if( this == &rhs ) {
         return *this;
      }

      // Copy the member(s) of the handle.
      m_key = rhs.m_key;

      // Return this object.
      return *this;
   }

   template< class T >
   StatusCode WriteHandle< T >::put( std::unique_ptr< T > obj ) {

      return m_key->dataStore()->record( std::move( obj ),
                                         m_key->objKey() );
   }

   template< class T >
   template< class AUX >
   StatusCode WriteHandle< T >::put( std::unique_ptr< T > obj, 
                                     std::unique_ptr< AUX > aux ) {

      if( ! m_key->dataStore()->record( std::move( obj ),
                                        m_key->objKey() ).isSuccess() ) {
         return StatusCode::FAILURE;
      }
      if( ! m_key->dataStore()->record( std::move( aux ),
                                        m_key->objKey() +
                                        "Aux." ).isSuccess() ) {
         return StatusCode::FAILURE;
      }
      return StatusCode::SUCCESS;
   }

   /// @}

} // namespace ASync

#endif // ASYNCBASECOMPS_WRITEHANDLE_H
