// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCBASECOMPS_CHECKMACROS_H
#define ASYNCBASECOMPS_CHECKMACROS_H

// Local include(s).
#include "AsyncBaseComps/MessagingMacros.h"

/// Macro used to check @c StatusCode return codes in functions that return
/// such codes themselves
#define ATH_CHECK( EXP )                                             \
   do {                                                              \
      const auto sc_result = EXP;                                    \
      if( ! sc_result.isSuccess() ) {                                \
         ATH_MSG_ERROR( "Failed to call: " << #EXP );                \
         return sc_result;                                           \
      }                                                              \
   } while( false )

#endif // ASYNCBASECOMPS_CHECKMACROS_H
