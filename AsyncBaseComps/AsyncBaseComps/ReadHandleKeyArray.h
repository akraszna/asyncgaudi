// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCBASECOMPS_READHANDLEKEYARRAY_H
#define ASYNCBASECOMPS_READHANDLEKEYARRAY_H

// Local include(s).
#include "AsyncBaseComps/ReadHandleKey.h"
#include "AsyncBaseComps/DataHandleKeyArray.h"

namespace ASync {

   /// Array of @c ReadHandleKey objects
   ///
   /// Such an object can be used to declare a variable number of input
   /// dependencies for an algorithm.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class ReadHandleKeyArray : public DataHandleKeyArray< ReadHandleKey > {

   public:
      /// Constructor to be used in user-level code
      ReadHandleKeyArray( IDataHandleHolder& owner )
      : DataHandleKeyArray< ReadHandleKey >( &owner ) {}

      /// Default constructor, needed for property handling
      ReadHandleKeyArray()
      : DataHandleKeyArray< ReadHandleKey >( nullptr ) {}

      // Use the base class's assignment operator
      using DataHandleKeyArray< ReadHandleKey >::operator=;

   }; // class ReadHandleKeyArray

} // namespace ASync

#endif // ASYNCBASECOMPS_READHANDLEKEYARRAY_H
