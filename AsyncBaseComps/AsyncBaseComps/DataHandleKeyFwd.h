// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCBASECOMPS_DATAHANDLEKEYFWD_H
#define ASYNCBASECOMPS_DATAHANDLEKEYFWD_H

// Gaudi include(s).
#include <GaudiKernel/StatusCode.h>

// System include(s).
#include <string>

// Forward declaration(s):
namespace ASync {
   class DataHandleKey;
}

namespace Gaudi {

   namespace Parsers {

      /// Function parsing a string property into a data handle key object
      StatusCode parse( ASync::DataHandleKey& key, const std::string& name );

   } // namespace Parsers

} // namespace Gaudi

#endif // ASYNCBASECOMPS_DATAHANDLEKEYFWD_H
