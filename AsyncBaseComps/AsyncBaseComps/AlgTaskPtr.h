// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCBASECOMPS_ALGTASKPTR_H
#define ASYNCBASECOMPS_ALGTASKPTR_H

// Gaudi include(s).
#include <GaudiKernel/StatusCode.h>

// TBB include(s).
#include <tbb/task.h>

// System include(s).
#include <memory>

namespace ASync {

   namespace details {

      /// Custom deleter used by @c std::unique_ptr to dispose of tasks
      ///
      /// This deleter must never actually be called. If an algorithm claims
      /// that it is an asynchronous algorithm, then it must always schedule
      /// the post-execution task given to it using
      /// @c ASync::Algorithm::enqueue. Not doing that is considered a coding
      /// error, which makes the job fail.
      ///
      /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
      ///
      class TaskDeleter {
      public:
         /// Operator deleting a @c tbb::task object
         void operator()( tbb::task* task );
      };

   } // namespace details

   /// Smart pointer for passing around algorithm post-execution tasks
   typedef std::unique_ptr< tbb::task,
                            details::TaskDeleter > AlgTaskPtr_t;

   /// Helper function to enqueue a task
   StatusCode enqueue( AlgTaskPtr_t postExecTask );

} // namespace ASync

#endif // ASYNCBASECOMPS_ALGTASKPTR_H
