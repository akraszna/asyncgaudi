// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCBASECOMPS_MESSAGINGMACROS_H
#define ASYNCBASECOMPS_MESSAGINGMACROS_H

// This is a GCC extension for getting the name of the current function.
#if defined( __GNUC__ )
#   define MSGSTREAM_FNAME __PRETTY_FUNCTION__
#else
#   define MSGSTREAM_FNAME ""
#endif

/// Common prefix for the non-usual messages
///
/// The idea is that a regular user usually only wants to see DEBUG, INFO
/// and some WARNING messages. So those should be reasonably short. On the
/// other hand serious warnings (ERROR, FATAL) messages should be as precise
/// as possible to make debugging the issue easier.
///
#define MSGSTREAM_REPORT_PREFIX \
   __FILE__ << ":" << __LINE__ << " (" << MSGSTREAM_FNAME << "): "

/// Macro used to print "serious" messages
#define ATH_MSG_LVL_SERIOUS( lvl, xmsg )                            \
   msgStream( lvl ) << MSGSTREAM_REPORT_PREFIX << xmsg << endmsg

/// Macro used to print "regular" messages
#define ATH_MSG_LVL_NOCHK( lvl, xmsg )                              \
   msgStream( lvl ) << xmsg << endmsg

/// Macro printing verbose messages
#define ATH_MSG_VERBOSE( xmsg )                                     \
   do {                                                             \
      if( msgLevel( MSG::VERBOSE ) ) {                              \
         ATH_MSG_LVL_SERIOUS( MSG::VERBOSE, xmsg );                 \
      }                                                             \
   } while( 0 )
/// Macro printing debug messages
#define ATH_MSG_DEBUG( xmsg )                                       \
   do {                                                             \
      if( msgLevel( MSG::DEBUG ) ) {                                \
         ATH_MSG_LVL_NOCHK( MSG::DEBUG, xmsg );                     \
      }                                                             \
   } while( 0 )
/// Macro printing info messages
#define ATH_MSG_INFO( xmsg )                                        \
   ATH_MSG_LVL_NOCHK( MSG::INFO,  xmsg )
/// Macro printing warning messages
#define ATH_MSG_WARNING( xmsg )                                     \
   ATH_MSG_LVL_NOCHK( MSG::WARNING, xmsg )
/// Macro printing error messages
#define ATH_MSG_ERROR( xmsg )                                       \
   ATH_MSG_LVL_SERIOUS( MSG::ERROR, xmsg )
/// Macro printing fatal messages
#define ATH_MSG_FATAL( xmsg )                                       \
   ATH_MSG_LVL_SERIOUS( MSG::FATAL, xmsg )
/// Macro printing messages that should always appear
#define ATH_MSG_ALWAYS( xmsg )                                      \
   ATH_MSG_LVL_NOCHK( MSG::ALWAYS, xmsg )

#endif // ASYNCBASECOMPS_MESSAGINGMACROS_H
