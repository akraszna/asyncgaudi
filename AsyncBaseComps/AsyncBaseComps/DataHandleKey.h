// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCBASECOMPS_DATAHANDLEKEY_H
#define ASYNCBASECOMPS_DATAHANDLEKEY_H

// Local include(s).
#include "AsyncBaseComps/DataHandleKeyFwd.h"

// Project include(s).
#include "AsyncEventLoopInterfaces/IDataStore.h"

// Gaudi include(s).
#include <GaudiKernel/DataHandle.h>
#include <GaudiKernel/ServiceHandle.h>
#include <GaudiKernel/StatusCode.h>

// System include(s).
#include <iosfwd>

namespace ASync {

   /// Base class for all data handle keys in the framework
   ///
   /// This is the class that all data handles inherit from,
   /// centralising some of the data handle code a bit.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class DataHandleKey : public Gaudi::DataHandle {

   public:
      /// Constructor
      DataHandleKey( const std::string& key, Gaudi::DataHandle::Mode mode,
                     IDataHandleHolder* owner );
      /// Copy constructor
      DataHandleKey( const DataHandleKey& parent );
      /// Destructor
      ~DataHandleKey();

      /// Assignment operator
      DataHandleKey& operator= ( const DataHandleKey& rhs );

      /// Function to be called in the initialisation of the component using
      /// this key
      StatusCode initialize();

      /// Assign the value of the key from a string (during configuration
      /// parsing)
      DataHandleKey& operator=( const std::string& name );

      /// Function providing access to the store that the key operates on
      ServiceHandle< IDataStore >& dataStore() const;

   protected:
      /// Handle to the data store
      mutable ServiceHandle< IDataStore > m_dataStore;

   }; // class DataHandleKey

} // namespace ASync

namespace std {

   /// Output operator. Needed by Gaudi's property management.
   ostream& operator<< ( ostream& out, const ASync::DataHandleKey& key );

} // namespace std

namespace Gaudi {

   namespace Parsers {

      /// Function parsing a string property into a data handle key object
      StatusCode parse( ASync::DataHandleKey& key, const std::string& name );

   } // namespace Parsers

} // namespace Gaudi

#endif // ASYNCBASECOMPS_DATAHANDLEKEY_H
