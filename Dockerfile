# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Docker configuration for building a "CUDA capable" image out of the
# repository's code.
#

# Use the NVidia CentOS7 CUDA image.
FROM nvidia/cuda:10.1-runtime-centos7

# Copy the TGZ file that was created by the CI.
COPY ./ci_build/*.tar.gz /root/

# Install the TGZ file.
RUN tar -C / --no-same-owner -xvf /root/*.tar.gz

# Clean up.
RUN rm -f /root/*.tar.gz

# Install the "which" command, as that's needed by the LCG scripts.
RUN yum install -y which

# Start the image with BASH by default.
CMD /bin/bash
