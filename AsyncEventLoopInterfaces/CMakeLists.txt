# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

# Set the name of the package.
atlas_subdir( AsyncEventLoopInterfaces )

# Set up the (interface) library.
atlas_add_library( AsyncEventLoopInterfacesLib
   AsyncEventLoopInterfaces/*.h
   INTERFACE
   PUBLIC_HEADERS AsyncEventLoopInterfaces
   LINK_LIBRARIES xAODRootAccess GaudiKernel )
