// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYCEVENTLOOPINTERFACES_IDATASTORE_H
#define ASYCEVENTLOOPINTERFACES_IDATASTORE_H

// Athena include(s).
#include "xAODRootAccess/tools/TDestructorRegistry.h"

// Gaudi include(s).
#include <GaudiKernel/IInterface.h>
#include <GaudiKernel/StatusCode.h>

// System include(s).
#include <memory>
#include <typeinfo>

/// Namespace holding all the GPU independent code of the project
///
/// So, mainly code participating in setting up / using Gaudi jobs
/// independently of the main Athena code.
///
/// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
///
namespace ASync {

   /// Abstract interface for interacting with the white board
   ///
   /// This interface is to be used by algorithms to retrieve/record objects
   /// from/to the white board.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class IDataStore : virtual public IInterface {

   public:
      /// Declare the interface ID
      DeclareInterfaceID( ASync::IDataStore, 1, 0 );

      /// @name Functions to be used by "regular user code"
      /// @{

      /// Function checking whether the store contains a given object
      template< class T >
      bool contains( const std::string& name ) const;

      /// Function used to retrieve an object from the white board
      template< class T >
      StatusCode retrieve( const T*& obj, const std::string& name );

      /// Function used to record an object onto the white board
      template< class T >
      StatusCode record( std::unique_ptr< T > obj, const std::string& name );

      /// @}

   protected:
      /// @name Functions to be used by framework/technical code
      /// @{

      /// Function retrieving the (real) type of the object in the store
      virtual const std::type_info* type( const std::string& name ) = 0;

      /// Function checking whether a given object exists or not
      virtual bool contains( const std::string& name,
                             const std::type_info& ti ) const = 0;

      /// Function providing the pointer for a requested object
      virtual const void* getObject( const std::string& name,
                                     const std::type_info& ti ) = 0;

      /// Function recording the object into the data store
      virtual StatusCode record( void*, const std::type_info& ti,
                                 const std::string& name ) = 0;

      /// @}

   }; // class IDataStore

   /// @name Implementation for the template functions
   /// @{

   template< class T >
   bool IDataStore::contains( const std::string& name ) const {

      // Forward the call to the non-template interface:
      return contains( name, typeid( T ) );
   }

   template< class T >
   StatusCode IDataStore::retrieve( const T*& obj, const std::string& name ) {

      // Call on the non-template function to attempt the retrieve:
      const void* ptr = getObject( name, typeid( T ) );
      if( ! ptr ) {
         return StatusCode::FAILURE;
      }

      // Forcefully cast the pointer, as we were promised that this should be
      // okay:
      obj = reinterpret_cast< const T* >( ptr );

      // We were successful:
      return StatusCode::SUCCESS;
   }

   template< class T >
   StatusCode IDataStore::record( std::unique_ptr< T > obj,
                                  const std::string& name ) {

      // Make sure that we'll be able to destruct this type later on:
      xAOD::TDestructorRegistry::instance().add< T >();

      // Try to record the object into the store:
      StatusCode sc = record( obj.get(), typeid( T ), name );
      if( ! sc.isSuccess() ) {
         return sc;
      }

      // If we were successful, the data store now owns the object. Tell the
      // unique pointer to let it go.
      obj.release();

      // We were successful:
      return StatusCode::SUCCESS;
   }

   /// @}

} // namespace ASync

#endif // ASYCEVENTLOOPINTERFACES_IDATASTORE_H
