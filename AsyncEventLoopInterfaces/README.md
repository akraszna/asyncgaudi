# Basic interfaces for the asynchronous event loop

This package collects the interface(s) used by all parts of the
asynchronous Gaudi implementation. To use the header(s) of the package,
use the `AsyncEventLoopInterfacesLib` library.
