Core CUDA Core
==============

This package collects all "helper" CUDA code. Code that is only ever
used on the host to interact with (a) device (memory).
