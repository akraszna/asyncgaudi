# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

# Set up the CUDA services.
from AsyncEventLoop.ServiceMgr import svcMgr
from AthCUDAServices.AthCUDAServicesConf import AthCUDA__KernelRunnerSvc
kernelSvc = AthCUDA__KernelRunnerSvc( 'AthCUDA::KernelRunnerSvc',
                                      NParallelKernels = 2 )
svcMgr += kernelSvc
from AthCUDAServices.AthCUDAServicesConf import AthCUDA__StreamPoolSvc
streamSvc = AthCUDA__StreamPoolSvc( 'AthCUDA::StreamPoolSvc',
                                    NStreams = 5 )
svcMgr += streamSvc

# Import the algorithm sequence object.
from AsyncEventLoop.AlgSequence import AlgSequence
algSeq = AlgSequence()

# Set up a pyramid of algorithms with semi-random properties.
import random
random.seed( 123 )
from AthCUDATests.AthCUDATestsConf import AthCUDA__CPUGPUCruncherAlg
for i in range( 1, 11 ):
   for j in range( 1, i + 1 ):
      alg = AthCUDA__CPUGPUCruncherAlg( 'CruncherAlg%i%i' % ( i, j ) )
      alg.FloatArraySize = int( 100 + 100000 * random.random() )
      alg.AverageRuntime = random.random() * 0.2 if ( i + j ) % 10 else 0.0
      alg.InputKeys = [ 'Object%i%i' % ( ip, jp ) for ( ip, jp ) in
                        zip( [ i - 1 ] * ( 2 if i > 1 else 0 ),
                              range( j - 1 if j > 1 else j,
                                     j + 1 if j < i else j ) ) ]
      alg.OutputKeys = [ 'Object%i%i' % ( i, j ) ]
      algSeq += alg
      pass
   pass

# Set some global job properties.
from AsyncEventLoop.AsyncEventLoopFlags import asyncEventLoopFlags
asyncEventLoopFlags.EvtMax.set_Value_and_Lock( 200 )
asyncEventLoopFlags.EventPrintoutInterval.set_Value_and_Lock( 10 )

# Dump the execution graph for debugging.
from AsyncEventLoop.AsyncEventLoopConf import ASync__SchedulerSvc
svcMgr += ASync__SchedulerSvc( 'ASync::SchedulerSvc',
                               DataFlowOutput = 'stress_flow.dot' )
