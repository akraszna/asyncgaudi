# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

# Configuration options for the job.
controlFlowConf = 'AthCUDATests/mcreco/cf.mcreco.TriggerOff.graphml'
dataFlowConf    = 'AthCUDATests/mcreco/df.mcreco.TriggerOff.3rdEvent.graphml'
timingsConf     = 'AthCUDATests/mcreco/averageTiming.mcreco.TriggerOff.json'

# Create a logger.
from AthenaCommon.Logging import logging
log = logging.getLogger( 'CPUGPUCrunch_mcreco' )

# Set up the CUDA services.
from AsyncEventLoop.ServiceMgr import svcMgr
from AthCUDAServices.AthCUDAServicesConf import AthCUDA__KernelRunnerSvc
kernelSvc = AthCUDA__KernelRunnerSvc( 'AthCUDA::KernelRunnerSvc',
                                      NParallelKernels = 50 )
svcMgr += kernelSvc
from AthCUDAServices.AthCUDAServicesConf import AthCUDA__StreamPoolSvc
streamSvc = AthCUDA__StreamPoolSvc( 'AthCUDA::StreamPoolSvc',
                                    NStreams = 10 )
svcMgr += streamSvc

# Import the algorithm sequence object.
from AsyncEventLoop.AlgSequence import AlgSequence
algSeq = AlgSequence()

# Read in the timing information of the algorithms.
import json
from AsyncEventLoop.Utils import findDataFile
timingFile = open( findDataFile( timingsConf ) )
timingJson = json.load( timingFile )

def criticalPathSelector( algName ):
   '''Custom selector for designating algorithms to be offloaded to the GPU

   It just hardcodes the names of a couple of heavy algorithms from the
   "critical path" that should be run on the GPU.
   '''
   if algName in [ 'InDetAmbiguitySolverAlg', 'MuonCombinedAlg',
                   'CaloCellMakerAlg' ]:
      return True
   else:
      return False

def randomSelector( algName ):
   '''Custom selector for designating algorithms to be offloaded to the GPU

   It just randomly selects a fraction of the algorithms to be offloaded.
   '''
   if hash( algName ) % 5 == 0:
      return True
   else:
      return False

def heavySelector( algName ):
   '''Custom selector for designating algorithms to be offloaded to the GPU

   It selects "heavy" algorithms off of the critical path.
   '''

   # Treat the critical path algorithms in a custom way.
   if algName in [ 'InDetAmbiguitySolverAlg', 'MuonCombinedAlg',
                   'CaloCellMakerAlg' ]:
      return False

   # Discard lightweight algorithms.
   if ( not algName in timingJson ) or \
      ( float( timingJson[ algName ] ) < 0.1 ):
      return False

   # Select some fraction of the surviving algorithms.
   if hash( algName ) % 2 == 0:
      return True
   else:
      return False

# Open the graphs.
import networkx as nx
controlFlowGraph = nx.read_graphml( findDataFile( controlFlowConf ) )
dataFlowGraph = nx.read_graphml( findDataFile( dataFlowConf ) )

# Choose the function to use for designating GPU offloaded algorithms.
from AsyncEventLoop.Utils import cpuOnlySelector
chosenSelector = criticalPathSelector
# chosenSelector = randomSelector
# chosenSelector = heavySelector
# chosenSelector = cpuOnlySelector

# Fill the algorithm sequence with their help.
from AsyncEventLoop.Utils import fillAlgSequence
from AsyncComps.AsyncCompsConf import ASync__CPUCruncherAlg
from AthCUDATests.AthCUDATestsConf import AthCUDA__CPUGPUCruncherAlg
fillAlgSequence( algSeq, controlFlowGraph, dataFlowGraph,
                 'AthSequencer/AthAlgSeq',
                 ASync__CPUCruncherAlg, AthCUDA__CPUGPUCruncherAlg,
                 chosenSelector )

# Set the timing information on the algorithm(s).
for alg in algSeq:
   if alg.getName() in timingJson:
      alg.AverageRuntime = float( timingJson[ alg.getName() ] )
      pass
   pass

# Set up the GPU algorithm(s) correctly.
from AsyncEventLoop.AsyncEventLoopFlags import asyncEventLoopFlags
for alg in algSeq:
   if alg.getType() == 'AthCUDA::CPUGPUCruncherAlg':
      alg.IsAsynchronous = not asyncEventLoopFlags.SynchronousScheduler()
      alg.IsIOBound = asyncEventLoopFlags.SynchronousScheduler()
      alg.FloatArraySize = 100
      alg.FloatOpsMultiplier = 1
      pass
   pass

# Let the user know what happened.
log.info( 'Created the job configuration using:' )
log.info( '  - control-flow: %s' % controlFlowConf )
log.info( '  - data-flow   : %s' % dataFlowConf )
log.info( '  - timings     : %s' % timingsConf )

# Set up the avalance scheduler in case we execute the job synchronously.
if asyncEventLoopFlags.SynchronousScheduler():
   from GaudiHive.GaudiHiveConf import AvalancheSchedulerSvc
   svcMgr += AvalancheSchedulerSvc( 'AvalancheSchedulerSvc',
               PreemptiveIOBoundTasks = True,
               MaxIOBoundAlgosInFlight = asyncEventLoopFlags.NAccTasks(),
               DataLoaderAlg = 'ASync::DataStoreLoaderAlg' )
   pass

# Configure the asynchronous service(s).
from AsyncEventLoop.AsyncEventLoopConf import ASync__SchedulerSvc, \
                                              ASync__DataStoreLoaderAlg
svcMgr += ASync__SchedulerSvc( 'ASync::SchedulerSvc',
                               DataLoaderAlg = 'ASync::DataStoreLoaderAlg',
                               DataFlowOutput = 'mcreco_dflow.dot' )
algSeq += ASync__DataStoreLoaderAlg()

# Set up a few more events in flight than how many threads we use.
# Otherwise the scheduler has a hard time keeping the TBB threads busy.
# (With the asynchronous algorithms in place...)
#from AsyncEventLoop.AsyncEventLoopConf import ASync__WhiteBoard
#svcMgr += ASync__WhiteBoard( 'ASync::WhiteBoard',
#            EventSlots = asyncEventLoopFlags.NThreads() * 2 )

# Set some global job properties.
from AsyncEventLoop.AsyncEventLoopFlags import asyncEventLoopFlags
asyncEventLoopFlags.EvtMax.set_Value_and_Lock( 50 )
asyncEventLoopFlags.EventPrintoutInterval.set_Value_and_Lock( 10 )
