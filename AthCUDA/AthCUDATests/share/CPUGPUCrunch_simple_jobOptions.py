# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

# Set up the CUDA services.
from AsyncEventLoop.ServiceMgr import svcMgr
from AthCUDAServices.AthCUDAServicesConf import AthCUDA__KernelRunnerSvc
kernelSvc = AthCUDA__KernelRunnerSvc( 'AthCUDA::KernelRunnerSvc',
                                      NParallelKernels = 50 )
svcMgr += kernelSvc
from AthCUDAServices.AthCUDAServicesConf import AthCUDA__StreamPoolSvc
streamSvc = AthCUDA__StreamPoolSvc( 'AthCUDA::StreamPoolSvc',
                                    NStreams = 10 )
svcMgr += streamSvc

# Import the algorithm sequence object.
from AsyncEventLoop.AlgSequence import AlgSequence
algSeq = AlgSequence()

# Set up the job's algorithms.
from AsyncComps.AsyncCompsConf import *
from AthCUDATests.AthCUDATestsConf import *
algSeq += ASync__CPUCruncherAlg( 'CPUAlg1', AverageRuntime = 0.1,
                                 OutputKeys = [ 'CPUObject1', 'CPUObject2' ] )
algSeq += ASync__CPUCruncherAlg( 'CPUAlg2', AverageRuntime = 0.01,
                                 InputKeys = [ 'CPUObject1' ],
                                 OutputKeys = [ 'CPUObject3' ] )
algSeq += ASync__CPUCruncherAlg( 'CPUAlg3', AverageRuntime = 0.1,
                                 InputKeys = [ 'CPUObject2' ],
                                 OutputKeys = [ 'CPUObject4' ] )

algSeq += AthCUDA__CPUGPUCruncherAlg( 'CPUGPUAlg1',
                                      FloatArraySize = 10000,
                                      AverageRuntime = 0.1,
                                      InputKeys = [ 'CPUObject1' ],
                                      OutputKeys = [ 'GPUObject1' ] )
algSeq += AthCUDA__CPUGPUCruncherAlg( 'CPUGPUAlg2',
                                      FloatArraySize = 100,
                                      AverageRuntime = 0.05,
                                      InputKeys = [ 'CPUObject2' ],
                                      OutputKeys = [ 'GPUObject2' ] )
algSeq += AthCUDA__CPUGPUCruncherAlg( 'CPUGPUAlg3',
                                      FloatArraySize = 100,
                                      AverageRuntime = 0.001,
                                      InputKeys = [ 'GPUObject1' ],
                                      OutputKeys = [ 'GPUObject3' ] )

# Set some global job properties.
from AsyncEventLoop.AsyncEventLoopFlags import asyncEventLoopFlags
asyncEventLoopFlags.EvtMax.set_Value_and_Lock( 1000 )
asyncEventLoopFlags.EventPrintoutInterval.set_Value_and_Lock( 100 )

# Set additional properties on the algorithms/services based on the global job
# properties.
if asyncEventLoopFlags.SynchronousScheduler():
   from GaudiHive.GaudiHiveConf import AvalancheSchedulerSvc
   svcMgr += AvalancheSchedulerSvc( 'AvalancheSchedulerSvc',
               PreemptiveIOBoundTasks = True,
               MaxIOBoundAlgosInFlight = asyncEventLoopFlags.NAccTasks() )
   for alg in [ algSeq.CPUGPUAlg1, algSeq.CPUGPUAlg2, algSeq.CPUGPUAlg3 ]:
      alg.IsAsynchronous = False
      alg.IsIOBound = True
      pass
else:
   from AsyncEventLoop.AsyncEventLoopConf import ASync__SchedulerSvc
   svcMgr += ASync__SchedulerSvc( 'ASync::SchedulerSvc',
               DataFlowOutput = 'data_flot.dot' )
   for alg in [ algSeq.CPUGPUAlg1, algSeq.CPUGPUAlg2, algSeq.CPUGPUAlg3 ]:
      alg.IsAsynchronous = True
      pass
   pass

# Set up a few more events in flight than how many threads we use.
# Otherwise the scheduler has a hard time keeping the TBB threads busy.
# (With the asynchronous algorithms in place...)
from AsyncEventLoop.AsyncEventLoopConf import ASync__WhiteBoard
svcMgr += ASync__WhiteBoard( 'ASync::WhiteBoard',
            EventSlots = asyncEventLoopFlags.NThreads() + 2 )
