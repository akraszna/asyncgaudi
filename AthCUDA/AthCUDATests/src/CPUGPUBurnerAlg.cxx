// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "CPUGPUBurnerAlg.h"
#include "CPUGPUBurnerTask.h"

// Project include(s).
#include "AsyncBaseComps/ReadHandle.h"
#include "AthCUDAKernel/KernelStatus.h"

// Athena include(s).
#include "AthContainers/AuxElement.h"
#include "AthContainers/DataVector.h"

DECLARE_COMPONENT( AthCUDATests::CPUGPUBurnerAlg )

namespace AthCUDATests {

   StatusCode CPUGPUBurnerAlg::initialize() {

      // Retrieve/initialise the necessary objects.
      ATH_CHECK( m_kernelRunnerSvc.retrieve() );

      // Tell the user what will happen.
      ATH_MSG_INFO( "Will perform " << m_calcIterations.value()
                    << " calculation interations on the " << m_inputKey.value()
                    << " container using "
                    << m_kernelRunnerSvc.typeAndName() );
      ATH_MSG_INFO( "Running in "
                    << ( isIOBound() ? "I/O-bound " : "non-I/O-bound " )
                    << ( isAsynchronous() ? "asynchronous" : "synchronous" )
                    << " mode" );

      // Return gracefully:
      return StatusCode::SUCCESS;
   }

   StatusCode CPUGPUBurnerAlg::execute( const EventContext& ) const {

      // Create the read handle.
      ASync::ReadHandle< DataVector< SG::AuxElement > >
         container( m_inputKey );

      // Access the auxiliary store of the container.
      SG::IAuxStore* store =
         const_cast< SG::IAuxStore* >( container->getStore() );
      if( store == nullptr ) {
         ATH_MSG_ERROR( "Couldn't access the auxiliary store of "
                        << m_inputKey );
         return StatusCode::FAILURE;
      }

      // Create the status object that we use to wait for the execution of the
      // GPU task with.
      AthCUDA::KernelStatus status;

      // Create the CPU / GPU burner task.
      auto task = makeCPUGPUBurnerTask( *store, m_calcIterations.value(),
                                        status );

      // Give it to the kernel runner service.
      ATH_CHECK( m_kernelRunnerSvc->execute( std::move( task ) ) );

      // Now wait for the GPU task to finish.
      ATH_CHECK( status.wait() );

      // Return gracefully:
      return StatusCode::SUCCESS;
   }

   StatusCode CPUGPUBurnerAlg::mainExecute( const EventContext&,
                                            ASync::AlgTaskPtr_t pet ) const {

      // Create the read handle.
      ASync::ReadHandle< DataVector< SG::AuxElement > >
         container( m_inputKey );

      // Access the auxiliary store of the container.
      SG::IAuxStore* store =
         const_cast< SG::IAuxStore* >( container->getStore() );
      if( store == nullptr ) {
         ATH_MSG_ERROR( "Couldn't access the auxiliary store of "
                        << m_inputKey );
         return StatusCode::FAILURE;
      }

      // Create the CPU / GPU burner task.
      auto task = makeCPUGPUBurnerTask( *store, m_calcIterations.value(),
                                        std::move( pet ) );

      // Give it to the kernel runner service.
      ATH_CHECK( m_kernelRunnerSvc->execute( std::move( task ) ) );

      // Return gracefully:
      return StatusCode::SUCCESS;
   }

   StatusCode CPUGPUBurnerAlg::postExecute( const EventContext& ) const {

      // Just print a debug message.
      ATH_MSG_DEBUG( "CPU / GPU burning task finished" );

      // Return gracefully:
      return StatusCode::SUCCESS;
   }

} // namespace AthCUDATests
