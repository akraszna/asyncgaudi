// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "CPUGPUBurnerTask.h"

// Project include(s).
#include "AthCUDAAuxStore/AuxStore.cuh"
#include "AthCUDAKernel/AuxKernelTask.cuh"

// Athena include(s).
#include "AthContainersInterfaces/AuxTypes.h"

// System include(s).
#include <cmath>

namespace {

   /// Functor performing the CPU/GPU burning on the particles
   class CPUGPUBurnerFtr {
   public:
      ATHCUDA_HOST_AND_DEVICE
      void operator()( std::size_t index, AthCUDA::AuxStore& aux,
                       std::size_t nIterations,
                       SG::auxid_t etaId, SG::auxid_t phiId,
                       SG::auxid_t ptId ) {

         // Make a local copy of the values of interest.
         const double eta = aux.array< float >( etaId )[ index ];
         const double phi = aux.array< float >( phiId )[ index ];
         double pt = aux.array< float >( ptId )[ index ];

         /// Perform the specified number of iterations.
         for( std::size_t i = 0; i < nIterations; ++i ) {
            if( eta < 0.0 ) {
               pt += 1.05 * std::cos( std::pow( std::abs( std::sin( phi + pt ) ),
                                                eta ) );
            } else {
               pt += 0.95 * std::sin( std::pow( std::abs( std::cos( phi + pt ) ),
                                                eta ) );
            }
         }

         // Update the auxiliary variable.
         aux.array< float >( ptId )[ index ] = pt;
         return;
      }
   }; // class CPUGPUBurnerFtr

} // private namespace

namespace AthCUDATests {

   /// Function getting the auxiliary ID of "eta"
   SG::auxid_t getEtaAuxID();
   /// Function getting the auxiliary ID of "phi"
   SG::auxid_t getPhiAuxID();
   /// Function getting the auxiliary ID of "pt"
   SG::auxid_t getPtAuxID();

   std::unique_ptr< AthCUDA::IKernelTask >
   makeCPUGPUBurnerTask( SG::IAuxStore& store, unsigned int calcIterations,
                         ASync::AlgTaskPtr_t postExecTask ) {

      // Cast the auxiliary container to the right type.
      AthCUDA::AuxStore& cudaStore =
         dynamic_cast< AthCUDA::AuxStore& >( store );

      // Construct the CPU / GPU burner task.
      return AthCUDA::make_AuxKernelTask< ::CPUGPUBurnerFtr >(
         std::move( postExecTask ), cudaStore, calcIterations,
         getEtaAuxID(), getPhiAuxID(), getPtAuxID() );
   }

   std::unique_ptr< AthCUDA::IKernelTask >
   makeCPUGPUBurnerTask( SG::IAuxStore& store, unsigned int calcIterations,
                         AthCUDA::KernelStatus& status ) {

      // Cast the auxiliary container to the right type.
      AthCUDA::AuxStore& cudaStore =
         dynamic_cast< AthCUDA::AuxStore& >( store );

      // Construct the CPU / GPU burner task.
      return AthCUDA::make_AuxKernelTask< ::CPUGPUBurnerFtr >(
         status, cudaStore, calcIterations,
         getEtaAuxID(), getPhiAuxID(), getPtAuxID() );
   }

} // namespace AthCUDATests
