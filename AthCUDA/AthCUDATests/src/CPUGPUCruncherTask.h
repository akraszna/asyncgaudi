// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCUDATESTS_CPUGPUCRUNCHERTASK_H
#define ATHCUDATESTS_CPUGPUCRUNCHERTASK_H

// Project include(s).
#include "AsyncBaseComps/AlgTaskPtr.h"
#include "AthCUDAInterfaces/IKernelTask.h"

// System include(s).
#include <memory>
#include <vector>

namespace AthCUDA {

   // Forward declaration(s).
   class KernelStatus;

   /// Function setting up an asynchronous CPU/GPU crunching task
   std::unique_ptr< IKernelTask >
   make_CPUGPUCruncherTask( unsigned int fpOps, std::vector< float >& array,
                            ASync::AlgTaskPtr_t pet );

   /// Function setting up a synchronous CPU/GPU crunching task
   std::unique_ptr< IKernelTask >
   make_CPUGPUCruncherTask( unsigned int fpOps, std::vector< float >& array,
                            KernelStatus& status );

} // namespace AthCUDA

#endif // ATHCUDATESTS_CPUGPUCRUNCHERTASK_H
