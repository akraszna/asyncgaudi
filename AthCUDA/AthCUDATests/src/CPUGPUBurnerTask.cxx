// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Athena include(s).
#include "AthContainersInterfaces/AuxTypes.h"
#include "AthContainers/AuxElement.h"

namespace AthCUDATests {

   /// Function getting the auxiliary ID of "eta"
   SG::auxid_t getEtaAuxID() {

      static const SG::AuxElement::Accessor< float > acc( "eta" );
      return acc.auxid();
   }

   /// Function getting the auxiliary ID of "phi"
   SG::auxid_t getPhiAuxID() {

      static const SG::AuxElement::Accessor< float > acc( "phi" );
      return acc.auxid();
   }

   /// Function getting the auxiliary ID of "pt"
   SG::auxid_t getPtAuxID() {

      static const SG::AuxElement::Accessor< float > acc( "pt" );
      return acc.auxid();
   }

} // namespace AthCUDATests
