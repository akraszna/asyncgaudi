// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "CPUGPUCruncherTask.h"

// Project include(s).
#include "AthCUDACore/Macros.cuh"
#include "AthCUDAKernel/ArrayKernelTask.cuh"

namespace {

   /// Simple functor for performing floating point operations on an array
   struct CPUGPUCruncherCalc {
      ATHCUDA_HOST_AND_DEVICE
      void operator()( std::size_t i, std::size_t arraySize,
                       unsigned int fpOps, float a, float* array ) {

         // Perform the requested number of floating point operations.
         for( unsigned int j = 0; j < fpOps; ++j ) {
            array[ i ] *= array[ ( i + j ) % arraySize ];
         }
         return;
      }
   }; // class CPUGPUCruncherCalc

} // private namespace

namespace AthCUDA {

   std::unique_ptr< IKernelTask >
   make_CPUGPUCruncherTask( unsigned int fpOps, std::vector< float >& array,
                            ASync::AlgTaskPtr_t pet ) {

      return make_ArrayKernelTask< ::CPUGPUCruncherCalc >( std::move( pet ),
                                                           array.size(),
                                                           array.size(), fpOps,
                                                           1.23f,
                                                           array.data() );
   }

   std::unique_ptr< IKernelTask >
   make_CPUGPUCruncherTask( unsigned int fpOps, std::vector< float >& array,
                            KernelStatus& status ) {

      return make_ArrayKernelTask< ::CPUGPUCruncherCalc >( status,
                                                           array.size(),
                                                           array.size(), fpOps,
                                                           1.23f,
                                                           array.data() );
   }

} // namespace AthCUDA
