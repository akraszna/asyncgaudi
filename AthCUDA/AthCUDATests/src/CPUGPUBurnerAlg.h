// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCUDATESTS_CPUGPUBURNERALG_H
#define ATHCUDATESTS_CPUGPUBURNERALG_H

// Project include(s).
#include "AsyncBaseComps/Algorithm.h"
#include "AsyncBaseComps/ReadHandleKey.h"
#include "AthCUDAInterfaces/IKernelRunnerSvc.h"

// Gaudi include(s).
#include <GaudiKernel/Property.h>
#include <GaudiKernel/ServiceHandle.h>

namespace AthCUDATests {

   /// Algorithm executing the CPU/GPU burning
   ///
   /// This is the main algorithm to profile CUDA task offloading with.
   /// It picks up a container of "particles" that were created in the event
   /// store by another algorithm, and performs some dummy calculations on the
   /// objects to test the CPU / GPU load balancing.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class CPUGPUBurnerAlg : public ASync::Algorithm {

   public:
      /// Inherit all constructor(s).
      using ASync::Algorithm::Algorithm;

      /// @name Basic algorithm function(s).
      /// @{

      /// Initialise the algorithm
      virtual StatusCode initialize() override;

      /// Execute the CPU / GPU burning synchronously
      virtual StatusCode execute( const EventContext& ) const override;

      /// @}

      /// @name Asynchronous algorithm function(s).
      /// @{

      /// Execute the CPU / GPU burning asynchronously
      virtual StatusCode mainExecute( const EventContext&,
                                      ASync::AlgTaskPtr_t pet ) const override;
      /// Post-execute step for the CPU / GPU burning
      virtual StatusCode postExecute( const EventContext& ) const override;

      /// @}

   private:
      /// @name Algorithm properties
      /// @{

      /// Input particles processed by the algorithm
      Gaudi::Property< ASync::ReadHandleKey > m_inputKey{ this, "InputKey",
         ASync::ReadHandleKey( "Particles", *this ),
         "Particle container to process" };
      /// Calculation iterations to perform on the particles
      Gaudi::Property< unsigned int > m_calcIterations{ this, "CalcIterations",
         100, "Calculation iterations to perform" };

      /// @}

      /// Service to run the calculation with
      ServiceHandle< AthCUDA::IKernelRunnerSvc > m_kernelRunnerSvc{ this,
         "KernelRunnerSvc", "AthCUDA::KernelRunnerSvc" };

   }; // class CPUGPUBurnerAlg

} // namespace AthCUDATests

#endif // ATHCUDATESTS_CPUGPUBURNERALG_H
