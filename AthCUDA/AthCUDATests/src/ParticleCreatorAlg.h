// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCUDATESTS_PARTICLECREATORALG_H
#define ATHCUDATESTS_PARTICLECREATORALG_H

// Project include(s).
#include "AsyncBaseComps/Algorithm.h"
#include "AsyncBaseComps/WriteHandleKey.h"

// Gaudi include(s).
#include <GaudiKernel/Property.h>

/// Namespace for the Athena CUDA tests
namespace AthCUDATests {

   /// Algorithm creating random particles
   ///
   /// It can be told how many particles to create as a maximum. It will
   /// generate particles with a flat distribution between 0 and that
   /// configured number.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class ParticleCreatorAlg : public ASync::Algorithm {

   public:
      /// Inherit all constructor(s).
      using ASync::Algorithm::Algorithm;

      /// @name Basic algorithm function(s).
      /// @{

      /// Function initialising the algorithm
      virtual StatusCode initialize() override;
      /// Function executing the algorithm
      virtual StatusCode execute( const EventContext& ctx ) const override;

      /// @}

   private:
      /// @name Algorithm properties
      /// @{

      /// Key to write the container with
      Gaudi::Property< ASync::WriteHandleKey > m_outputKey{ this, "OutputKey",
         ASync::WriteHandleKey( "Particles", *this ),
         "Key to write a container with" };

      /// Maximum number of particles to create
      Gaudi::Property< int > m_maxParticles{ this, "MaxParticles", 100,
         "The maximum number of particles to generate" };

      /// @}

   }; // class ParticleCreatorAlg

} // namespace AthCUDATests

#endif // ATHCUDATESTS_PARTICLECREATORALG_H
