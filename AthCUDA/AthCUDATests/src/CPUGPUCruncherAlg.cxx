// Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "CPUGPUCruncherAlg.h"
#include "CPUGPUCruncherTask.h"

// Project include(s).
#include "AthCUDAKernel/KernelStatus.h"

// System include(s).
#include <memory>
#include <vector>

DECLARE_COMPONENT( AthCUDA::CPUGPUCruncherAlg )

namespace AthCUDA {

   StatusCode CPUGPUCruncherAlg::initialize() {

      // Retrieve/initialise the necessary object(s).
      ATH_CHECK( m_calibSvc.retrieve() );
      ATH_CHECK( m_kernelRunnerSvc.retrieve() );

      // Make sure that at least one output key is set up, otherwise
      // the algorithm won't be able to work.
      if( m_outputKeys.value().size() == 0 ) {
         const std::string outputName = name() + "_Output";
         ATH_MSG_WARNING( "No output key was specified, using: "
                          << outputName );
         m_outputKeys.value().emplace_back( outputName, *this );
      }

      // Get "safe" values for the times.
      const double avgTime = std::max( m_avgRuntime.value(), 0.0 );
      const double varTime = std::max( m_varRuntime.value(), 0.0 );

      // Calculate how many FP operations to execute.
      const unsigned int milliseconds_count = avgTime * 1000;
      const std::chrono::milliseconds milliseconds( milliseconds_count );
      m_fpOps = ( m_calibSvc->getFPOperationsFor( m_floatArraySize.value(),
                                                  milliseconds ) *
                  m_fopsMultiplier.value() );

      // Tell the user what is about to happen.
      ATH_MSG_INFO( "Will execute for " << avgTime << " +- " << varTime
                    << " second(s)" );
      ATH_MSG_INFO( "For this, will execute " << m_fpOps
                    << " floating point operation(s) on "
                    << m_floatArraySize.value() << " float values" );
      ATH_MSG_INFO( "Running in "
                    << ( isIOBound() ? "I/O-bound " : "non-I/O-bound " )
                    << ( isAsynchronous() ? "asynchronous" : "synchronous" )
                    << " mode" );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode CPUGPUCruncherAlg::execute( const EventContext& ) const {

      // Create a vector of floats with the configured size. This will be the
      // array to run the calculation on.
      auto array =
         std::make_unique< std::vector< float > >( m_floatArraySize.value(),
                                                   0 );

      // Create the status object that we use to wait for the execution of the
      // GPU task with.
      KernelStatus status;

      // Set up a calculation task on this array.
      auto task = make_CPUGPUCruncherTask( m_fpOps, *array,
                                           status );
      ATH_CHECK( m_kernelRunnerSvc->execute( std::move( task ) ) );

      // Now wait for the calculation to finish.
      ATH_CHECK( status.wait() );

      // Record it into the event store.
      ATH_CHECK( evtStore()->record( std::move( array ),
                                     m_outputKeys.value()[ 0 ].objKey() ) );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode CPUGPUCruncherAlg::mainExecute( const EventContext&,
                                              ASync::AlgTaskPtr_t pet ) const {

      // Create a vector of floats with the configured size. This will be the
      // array to run the calculation on.
      auto array =
         std::make_unique< std::vector< float > >( m_floatArraySize.value(),
                                                   1.34f );

      // Set up a calculation task on this array.
      auto task = make_CPUGPUCruncherTask( m_fpOps, *array,
                                           std::move( pet ) );
      ATH_CHECK( m_kernelRunnerSvc->execute( std::move( task ) ) );

      // Record it into the event store.
      ATH_CHECK( evtStore()->record( std::move( array ),
                                     m_outputKeys.value()[ 0 ].objKey() ) );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode CPUGPUCruncherAlg::postExecute( const EventContext& ) const {

      // Just print a debug message.
      ATH_MSG_DEBUG( "CPU / GPU crunching task finished" );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

} // namespace AthCUDA
