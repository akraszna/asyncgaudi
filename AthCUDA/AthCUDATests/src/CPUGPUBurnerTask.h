// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCUDATESTS_CPUGPUBURNERTASK_H
#define ATHCUDATESTS_CPUGPUBURNERTASK_H

// Project include(s).
#include "AthCUDAInterfaces/IKernelTask.h"
#include "AsyncBaseComps/AlgTaskPtr.h"

// System include(s).
#include <memory>

// Forward declaration(s).
namespace SG {
   class IAuxStore;
}
namespace AthCUDA {
   class KernelStatus;
}

namespace AthCUDATests {

   /// Helper function creating the CPU / GPU burner task
   ///
   /// This C++ interface is used to pass the created task from the CUDA code
   /// to the C++ code.
   ///
   /// @param store The auxiliary store to process
   /// @param calcIterations The number of calculation iterations to run
   /// @param postExecTask The TBB task to execute when the CUDA task finished
   /// @return The task to give to the kernel runner service for execution
   ///
   std::unique_ptr< AthCUDA::IKernelTask >
   makeCPUGPUBurnerTask( SG::IAuxStore& store, unsigned int calcIterations,
                         ASync::AlgTaskPtr_t postExecTask );

   /// Helper function creating the CPU / GPU burner task
   ///
   /// This C++ interface is used to pass the created task from the CUDA code
   /// to the C++ code.
   ///
   /// @param store The auxiliary store to process
   /// @param calcIterations The number of calculation iterations to run
   /// @param status Status object for the GPU execution
   /// @return The task to give to the kernel runner service for execution
   ///
   std::unique_ptr< AthCUDA::IKernelTask >
   makeCPUGPUBurnerTask( SG::IAuxStore& store, unsigned int calcIterations,
                         AthCUDA::KernelStatus& status );

} // namespace AthCUDATests

#endif // ATHCUDATESTS_CPUGPUBURNERTASK_H
