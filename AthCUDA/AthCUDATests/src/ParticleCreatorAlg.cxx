// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "ParticleCreatorAlg.h"

// Project include(s).
#include "AthCUDAAuxStore/MakeAuxStore.h"
#include "AsyncBaseComps/WriteHandle.h"

// Athena include(s).
#include "AthContainers/AuxElement.h"
#include "AthContainers/DataVector.h"
#include "xAODCore/tools/PrintHelpers.h"

// System include(s).
#include <cmath>
#include <random>

DECLARE_COMPONENT( AthCUDATests::ParticleCreatorAlg )

namespace AthCUDATests {

   StatusCode ParticleCreatorAlg::initialize() {

      // Initialise the write handle.
      ATH_CHECK( m_outputKey.initialize() );

      // Tell the user what will happen.
      ATH_MSG_INFO( "Will produce [0 - " << m_maxParticles.value()
                    << "] random particles for container "
                    << m_outputKey.value() );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode ParticleCreatorAlg::execute( const EventContext& ctx ) const {

      // The type of the generated container.
      typedef DataVector< SG::AuxElement > Container_t;

      // Set up a particle array.
      auto aux = AthCUDA::makeAuxStore();
      auto particles = std::make_unique< Container_t >();
      particles->setStore( aux.get() );

      // Accessors used in setting up the particles.
      static const SG::AuxElement::Accessor< float > ptAcc( "pt" ),
         etaAcc( "eta" ), phiAcc( "phi" );

      // The random number generators.
      std::mt19937 gen( ctx.evt() );
      std::uniform_int_distribution< std::size_t >
         nGen( 0, m_maxParticles.value() );
      std::normal_distribution< float > ptGen( 20000.0, 5000.0 );
      std::normal_distribution< float > etaGen( 0.0, 1.5 );
      std::uniform_real_distribution< float > phiGen( -M_PI, M_PI );

      // Generate random particles for the container.
      const std::size_t nParticles = nGen( gen );
      particles->reserve( nParticles );
      for( std::size_t i = 0; i < nParticles; ++i ) {
         SG::AuxElement* p = new SG::AuxElement();
         particles->push_back( p );
         ptAcc( *p )  = ptGen( gen );
         etaAcc( *p ) = etaGen( gen );
         phiAcc( *p ) = phiGen( gen );
      }

      // Print the particles for debugging.
      ATH_MSG_DEBUG( "Generated the following " << particles->size()
                     << " particles in event " << ctx.evt() << ":" );
      for( const SG::AuxElement* e : *particles ) {
         ATH_MSG_DEBUG( *e );
      }

      // Write the container to the event store.
      ASync::WriteHandle< Container_t > writer( m_outputKey );
      ATH_CHECK( writer.put( std::move( particles ), std::move( aux ) ) );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

} // namespace AthCUDATests
