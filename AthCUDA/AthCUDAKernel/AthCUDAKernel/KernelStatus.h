// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCUDAKERNEL_KERNELSTATUS_H
#define ATHCUDAKERNEL_KERNELSTATUS_H

// Gaudi include(s).
#include <GaudiKernel/StatusCode.h>

// System include(s).
#include <mutex>

namespace AthCUDA {

   /// Helper object used for synchronising GPU kernel tasks
   ///
   /// It is meant to be used as a mixture of @c std::future and
   /// @c StatusCode. Note that it should only be used for tests made
   /// outside of Gaudi jobs. Inside of a Gaudi job one should make
   /// use of the asynchronous execution provided by ASync::SchedulerSvc.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class KernelStatus {

   public:
      /// Default constructor
      KernelStatus();

      /// Function called by the framework when the CUDA kernel finished
      void finished( StatusCode code );

      /// Wait for the execution of the kernel to finish
      StatusCode wait();

   private:
      /// The result of the kernel execution
      StatusCode m_code;
      /// Mutex used for waiting for the CUDA task to finish
      std::mutex m_mutex;

   }; // class KernelStatus

} // namespace AthCUDA

#endif // ATHCUDAKERNEL_KERNELSTATUS_H
