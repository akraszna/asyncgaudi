// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCUDAKERNEL_AUXKERNELTASK_CUH
#define ATHCUDAKERNEL_AUXKERNELTASK_CUH

// Local include(s).
#include "AthCUDAKernel/KernelStatus.h"

// Project include(s).
#include "AthCUDAInterfaces/IKernelTask.h"
#include "AthCUDACore/Info.h"
#include "AthCUDACore/StreamHolderHelpers.cuh"
#include "AthCUDAAuxStore/AuxStore.cuh"
#include "AsyncBaseComps/AlgTaskPtr.h"

// System include(s).
#include <memory>
#include <tuple>
#include <type_traits>

namespace AthCUDA {

   /// Task launching/executing a calculation on an auxiliary container
   ///
   /// Such an object is created behind the scenes by the function(s) that
   /// the user is supposed to call, to pass it to the service that executes
   /// kernel functions on a GPU or on the CPU.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   template< class FUNCTOR, typename... ARGS >
   class AuxKernelTask : public IKernelTask {

      // At least one argument has to be provided.
      static_assert( sizeof...( ARGS ) > 0,
                     "At least one functor argument must be provided" );

   public:
      /// Constructor to use in a non-blocking execution
      AuxKernelTask( ASync::AlgTaskPtr_t postExecTask, AuxStore& aux,
                     ARGS... args );
      /// Constructor to use in a blocking execution
      AuxKernelTask( KernelStatus& status, AuxStore& aux, ARGS... args );

      /// @name Function(s) inherited from @c AthCUDA::IKernelTask
      /// @{

      /// Execute the kernel using a specific stream
      virtual StatusCode execute( StreamHolder& stream ) override;

      /// Function called when an asynchronous execution finishes
      virtual StatusCode finished( StatusCode code,
                                   KernelExecMode mode ) override;

      /// @}

   private:
      /// A possible task object to use for executing a post-execute step
      ASync::AlgTaskPtr_t m_postExecTask;
      /// A possible status object to notify about the task finishing
      KernelStatus* m_status;
      /// The auxiliary container to execute the calculation on
      AuxStore& m_aux;
      /// The arguments to pass to the functor
      std::tuple< ARGS... > m_args;

   }; // class AuxKernelTask

   /// Helper function for creating an instance of @c AthCUDA::AuxKernelTask
   template< class FUNCTOR, typename... ARGS >
   std::unique_ptr< AuxKernelTask< FUNCTOR, ARGS... > >
   make_AuxKernelTask( ASync::AlgTaskPtr_t postExecTask, AuxStore& aux,
                       ARGS... args );

   /// Helper function for creating an instance of @c AthCUDA::AuxKernelTask
   template< class FUNCTOR, typename... ARGS >
   std::unique_ptr< AuxKernelTask< FUNCTOR, ARGS... > >
   make_AuxKernelTask( KernelStatus& status, AuxStore& aux, ARGS... args );

} // namespace AthCUDA

namespace {

   /// Function executing the user functor on the host
   ///
   /// @param aux  The auxiliary container to process
   /// @param args Mixture of user arguments and auxiliary IDs
   ///
   template< class FUNCTOR, class... ARGS >
   StatusCode
   hostExecute( AthCUDA::AuxStore& aux, ARGS... args ) {

      // Instantiate the functor
      auto functor = FUNCTOR();

      // Process each element of the auxiliary container using it.
      const std::size_t n = aux.arraySize();
      for( std::size_t i = 0; i < n; ++i ) {
         functor( i, aux, args... );
      }
      return StatusCode::SUCCESS;
   }

#ifdef __CUDACC__
   /// Kernel executing the user functor on a device/GPU
   ///
   /// @param csize The size of the auxiliary container
   /// @param vsize The number of variables managed by the auxiliary
   ///              container
   /// @param vars  Pointers to all the variables allocated on the host
   /// @param args  Mixture of user arguments and auxiliary IDs
   ///
   template< class FUNCTOR, typename... ARGS >
   __global__
   void deviceKernel( std::size_t csize, std::size_t vsize, void** vars,
                      ARGS... args ) {

      // Find the current index that we need to process.
      const std::size_t i = blockIdx.x * blockDim.x + threadIdx.x;
      if( i >= csize ) {
         return;
      }

      // Construct the auxiliary container.
      AthCUDA::AuxStore aux( csize, vsize, vars );

      // Execute the user's function for this index.
      FUNCTOR()( i, aux, args... );
      return;
   }

   /// Function executing the user functor on a device/GPU
   ///
   /// @param stream The CUDA stream to schedule the execution to/with
   /// @param aux The auxiliary container to process
   /// @param args Mixture of user arguments and auxiliary IDs
   ///
   template< class FUNCTOR, class... ARGS >
   StatusCode deviceExecute( cudaStream_t stream,
                             AthCUDA::AuxStore& aux,
                             ARGS... args ) {

      // Decide how many threads to use per execution block. Default
      // to 1024, but scale back from it if necessary.
      int nThreadsPerBlock = 1024;
      for( int i = 0; i < AthCUDA::Info::instance().nDevices(); ++i ) {
         while( nThreadsPerBlock >
                AthCUDA::Info::instance().maxThreadsPerBlock()[ i ] ) {
            nThreadsPerBlock /= 2;
         }
      }

      // If the array is not even this large, then reduce the value to the
      // size of the array.
      if( aux.arraySize() < nThreadsPerBlock ) {
         nThreadsPerBlock = aux.arraySize();
      }

      // Schedule the data movement and the calculation.
      const std::size_t n = aux.arraySize();
      const int nBlocks = ( ( n + nThreadsPerBlock - 1 ) /
                            nThreadsPerBlock );
      auto vars = aux.attachTo( stream );
      deviceKernel< FUNCTOR ><<< nBlocks, nThreadsPerBlock, 0, stream >>>(
         n, vars.first, vars.second, args... );
      aux.retrieveFrom( stream );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }
#endif // __CUDACC__

   /// @name @c std::tuple handling helper functions
   /// @{

   template< class TUPLE, std::size_t... Ns >
   auto tupleTailImpl( std::index_sequence< Ns... >, const TUPLE& t ) {
      return std::make_tuple( std::get< Ns + 1u >( t )... );
   }

   template< class TUPLE >
   auto tupleTail( const TUPLE& t ) {
      return tupleTailImpl(
         std::make_index_sequence< std::tuple_size< TUPLE >() - 1u >(), t );
   }

   /// @}

   /// Helper class for implementing @c AthCUDA::AuxKernelTask::execute.
   template< class FUNCTOR >
   class ExecuteImpl {
   public:
      /// Function called at the end of the recursive function calls. This
      /// is the function that actually does something.
      template< typename... ARGS1 >
      static StatusCode
      execute( cudaStream_t stream, AthCUDA::AuxStore& aux,
               const std::tuple<>&, ARGS1... args ) {

         // If the container is empty, return right away.
         if( aux.arraySize() == 0 ) {
            return StatusCode::SUCCESS;
         }

         // Check if we should run the task on a GPU.
#ifdef __CUDACC__
         if( stream ) {
            return deviceExecute< FUNCTOR >( stream, aux, args... );
         }
#endif // __CUDACC__

         // This is just to silence a compiler warning when CUDA is not used.
         if( stream ) {}

         // If not, the let's run it in the current thread on the CPU.
         return hostExecute< FUNCTOR >( aux, args... );
      }

      /// Function used to recursively pull out all function arguments from
      /// @c AthCUDA::AuxKernelTask::m_args.
      template< typename... ARGS1, typename... ARGS2 >
      static StatusCode
      execute( cudaStream_t stream, AthCUDA::AuxStore& aux,
               const std::tuple< ARGS1... >& t, ARGS2... args ) {

         return execute( stream, aux, ::tupleTail( t ), args...,
                         std::get< 0 >( t ) );
      }
   }; // class ExecuteImpl

} // private namespace

namespace AthCUDA {

   template< class FUNCTOR, typename... ARGS >
   AuxKernelTask< FUNCTOR, ARGS... >::
   AuxKernelTask( ASync::AlgTaskPtr_t postExecTask, AuxStore& aux,
                  ARGS... args )
   : m_postExecTask( std::move( postExecTask ) ), m_status( nullptr ),
     m_aux( aux ), m_args( args... ) {

   }

   template< class FUNCTOR, typename... ARGS >
   AuxKernelTask< FUNCTOR, ARGS... >::
   AuxKernelTask( KernelStatus& status, AuxStore& aux, ARGS... args )
   : m_postExecTask(), m_status( &status ), m_aux( aux ), m_args( args... ) {

   }

   template< class FUNCTOR, typename... ARGS >
   StatusCode AuxKernelTask< FUNCTOR, ARGS... >::
   execute( StreamHolder& stream ) {

      return ::ExecuteImpl< FUNCTOR >::execute( getStream( stream ), m_aux,
                                                ::tupleTail( m_args ),
                                                std::get< 0 >( m_args ) );
   }

   template< class FUNCTOR, typename... ARGS >
   StatusCode AuxKernelTask< FUNCTOR, ARGS... >::
   finished( StatusCode code, KernelExecMode ) {

      // If we have a TBB task object, enqueue it now.
      if( m_postExecTask ) {
         const StatusCode sc = ASync::enqueue( std::move( m_postExecTask ) );
         if( ! sc.isSuccess() ) {
            return sc;
         }
      }
      // If we have a simple status object, notify it now.
      if( m_status ) {
         m_status->finished( code );
      } else {
         code.ignore();
      }
      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   template< class FUNCTOR, typename... ARGS >
   std::unique_ptr< AuxKernelTask< FUNCTOR, ARGS... > >
   make_AuxKernelTask( ASync::AlgTaskPtr_t postExecTask, AuxStore& aux,
                       ARGS... args ) {
      return
         std::make_unique< AuxKernelTask< FUNCTOR, ARGS... > >(
            std::move( postExecTask ), aux, args... );
   }

   template< class FUNCTOR, typename... ARGS >
   std::unique_ptr< AuxKernelTask< FUNCTOR, ARGS... > >
   make_AuxKernelTask( KernelStatus& status, AuxStore& aux, ARGS... args ) {
      return
         std::make_unique< AuxKernelTask< FUNCTOR, ARGS... > >(
            status, aux, args... );
   }

} // namespace AthCUDA

#endif // ATHCUDAKERNEL_AUXKERNELTASK_CUH
