// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AthCUDAKernel/AuxKernelTask.cuh"

// Project include(s).
#include "AthCUDAInterfaces/StreamHolder.h"
#include "AthCUDACore/Info.h"
#include "AthCUDACore/Macros.cuh"
#include "AthCUDAAuxStore/AuxStore.cuh"

// System include(s).
#undef NDEBUG
#include <cassert>
#include <cmath>
#include <string>
#include <vector>

// Forward declaration(s).
namespace SG {
   class IAuxStore;
   class IConstAuxStore;
}

/// Helper function for filling an auxiliary container with a variable
template< class T >
SG::auxid_t fillVariable( SG::IAuxStore& store, const std::string& name,
                          const std::vector< T >& vars );

/// Helper function for extracting a variable from an auxiliary container
template< class T >
void extractVariable( const SG::IConstAuxStore& store, const std::string& name,
                      std::vector< T >& vars );

/// Functor multiplying the "pt" variable by some amount
class PtMultiply {
public:
   ATHCUDA_HOST_AND_DEVICE
   void operator()( std::size_t i, AthCUDA::AuxStore& aux,
                    float multiplier, SG::auxid_t ptId ) {

      aux.array< float >( ptId )[ i ] *= multiplier;
   }
}; // class PtMultiply

int main() {

   // Instantiate the container to be tested.
   AthCUDA::AuxStore auxCuda;

   // Set up the variables that we'll add in it.
   static const int CONTAINER_SIZE = 10;
   std::vector< float > eta, phi, pt;
   for( int i = 0; i < CONTAINER_SIZE; ++i ) {
      eta.push_back( -2.5f + 0.1f * i );
      phi.push_back( -M_PI + 0.2f * i );
      pt.push_back( 10000.0f + 1000.0f * i );
   }

   // Put these variables on the container.
   fillVariable( auxCuda, "eta", eta );
   fillVariable( auxCuda, "phi", phi );
   const SG::auxid_t ptId  = fillVariable( auxCuda, "pt",  pt );

   // Allocate a CUDA stream for the test.
   cudaStream_t stream = nullptr;
   if( AthCUDA::Info::instance().nDevices() > 0 ) {
      CUDA_EXP_CHECK( cudaStreamCreate( &stream ) );
   }
   AthCUDA::StreamHolder sholder( stream, nullptr );

   // Create and run the calculation task on the auxiliary container.
   AthCUDA::KernelStatus dummy;
   auto task = AthCUDA::make_AuxKernelTask< PtMultiply >( dummy, auxCuda, 2.0f,
                                                          ptId );
   task->execute( sholder );

   // Wait for the task to finish, and then destroy the CUDA stream.
   if( stream ) {
      CUDA_EXP_CHECK( cudaDeviceSynchronize() );
      CUDA_EXP_CHECK( cudaStreamDestroy( stream ) );
   }

   // Check the results.
   assert( auxCuda.arraySize() == CONTAINER_SIZE );
   std::vector< float > etaNew, phiNew, ptNew;
   extractVariable( auxCuda, "eta", etaNew );
   extractVariable( auxCuda, "phi", phiNew );
   extractVariable( auxCuda, "pt",  ptNew );
   for( int i = 0; i < CONTAINER_SIZE; ++i ) {
      assert( std::abs( etaNew[ i ] - eta[ i ] ) < 0.001 );
      assert( std::abs( phiNew[ i ] - phi[ i ] ) < 0.001 );
      assert( std::abs( ptNew[ i ] - pt[ i ] * 2.0f ) < 0.001 );
   }

   // Return gracefully.
   return 0;
}
