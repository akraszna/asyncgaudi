// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Athena include(s).
#include "AthContainersInterfaces/IConstAuxStore.h"
#include "AthContainersInterfaces/IAuxStore.h"
#include "AthContainers/AuxElement.h"
#include "AthContainers/DataVector.h"

// System include(s).
#include <cassert>
#include <vector>

template< class T >
SG::auxid_t fillVariable( SG::IAuxStore& store, const std::string& name,
                          const std::vector< T >& vars ) {

   // A security check.
   assert( vars.size() >= store.size() );

   // Set up the interface container.
   DataVector< SG::AuxElement > interface;
   for( std::size_t i = 0; i < store.size(); ++i ) {
      interface.push_back( new SG::AuxElement() );
   }
   interface.setStore( &store );
   for( std::size_t i = store.size(); i < vars.size(); ++i ) {
      interface.push_back( new SG::AuxElement() );
   }

   // Set this variable on all elements of the interface container.
   SG::AuxElement::Accessor< T > acc( name );
   for( std::size_t i = 0; i < vars.size(); ++i ) {
      acc( *( interface[ i ] ) ) = vars[ i ];
   }

   // Return the auxiliary ID of the variable.
   return acc.auxid();
}

template< class T >
void extractVariable( const SG::IConstAuxStore& store, const std::string& name,
                      std::vector< T >& vars ) {

   // Set up the interface container.
   DataVector< SG::AuxElement > interface;
   for( std::size_t i = 0; i < store.size(); ++i ) {
      interface.push_back( new SG::AuxElement() );
   }
   interface.setStore( &store );

   // Extract this variable from the container.
   vars.clear();
   for( const SG::AuxElement* e : interface ) {
      vars.push_back( e->auxdataConst< T >( name ) );
   }

   return;
}

// Instantiate the functions for a few types.
#define INST( TYPE )                                                        \
   template SG::auxid_t fillVariable< TYPE >( SG::IAuxStore&,               \
                                              const std::string&,           \
                                              const std::vector< TYPE >& ); \
   template void extractVariable< TYPE >( const SG::IConstAuxStore&,        \
                                          const std::string&,               \
                                          std::vector< TYPE >& )

INST( float );

#undef INST
