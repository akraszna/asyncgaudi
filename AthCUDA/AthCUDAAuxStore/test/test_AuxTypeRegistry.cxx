// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Athena include(s).
#include "AthContainers/AuxTypeRegistry.h"

// System include(s).
#include <vector>

/// Function providing / generating an auxiliary ID
template< typename T >
SG::auxid_t getAuxID( const std::string& name ) {

   return SG::AuxTypeRegistry::instance().getAuxID< T >( name );
}

// Instantiate the function for a few template types
#define INST( TYPE )                                                          \
   template SG::auxid_t getAuxID< TYPE >( const std::string& )

INST( int );
INST( float );
INST( std::vector< int > );

#undef INST
