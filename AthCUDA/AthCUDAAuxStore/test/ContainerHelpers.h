// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCUDAAUXSTORE_CONTAINERHELPERS_H
#define ATHCUDAAUXSTORE_CONTAINERHELPERS_H

// Athena include(s).
#include "AthContainersInterfaces/AuxTypes.h"

// System include(s).
#include <string>
#include <vector>

// Forward declaration(s).
namespace SG {
   class IAuxStore;
   class IConstAuxStore;
}

/// Helper function for filling an auxiliary container with variable
template< class T >
SG::auxid_t fillVariable( SG::IAuxStore& store, const std::string& name,
                          const std::vector< T >& vars );

/// Helper function for extracting a variable from an auxiliary container
template< class T >
void extractVariable( const SG::IConstAuxStore& store, const std::string& name,
                      std::vector< T >& vars );

#endif // ATHCUDAAUXSTORE_CONTAINERHELPERS_H
