// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AthCUDAAuxStore/AuxStore.cuh"
#include "ContainerHelpers.h"

// Project include(s).
#include "AthCUDACore/Info.h"
#include "AthCUDACore/Macros.cuh"

// System include(s).
#undef NDEBUG
#include <cassert>
#include <cmath>
#include <iostream>
#include <vector>

/// Kernel incrementing the values in the "var1" auxiliary variable
__global__
void incrementVar1( std::size_t csize, std::size_t vsize, void** vars,
                    SG::auxid_t var1Id ) {

   // Find the current index that we need to process.
   const std::size_t i = blockIdx.x * blockDim.x + threadIdx.x;
   if( i >= csize ) {
      return;
   }

   // Construct the auxiliary container.
   AthCUDA::AuxStore aux( csize, vsize, vars );

   // Perform the variable increment.
   aux.array< float >( var1Id )[ i ] += 0.5;

   return;
}

int main() {

   // Don't even attempt the test if a GPU is not available.
   if( AthCUDA::Info::instance().nDevices() == 0 ) {
      std::cout << "Test skipped, as no GPU is available" << std::endl;
      return 0;
   }

   // Instantiate the container to be tested.
   AthCUDA::AuxStore auxCuda;

   // Set up the variables that we'll add in it.
   static const int CONTAINER_SIZE = 10;
   std::vector< float > var1;
   std::vector< int > var2;
   std::vector< std::vector< int > > var3;
   for( int i = 0; i < CONTAINER_SIZE; ++i ) {
      var1.push_back( 1.5f * i );
      var2.push_back( 3 * i );
      var3.push_back( { 1, 2 } );
   }

   // Put these variables on the container.
   const SG::auxid_t id1 = fillVariable( auxCuda, "var1", var1 );
   const SG::auxid_t id2 = fillVariable( auxCuda, "var2", var2 );
   const SG::auxid_t id3 = fillVariable( auxCuda, "var3", var3 );

   // Allocate a CUDA stream for the test.
   cudaStream_t stream = nullptr;
   CUDA_EXP_CHECK( cudaStreamCreate( &stream ) );

   // Run a simple kernel on this container.
   auto variables = auxCuda.attachTo( stream );
   incrementVar1<<< 1, CONTAINER_SIZE >>>( auxCuda.arraySize(),
                                           variables.first,
                                           variables.second, id1 );
   auxCuda.retrieveFrom( stream );
   CUDA_EXP_CHECK( cudaDeviceSynchronize() );

   // Destroy the CUDA stream.
   CUDA_EXP_CHECK( cudaStreamDestroy( stream ) );

   // Check the updated container using the SG::IConstAuxStore interface.
   assert( auxCuda.arraySize() == CONTAINER_SIZE );
   std::vector< float > var1update;
   extractVariable( auxCuda, "var1", var1update );
   for( int i = 0; i < CONTAINER_SIZE; ++i ) {
      assert( std::abs( var1update[ i ] - 0.5f - var1[ i ] ) < 0.001 );
   }
   std::vector< int > var2update;
   extractVariable( auxCuda, "var2", var2update );
   assert( var2update == var2 );
   std::vector< std::vector< int > > var3update;
   extractVariable( auxCuda, "var3", var3update );
   assert( var3update == var3 );

   // Check the updated container using the xAOD::AuxCUDAContainer interface.
   for( int i = 0; i < CONTAINER_SIZE; ++i ) {
      assert( std::abs( auxCuda.array< float >( id1 )[ i ] - 0.5f -
                        var1[ i ] ) < 0.0001 );
      assert( auxCuda.array< int >( id2 )[ i ] == var2[ i ] );
   }

   // Return gracefully.
   return 0;
}
