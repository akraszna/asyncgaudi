// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Athena include(s).
#include "AthContainersInterfaces/IAuxTypeVector.h"
#include "AthContainers/tools/AuxTypeVector.h"

// System include(s).
#include <memory>

/// Function creating an SG::AuxTypeVector "reference" object
template< typename T >
std::unique_ptr< SG::IAuxTypeVector > makeRefVector( std::size_t size,
                                                     std::size_t capacity ) {


   return std::make_unique< SG::AuxTypeVector< T > >( size, capacity );
}

// Instantiate the function for a few template types
#define INST( TYPE )                                                          \
   template std::unique_ptr< SG::IAuxTypeVector >                             \
            makeRefVector< TYPE >( std::size_t, std::size_t )

INST( int );
INST( long );

#undef INST
