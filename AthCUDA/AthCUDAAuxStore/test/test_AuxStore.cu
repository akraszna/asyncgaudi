// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AthCUDAAuxStore/AuxStore.cuh"
#include "ContainerHelpers.h"

// System include(s).
#undef NDEBUG
#include <cassert>
#include <cmath>
#include <vector>

int main() {

   // Instantiate the container to be tested.
   AthCUDA::AuxStore auxCuda;

   // Set up the variables that we'll add in it.
   static const int CONTAINER_SIZE = 10;
   std::vector< float > var1;
   std::vector< int > var2;
   std::vector< std::vector< int > > var3;
   for( int i = 0; i < CONTAINER_SIZE; ++i ) {
      var1.push_back( 1.5f * i );
      var2.push_back( 3 * i );
      var3.push_back( { 1, 2 } );
   }

   // Put these variables on the container.
   const SG::auxid_t id1 = fillVariable( auxCuda, "var1", var1 );
   const SG::auxid_t id2 = fillVariable( auxCuda, "var2", var2 );
   const SG::auxid_t id3 = fillVariable( auxCuda, "var3", var3 );

   // Instantiate another auxiliary container from the payload of the first
   // one. Just like we'll do in device/GPU code.
   auto variables = auxCuda.attachTo( nullptr );
   AthCUDA::AuxStore auxCopy( auxCuda.size(), variables.first,
                              variables.second );

   // Check the new container using the xAOD::AuxCUDAContainer interface.
   assert( auxCopy.arraySize() == CONTAINER_SIZE );
   for( std::size_t i = 0; i < auxCopy.arraySize(); ++i ) {
      assert( std::abs( auxCopy.array< float >( id1 )[ i ] -
                        auxCuda.array< float >( id1 )[ i ] ) < 0.001 );
      assert( auxCopy.array< int >( id2 )[ i ] ==
              auxCuda.array< int >( id2 )[ i ] );
   }

   // Check that the non-trivial variable is not available.
   assert( auxCopy.array< std::vector< int > >( id3 ) == nullptr );

   // Return gracefully.
   return 0;
}
