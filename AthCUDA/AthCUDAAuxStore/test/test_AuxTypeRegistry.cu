// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AthCUDAAuxStore/tools/AuxTypeRegistry.h"
#include "AthCUDAAuxStore/tools/IAuxTypeVector.cuh"

// System include(s).
#undef NDEBUG
#include <cassert>
#include <memory>
#include <string>
#include <vector>

/// Function providing / generating an auxiliary ID
template< typename T >
SG::auxid_t getAuxID( const std::string& name );

int main() {

   // Access the singleton.
   AthCUDA::AuxTypeRegistry& reg = AthCUDA::AuxTypeRegistry::instance();

   // Test that vectors for primitive types can be made.
   std::unique_ptr< AthCUDA::IAuxTypeVector > vec1(
      reg.makeVector( getAuxID< int >( "var1" ), 10, 20 ) );
   assert( vec1.get() != nullptr );
   assert( vec1->size() == 10 );

   std::unique_ptr< AthCUDA::IAuxTypeVector > vec2(
      reg.makeVector( getAuxID< float >( "var2" ), 15, 20 ) );
   assert( vec2.get() != nullptr );
   assert( vec2->size() == 15 );

   // Check that a complex type can not be created like this.
   std::unique_ptr< AthCUDA::IAuxTypeVector > vec3(
      reg.makeVector( getAuxID< std::vector< int > >( "var3" ), 10, 20 ) );
   assert( vec3.get() == nullptr );

   // Return gracefully.
   return 0;
}
