// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AthCUDAAuxStore/tools/AuxTypeVectorFactory.h"
#include "AthCUDAAuxStore/tools/IAuxTypeVector.cuh"

// System include(s).
#undef NDEBUG
#include <cassert>
#include <memory>

int main() {

   // Smart pointer to receive vectors into.
   typedef std::unique_ptr< AthCUDA::IAuxTypeVector > vec_t;

   // Try to instantiate a few simple vectors, and do some very simple tests
   // with them.
   vec_t vec1( AthCUDA::AuxTypeVectorFactory< int >().create( 20, 50 ) );
   assert( vec1->size() == 20 );
   vec_t vec2( AthCUDA::AuxTypeVectorFactory< float >().create( 30, 50 ) );
   assert( vec2->size() == 30 );

   // Return gracefully.
   return 0;
}
