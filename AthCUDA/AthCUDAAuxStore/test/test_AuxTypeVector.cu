// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AthCUDAAuxStore/tools/AuxTypeVector.cuh"

// Athena include(s).
#include "AthContainersInterfaces/IAuxTypeVector.h"

// System include(s).
#undef NDEBUG
#include <cassert>
#include <iostream>
#include <memory>
#include <vector>

/// Function creating an SG::AuxTypeVector "reference" object
template< typename T >
std::unique_ptr< SG::IAuxTypeVector > makeRefVector( std::size_t size,
                                                     std::size_t capacity );

/// Function comparing the payload of a reference vector vs. a test one
template< typename T >
bool isIdentical( SG::IAuxTypeVector& refVec,
                  AthCUDA::AuxTypeVector< T >& testVec ) {

   // Test that the two vectors have the same size.
   if( refVec.size() != testVec.size() ) {
      std::cerr << "refVec.size() = " << refVec.size()
                << ", testVec.size() = " << testVec.size() << std::endl;
      return false;
   }
   // Test that the two vectors have the same payload. If not, print all the
   // differences before returning with a failure. (For better debugging...)
   bool result = true;
   const std::size_t size = refVec.size();
   for( std::size_t i = 0; i < size; ++i ) {
      if( *( static_cast< T* >( refVec.toPtr() ) + i ) !=
          *( static_cast< T* >( testVec.toPtr() ) + i ) ) {
         std::cerr << "refVec[ " << i << " ] = "
                   << *( static_cast< T* >( refVec.toPtr() ) + i )
                   << ", testVec[ " << i << " ] = "
                   << *( static_cast< T* >( testVec.toPtr() ) + i )
                   << std::endl;
         result = false;
      }
   }
   return result;
}

template< typename T >
void test1() {

   // Fill SG::AuxTypeVector and AthCUDA::AuxTypeVector objects with the same
   // starting payload.
   std::unique_ptr< SG::IAuxTypeVector > refVec = makeRefVector< T >( 10, 10 );
   AthCUDA::AuxTypeVector< T > testVec( 10, 10 );
   for( int i = 0; i < 10; ++i ) {
      *( static_cast< T* >( refVec->toPtr() ) + i ) = i;
      *( static_cast< T* >( testVec.toPtr() ) + i ) = i;
   }
   assert( isIdentical( *refVec, testVec ) );

   // Test the resizing of the vectors.
   assert( refVec->resize( 50 ) == false );
   assert( testVec.resize( 50 ) == false );
   assert( isIdentical( *refVec, testVec ) );
   refVec->resize( 15 );
   testVec.resize( 15 );
   assert( isIdentical( *refVec, testVec ) );

   // Test shifting the payload of the vectors.
   refVec->shift( 5, 5 );
   testVec.shift( 5, 5 );
   assert( isIdentical( *refVec, testVec ) );
   refVec->shift( 10, -5 );
   testVec.shift( 10, -5 );
   assert( isIdentical( *refVec, testVec ) );

   // Prepare a small vector whose content can be inserted into the vectors.
   std::vector< T > insVec;
   for( int i = 0; i < 5; ++i ) {
      insVec.push_back( i );
   }

   // Test inserting some content into the vectors.
   refVec->insertMove( 7, &( insVec.front() ), &( insVec.back() ) );
   testVec.insertMove( 7, &( insVec.front() ), &( insVec.back() ) );
   assert( isIdentical( *refVec, testVec ) );
   refVec->insertMove( 10, &( insVec.front() ), &( insVec.back() ) );
   testVec.insertMove( 10, &( insVec.front() ), &( insVec.back() ) );
   assert( isIdentical( *refVec, testVec ) );

   // State that the test was successful.
   std::cout << "test1 successful" << std::endl;
   return;
}

int main() {

   // Run the test for a few primitive types.
   test1< int >();
   test1< long >();

   // Return gracefully.
   return 0;
}
