// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AthCUDAAuxStore/tools/AuxTypeRegistry.h"
#include "AthCUDAAuxStore/tools/AuxTypeVectorFactory.h"

// Athena include(s).
#include "AthContainers/AuxTypeRegistry.h"

namespace AthCUDA {

   AuxTypeRegistry& AuxTypeRegistry::instance() {

      static AuxTypeRegistry reg;
      return reg;
   }

   IAuxTypeVector* AuxTypeRegistry::makeVector( SG::auxid_t auxid,
                                                std::size_t size,
                                                std::size_t capacity ) {

      // Make sure that the vector is large enough.
      if( m_factoryVec.size() <= auxid ) {
         m_factoryVec.grow_to_at_least( auxid + 1, nullptr );
      }
      if( m_factoryStateVec.size() <= auxid ) {
         m_factoryStateVec.grow_to_at_least( auxid + 1, FactoryUnknown );
      }

      // Find an appropriate factory for this auxiliary type.
      if( ( ! m_factoryVec[ auxid ] ) &&
          ( m_factoryStateVec[ auxid ] == FactoryUnknown ) ) {

         // Ask @c SG::AuxTypeRegistry for the type of this variable.
         static SG::AuxTypeRegistry& reg = SG::AuxTypeRegistry::instance();
         const std::type_info* ti = reg.getType( auxid );
         if( ti ) {
            auto factoryItr = m_factoryMap.find( ti->name() );
            if( factoryItr != m_factoryMap.end() ) {
               m_factoryVec[ auxid ] = factoryItr->second.get();
               m_factoryStateVec[ auxid ] = FactoryAvailable;
            } else {
               m_factoryStateVec[ auxid ] = FactoryMissing;
            }
         } else {
            m_factoryStateVec[ auxid ] = FactoryMissing;
         }
      }

      // Decide what we can do.
      IAuxTypeVectorFactory* factory = m_factoryVec[ auxid ];
      if( factory ) {
         return factory->create( size, capacity );
      } else {
         return nullptr;
      }
   }

   std::size_t AuxTypeRegistry::getEltSize( SG::auxid_t auxid ) const {

      // Simply forward this call to @c SG::AuxTypeRegistry
      return SG::AuxTypeRegistry::instance().getEltSize( auxid );
   }

   AuxTypeRegistry::AuxTypeRegistry()
   : m_factoryMap(), m_factoryVec() {

      // Reserve large enough vectors up front, to make it unlikely to have
      // to increase their size later on.
      m_factoryVec.reserve( SG::auxid_set_size_hint );
      m_factoryStateVec.reserve( SG::auxid_set_size_hint );

/// Helper macro used for setting up @c m_factoryMap
#define ADD_FACTORY( TYPE )                                              \
   m_factoryMap[ typeid( TYPE ).name() ] =                               \
      std::make_unique< AuxTypeVectorFactory< TYPE > >()

      // Set up factories for all primitive types.
      ADD_FACTORY( char );
      ADD_FACTORY( unsigned char );
      ADD_FACTORY( short );
      ADD_FACTORY( unsigned short );
      ADD_FACTORY( int );
      ADD_FACTORY( unsigned int );
      ADD_FACTORY( long );
      ADD_FACTORY( unsigned long );
      ADD_FACTORY( long long );
      ADD_FACTORY( unsigned long long );
      ADD_FACTORY( float );
      ADD_FACTORY( double );

// Clean up.
#undef ADD_FACTORY
   }

} // namespace AthCUDA
