// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AthCUDAAuxStore/tools/AuxDeviceVector.cuh"

// System include(s).
#include <stdexcept>

namespace AthCUDA {

   template< typename T >
   ATHCUDA_HOST_AND_DEVICE
   AuxDeviceVector< T >::AuxDeviceVector( void* ptr, std::size_t size )
   : m_ptr( reinterpret_cast< T* >( ptr ) ), m_size( size ) {

   }

   template< typename T >
   ATHCUDA_HOST_AND_DEVICE
   void* AuxDeviceVector< T >::toPtr() {

      return m_ptr;
   }

   template< typename T >
   ATHCUDA_HOST_AND_DEVICE
   size_t AuxDeviceVector< T >::size() const {

      return m_size;
   }

   template< typename T >
   ATHCUDA_HOST
   bool AuxDeviceVector< T >::resize( size_t ) {

      // This function is not supposed to be called.
      throw std::domain_error( "Function not available" );
   }

   template< typename T >
   ATHCUDA_HOST
   void AuxDeviceVector< T >::reserve( size_t ) {

      // This function is not supposed to be called.
      throw std::domain_error( "Function not available" );
   }

   template< typename T >
   ATHCUDA_HOST
   void AuxDeviceVector< T >::shift( size_t, ptrdiff_t ) {

      // This function is not supposed to be called.
      throw std::domain_error( "Function not available" );
   }

   template< typename T >
   ATHCUDA_HOST
   bool AuxDeviceVector< T >::insertMove( size_t, void*, void* ) {

      // This function is not supposed to be called.
      throw std::domain_error( "Function not available" );
   }

   template< typename T >
   ATHCUDA_HOST
   void* AuxDeviceVector< T >::attachTo( cudaStream_t ) {

      // This function is not supposed to be called.
      throw std::domain_error( "Function not available" );
   }

   template< typename T >
   ATHCUDA_HOST
   void AuxDeviceVector< T >::retrieveFrom( cudaStream_t ) {

      // This function is not supposed to be called.
      throw std::domain_error( "Function not available" );
   }

} // namespace AthCUDA

/// Macro helping to instantiate the class for all POD types.
#define INST_AUXVEC( TYPE )                           \
   template class AthCUDA::AuxDeviceVector< TYPE >

// Instantiate the type for all POD types.
INST_AUXVEC( char );
INST_AUXVEC( unsigned char );
INST_AUXVEC( short );
INST_AUXVEC( unsigned short );
INST_AUXVEC( int );
INST_AUXVEC( unsigned int );
INST_AUXVEC( long );
INST_AUXVEC( unsigned long );
INST_AUXVEC( long long );
INST_AUXVEC( unsigned long long );
INST_AUXVEC( float );
INST_AUXVEC( double );

// Clean up.
#undef INST_AUXVEC
