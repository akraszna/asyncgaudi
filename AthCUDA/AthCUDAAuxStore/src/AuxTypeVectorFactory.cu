// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AthCUDAAuxStore/tools/AuxTypeVectorFactory.h"
#include "AthCUDAAuxStore/tools/AuxTypeVector.cuh"

// System include(s).
#include <vector>

namespace AthCUDA {

   template< typename T >
   IAuxTypeVector*
   AuxTypeVectorFactory< T >::create( std::size_t size,
                                         std::size_t capacity ) {

      return new AuxTypeVector< T >( size, capacity );
   }

} // namespace AthCUDA

/// Macro helping to instantiate the class for all POD types.
#define INST_FACT( TYPE )                                  \
   template class AthCUDA::AuxTypeVectorFactory< TYPE >

// Instantiate the type for all POD types.
INST_FACT( char );
INST_FACT( unsigned char );
INST_FACT( short );
INST_FACT( unsigned short );
INST_FACT( int );
INST_FACT( unsigned int );
INST_FACT( long );
INST_FACT( unsigned long );
INST_FACT( long long );
INST_FACT( unsigned long long );
INST_FACT( float );
INST_FACT( double );

// Clean up.
#undef INST_FACT
