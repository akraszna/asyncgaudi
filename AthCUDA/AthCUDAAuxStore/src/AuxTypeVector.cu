// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AthCUDAAuxStore/tools/AuxTypeVector.cuh"

// Project include(s).
#include "AthCUDACore/Macros.cuh"

// System include(s).
#include <cassert>
#include <cmath>
#include <cstring>
#include <vector>

namespace {

   /// Helper function for making a non-pinned array in the host's memory
   template< typename T >
   std::unique_ptr< T > make_host_array( std::size_t size ) {
      return std::unique_ptr< T >( new T[ size ] );
   }

} // private namespace

namespace AthCUDA {

   template< typename T >
   ATHCUDA_HOST
   AuxTypeVector< T >::AuxTypeVector( std::size_t size,
                                      std::size_t capacity )
   : m_host( ::make_host_array< T >( capacity ) ), m_hostUpToDate( true ),
     m_cudaHost(), m_cudaDevice(),
     m_size( size ), m_capacity( capacity ) {

   }

   template< typename T >
   ATHCUDA_HOST_AND_DEVICE
   void* AuxTypeVector< T >::toPtr() {

#ifdef __CUDA_ARCH__
      return nullptr;
#else
      if( ! m_hostUpToDate ) {
         std::memcpy( m_host.get(), m_cudaHost.get(), m_size * sizeof( T ) );
         m_hostUpToDate = true;
      }
      return m_host.get();
#endif // __CUDA_ARCH__
   }

   template< typename T >
   ATHCUDA_HOST_AND_DEVICE
   size_t AuxTypeVector< T >::size() const {

      return m_size;
   }

   template< typename T >
   ATHCUDA_HOST
   bool AuxTypeVector< T >::resize( size_t sz ) {

      // Check if anything needs to be done.
      if( m_size == sz ) {
         // Iterators remained valid.
         return true;
      }

      // Check if we need to allocate a larger array or not.
      if( sz <= m_capacity ) {
         if( sz > m_size ) {
            std::memset( m_host.get() + m_size, 0,
                         ( sz - m_size ) * sizeof( T ) );
         }
         m_size = sz;
         // Iterators remained valid.
         return true;
      }

      // Decide what the size of the extended array should be.
      std::size_t newSize = m_size;
      do {
         newSize *= 2;
      } while( newSize < sz );

      // Allocate the new array.
      auto newVariable = ::make_host_array< T >( newSize );

      // Fill it with the old array's contents.
      void* newPtr = newVariable.get();
      const void* oldPtr = m_host.get();
      if( newPtr && oldPtr ) {
         std::memcpy( newPtr, oldPtr, m_size * sizeof( T ) );
      }
      std::memset( newVariable.get() + m_size, 0,
                   ( sz - m_size ) * sizeof( T ) );

      // And now replace the old array with the new one.
      m_host.swap( newVariable );
      m_size = sz;
      m_capacity = newSize;

      // Iterators are now invalid.
      return false;
   }

   template< typename T >
   ATHCUDA_HOST
   void AuxTypeVector< T >::reserve( size_t sz ) {

      // Check if anything needs to be done.
      if( m_capacity >= sz ) {
         return;
      }

      // Allocate the new array.
      auto newVariable = ::make_host_array< T >( sz );

      // Fill it with the old array's contents.
      void* newPtr = newVariable.get();
      const void* oldPtr = m_host.get();
      if( newPtr && oldPtr ) {
         std::memcpy( newPtr, oldPtr, m_size * sizeof( T ) );
      }
      std::memset( newVariable.get() + m_size, 0,
                   ( sz - m_size ) * sizeof( T ) );

      // And now replace the old array with the new one.
      m_host.swap( newVariable );
      m_capacity = sz;
      return;
   }

   template< typename T >
   ATHCUDA_HOST
   void AuxTypeVector< T >::shift( size_t pos, ptrdiff_t offs ) {

      // A security check.
      assert( pos < m_size );

      // Create a temporary array in host memory for the operation.
      std::vector< T > tempArray( m_size - pos );

      // Copy the data, that we will be relocating, to this area first.
      std::memcpy( tempArray.data(), m_host.get() + pos,
                   tempArray.size() * sizeof( T ) );

      // Handle positive and negative shifts differently.
      if( offs < 0 ) {

         // Make sure that the position and offset values make sense.
         if( std::abs( offs ) > static_cast< ptrdiff_t >( pos ) ) {
            offs = -pos;
         }

         // Copy the temporary data into the right place.
         std::memcpy( m_host.get() + pos + offs,
                      tempArray.data(),
                      tempArray.size() * sizeof( T ) );

         // Reduce the size of the vector.
         this->resize( m_size + offs );
         
         // Zero out the last bits.
         std::memset( m_host.get() + m_size, 0,
                      std::abs( offs ) * sizeof( T ) );

      } else if( offs > 0 ) {

         // Increase the size of the vector.
         this->resize( m_size + offs );

         // Copy the temporary data into the right place.
         std::memcpy( m_host.get() + pos + offs,
                      tempArray.data(),
                      tempArray.size() * sizeof( T ) );

         // Zero out the middle bits.
         std::memset( m_host.get() + pos, 0, offs * sizeof( T ) );
      }

      return;
   }

   template< typename T >
   ATHCUDA_HOST
   bool AuxTypeVector< T >::insertMove( size_t pos, void* beg, void* end ) {

      // A security check.
      assert( m_size > pos );

      // The size of the inserted range.
      const std::size_t insSize = ( static_cast< T* >( end ) -
                                    static_cast< T* >( beg ) );

      // If nothing is getting inserted stop right away.
      if( insSize == 0 ) {
         return true;
      }

      // Increase the size of the vector.
      const bool result = resize( m_size + insSize );

      // Move the existing data to the right place.
      assert( ( static_cast< int >( m_size ) - static_cast< int >( pos ) -
                static_cast< int >( insSize ) ) >= 0 );
      std::vector< T > tempArray( m_size - pos );
      std::memcpy( tempArray.data(), m_host.get() + pos,
                   tempArray.size() * sizeof( T ) );
      std::memcpy( m_host.get() + pos + insSize, tempArray.data(),
                   tempArray.size() * sizeof( T ) );

      // Insert the received memory content.
      std::memcpy( m_host.get() + pos, beg, insSize * sizeof( T ) );

      // Tell the caller whether iterators got invalidated.
      return result;
   }

   template< typename T >
   ATHCUDA_HOST
   void* AuxTypeVector< T >::attachTo( cudaStream_t stream ) {

      if( stream != nullptr ) {
         m_cudaHost   = AthCUDA::make_host_array< T >( m_size );
         m_cudaDevice = AthCUDA::make_device_array< T >( m_size );
         std::memcpy( m_cudaHost.get(), m_host.get(), m_size * sizeof( T ) );
         CUDA_EXP_CHECK( cudaMemcpyAsync( m_cudaDevice.get(),
                                          m_cudaHost.get(),
                                          m_size * sizeof( T ),
                                          cudaMemcpyHostToDevice,
                                          stream ) );
         return m_cudaDevice.get();
      } else {
         return m_host.get();
      }
   }

   template< typename T >
   ATHCUDA_HOST
   void AuxTypeVector< T >::retrieveFrom( cudaStream_t stream ) {

      if( stream != nullptr ) {
         CUDA_EXP_CHECK( cudaMemcpyAsync( m_cudaHost.get(),
                                          m_cudaDevice.get(),
                                          m_size * sizeof( T ),
                                          cudaMemcpyDeviceToHost,
                                          stream ) );
         m_hostUpToDate = false;
      }
      return;
   }

} // namespace AthCUDA

/// Macro helping to instantiate the class for all POD types.
#define INST_AUXVEC( TYPE )                           \
   template class AthCUDA::AuxTypeVector< TYPE >

// Instantiate the type for all POD types.
INST_AUXVEC( char );
INST_AUXVEC( unsigned char );
INST_AUXVEC( short );
INST_AUXVEC( unsigned short );
INST_AUXVEC( int );
INST_AUXVEC( unsigned int );
INST_AUXVEC( long );
INST_AUXVEC( unsigned long );
INST_AUXVEC( long long );
INST_AUXVEC( unsigned long long );
INST_AUXVEC( float );
INST_AUXVEC( double );

// Clean up.
#undef INST_AUXVEC
