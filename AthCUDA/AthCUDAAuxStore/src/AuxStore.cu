// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AthCUDAAuxStore/AuxStore.cuh"
#include "AthCUDAAuxStore/tools/AuxTypeRegistry.h"
#include "AthCUDAAuxStore/tools/AuxDeviceVector.cuh"

// Athena include(s).
#include "AthContainers/AuxStoreInternal.h"

// System include(s).
#include <cassert>
#include <cstring>

namespace AthCUDA {

   ATHCUDA_HOST
   AuxStore::AuxStore()
   : m_size( 0 ), m_vecs(), m_deviceBuffer( nullptr ), m_hostBuffer( nullptr ),
     m_auxids( new SG::auxid_set_t() ),
     m_store( new SG::AuxStoreInternal() ) {

   }

   ATHCUDA_HOST_AND_DEVICE
   AuxStore::AuxStore( std::size_t size, std::size_t nVars, void** vars )
   : m_size( size ), m_vecs( nVars ), m_deviceBuffer( nullptr ),
     m_hostBuffer( nullptr ),
     m_auxids( nullptr ), m_store( nullptr ) {

      // Hold on to each variable with the same (char) type for now. Later on
      // this code should become smarter.
      for( std::size_t i = 0; i < nVars; ++i ) {
         if( ! vars[ i ] ) {
            m_vecs[ i ] = nullptr;
         } else {
            m_vecs[ i ] = new AuxDeviceVector< char >( vars[ i ], size );
         }
      }
   }

   ATHCUDA_HOST_AND_DEVICE
   AuxStore::~AuxStore() {

      const std::size_t s = m_vecs.size();
      for( std::size_t i = 0; i < s; ++i ) {
         if( m_vecs[ i ] ) {
            delete m_vecs[ i ];
         }
      }
#ifndef __CUDA_ARCH__
      if( m_deviceBuffer ) {
         delete m_deviceBuffer;
      }
      if( m_hostBuffer ) {
         delete m_hostBuffer;
      }
      if( m_auxids ) {
         delete m_auxids;
      }
      if( m_store ) {
         delete m_store;
      }
#endif // not __CUDA_ARCH__
   }

   /// This function behaves exactly the same as @c size(). The only reason
   /// that it exists is that @c SG::IConstAuxStore defines @c size() as a
   /// host-only function.
   ///
   /// @return The size of the arrays held by the store
   ///
   ATHCUDA_HOST_AND_DEVICE
   std::size_t AuxStore::arraySize() const {

      return m_size;
   }

   /// This function can be used to transfer variables to a CUDA kernel call.
   /// The return values of the function should be possible to use as kernel
   /// function arguments.
   ///
   /// @param stream The stream to perform the memory copy with
   /// @return Variables to pass to a CUDA kernel
   ///
   ATHCUDA_HOST
   AuxStore::ExposedVars_t
   AuxStore::attachTo( cudaStream_t stream ) {

      // Protect the function from parallel execution.
      guard_t guard( m_mutex );

      // If there are no variables that can be exposed to the GPU, return
      // quickly.
      if( m_vecs.size() == 0 ) {
         return ExposedVars_t( 0, nullptr );
      }

      // Prepare the host array with the content that we need.
      const std::size_t s = m_vecs.size();
      if( m_hostBuffer == nullptr ) {
         m_hostBuffer = new host_array< void* >();
      }
      *m_hostBuffer = make_host_array< void* >( s );
      for( std::size_t i = 0; i < s; ++i ) {
         if( m_vecs[ i ] ) {
            m_hostBuffer->get()[ i ] = m_vecs[ i ]->attachTo( stream );
         } else {
            m_hostBuffer->get()[ i ] = nullptr;
         }
      }

      // Do different things based on whether we actually send the data to
      // a GPU or not.
      if( stream == nullptr ) {
         // Just use the host buffer in the return value.
         return ExposedVars_t( s, m_hostBuffer->get() );
      } else {

         // Create the on-device buffer.
         if( m_deviceBuffer == nullptr ) {
            m_deviceBuffer = new device_array< void* >();
         }
         *m_deviceBuffer = make_device_array< void* >( s );
         // Schedule the data copy to it.
         CUDA_EXP_CHECK( cudaMemcpyAsync( m_deviceBuffer->get(),
                                          m_hostBuffer->get(),
                                          s * sizeof( void* ),
                                          cudaMemcpyHostToDevice,
                                          stream ) );
         // Return the device buffer.
         return ExposedVars_t( s, m_deviceBuffer->get() );
      }
   }

   /// This function needs to be called after a kernel has finished its
   /// execution, to ensure that all variables are copied back from the device
   /// into the host's memory.
   ///
   /// @param stream The stream to perform the memory copy with
   ///
   ATHCUDA_HOST
   void AuxStore::retrieveFrom( cudaStream_t stream ) {

      // Protect the function from parallel execution.
      guard_t guard( m_mutex );

      // Ask all vectors to pull their data back.
      for( std::size_t i = 0; i < m_vecs.size(); ++i ) {
         if( m_vecs[ i ] ) {
            m_vecs[ i ]->retrieveFrom( stream );
         }
      }
      return;
   }

   const void* AuxStore::getData( SG::auxid_t auxid ) const {

      // Protect the function from parallel execution.
      guard_t guard( m_mutex );

      // Check whether we have this variable.
      if( ( auxid >= m_vecs.size() ) || ( m_vecs[ auxid ] == nullptr ) ) {
         // If not, leave it up to the general store to either find it, or
         // complain.
         return m_store->getData( auxid );
      }
      // Return it if we do.
      return m_vecs[ auxid ]->toPtr();
   }

   /// In this simple example we don't worry about decorations.
   void* AuxStore::getDecoration( SG::auxid_t, size_t, size_t ) {

      return nullptr;
   }

   const SG::auxid_set_t& AuxStore::getAuxIDs() const {

      assert( m_auxids != nullptr );
      return *m_auxids;
   }

   bool AuxStore::isDecoration( SG::auxid_t ) const {

      return false;
   }

   void AuxStore::lock() {

   }

   bool AuxStore::clearDecorations() {

      return false;
   }

   size_t AuxStore::size() const {

      return m_size;
   }

   void AuxStore::lockDecoration( SG::auxid_t ) {

   }

   void* AuxStore::getData( SG::auxid_t auxid, size_t size, size_t capacity ) {

      // Check some assumptions.
      assert( m_store != nullptr );

      // Try to allocate the variable in CUDA Unified Memory.
      void* ptr = getDataCUDA( auxid, size, capacity );
      if( ptr ) {
         return ptr;
      }

      // If we couldn't, then leave it up to the helper store to take hold of
      // this variable.
      ptr = m_store->getData( auxid, size, capacity );
      if( ptr ) {
         m_auxids->insert( auxid );
         return ptr;
      }

      // This should not really happen, but apparently something went very
      // wrong...
      return nullptr;
   }

   const SG::auxid_set_t& AuxStore::getWritableAuxIDs() const {

      assert( m_auxids != nullptr );
      return *m_auxids;
   }

   bool AuxStore::resize( size_t size ) {

      // Protect the function from parallel execution.
      guard_t guard( m_mutex );

      // Check some assumptions.
      assert( m_store != nullptr );

      // Helper variable.
      bool nomove = true;
      // Resize all the vectors.
      const std::size_t v_size = m_vecs.size();
      for( std::size_t i = 0; i < v_size; ++i ) {
         IAuxTypeVector* v = m_vecs[ i ];
         if( ! v ) {
            continue;
         }
         if( ! v->resize( size ) ) {
            nomove = false;
         }
      }
      // Resize the helper store.
      if( ! m_store->resize( size ) ) {
         nomove = false;
      }
      // Remember the new size.
      m_size = size;
      // Return whether the iterators *not* have been invalidated.
      return nomove;
   }

   void AuxStore::reserve( size_t capacity ) {

      // Protect the function from parallel execution.
      guard_t guard( m_mutex );

      // Check some assumptions.
      assert( m_store != nullptr );

      // Reserve the memory in all vectors.
      const std::size_t v_size = m_vecs.size();
      for( std::size_t i = 0; i < v_size; ++i ) {
         IAuxTypeVector* v = m_vecs[ i ];
         if( ! v ) {
            continue;
         }
         v->reserve( capacity );
      }
      // Reserve the memory in the helper store.
      m_store->reserve( capacity );
      return;
   }

   void AuxStore::shift( size_t pos, ptrdiff_t offs ) {

      // Protect the function from parallel execution.
      guard_t guard( m_mutex );

      // Check some assumptions.
      assert( m_store != nullptr );

      // Perform the shift on all vectors.
      const std::size_t v_size = m_vecs.size();
      for( std::size_t i = 0; i < v_size; ++i ) {
         IAuxTypeVector* v = m_vecs[ i ];
         if( ! v ) {
            continue;
         }
         v->shift( pos, offs );
      }
      // Perform the shift on the helper store.
      m_store->shift( pos, offs );
      return;
   }

   bool AuxStore::insertMove( size_t pos, SG::IAuxStore& other,
                              const SG::auxid_set_t& ignore ) {

      // Protect the function from parallel execution.
      guard_t guard( m_mutex );

      // Check some assumptions.
      assert( m_auxids != nullptr );
      assert( m_store != nullptr );

      // Helper variable.
      bool nomove = true;

      // Access the registry to get element size multipliers.
      const AuxTypeRegistry& r = AuxTypeRegistry::instance();

      // Check if anything needs to be done.
      const std::size_t other_size = other.size();
      if( other_size == 0 ) {
         return true;
      }

      // Process the variables that the container already has.
      for( SG::auxid_t id : *m_auxids ) {
         // Try to access the variable in CUDA Unified Memory.
         IAuxTypeVector* v_dst = nullptr;
         if( id < m_vecs.size() ) {
            v_dst = m_vecs[ id ];
         }
         if( ! v_dst ) {
            continue;
         }
         // If we succeeded, copy the contents of the other container.
         if( other.getData( id ) ) {
            void* src_ptr = other.getData( id, other_size, other_size );
            assert( src_ptr != nullptr );
            if( ! v_dst->insertMove( pos, src_ptr,
                                     reinterpret_cast< char* >( src_ptr ) +
                                     other_size * r.getEltSize( id ) ) ) {
               nomove = false;
            }
         } else {
            // If the other container doesn't have this variable, then we just
            // perform a shift on our variable.
            const void* orig = v_dst->toPtr();
            v_dst->shift( pos, other_size );
            if( orig != v_dst->toPtr() ) {
               nomove = false;
            }
         }
      }

      // Add any new variables not present in the original container.
      for( SG::auxid_t id : other.getAuxIDs() ) {
         // Check whether this variable should be handled.
         if( m_auxids->test( id ) || ignore.test( id ) ) {
            continue;
         }
         if( other.getData( id ) ) {
            // Get hold of the variable in the other container.
            void* src_ptr = other.getData( id, other_size, other_size );
            assert( src_ptr != nullptr );
            std::size_t sz = m_size;
            if( m_size < other_size ) {
               sz = other_size + pos;
            }
            // Try to get a variable in CUDA Unified Memory.
            if( ! getDataCUDA( id, sz, sz ) ) {
               continue;
            }
            m_vecs[ id ]->resize( sz - other_size );
            m_vecs[ id ]->insertMove( pos, src_ptr,
                                      reinterpret_cast< char* >( src_ptr ) +
                                      other_size * r.getEltSize( id ) );
            nomove = false;
         }
      }

      // Insert into the helper store.
      if( ! m_store->insertMove( pos, other, ignore ) ) {
         nomove = false;
      }
      // Return whether the iterators *not* have been invalidated.
      return nomove;
   }

   void* AuxStore::getDataCUDA( SG::auxid_t auxid, size_t size,
                                size_t capacity ) {

      // Protect the function from parallel execution.
      guard_t guard( m_mutex );

      // Check if we already have this variable.
      if( ( m_vecs.size() > auxid ) && ( m_vecs[ auxid ] != nullptr ) ) {
         return m_vecs[ auxid ]->toPtr();
      }

      // Check if we can instantiate such a variable in CUDA Unified Memory.
      IAuxTypeVector* v =
         AuxTypeRegistry::instance().makeVector( auxid, size, capacity );
      if( ! v ) {
         return nullptr;
      }

      // Get hold of the variable.
      if( m_vecs.size() <= auxid ) {
         m_vecs.resize( auxid + 1 );
      }
      m_vecs[ auxid ] = v;
      m_auxids->insert( auxid );
      return v->toPtr();
   }

} // namespace AthCUDA
