// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AthCUDAAuxStore/MakeAuxStore.h"
#include "AthCUDAAuxStore/AuxStore.cuh"

namespace AthCUDA {

   std::unique_ptr< SG::IAuxStore > makeAuxStore() {

      return std::unique_ptr< SG::IAuxStore >( new AuxStore() );
   }

} // namespace AthCUDA
