// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCUDAAUXSTORE_TOOLS_AUXTYPEREGISTRY_H
#define ATHCUDAAUXSTORE_TOOLS_AUXTYPEREGISTRY_H

// Athena include(s).
#include "AthContainersInterfaces/AuxTypes.h"
#include "AthContainers/tools/threading.h"

// TBB include(s).
#include <tbb/concurrent_unordered_map.h>
#include <tbb/concurrent_vector.h>

// System include(s).
#include <memory>
#include <typeinfo>

namespace AthCUDA {

   // Forward declaration(s).
   class IAuxTypeVector;
   class IAuxTypeVectorFactory;

   /// Class used for managing CUDA auxiliary vector objects
   ///
   /// Just like @c AthCUDA::AuxTypeVector, this class is modeled after a
   /// class (@c SG::AuxTypeRegistry) sitting in the core ATLAS EDM code.
   /// It takes care of managing @c AthCUDA::IAuxTypeVector type objects
   /// in type independent code.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class AuxTypeRegistry {

   public:
      /// Function implementing the singleton pattern for the class
      static AuxTypeRegistry& instance();

      /// Construct a new vector to hold an auxiliary variable
      IAuxTypeVector* makeVector( SG::auxid_t auxid, std::size_t size,
                                  std::size_t capacity );

      /// Get the in-memory size of an element of this type
      std::size_t getEltSize( SG::auxid_t auxid ) const;

   private:
      /// Private constructor, to implement the singleton pattern
      AuxTypeRegistry();

      /// Container of all available vector factories
      tbb::concurrent_unordered_map< std::string,
                       std::unique_ptr< IAuxTypeVectorFactory > > m_factoryMap;

      /// Vector associating vector factories to auxiliary IDs
      tbb::concurrent_vector< IAuxTypeVectorFactory* >
         m_factoryVec;

      /// Helper type for bookkeeping the factories
      enum FactoryState {
         /// A factory was not searched for yet
         FactoryUnknown = 0,
         /// A factory was searched for, but is not available
         FactoryMissing = 1,
         /// A factory is available
         FactoryAvailable = 2
      }; // enum FactoryState
      /// Vector keeping the "state" of the vector factories
      tbb::concurrent_vector< FactoryState > m_factoryStateVec;

   }; // class AuxTypeRegistry

} // namespace AthCUDA

#endif // ATHCUDAAUXSTORE_TOOLS_AUXTYPEREGISTRY_H
