// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCUDAAUXSTORE_TOOLS_IAUXTYPEVECTOR_CUH
#define ATHCUDAAUXSTORE_TOOLS_IAUXTYPEVECTOR_CUH

// Project include(s).
#include "AthCUDACore/Macros.cuh"

// System include(s).
#include <cstddef>

namespace AthCUDA {

   /// A CUDA aware version of @c SG::IAuxTypeVector
   ///
   /// Instead of using @c SG::IAuxTypeVector itself in the device code,
   /// we use this class. Note that only some of the functions are meant
   /// to be used in device code. Most of them should only be used on the
   /// host...
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class IAuxTypeVector {

   public:
      /// Destructor.
      ATHCUDA_HOST_AND_DEVICE
#ifndef __CUDA_ARCH__
      virtual
#endif // __CUDA_ARCH__
      ~IAuxTypeVector() {}

      /// Return a pointer to the start of the vector's data.
      ATHCUDA_HOST_AND_DEVICE
      virtual void* toPtr() = 0;

      /// Return the size of the vector.
      ATHCUDA_HOST_AND_DEVICE
      virtual size_t size() const = 0;

      /// Change the size of the vector.
      ///
      /// @param sz The new vector size.
      /// @return @c true if it is known that iterators have not been
      ///         invalidated, @c false otherwise
      ///
      ATHCUDA_HOST
      virtual bool resize( size_t sz ) = 0;

      /// Change the allocated size of the vector.
      ///
      /// @param sz The new vector allocated size.
      /// @return @c true if it is known that iterators have not been
      ///         invalidated, @c false otherwise
      ///
      ATHCUDA_HOST
      virtual void reserve( size_t sz ) = 0;

      /// Shift the elements of the vector.
      ///
      /// @param pos The starting index for the shift.
      /// @param offs The (signed) amount of the shift.
      ///
      ATHCUDA_HOST
      virtual void shift( size_t pos, ptrdiff_t offs ) = 0;

      /// Insert elements into the vector via move semantics.
      ///
      /// @param pos The starting index of the insertion.
      /// @param beg Start of the range of elements to insert.
      /// @param end End of the range of elements to insert.
      /// @return @c true if it is known that the vector's memory did not
      ///         move, @c false otherwise.
      ///
      ATHCUDA_HOST
      virtual bool insertMove( size_t pos, void* beg, void* end ) = 0;

      /// Attach the memory managed by the vector to a given CUDA stream
      ///
      /// @param stream The CUDA stream to attach the managed memory to
      /// @return The pointer to the array in device memory
      ///
      ATHCUDA_HOST
      virtual void* attachTo( cudaStream_t stream ) = 0;

      /// Retrieve the memory from the device using a CUDA stream
      ///
      /// @param stream The CUDA stream to retrieve the memory with
      ///
      ATHCUDA_HOST
      virtual void retrieveFrom( cudaStream_t stream ) = 0;

   }; // class IAuxTypeVector

} // namespace AthCUDA

#endif // ATHCUDAAUXSTORE_TOOLS_IAUXTYPEVECTOR_CUH
