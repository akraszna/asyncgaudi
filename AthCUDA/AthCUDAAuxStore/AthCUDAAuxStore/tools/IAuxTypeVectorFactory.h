// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCUDAAUXSTORE_TOOLS_IAUXTYPEVECTORFACTORY_H
#define ATHCUDAAUXSTORE_TOOLS_IAUXTYPEVECTORFACTORY_H

// System include(s).
#include <cstddef>

namespace AthCUDA {

   // Forward declaration(s).
   class IAuxTypeVector;

   /// A much simplified version of @c SG::IAuxTypeVectorFactory
   ///
   /// Just like @c SG::IAuxTypeVectorFactory, this interface is used to
   /// conveniently create @c AthCUDA::IAuxTypeVector type objects at
   /// runtime.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class IAuxTypeVectorFactory {

   public:
      /// Virtual destructor, to make vtable happy...
      virtual ~IAuxTypeVectorFactory() = default;

      /// Function creating a new vector (handling) object
      virtual IAuxTypeVector* create( std::size_t size,
                                      std::size_t capacity ) = 0;

   }; // class IAuxTypeVectorFactory

} // namespace AthCUDA

#endif // ATHCUDAAUXSTORE_TOOLS_IAUXTYPEVECTORFACTORY_H
