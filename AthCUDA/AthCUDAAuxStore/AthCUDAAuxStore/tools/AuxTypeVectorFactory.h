// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCUDAAUXSTORE_TOOLS_AUXTYPEVECTORFACTORY_H
#define ATHCUDAAUXSTORE_TOOLS_AUXTYPEVECTORFACTORY_H

// Local include(s).
#include "AthCUDAAuxStore/tools/IAuxTypeVectorFactory.h"

namespace AthCUDA {

   /// Implementation for @c AthCUDA::IAuxTypeVectorFactory
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   template< typename T >
   class AuxTypeVectorFactory : public IAuxTypeVectorFactory {

   public:
      /// @name Interface inherited from @c xAOD::CUDA::IAuxTypeVectorFactory
      /// @{

      /// Function creating a new vector (handling) object
      virtual IAuxTypeVector* create( std::size_t size,
                                      std::size_t capacity ) override;

      /// @}

   }; // class AuxTypeVectorFactory

} // namespace AthCUDA

#endif // ATHCUDAAUXSTORE_TOOLS_AUXTYPEVECTORFACTORY_H
