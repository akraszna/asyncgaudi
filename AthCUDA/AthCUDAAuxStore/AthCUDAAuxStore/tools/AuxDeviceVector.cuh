// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCUDAAUXSTORE_TOOLS_AUXDEVICEVECTOR_CUH
#define ATHCUDAAUXSTORE_TOOLS_AUXDEVICEVECTOR_CUH

// Local include(s).
#include "AthCUDAAuxStore/tools/IAuxTypeVector.cuh"

// Project include(s).
#include "AthCUDACore/Memory.cuh"

// System include(s).
#include <type_traits>

namespace AthCUDA {

   /// Class managing one auxiliary variable in CUDA memory on the device
   ///
   /// Once @c AthCUDA::AuxTypeVector took care of exposing a variable to
   /// the device, this class takes care of presenting the variable to the
   /// user on the device in a convenient way.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   template< typename T >
   class AuxDeviceVector : public IAuxTypeVector {

   public:
      /// Make sure that we're only trying to use this class on simple types
      static_assert( std::is_pod< T >::value == true,
                     "AthCUDA::AuxDeviceVector is only available for "
                     "POD types" );

      /// Constructor with the already allocated variable
      ///
      /// @param ptr Pointer to the variable array in device memory
      /// @param size The size of the array pointed to by @c ptr
      ///
      ATHCUDA_HOST_AND_DEVICE
      AuxDeviceVector( void* ptr, std::size_t size );

      /// @name Interface inherited from @c xAOD::CUDA::IAuxTypeVector
      /// @{

      /// Return a pointer to the start of the vector's data.
      ATHCUDA_HOST_AND_DEVICE
      virtual void* toPtr() override;

      /// Return the size of the vector.
      ATHCUDA_HOST_AND_DEVICE
      virtual size_t size() const override;

      /// Change the size of the vector.
      ///
      /// @param sz The new vector size.
      /// @return @c true if it is known that iterators have not been
      ///         invalidated, @c false otherwise
      ///
      ATHCUDA_HOST
      virtual bool resize( size_t sz ) override;

      /// Change the allocated size of the vector.
      ///
      /// @param sz The new vector allocated size.
      /// @return @c true if it is known that iterators have not been
      ///         invalidated, @c false otherwise
      ///
      ATHCUDA_HOST
      virtual void reserve( size_t sz ) override;

      /// Shift the elements of the vector.
      ///
      /// @param pos The starting index for the shift.
      /// @param offs The (signed) amount of the shift.
      ///
      ATHCUDA_HOST
      virtual void shift( size_t pos, ptrdiff_t offs ) override;

      /// Insert elements into the vector via move semantics.
      ///
      /// @param pos The starting index of the insertion.
      /// @param beg Start of the range of elements to insert.
      /// @param end End of the range of elements to insert.
      /// @return @c true if it is known that the vector's memory did not
      ///         move, @c false otherwise.
      ///
      ATHCUDA_HOST
      virtual bool insertMove( size_t pos, void* beg, void* end ) override;

      /// Attach the memory managed by the vector to a given CUDA stream
      ///
      /// @param stream The CUDA stream to attach the managed memory to
      ///
      ATHCUDA_HOST
      virtual void* attachTo( cudaStream_t stream ) override;

      /// Retrieve the memory from the device using a CUDA stream
      ///
      /// @param stream The CUDA stream to retrieve the memory with
      ///
      ATHCUDA_HOST
      virtual void retrieveFrom( cudaStream_t stream ) override;

      /// @}

   private:
      /// The variable on the device
      T* m_ptr;
      /// The current size of the managed variable array
      std::size_t m_size;

   }; // class AuxDeviceVector

} // namespace AthCUDA

#endif // ATHCUDAAUXSTORE_TOOLS_AUXDEVICEVECTOR_CUH
