// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCUDAAUXSTORE_AUXSTORE_CUH
#define ATHCUDAAUXSTORE_AUXSTORE_CUH

// Local include(s).
#include "AthCUDAAuxStore/tools/IAuxTypeVector.cuh"

// Project include(s).
#include "AthCUDACore/Macros.cuh"
#include "AthCUDACore/Memory.cuh"

// Athena include(s).
#include "AthContainersInterfaces/IAuxStore.h"
#include "AthContainers/tools/threading.h"

// System include(s).
#include <memory>
#include <utility>

namespace AthCUDA {

   /// Container helping with exposing xAOD data to CUDA kernels
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class AuxStore : public SG::IAuxStore {

   public:
      /// Default constructor
      ATHCUDA_HOST
      AuxStore();
      /// Constructor from existing data
      ATHCUDA_HOST_AND_DEVICE
      AuxStore( std::size_t size, std::size_t nVars, void** vars );
      /// Destructor
      ATHCUDA_HOST_AND_DEVICE
      ~AuxStore();

      /// @name Interface for using the data on a (GPU) device
      /// @{

      /// Get the size of the variable arrays
      ATHCUDA_HOST_AND_DEVICE
      std::size_t arraySize() const;

      /// Function for accessing a variable array (non-const)
      template< typename T >
      ATHCUDA_HOST_AND_DEVICE
      T* array( SG::auxid_t auxid );

      /// Function for accessing a variable array (const)
      template< typename T >
      ATHCUDA_HOST_AND_DEVICE
      const T* array( SG::auxid_t auxid ) const;

      /// Type of the variable returned by the @c attachTo function
      typedef std::pair< std::size_t, void** > ExposedVars_t;

      /// Get the array of variables to be used on a CUDA device
      ATHCUDA_HOST
      ExposedVars_t attachTo( cudaStream_t stream );

      /// Retrieve the variables from a CUDA device
      ATHCUDA_HOST
      void retrieveFrom( cudaStream_t stream );

      /// @}

      /// @name Interface inherited from @c SG::IConstAuxStore
      /// @{

      /// Get a pointer to one auxiliary variable
      virtual const void* getData( SG::auxid_t auxid ) const override;

      /// Create a decoration on the container
      virtual void* getDecoration( SG::auxid_t auxid, size_t size,
                                   size_t capacity ) override;

      /// Get the identifiers of all available variables
      virtual const SG::auxid_set_t& getAuxIDs() const override;

      /// Test if a given auxiliary variable is a decoration
      virtual bool isDecoration( SG::auxid_t auxid ) const override;

      /// Lock the container against creating decorations
      virtual void lock() override;

      /// Clear all decorations
      virtual bool clearDecorations() override;

      /// Get the size of the container
      virtual size_t size() const override;

      /// Lock a specific decoration
      virtual void lockDecoration( SG::auxid_t auxid ) override;

      /// @}

      /// @name Interface inherited from @c SG::IAuxStore
      /// @{

      /// Get a (possibly new) pointer to one auxiliary variable
      virtual void* getData( SG::auxid_t auxid, size_t size,
                             size_t capacity ) override;

      /// Get the auxiliary IDs of the writable variables
      virtual const SG::auxid_set_t& getWritableAuxIDs() const override;

      /// Change the size of the container
      virtual bool resize( size_t size ) override;

      /// Change the capacity of the auxiliary vectors
      virtual void reserve( size_t capacity ) override;

      /// Shift elements of the container
      virtual void shift( size_t pos, ptrdiff_t offs ) override;

      /// Move all elements from @c other to this store
      virtual bool insertMove( size_t pos, SG::IAuxStore& other,
                               const SG::auxid_set_t& ignore = {} ) override;

      /// @}

   private:
      /// Try to reserve a new variable in CUDA Unified memory
      void* getDataCUDA( SG::auxid_t auxid, size_t size,
                         size_t capacity );

      /// @name Variables used in both the host and device code
      /// @{

      /// The size of the container
      std::size_t m_size;

      /// Array of helper objects managing the variables in CUDA unified memory
      AthCUDA::array< IAuxTypeVector* > m_vecs;

      /// @}

      /// @name Variables used only in the host code
      ///
      /// I use bare pointers for everything to avoid having to hide the
      /// variables during device code compilation. (They can't be created
      /// in device code...) Not pretty, but it's good enough for now.
      ///
      /// @{

      /// Buffer in CUDA device memory holding the allocated variables
      AthCUDA::device_array< void* >* m_deviceBuffer;
      /// Buffer on the host, holding the allocated variables
      AthCUDA::host_array< void* >* m_hostBuffer;

      /// Auxiliary IDs held by the store
      SG::auxid_set_t* m_auxids;

      /// Store holding all non-trivial variables
      SG::IAuxStore* m_store;

      /// Mutex used to synchronise the modifications to the cache vector
      typedef AthContainers_detail::mutex mutex_t;
      typedef AthContainers_detail::lock_guard< mutex_t > guard_t;
      mutable mutex_t m_mutex;

      /// @}

   }; // class AuxStore

   template< typename T >
   ATHCUDA_HOST_AND_DEVICE
   T* AuxStore::array( SG::auxid_t auxid ) {

      // Check whether the requested variable exists.
      if( ( auxid >= m_vecs.size() ) || ( m_vecs[ auxid ] == nullptr ) ) {
         return nullptr;
      }
      // If it does, let's assume that it is the right type. Later on some
      // further checks could be added here.
      return static_cast< T* >( m_vecs[ auxid ]->toPtr() );
   }

   template< typename T >
   ATHCUDA_HOST_AND_DEVICE
   const T* AuxStore::array( SG::auxid_t auxid ) const {

      // Check whether the requested variable exists.
      if( ( auxid >= m_vecs.size() ) || ( m_vecs[ auxid ] == nullptr ) ) {
         return nullptr;
      }
      // If it does, let's assume that it is the right type. Later on some
      // further checks could be added here.
      return static_cast< const T* >( m_vecs[ auxid ]->toPtr() );
   }

} // namespace AthCUDA

#endif // ATHCUDAAUXSTORE_AUXSTORE_CUH
