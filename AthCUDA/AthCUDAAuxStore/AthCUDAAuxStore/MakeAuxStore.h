// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCUDAAUXSTORE_MAKEAUXSTORE_H
#define ATHCUDAAUXSTORE_MAKEAUXSTORE_H

// Athena include(s).
#include "AthContainersInterfaces/IAuxStore.h"

// System include(s).
#include <memory>

namespace AthCUDA {

   /// Helper function creating a CUDA-capable auxiliary container in C++ code
   std::unique_ptr< SG::IAuxStore > makeAuxStore();

} // namespace AthCUDA

#endif // ATHCUDAAUXSTORE_MAKEAUXSTORE_H
