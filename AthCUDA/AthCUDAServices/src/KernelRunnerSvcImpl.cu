// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "KernelRunnerSvcImpl.h"

// Project include(s).
#include "AthCUDACore/Macros.cuh"
#include "AthCUDACore/StreamHolderHelpers.cuh"
#include "AthCUDACore/TaskArena.h"

// System include(s).
#include <cassert>
#include <iostream>
#include <memory>

namespace {

#ifdef __CUDACC__
   /// Type used internally to signal the finish of kernel tasks
   class KernelFinishData {
   public:
      /// Constructor
      KernelFinishData( const AthCUDA::KernelRunnerSvcImplCallback& callback,
                        std::unique_ptr< AthCUDA::IKernelTask > task )
      : m_callback( callback ), m_task( std::move( task ) ) {}

      /// Callback to the kernel runner service
      AthCUDA::KernelRunnerSvcImplCallback m_callback;
      /// The task that's being executed
      std::unique_ptr< AthCUDA::IKernelTask > m_task;
   }; // struct KernelFinishData

   /// Function set up to be called by CUDA when a task finishes
   void setKernelFinished( void* userData ) {

      // Cast the user data to the right type.
      KernelFinishData* data =
         reinterpret_cast< KernelFinishData* >( userData );
      assert( data != nullptr );

      // Check if there was an error in the execution, and finish
      // the task accordingly.
      const StatusCode sc =
         data->m_task->finished( cudaGetLastError() == cudaSuccess ?
                                 StatusCode::SUCCESS : StatusCode::FAILURE,
                                 AthCUDA::IKernelTask::Asynchronous ); 
      if( ! sc.isSuccess() ) {
         std::cerr << "Failed to finish a CUDA kernel task!" << std::endl;
         std::abort();
      }

      // Tell the service that a task has finished executing.
      data->m_callback();

      // Delete the data object.
      delete data;
      return;
   }
#endif // __CUDACC__

   /// Functor scheduling an @c AthCUDA::IKernelTask for execution
   class KernelSchedulerTask {

   public:
      /// Constructor with all necessary parameters
      KernelSchedulerTask( const AthCUDA::KernelRunnerSvcImplCallback& callback,
                           std::unique_ptr< AthCUDA::IKernelTask > task,
                           AthCUDA::KernelRunnerSvcImpl& svcImpl )
      : m_callback( callback ), m_task( std::move( task ) ),
        m_svcImpl( &svcImpl ) {}

      /// Operator executing the functor
      void operator()() const {

         // Get a stream for the job.
         auto stream = m_svcImpl->getAvailableStream();
         assert( stream );

         // First off, let the task schedule all of its own operations.
         if( ! m_task->execute( stream ).isSuccess() ) {
            std::cerr << "Failed to schedule the launch of a GPU task"
                      << std::endl;
         }

         // Now add a step after those to the stream, one that signals to
         // us that the task is done.
         CUDA_EXP_CHECK( cudaLaunchHostFunc( getStream( stream ),
                                             ::setKernelFinished,
                            new ::KernelFinishData( m_callback,
                                                    std::move( m_task ) ) ) );
      }

   private:
      /// Callback to the kernel runner service
      AthCUDA::KernelRunnerSvcImplCallback m_callback;
      /// The task that's being executed
      mutable std::unique_ptr< AthCUDA::IKernelTask > m_task;
      /// Pointer to the service implementation object
      AthCUDA::KernelRunnerSvcImpl* m_svcImpl;

   }; // class KernelSchedulerTask

} // private namespace

namespace AthCUDA {

   StatusCode
   KernelRunnerSvcImpl::execute( std::unique_ptr< IKernelTask > task ) {

      // Schedule a task that will take care of scheduling/launching the CUDA
      // kernel.
      taskArena().enqueue( ::KernelSchedulerTask( m_callback,
                                                  std::move( task ),
                                                  *this )
#if TBB_INTERFACE_VERSION_MAJOR < 12
                           , tbb::priority_normal
#endif // TBB_INTERFACE_VERSION_MAJOR < 12
                           );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

} // namespace AthCUDA
