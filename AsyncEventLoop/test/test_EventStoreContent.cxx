// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "../src/evloop/EventStoreContent.h"

// Project include(s).
#include "AsyncBaseComps/Algorithm.h"
#include "AsyncEventLoop/Job.h"

// Gaudi include(s).
#include <GaudiKernel/MsgStream.h>

// System include(s).
#undef NDEBUG
#include <cassert>
#include <vector>

/// Helper algorithm type used in the test
class TestAlgorithm : public Gaudi::Algorithm {

public:
   // Inherit the base class's constructor(s).
   using Gaudi::Algorithm::Algorithm;
   /// Dummy execute function
   virtual StatusCode execute( const EventContext& ) const override {
      return StatusCode::SUCCESS;
   }
}; // class TestAlgorithm

int main() {

   // Set up a Job object, to get a valid service locator.
   ASync::Job job;
   assert( job.configure().isSuccess() == true );

   // Set up a bunch of test algorithms with different inputs and outputs.
   TestAlgorithm trackReco( "trackReco", job.svcLoc() );
   trackReco.addDependency( DataObjID( "Tracks" ),
                            Gaudi::DataHandle::Writer );
   TestAlgorithm clusterReco( "clusterReco", job.svcLoc() );
   clusterReco.addDependency( DataObjID( "Clusters" ),
                              Gaudi::DataHandle::Writer );
   TestAlgorithm electronReco( "electronReco", job.svcLoc() );
   electronReco.addDependency( DataObjID( "Tracks" ),
                               Gaudi::DataHandle::Reader );
   electronReco.addDependency( DataObjID( "Clusters" ),
                               Gaudi::DataHandle::Reader );
   electronReco.addDependency( DataObjID( "Electrons" ),
                               Gaudi::DataHandle::Writer );
   TestAlgorithm monitoring( "monitoring", job.svcLoc() );
   monitoring.addDependency( DataObjID( "Electrons"),
                             Gaudi::DataHandle::Reader );

   // Set up the object to be tested.
   std::vector< Gaudi::Algorithm* > gaudiAlgs{ &trackReco, &clusterReco,
                                               &electronReco, &monitoring };
   MsgStream msg( nullptr, "test_EventStoreContent" );
   ASync::EventStoreContent content( gaudiAlgs, msg );

   // Check a regular execution.
   content.reset();
   assert( content.isAlgExecutable( 0 ) == true );
   assert( content.isAlgExecutable( 1 ) == true );
   assert( content.isAlgExecutable( 2 ) == false );
   assert( content.isAlgExecutable( 3 ) == false );
   assert( content.setAlgExecuted( 0 ).isSuccess() == true );
   assert( content.isAlgExecutable( 0 ) == true );
   assert( content.isAlgExecutable( 1 ) == true );
   assert( content.isAlgExecutable( 2 ) == false );
   assert( content.isAlgExecutable( 3 ) == false );
   assert( content.setAlgExecuted( 1 ).isSuccess() == true );
   assert( content.isAlgExecutable( 0 ) == true );
   assert( content.isAlgExecutable( 1 ) == true );
   assert( content.isAlgExecutable( 2 ) == true );
   assert( content.isAlgExecutable( 3 ) == false );
   assert( content.setAlgExecuted( 2 ).isSuccess() == true );
   assert( content.isAlgExecutable( 0 ) == true );
   assert( content.isAlgExecutable( 1 ) == true );
   assert( content.isAlgExecutable( 2 ) == true );
   assert( content.isAlgExecutable( 3 ) == true );

   // Check that the event reset works.
   content.reset();
   assert( content.isAlgExecutable( 0 ) == true );
   assert( content.isAlgExecutable( 1 ) == true );
   assert( content.isAlgExecutable( 2 ) == false );
   assert( content.isAlgExecutable( 3 ) == false );

   // Return gracefully.
   return 0;
}
