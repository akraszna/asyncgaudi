// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "../src/evloop/AlgExecutionStates.h"

// Gaudi include(s).
#include <GaudiKernel/MsgStream.h>

// System include(s).
#undef NDEBUG
#include <cassert>

int main() {

   // Save some typing...
   using State = ASync::AlgExecutionStates::State;

   // Dummy message stream object used by the test.
   MsgStream msgStream( nullptr, "test_AlgExecutionStates" );

   // Check some basics on a new object.
   ASync::AlgExecutionStates obj( 10, msgStream );
   assert( obj.size() == 10 );
   assert( obj.sizeOfSubset( State::CONTROLREADY ) == 10 );
   assert( obj.contains( State::CONTROLREADY ) == true );
   assert( obj.containsAny( { State::CONTROLREADY,
                              State::SCHEDULED } ) == true );
   assert( obj.containsOnly( { State::CONTROLREADY } ) == true );
   assert( obj[ 0 ] == State::CONTROLREADY );

   // Check that we can iterate over all CONTROLREADY state elements.
   std::vector< std::size_t > initialAlgs;
   for( auto itr = obj.begin( State::CONTROLREADY );
        itr != obj.end( State::CONTROLREADY ); ++itr ) {
      initialAlgs.push_back( *itr );
   }
   assert( initialAlgs.size() == 10 );
   static const std::vector< std::size_t > refInitialAlgs =
      { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
   assert( initialAlgs == refInitialAlgs );

   // Try switching the states of some algorithms.
   assert( obj.set( 0, State::CONTROLREADY ).isSuccess() == true );
   assert( obj.set( 0, State::DATAREADY ).isSuccess() == true );
   assert( obj.set( 0, State::EVTACCEPTED ).isSuccess() == false );

   // Check that the object is indeed in an error state now.
   assert( obj.contains( State::ERROR ) == true );
   assert( obj.sizeOfSubset( State::ERROR ) == 1 );

   // Check that resetting the object works as expected.
   obj.reset();
   assert( obj.sizeOfSubset( State::CONTROLREADY ) == 10 );

   // Switch some algorithms to the DATAREADY state.
   assert( obj.set( 0, State::DATAREADY ).isSuccess() == true );
   assert( obj.set( 2, State::DATAREADY ).isSuccess() == true );
   assert( obj.set( 4, State::DATAREADY ).isSuccess() == true );

   // Now check that we can iterate over the DATAREADY elements.
   std::vector< std::size_t > datareadyAlgs;
   for( auto itr = obj.begin( State::DATAREADY );
        itr != obj.end( State::DATAREADY ); ++itr ) {
      datareadyAlgs.push_back( *itr );
   }
   assert( datareadyAlgs.size() == 3 );
   static const std::vector< std::size_t > refDatareadyAlgs = { 0, 2, 4 };
   assert( datareadyAlgs == refDatareadyAlgs );

   // And that we don't find any EVTACCEPTED elements.
   std::vector< std::size_t > evtacceptedAlgs;
   for( auto itr = obj.begin( State::EVTACCEPTED );
        itr != obj.end( State::EVTACCEPTED ); ++itr ) {
      evtacceptedAlgs.push_back( *itr );
   }
   assert( evtacceptedAlgs.size() == 0 );

   // Return gracefully.
   return 0;
}
