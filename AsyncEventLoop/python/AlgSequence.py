# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

# Local import(s).
import AsyncEventLoop.ConfigurableList

class AlgSequence( AsyncEventLoop.ConfigurableList.ConfigurableList ):
   '''Asynchronous algorithm sequence

   This is a light-weight emulation of Athena's AthSequencer class,
   implementing a simple algorithm sequence for async_athena.py jobs.
   '''

   # The only instalnce of the algorithm sequence.
   _instance = None

   def __new__( cls, *args, **kwargs ):
      '''Function implementing a singleton behaviour for the sequence

      Since in the simple configurations that we use in this project we only
      ever use a single algorithm sequence, with no control-flow implemented
      amongst its algorithms, it's okay to implement the __new__ function in
      such a simple way.
      '''

      # Create the singleton instance if it doesn't exist yet.
      if not AlgSequence._instance:
         AlgSequence._instance = super( AlgSequence, cls ).__new__( cls, *args,
                                                                    **kwargs )
         AlgSequence._instance._configurables = []
         pass

      # Return the singleton instance.
      return AlgSequence._instance

   def __init__( self ):
      '''Algorithm sequence constructor
      '''
      # Call the __init__ function of the base class explicitly.
      super( AlgSequence, self ).__init__()
      return
