# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

# Local import(s).
import AsyncEventLoop.ConfigurableList

class ServiceMgr( AsyncEventLoop.ConfigurableList.ConfigurableList ):
   '''Service manager

   Unlike in Athena, in these simplified configurations 'ServiceMgr' is not
   an object that would be in direct communication with the C++ object of the
   same type/name.

   In this setup it's a simple list of service configurable objects that is
   used to allow job configurations to set properties on various services,
   which would all eventually have to be explicitly created through an
   algorithm. As this machinery is only taking care of setting the properties
   of the "eventual" services in the JobOptionsSvc.
   '''

   # The only instalnce of the service manager.
   _instance = None

   def __new__( cls, *args, **kwargs ):
      '''Function implementing a singleton behaviour for the service manager

      Implemented in the same way as for AlgSequence...
      '''

      # Create the singleton instance if it doesn't exist yet.
      if not ServiceMgr._instance:
         ServiceMgr._instance = super( ServiceMgr, cls ).__new__( cls, *args,
                                                                  **kwargs )
         ServiceMgr._instance._configurables = []
         pass

      # Return the singleton instance.
      return ServiceMgr._instance

   def __init__( self ):
      '''Service manager constructor
      '''
      # Call the __init__ function of the base class explicitly.
      super( ServiceMgr, self ).__init__()
      return

# A convenient object to use in job configurations.
svcMgr = ServiceMgr()
