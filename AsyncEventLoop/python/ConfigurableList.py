# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

class ConfigurableList( object ):
   '''Generic list of Gaudi/Athena (named) configurables

   This is a common base class for AlgSequence and ServiceMgr in this simple
   mock of the full Athena python configuration.
   '''

   def __init__( self ):
      '''Configurable list constructor
      '''

      return

   def __getitem__( self, index ):
      '''Return one configurable from the list by index

      This is to allow getting the n'th element of an algorithm sequence
      including the n'th element from the back of it if needed.

      Keyword arguments:
        index -- The index of the configurable to get from the list
      '''

      return self._configurables[ index ]

   def __getattr__( self, name ):
      '''Access one configurable in this list by name

      This is to allow modifying the properties of configurables in a
      list conveniently.

      Keyword arguments:
        name -- The name of the configurable to look up in the list
      '''

      # Look up the algorithm by name.
      for configurable in self._configurables:
         if configurable.getName() == name:
            return configurable
         pass

      # If no algorithm with this name was found, that's a problem.
      raise AttributeError( 'Configurable with name "%s" was not ' \
                            'found' % name )

   def __delattr__( self, name ):
      '''Remove one configurable from this list, by name

      This is to allow removing configurables from this list in case that
      would be needed.

      Keyword arguments:
        name -- The name of the configurable to delete from the list
      '''

      # Look up the algorithm by name.
      for configurable in self._configurables:
         if configurable.getName() == name:
            # If we found it, remove it.
            self._configurables.remove( configurable )
            return
         pass

      # If no algorithm with this name was found, that's a problem.
      raise AttributeError( 'Configurable with name "%s" was not ' \
                            'found' % name )

   def __iter__( self ):
      '''Create an iterator over all the configurables of this list

      This is to allow for a Python-like iteration over all configurables
      that are part of the list.
      '''

      # Create the iterator to process the internal list of configurables.
      return iter( self._configurables )

   def __iadd__( self, configurable ):
      '''Add one configurable to the list

      This function is used to add one configurable to the list object,
      using the '+=' operator.

      Keyword arguments:
         configurable -- The configurable to add to the list
      '''

      # Add the configurable to the internal list.
      self._configurables.append( configurable )

      # Return the modified object.
      return self
