# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

# Athena import(s).
from AthenaCommon.Logging import logging

# System import(s).
import os

def findDataFile( logicalName ):
   '''Find a file using the DATAPATH environment variable

   This function can be used in job configurations to look up data files
   conveniently.

   Keyword arguments:
     logicalName -- The file/directory to find using DATAPATH

   Returns the absolute path of the file/directory if successful,
   None otherwise.
   '''

   # Access the DATPATH environment variable, and split it into individual
   # paths.
   paths = os.getenv( 'DATAPATH', '' ).split( ':' )

   # Loop over the paths in the specified order.
   for path in paths:
      # If the file/directory that we're looking for is available under
      # this path, then stop here.
      fullPath = os.path.join( path, logicalName )
      if os.path.exists( fullPath ):
         return fullPath
      pass

   # Apparently we didn't find the file/directory. :-()
   log = logging.getLogger( 'findDataFile' )
   log.warning( 'Couldn\'t find file/directory "%s"' % logicalName )
   return None

def cpuOnlySelector( algName ):
   '''Function that can be used to set all algorithms to be CPU-only ones
   '''
   return False

def fillAlgSequence( algSeq, cfGraph, dfGraph, seqName,
                     cpuCruncherType, gpuCruncherType,
                     gpuSelector = cpuOnlySelector ):
   '''Fill an algorithm sequence with the specified configuration

   This function should be used to set up jobs resembling ATLAS reconstruction
   configurations.

   Keyword arguments:
     algSeq   -- The AlgSequence object to add the algorithms to
     cfGraph  -- The control-flow graph
     dfGraph  -- The data-flow graph
     seqName  -- Name of the sequence to start the recursive operations from
     cpuCruncherType -- The algorithm type used for CPU crunching
     gpuCruncherType -- The algorithm type used for GPU(/CPU) crunching
     gpuSelector     -- Function/functor selecting algorithms to offload
   '''

   # Loop over the children of the starting algorithm sequence of the
   # config-flow diagram.
   for alg in cfGraph[ seqName ]:

      # Get the type and name of the algorithm.
      algName = alg.split( '/' )[ 1 ] if '/' in alg else alg
      algType = alg.split( '/' )[ 0 ] if '/' in alg else 'Algorithm'

      # If we encountered a sub-sequence, process its elements recursively.
      if algType == 'AthSequencer':
         fillAlgSequence( algSeq, cfGraph, dfGraph, alg,
                          cpuCruncherType, gpuCruncherType )
         pass

      # Add the algorithm to the sequence if it's not there yet.
      if not hasattr( algSeq, algName ):

         # Create the cruncher algorithm.
         cfg = None
         if gpuSelector( algName ):
            cfg = gpuCruncherType( algName )
         else:
            cfg = cpuCruncherType( algName )
            pass

         # Declare all input data dependencies on it.
         for inNode, outNode in dfGraph.in_edges( algName ):
            if inNode not in cfg.InputKeys:
               cfg.InputKeys.append( inNode )
               pass
            pass
         # Declare all the output data dependencies on it.
         for inNode, outNode in dfGraph.out_edges( algName ):
            if outNode not in cfg.OutputKeys:
               cfg.OutputKeys.append( outNode )
               pass
            pass
         # Set the timing on the algorithm to 0 by default.
         cfg.AverageRuntime = 0.0
         cfg.RuntimeVariation = 0.0

         # Add the algorithm to the main algorithm sequence.
         algSeq += cfg
         pass

      pass

   return
