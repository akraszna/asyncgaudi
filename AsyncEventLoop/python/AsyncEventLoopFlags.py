# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

# Athena import(s).
from AthenaCommon.JobProperties import JobProperty, JobPropertyContainer
from AthenaCommon.JobProperties import jobproperties

# Define the individual job properties.

class EvtMax( JobProperty ):
   '''The maximum number of events to process'''
   statusOn     = True
   allowedTypes = [ 'int' ]
   StoredValue  = 100
   pass

class EventPrintoutInterval( JobProperty ):
   '''Event printout interval for the event loop manager'''
   statusOn     = True
   allowedTypes = [ 'int' ]
   StoredValue  = 1
   pass

class NThreads( JobProperty ):
   '''The number of parallel threads to use in the job'''
   statusOn     = True
   allowedTypes = [ 'int' ]
   StoredValue  = 1
   pass

class NAccTasks( JobProperty ):
   '''The number of GPU/accelerator tasks to use in parallel in the job'''
   statusOn     = True
   allowedTypes = [ 'int' ]
   StoredValue  = 0
   pass

class SynchronousScheduler( JobProperty ):
   '''Whether the job will be executed by the synchronous scheduler or not'''
   statusOn     = True
   allowedTypes = [ 'bool' ]
   StoredValue  = False
   pass

# Set up the container for these job properies.

class AsyncEventLoopFlags( JobPropertyContainer ):
   '''Container of all job properties known to AsyncEventLoop'''
   pass

jobproperties.add_Container( AsyncEventLoopFlags )

# Add all job properties to the container.
jobproperties.AsyncEventLoopFlags.add_JobProperty( EvtMax )
jobproperties.AsyncEventLoopFlags.add_JobProperty( EventPrintoutInterval )
jobproperties.AsyncEventLoopFlags.add_JobProperty( NThreads )
jobproperties.AsyncEventLoopFlags.add_JobProperty( NAccTasks )
jobproperties.AsyncEventLoopFlags.add_JobProperty( SynchronousScheduler )

# Set up a helper variable for accessing the job properties.
asyncEventLoopFlags = jobproperties.AsyncEventLoopFlags
