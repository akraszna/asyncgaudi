// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCEVENTLOOP_JOB_H
#define ASYNCEVENTLOOP_JOB_H

// Gaudi include(s).
#include <GaudiKernel/StatusCode.h>
#include <GaudiKernel/SmartIF.h>
#include <GaudiKernel/ISvcLocator.h>
#include <GaudiKernel/ISvcManager.h>
#include <GaudiKernel/IAppMgrUI.h>
#include <GaudiKernel/IProperty.h>
#include <GaudiKernel/IMessageSvc.h>
#include <GaudiKernel/MsgStream.h>

// System include(s).
#include <map>
#include <string>
#include <vector>

namespace ASync {

   /// Main class controlling/executing a Gaudi job
   ///
   /// The idea here is to simplify spinning up a Gaudi job for the
   //// asynchronous ests as much as possible.
   ///
   /// @author Martin Errenst <Martin.Errenst@cern.ch>
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class Job {

   public:
      /// Default constructor
      Job();
      /// Job with specific number of threads
      Job( size_t nThreads );

      /// @name Function(s) used to configure job
      /// @{

      /// Get the type/name of the scheduler that will be used
      const std::string& schedulerName() const;
      /// Set the type/name of the scheduler that should be used
      void setSchedulerName( const std::string& name );

      /// Get the names of the input files that the job will use
      const std::vector< std::string >& inputFileNames() const;
      /// Set the names of the input files that the job should use
      void setInputFileNames( const std::vector< std::string >& names );

      /// Set up all the components, to allow the user to configure them
      StatusCode configure();

      /// @}

      /// @name Function(s) used to execute the job
      /// @{

      /// Run the configured job
      StatusCode run();

      /// @}

      /// @name Accessors for the various services/components used underneath
      /// @{

      /// Access the service locator
      ISvcLocator* svcLoc();
      /// Access the service manager
      ISvcManager* svcMgr();
      /// Access the application manager
      IAppMgrUI* appMgr();
      /// Access the property manager
      IProperty* propMgr();

      /// @}

      /// Type for the algorithm/tool properties
      typedef std::vector< std::pair< std::string, std::string > > Props_t;

      /// @name Algorithm creation
      /// @{

      /// Add an algorithm with the specified properties to the job
      StatusCode addAlgorithm( const std::string& typeName,
                               const std::string& instanceName,
                               const Props_t& properties = Props_t() );

      /// @}

      /// @name Property setter/getter functions
      /// @{

      /// Set properties for a given algorithm/tool instance
      StatusCode setProperties( const std::string& instance,
                                const Props_t& properties = Props_t() );

      /// Set one property of a service
      StatusCode setProperty( const std::string& service,
                              const std::string& property,
                              const std::string& value );
      /// Get the value of a given property from a service
      StatusCode getProperty( const std::string& service,
                              const std::string& property,
                              std::string& value ) const;

      /// Set one property of the application manager
      StatusCode setProperty( const std::string& property,
                              const std::string& value );
      /// Get the value of a given property from the application manager
      StatusCode getProperty( const std::string& property,
                              std::string& value ) const;

      /// @}

   private:
      /// @name Messaging functions/variables
      /// @{

      /// Test the message stream output level
      bool msgLevel( MSG::Level lvl ) const;

      /// Get the message stream object
      MsgStream& msgStream() const;

      /// Get the message stream object with a specific output level
      MsgStream& msgStream( MSG::Level lvl ) const;

      /// @}

      /// @name Components needed for a Gaudi job
      /// @{

      /// The property manager
      SmartIF< IProperty > m_propMgr{ nullptr };
      /// The service locator
      SmartIF< ISvcLocator > m_svcLoc{ nullptr };
      /// The service manager
      SmartIF< ISvcManager > m_svcMgr{ nullptr };
      /// The application manager
      SmartIF< IAppMgrUI > m_appMgr{ nullptr };
      /// The message service
      SmartIF< IMessageSvc > m_msgSvc{ nullptr };

      /// @}

      /// @name Additional member variables
      /// @{

      /// Number of threads to use for the job
      std::size_t m_nThreads;

      /// The type/name of the scheduler to use
      std::string m_schedulerName = "ASync::SchedulerSvc";

      /// Input files to read with the job
      std::vector< std::string > m_inputFiles;

      /// @}

   }; // class Job

} // namespace ASync

#endif // ASYNCEVENTLOOP_JOB_H
