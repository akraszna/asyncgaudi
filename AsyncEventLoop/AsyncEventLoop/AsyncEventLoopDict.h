// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCEVENTLOOP_ASYNCEVENTLOOPDICT_H
#define ASYNCEVENTLOOP_ASYNCEVENTLOOPDICT_H

// Local include(s).
#include "AsyncEventLoop/Job.h"

#endif // ASYNCEVENTLOOP_ASYNCEVENTLOOPDICT_H
