#!/usr/bin/env python
# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Main script to execute asynchronous test jobs with.
#

# Project import(s).
import AsyncEventLoop.AlgSequence
import AsyncEventLoop.AsyncEventLoopFlags
import AsyncEventLoop.ServiceMgr

# Athena import(s).
import AthenaCommon.Include
import AthenaCommon.Logging

# Gaudi import(s).
from GaudiKernel.ProcessJobOptions import InstallRootLoggingHandler

# ROOT import(s).
import ROOT

# System import(s).
import argparse
import logging
import sys

def scCheck( sc ):
   '''
   Helper function for calling StatusCode returning functions in main()
   '''
   if not sc.isSuccess():
      raise RuntimeError( 'StatusCode check failed' )
   return

def main():
   '''
   C(++) style main function for the script
   '''

   # Set up / parse the command line arguments.
   parser = argparse.ArgumentParser( description = '''
   This script can be used to execute "simple" jobOption-like
   configurations using ASync::Job. It provides a much simplified
   interface/feature set compared to athena.py
   ''' )
   parser.add_argument( 'jobOptions', metavar = 'jobOption', nargs = '+',
                        help = 'The job configuration to use/execute' )
   parser.add_argument( '-t', '--threads', type = int, default = 1,
                        help = 'Number of parallel CPU threads to use' )
   parser.add_argument( '-a', '--accelerator-tasks', type = int, default = 0,
                        dest = 'accelerator_tasks',
                        help = 'Number of parallel accelerator/GPU tasks' )
   parser.add_argument( '-y', '--synchronous', action = 'store_true',
                        default = False,
                        help = 'Run the job with the synchronous scheduler' )
   parser.add_argument( '-l', '--output-level', type = int,
                        dest = 'output_level', default = logging.INFO,
                        help = 'The global output level to use in the script' )
   parser.add_argument( '-s', '--show-trace', action = 'store_true',
                        dest = 'show_trace', default = False,
                        help = 'Show a trace of all included jobOptions' )
   args = parser.parse_args()

   # Update the job properties with the command line arguments.
   flags = AsyncEventLoop.AsyncEventLoopFlags.asyncEventLoopFlags
   flags.NThreads = args.threads
   flags.NAccTasks = args.accelerator_tasks
   flags.SynchronousScheduler = args.synchronous

   # Configure the logging.
   logger = AthenaCommon.Logging.logging.getLogger( 'async_athena' )

   # Greet the user.
   logger.info( '-----------------------------------------------------------' )
   logger.info( '-           Welcome to Asynchronous Athena! :-)           -' )
   logger.info( '-----------------------------------------------------------' )

   # Read in the user's job option(s).
   AthenaCommon.Include.include.setShowIncludes( args.show_trace )
   for jobO in args.jobOptions:
      AthenaCommon.Include.include( jobO )
      pass

   # Set up the job object.
   job = ROOT.ASync.Job( args.threads )
   if flags.SynchronousScheduler():
      job.setSchedulerName( 'AvalancheSchedulerSvc' )
      pass
   scCheck( job.configure() )

   # Add all algorithms to the job.
   for alg in AsyncEventLoop.AlgSequence.AlgSequence():
      scCheck( job.addAlgorithm( alg.getType(), alg.getName() ) )
      for propname in alg.__slots__:
         if hasattr( alg, propname ):
            scCheck( job.setProperty( alg.getName(), propname,
                                      '%s' % getattr( alg, propname ) ) )
            pass
         pass
      pass

   # Set the properties of all of the services of the job.
   for svc in AsyncEventLoop.ServiceMgr.svcMgr:
      for propname in svc.__slots__:
         if hasattr( svc, propname ):
            scCheck( job.setProperty( svc.getName(), propname,
                                      '%s' % getattr( svc, propname ) ) )
            pass
         pass
      pass

   # Set properties on the job object based on the job property values.
   scCheck( job.setProperty( 'ASync::EventLoopMgr', 'EventPrintoutInterval',
                             '%s' % flags.EventPrintoutInterval() ) )
   scCheck( job.setProperty( 'EvtMax', '%s' % flags.EvtMax() ) )

   # Run the job.
   scCheck( job.run() )

   # Return gracefully.
   return 0

# Execute the main() function when using this file as a script.
if __name__ == '__main__':
   sys.exit( main() )
   pass
