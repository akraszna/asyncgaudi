// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AsyncEventLoop/Job.h"

// Project include(s).
#include "AsyncBaseComps/CheckMacros.h"
#include "AsyncBaseComps/MessagingMacros.h"

// Athena include(s).
#include "AthenaKernel/errorcheck.h"
#include "xAODRootAccess/Init.h"

// Gaudi include(s).
#include <GaudiKernel/Bootstrap.h>
#include <GaudiKernel/GaudiException.h>
#include <GaudiKernel/IAlgManager.h>
#include <GaudiKernel/Algorithm.h>
#include <GaudiKernel/Parsers.h>
#include <GaudiKernel/IJobOptionsSvc.h>

// ROOT include(s).
#include <TROOT.h>

// System include(s).
#include <iostream>
#include <set>
#include <thread>

namespace {

   /// Function translating a list of string properties into a format recognised
   /// by the Gaudi property parsing.
   std::string to_string( const std::vector< std::string >& strings ) {

      std::string result;
      result += "[";
      for( size_t i = 0; i < strings.size(); ++i ) {
         result += "\"" + strings[ i ] + "\"";
         if( i < strings.size() - 1 ) {
            result += ", ";
         }
      }
      result += "]";
      return result;
   }

   /// Helper function used to extend the top algorithm property with the name
   /// of one additional algorithm.
   StatusCode addTopAlgorithm( std::string& prop, const std::string& algName ) {

      // Use the built-in Gaudi parser to take the existing property name apart
      // into a vector of strings.
      std::vector< std::string > algs;
      StatusCode sc = Gaudi::Parsers::parse( algs, prop );
      if( ! sc.isSuccess() ) {
         return sc;
      }

      // Add our new algorithm to it, making sure that duplicates are removed.
      std::set< std::string > algsAsSet( algs.begin(), algs.end() );
      algsAsSet.insert( algName );
      std::vector< std::string > extendedAlgs( algsAsSet.begin(),
                                               algsAsSet.end() );

      // Now translate the new list back into a string.
      prop = to_string( extendedAlgs );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

} // private namespace

namespace ASync {

   Job::Job()
   : m_nThreads( std::thread::hardware_concurrency() ) {

   }

   Job::Job( size_t nThreads )
   : m_nThreads( nThreads ) {

   }

   const std::string& Job::schedulerName() const {

      return m_schedulerName;
   }

   void Job::setSchedulerName( const std::string& name ) {

      m_schedulerName = name;
      return;
   }

   const std::vector< std::string >& Job::inputFileNames() const {

      return m_inputFiles;
   }

   void Job::setInputFileNames( const std::vector< std::string >& names ) {

      m_inputFiles = names;
      return;
   }

   StatusCode Job::configure() {

      // Set up the environment for xAOD handling.
      ROOT::EnableThreadSafety();
      if( ! xAOD::Init().isSuccess() ) {
         ATH_MSG_ERROR( "Couldn't set up the xAOD environment" );
         return StatusCode::FAILURE;
      }

      // Check that the user is not re-initialising the object.
      if( m_appMgr.isValid() ) {
         ATH_MSG_ERROR( "Not possible to re-initialise the object" );
         return StatusCode::FAILURE;
      }

      // Set up the services/components.
      m_appMgr  = SmartIF< IAppMgrUI >( Gaudi::createApplicationMgr() );
      m_propMgr = m_appMgr.as< IProperty >();
      m_svcLoc  = m_appMgr.as< ISvcLocator >();
      m_svcMgr  = m_appMgr.as< ISvcManager >();
      m_msgSvc  = m_appMgr.as< IMessageSvc >();

      // Check that all interfaces were retrieved correctly.
      if( ( ! m_appMgr.isValid() ) || ( ! m_svcLoc.isValid() ) ||
          ( ! m_svcMgr.isValid() ) || ( ! m_propMgr.isValid() ) ) {
         ATH_MSG_FATAL( "Couldn't retrieve all necessary services "
                        "successfully" );
         return StatusCode::FAILURE;
      }

      // Set the default properties of the application manager.
      ATH_CHECK( setProperty( "JobOptionsType", "FILE" ) );
      ATH_CHECK( setProperty( "JobOptionsPath",
                              "AsyncEventLoop/startup.jobo.txt" ) );
      ATH_CHECK( setProperty( "StatusCodeCheck", "True" ) );

      // Configure the application manager.
      // This has to happen after the JobOptionsSvc has been configured
      // (tries to load the in the JobOptionsPath specified file)
      // Buf before the following services are configured
      // (setProperty uses the JobOptionsSvc internally)
      ATH_CHECK( m_appMgr->configure() );

      // Set the number of events to process.
      ATH_CHECK( setProperty( "EvtMax", "100" ) );

      // Set up the services for N threads.
      ATH_CHECK( setProperty( "ASync::WhiteBoard", "EventSlots",
                              std::to_string( m_nThreads ) ) );
      ATH_CHECK( setProperty( m_schedulerName, "ThreadPoolSize",
                              std::to_string( m_nThreads ) ) );

      // Tell the white board which files to read.
      ATH_CHECK( setProperty( "ASync::WhiteBoard", "InputFiles",
                              to_string( m_inputFiles ) ) );

      // Set up the scheduler.
      ATH_CHECK( setProperty( "ASync::EventLoopMgr", "SchedulerSvc",
                              m_schedulerName ) );
      ATH_CHECK( setProperty( m_schedulerName, "CheckDependencies",
                              "True" ) );
      ATH_CHECK( setProperty( m_schedulerName, "WhiteboardSvc",
                              "ASync::WhiteBoard" ) );

      // If the user specified some input files, use the DataStoreLoaderAlg
      // algorithm to convince the scheduler to run on objects coming from the
      // input.
      if( m_inputFiles.size() ) {
         ATH_CHECK( addAlgorithm( "ASync::DataStoreLoaderAlg",
                                  "DataStoreLoader" ) );
         ATH_CHECK( setProperty( m_schedulerName, "DataLoaderAlg",
                                 "DataStoreLoader" ) );
      }

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode Job::run() {

      // Initialise the application manager.
      ATH_CHECK( m_appMgr->initialize() );

      // Check that there are algorithms configured.
      std::string prop;
      ATH_CHECK( getProperty( "TopAlg", prop ) );
      if( prop == "[  ]" ) {
         ATH_MSG_FATAL( "No algorithms were set up" );
         return StatusCode::FAILURE;
      }

      // Run the job.
      ATH_CHECK( m_appMgr->run() );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   ISvcLocator* Job::svcLoc() {

      return m_svcLoc.pRef();
   }

   ISvcManager* Job::svcMgr() {

      return m_svcMgr.pRef();
   }

   IAppMgrUI* Job::appMgr() {

      return m_appMgr.pRef();
   }

   IProperty* Job::propMgr() {

      return m_propMgr.pRef();
   }

   StatusCode Job::addAlgorithm( const std::string& typeName,
                                 const std::string& algInstanceName,
                                 const Props_t& properties ) {

      // Make sure that the object is initialised already.
      if( ! m_appMgr.isValid() ) {
         ATH_MSG_FATAL( "Object is not yet configured" );
         return StatusCode::FAILURE;
      }

      // Set all the algorithm's properties.
      ATH_CHECK( setProperties( algInstanceName,
                                { { "WhiteboardSvc",
                                    "ASync::WhiteBoard" } } ) );
      ATH_CHECK( setProperties( algInstanceName, properties ) );

      // Access the IAlgManager interface of the application manager.
      IAlgManager* algMgr = m_appMgr.as< IAlgManager >();
      if( ! algMgr ) {
         ATH_MSG_ERROR( "Couldn't get an IAlgManager interface for the "
                        "application manager" );
         return StatusCode::FAILURE;
      }

      // Create the algorithm.
      IAlgorithm* ialg = nullptr;
      ATH_CHECK( algMgr->createAlgorithm( typeName, algInstanceName, ialg ) );

      // Add the algorithm as a top algorithm to the job.
      std::string prop;
      ATH_CHECK( getProperty( "TopAlg", prop ) );
      ATH_CHECK( addTopAlgorithm( prop, typeName + "/" + algInstanceName ) );
      ATH_CHECK( setProperty( "TopAlg", prop ) );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode Job::setProperties( const std::string& instance,
                                  const Props_t& properties ) {

      // Make sure that the object is initialised already.
      if( ! m_appMgr.isValid() ) {
         ATH_MSG_FATAL( "Object is not yet configured" );
         return StatusCode::FAILURE;
      }

      // Access the JobOptionsSvc.
      IJobOptionsSvc* joboSvc =
         m_svcLoc->service( "JobOptionsSvc" ).as< IJobOptionsSvc >();
      if( ! joboSvc ) {
         ATH_MSG_ERROR( "Couldn't access the JobOptionsSvc" );
         return StatusCode::FAILURE;
      }

      // Set the properties.
      for( const auto& prop : properties ) {
         const StringProperty sprop( prop.first, prop.second );
         ATH_CHECK( joboSvc->addPropertyToCatalogue( instance, sprop ) );
      }

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode Job::setProperty( const std::string& service,
                                const std::string& property,
                                const std::string& value ) {

      // Make sure that the object is initialised already.
      if( ! m_appMgr.isValid() ) {
         ATH_MSG_FATAL( "Object is not yet configured" );
         return StatusCode::FAILURE;
      }

      // Access the JobOptionsSvc.
      IJobOptionsSvc* joboSvc =
         m_svcLoc->service( "JobOptionsSvc" ).as< IJobOptionsSvc >();
      if( ! joboSvc ) {
         ATH_MSG_ERROR( "Couldn't access the JobOptionsSvc" );
         return StatusCode::FAILURE;
      }

      // Set the property.
      const StringProperty stringprop( property, value );
      ATH_CHECK( joboSvc->addPropertyToCatalogue( service,
                                                  stringprop ) );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }


   StatusCode Job::getProperty( const std::string& service,
                                const std::string& property,
                                std::string& value ) const {

      // Make sure that the object is initialised already.
      if( ! m_appMgr.isValid() ) {
         ATH_MSG_FATAL( "Object is not yet configured" );
         return StatusCode::FAILURE;
      }

      // Find the service.
      IProperty* svc = m_svcLoc->service( service ).as< IProperty >();
      if( ! svc ) {
         ATH_MSG_ERROR( "Couldn't find service \"" << service << "\"" );
         return StatusCode::FAILURE;
      }

      // Get its property.
      ATH_CHECK( svc->getProperty( property, value ) );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode Job::setProperty( const std::string& property,
                                const std::string& value ) {

      // Make sure that the object is initialised already.
      if( ! m_appMgr.isValid() ) {
         ATH_MSG_FATAL( "Object is not yet configured" );
         return StatusCode::FAILURE;
      }

      // Set the property.
      ATH_CHECK( m_propMgr->setProperty( property, value ) );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode Job::getProperty( const std::string& property,
                                std::string& value ) const {

      // Make sure that the object is initialised already.
      if( ! m_appMgr.isValid() ) {
         ATH_MSG_FATAL( "Object is not yet configured" );
         return StatusCode::FAILURE;
      }

      // Get the property.
      ATH_CHECK( m_propMgr->getProperty( property, value ) );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   bool Job::msgLevel( MSG::Level lvl ) const {

      // Have the same behaviour as CommonMessaging.
      return msgStream().level() <= lvl;
   }

   MsgStream& Job::msgStream() const {

      static MsgStream msg( m_msgSvc, "ASync::Job" );
      return msg;
   }

   MsgStream& Job::msgStream( MSG::Level lvl ) const {

      // Return the stream object, set to the appropriate message level.
      return ( msgStream() << lvl );
   }

} // namespace ASync
