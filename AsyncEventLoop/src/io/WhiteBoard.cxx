// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "WhiteBoard.h"

// Project include(s).
#include "AsyncBaseComps/MessagingMacros.h"
#include "AsyncBaseComps/CheckMacros.h"

// Gaudi include(s).
#include <GaudiKernel/GaudiException.h>
#include <GaudiKernel/DataObject.h>

// ROOT include(s).
#include <ThreadLocalStorage.h>
#include <TClass.h>

DECLARE_COMPONENT( ASync::WhiteBoard )

namespace {

   /// Function collecting the names of the objects available from the input
   DataObjIDColl availableObjects( ASync::TEvent& event ) {

      // Get the input event format object:
      const xAOD::EventFormat* ef = event.inputEventFormat();

      // Check which objects are actually in the input file, and remember their
      // names.
      DataObjIDColl result;
      for( auto itr : *ef ) {

         // The event format element:
         const xAOD::EventFormatElement& efe = itr.second;

         // Just skip all branches ending in "Aux.":
         if( efe.branchName().rfind( "Aux." ) ==
             ( efe.branchName().size() - 4 ) ) {
            continue;
         }
         // Also skip dynamic branches:
         if( efe.parentName() != "" ) {
            continue;
         }
         // Skip the HLT objects for now, as they are not up to date in the
         // master branch of athena:
         if( efe.branchName().find( "HLT_" ) == 0 ) {
            continue;
         }

         // Don't bother if the branch is not available:
         if( ! event.m_inTree->GetBranch( efe.branchName().c_str() ) ) {
            continue;
         }

         // Check if the class in question is known:
         static const bool LOAD = true;
         static const bool SILENT = true;
         ::TClass* cl = ::TClass::GetClass( efe.className().c_str(), LOAD,
                                            SILENT );
         if( ! cl ) {
            continue;
         }

         // Don't add auxiliary store objects:
         if( cl->InheritsFrom( "SG::IConstAuxStore" ) ) {
            continue;
         }

         // Check if we have a type-info for this class:
         const std::type_info* ti = cl->GetTypeInfo();
         if( ! ti ) {
            continue;
         }

         // Skip objects that the event doesn't actually contain:
         static const bool METADATA = false;
         if( ! event.contains( efe.branchName(), *ti, METADATA ) ) {
            continue;
         }

         // Apparently this is an object that the input file can provide:
         result.insert( efe.branchName() );
      }

      // Return the collected list:
      return result;
   }

} // private namespace

namespace ASync {

   /// The partition used at the moment in a given thread
   TTHREAD_TLS( WhiteBoard::Partition* ) s_currentPartition( nullptr );

   StatusCode WhiteBoard::initialize() {

      // First, initialise the base class.
      ATH_CHECK( Service::initialize() );

      // Tell the user what's happening.
      ATH_MSG_INFO( "Initialising the asynchronous white board" );
      if( m_inputFiles.value().size() ) {
         ATH_MSG_INFO( "Reading input from file(s):" );
         for( const std::string& fname : m_inputFiles.value() ) {
            ATH_MSG_INFO( "  - " << fname );
         }
      } else {
         ATH_MSG_INFO( "Not using input from files" );
      }

      // Check that the user asked for a reasonable number of slots:
      if( m_slots < 1 ) {
         ATH_MSG_ERROR( "Invalid number of slots (" << m_slots
                        << ") requested" );
         return StatusCode::FAILURE;
      }
      ATH_MSG_INFO( "Configuring WhiteBoard with " << m_slots.value()
                    << " event slots" );

      // Set up the partitions.
      for( int i = 0; i < m_slots; ++i ) {
         // Set up the new partition.
         m_partitions.emplace_back( new Partition() );
         // Add it as a free slot into the internal queue.
         m_freeSlots.push( i );
         // If input files were specified:
         if( m_inputFiles.value().size() ) {
            // Add the input file(s) to the chain.
            TChain& chain = m_partitions.back()->m_chain;
            for( const std::string& fname : m_inputFiles ) {
               if( ! chain.Add( fname.c_str() ) ) {
                  ATH_MSG_ERROR( "Couldn't add \"" << fname
                                 << "\" to the input TChain" );
                  return StatusCode::FAILURE;
               }
            }
            // Tell TEvent to read from this input.
            TEvent& event = m_partitions.back()->m_event;
            if( ! event.readFrom( &chain ).isSuccess() ) {
               ATH_MSG_ERROR( "Couldn't connect TEvent object to the input "
                              "TChain" );
               return StatusCode::FAILURE;
            }
            // Load the first event, and scan the input for available objects.
            if( event.getEntry( 0 ) < 0 ) {
               ATH_MSG_ERROR( "Couldn't load the first entry from the input" );
               return StatusCode::FAILURE;
            }
            m_partitions.back()->m_inputObjs = ::availableObjects( event );
            m_partitions.back()->m_objs = m_partitions.back()->m_inputObjs;
         }
      }

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode WhiteBoard::selectStore( size_t partitionIndex ) {

      // Make sure that the requested index is valid.
      if( partitionIndex >= m_partitions.size() ) {
         ATH_MSG_ERROR( "Requested partition index " << partitionIndex
                        << ", while only " << m_partitions.size()
                        << " partitions are available" );
         return StatusCode::FAILURE;
      }

      // Update the thread local variable.
      s_currentPartition = m_partitions[ partitionIndex ].get();

      // Set the event and store objects of the partition active in this
      // thread.
      s_currentPartition->m_event.setActive();
      s_currentPartition->m_store.setActive();

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode WhiteBoard::clearStore( size_t partitionIndex ) {

      // Make sure that the requested index is valid.
      if( partitionIndex >= m_partitions.size() ) {
         ATH_MSG_ERROR( "Requested partition index " << partitionIndex
                        << ", while only " << m_partitions.size()
                        << " partitions are available" );
         return StatusCode::FAILURE;
      }

      // Access the requested partition.
      Partition& part = *( m_partitions[ partitionIndex ] );

      // Acquire a lock on the partition.
      std::lock_guard< std::recursive_mutex > lock( part.m_mutex );

      // Clear the store.
      part.m_store.clear();
      part.m_objs = part.m_inputObjs;

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode WhiteBoard::setNumberOfStores( size_t slots ) {

      // Make sure that the user is not asking for anything unreasonable.
      if( ( static_cast< int >( slots ) != m_slots ) &&
          ( FSMState() == Gaudi::StateMachine::INITIALIZED ) ) {
         ATH_MSG_FATAL( "Application already initialised, can't change the "
                        "number of stores anymore" );
         return StatusCode::FAILURE;
      }

      // Update the number of slots.
      m_slots = slots;

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   size_t WhiteBoard::getNumberOfStores() const {

      return m_slots;
   }

   bool WhiteBoard::exists( const DataObjID& id ) {

      // Make sure that we have a partition set up for the current thread.
      if( ! s_currentPartition ) {
         ATH_MSG_ERROR( "No partition is set up for the current thread" );
         return false;
      }

      // Simply look for the data object in the list maintained in the
      // current parition.
      if( s_currentPartition->m_objs.find( id ) !=
          s_currentPartition->m_objs.end() ) {
         return true;
      } else {
         return false;
      }
   }

   size_t WhiteBoard::allocateStore( int evtnumber ) {

      // Take the next free slot from the internal queue.
      size_t slot = std::string::npos;
      if( m_freeSlots.try_pop( slot ) ) {

         // Check that we received something reasonable.
         if( ( slot == std::string::npos ) ||
             ( slot >= m_partitions.size() ) ) {
            ATH_MSG_ERROR( "Slot number is not valid" );
            return std::string::npos;
         }

         // Check that the slot is indeed free.
         if( m_partitions[ slot ]->m_eventNumber != -1  ){
            ATH_MSG_ERROR( "Slot is not properly freed" );
            return std::string::npos;
         }

         // Assign the slot to the received event number.
         m_partitions[ slot ]->m_eventNumber = evtnumber;

         // If input files were set up, switch the partition to the right
         // input event, and declare all xAOD objects from the file available
         // for consumption.
         if( m_inputFiles.value().size() ) {
            m_partitions[ slot ]->m_event.getEntry( evtnumber );
            m_partitions[ slot ]->m_objs = m_partitions[ slot ]->m_inputObjs;
         }
         return slot;
      }

      // We should never get here...
      ATH_MSG_ERROR( "Couldn't find an available partition" );
      return std::string::npos;
   }

   StatusCode WhiteBoard::freeStore( size_t partitionIndex ) {

      // Check that the partition index makes sense.
      if( partitionIndex >= m_partitions.size() ){
         ATH_MSG_ERROR( "Partition index is not valid" );
         return StatusCode::FAILURE;
      }

      // Mark the partition unused, and add its slot to the list of
      // available slots.
      m_partitions[ partitionIndex ]->m_eventNumber = -1;
      m_freeSlots.push( partitionIndex );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   size_t WhiteBoard::getPartitionNumber( int eventnumber ) const {

      // Look for the partition with this event number.
      for( size_t i = 0; i < m_partitions.size(); ++i ) {
         if( m_partitions[ i ]->m_eventNumber == eventnumber ) {
            return i;
         }
      }

      // If we didn't find the requested partition:
      ATH_MSG_ERROR( "Couldn't find partition for event #" << eventnumber );  
      return std::string::npos;
   }

   size_t WhiteBoard::freeSlots() {

      return m_freeSlots.unsafe_size();
   }

   const std::type_info* WhiteBoard::type( const std::string& name ) {

      // Make sure that we have a partition set up for the current thread.
      if( ! s_currentPartition ) {
         ATH_MSG_ERROR( "No partition is set up for the current thread" );
         return nullptr;
      }

      // Acquire a lock on the partition.
      std::lock_guard< std::recursive_mutex >
         lock( s_currentPartition->m_mutex );

      // Try to get the type from the transient store first.
      const std::type_info* ti = s_currentPartition->m_store.type( name );
      if( ti ) {
         return ti;
      }

      // If it was not there, then ask the event object instead.
      return s_currentPartition->m_event.type( name );
   }

   bool WhiteBoard::contains( const std::string& name,
                              const std::type_info& ti ) const {

      // Make sure that we have a partition set up for the current thread.
      if( ! s_currentPartition ) {
         ATH_MSG_ERROR( "No partition is set up for the current thread" );
         return false;
      }

      // Acquire a lock on the partition.
      std::lock_guard< std::recursive_mutex >
         lock( s_currentPartition->m_mutex );

      // Simply ask the event store.
      static const bool METADATA = false;
      return s_currentPartition->m_event.contains( name, ti, METADATA );
   }

   const void* WhiteBoard::getObject( const std::string& name,
                                      const std::type_info& ti ) {

      // Make sure that we have a partition set up for the current thread.
      if( ! s_currentPartition ) {
         ATH_MSG_ERROR( "No partition is set up for the current thread" );
         return nullptr;
      }

      // Acquire a lock on the partition.
      std::lock_guard< std::recursive_mutex >
         lock( s_currentPartition->m_mutex );

      // Look among the output objects first.
      const void* result =
         s_currentPartition->m_event.getOutputObject( name, ti );
      // Check if it succeeded.
      if( ! result ) {
         // Try the input then.
         result = s_currentPartition->m_event.getInputObject( name, ti );
         if( ! result ) {
            ATH_MSG_ERROR( "Couldn't (const) retrieve \"" << name << "\"" );
            return nullptr;
         }
      } else {
         // Even if there is an output object with this key, it may
         // be an input object that was copied to the output. So let's
         // try to silently retrieve an input object as well. This makes
         // sure that the input/output object is updated for the current
         // event.
         static const bool SILENT = true;
         s_currentPartition->m_event.getInputObject( name, ti, SILENT );
      }

      // We were successful.
      return result;
   }

   StatusCode WhiteBoard::record( void* obj, const std::type_info& ti,
                                  const std::string& name ) {

      // Make sure that we have a partition set up for the current thread.
      if( ! s_currentPartition ) {
         ATH_MSG_ERROR( "No partition is set up for the current thread" );
         return StatusCode::FAILURE;
      }

      // Acquire a lock on the partition.
      std::lock_guard< std::recursive_mutex >
         lock( s_currentPartition->m_mutex );

      // Helper accessor to the store.
      TStore& store = s_currentPartition->m_store;

      /*
      // Check if it's possible to record the object with a dictionary.
      xAOD::TReturnCode result =
         store.record( obj, name, SG::normalizedTypeinfoName( ti ) );
      // If it's a success or a failure, let's stop here. Only go on for a
      // recoverable error:
      if( result.isSuccess() ) {
         // Remember that we recorded a new object.
         s_currentPartition->m_objs.insert( DataObjID( name ) );
         // Return gracefully.
         return StatusCode::SUCCESS;
      }
      if( result.isFailure() ) {
         ATH_MSG_ERROR( "Couldn't record object \"" << name << "\"" );
         return StatusCode::FAILURE;
      }
      */

      // Apparently we don't have a dictionary for this type. Let's fall back
      // to a slightly dumber implementation then...

      // Leave it to the non-template function to record the object.
      if( ! store.record( obj, name, ti ).isSuccess() ) {
         ATH_MSG_ERROR( "Failed to record object \"" << name << "\"" );
         return StatusCode::FAILURE;
      }

      // Remember that we recorded a new object.
      s_currentPartition->m_objs.insert( DataObjID( name ) );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

} // namespace ASync
