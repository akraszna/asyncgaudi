// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "TStore.h"

// Athena include(s).
#include "xAODRootAccess/tools/THolder.h"
#include "xAODRootAccess/tools/Message.h"

// ROOT include(s).
#include <TClass.h>

namespace ASync {

   xAOD::TReturnCode TStore::record( void* obj, const std::string& key,
                                     const std::string& classname,
                                     ::Bool_t isOwner ) {

      // Leave the work to the base class:
      return xAOD::TStore::record( obj, key, classname, isOwner );
   }

   xAOD::TReturnCode TStore::record( void* obj, const std::string& key,
                                     const std::type_info& ti ) {

      // Leave the work to the base class:
      return xAOD::TStore::record( obj, key, ti );
   }

   const std::type_info* TStore::type( const std::string& name ) const {

      // Check if we know about this object:
      auto itr = m_objects.find( name );
      if( itr == m_objects.end() ) {
         return nullptr;
      }

      // If yes, get the std::type_info object describing it:
      const xAOD::THolder* holder = itr->second;
      if( holder->getTypeInfo() ) {
         return holder->getTypeInfo();
      }
      if( holder->getClass() ) {
         return holder->getClass()->GetTypeInfo();
      }

      // Something weird happened...
      ::Error( "ASync::TStore::type",
               XAOD_MESSAGE( "Internal problem encountered..." ) );
      return nullptr;
   }

} // namespace ASync
