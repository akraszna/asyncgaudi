// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCEVENTLOOP_TEVENT_H
#define ASYNCEVENTLOOP_TEVENT_H

// Athena include(s).
#include "xAODRootAccess/TEvent.h"

namespace ASync {

   /// TEvent object used internally by @c ASync::WhiteBoard
   ///
   /// @author Attila Kraznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class TEvent : public xAOD::TEvent {

   public:
      /// Inherit the @c xAOD::TEvent constructors
      using xAOD::TEvent::TEvent;

      /// @name Functions made public for the sake of @c AthCUDA::WhiteBoard
      /// @{

      /// Internal function checking if an object is in the input
      ::Bool_t contains( const std::string& key,
                         const std::type_info& ti,
                         ::Bool_t metadata = kFALSE );

      /// Function for retrieving an output object in a non-template way
      void* getOutputObject( const std::string& key,
                             const std::type_info& ti,
                             ::Bool_t metadata = kFALSE ) const;
      /// Function for retrieving an input object in a non-template way
      const void* getInputObject( const std::string& key,
                                  const std::type_info& ti,
                                  ::Bool_t silent = kFALSE,
                                  ::Bool_t metadata = kFALSE );

      /// @}

      /// Function getting the type of a given object
      const std::type_info* type( const std::string& name );

      /// Make the input tree variable public
      using xAOD::TEvent::m_inTree;

   protected:
      // Declare that the functions not implemented here should be taken from
      // the xAOD::TEvent base class.
      using xAOD::TEvent::getOutputObject;
      using xAOD::TEvent::getInputObject;

   }; // class TEvent

} // namespace ASync

#endif // ASYNCEVENTLOOP_TEVENT_H
