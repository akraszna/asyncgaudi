// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCEVENTLOOP_WHITEBOARD_H
#define ASYNCEVENTLOOP_WHITEBOARD_H

// Local include(s).
#include "TEvent.h"
#include "TStore.h"

// Project include(s).
#include "AsyncEventLoopInterfaces/IDataStore.h"

// Gaudi include(s).
#include <GaudiKernel/Service.h>
#include <GaudiKernel/IHiveWhiteBoard.h>

// ROOT include(s).
#include <TChain.h>

// TBB include(s).
#include <tbb/concurrent_queue.h>

// System include(s).
#include <vector>
#include <memory>
#include <mutex>
#include <string>

namespace ASync {

   /// White board used in the asynchronous scheduling tests
   ///
   /// This is meant specifically for reading/handling xAOD objects using
   /// xAODRootAccess.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class WhiteBoard : public extends< Service, IHiveWhiteBoard, IDataStore > {

   public:
      /// Inherit the base class's constructor(s)
      using extends::extends;

      /// @name Function(s) inherited from IService
      /// @{

      /// Function initialising the white board
      virtual StatusCode initialize() override;

      /// @}

      /// @name Function(s) inherited from @c IHiveWhiteBoard
      /// @{

      /// Activate a given 'slot' in a given thread
      virtual StatusCode selectStore( size_t partitionIndex ) override;

      /// Clear the specified slot
      virtual StatusCode clearStore( size_t partitionIndex ) override;

      /// Set the number of slots
      virtual StatusCode setNumberOfStores( size_t slots ) override;
      /// Get the number of slots
      virtual size_t getNumberOfStores() const override;

      /// Check if a data object exists in the store.
      virtual bool exists( const DataObjID& ) override;

      /// Allocate a store partition for a new event
      virtual size_t allocateStore( int evtnumber ) override;

      /// Free a store partition
      virtual StatusCode freeStore( size_t partitionIndex ) override;

      /// Get the partition number corresponding to a given event
      virtual size_t getPartitionNumber( int eventnumber ) const override;

      /// Get the number of free slots
      virtual size_t freeSlots() override;

      /// @}

      /// @name Function(s) inherited from @c ASync::IDataStore
      /// @{

      /// Function retrieving the (real) type of the object in the store
      const std::type_info* type( const std::string& name ) override;

      /// Function checking whether a given object exists or not
      bool contains( const std::string& name,
                     const std::type_info& ti ) const override;

      /// Function providing the pointer for a requested object
      const void* getObject( const std::string& name,
                             const std::type_info& ti ) override;

      /// Function recording the object into the data store
      StatusCode record( void*, const std::type_info& ti,
                         const std::string& name ) override;

      /// @}

      /// Struct describing one partition in the white board
      struct Partition {
         /// Event number processed by this partition
         int m_eventNumber{ -1 };
         /// The in-memory store for the partition
         TStore m_store;
         /// Chain set up to read input from xAOD files
         ::TChain m_chain{ "CollectionTree" };
         /// The file reader object for the partition
         TEvent m_event;
         /// Data objects coming from the input file
         DataObjIDColl m_inputObjs;
         /// All data objects available in the partition
         DataObjIDColl m_objs;
         /// Mutex for accessing the partition
         std::recursive_mutex m_mutex;
      }; // struct Partition

   private:
      /// @name White board properties
      /// @{

      /// Number of event slots/partitions to provide
      Gaudi::Property< int > m_slots{ this, "EventSlots", 1,
         "Number of event slots" };

      /// Input file names to read xAOD events from
      Gaudi::Property< std::vector< std::string > >
         m_inputFiles{ this, "InputFiles", std::vector< std::string >(),
            "Names of the input files to use" };

      /// @}

      /// Partitions handled by the white board
      std::vector< std::unique_ptr< Partition > > m_partitions;

      /// Free slot identifiers
      tbb::concurrent_queue< size_t > m_freeSlots;

   }; // class WhiteBoard

} // namespace ASync

#endif // ASYNCEVENTLOOP_WHITEBOARD_H
