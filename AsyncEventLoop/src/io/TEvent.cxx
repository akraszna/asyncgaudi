// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "TEvent.h"

// Athena include(s).
#include "xAODRootAccess/tools/TObjectManager.h"
#include "xAODRootAccess/tools/THolder.h"
#include "xAODRootAccess/tools/Message.h"

// ROOT include(s).
#include <TClass.h>

namespace ASync {

   ::Bool_t TEvent::contains( const std::string& key,
                              const std::type_info& ti,
                              ::Bool_t metadata ) {

      // Leave the heavy-lifting up to the base class:
      return xAOD::TEvent::contains( key, ti, metadata );
   }

   void* TEvent::getOutputObject( const std::string& key,
                                  const std::type_info& ti,
                                  ::Bool_t metadata ) const {

      // Leave the heavy-lifting up to the base class:
      return xAOD::TEvent::getOutputObject( key, ti, metadata );
   }

   const void* TEvent::getInputObject( const std::string& key,
                                       const std::type_info& ti,
                                       ::Bool_t silent,
                                       ::Bool_t metadata ) {

      // Leave the heavy-lifting up to the base class:
      return xAOD::TEvent::getInputObject( key, ti, silent, metadata );
   }

   const std::type_info* TEvent::type( const std::string& name ) {

      // Ask for the object with a dummy type. Triggering its loading if
      // necessary.
      static const bool SILENT = true;
      static const bool METADATA = false;
      this->getInputObject( name, typeid( int ), SILENT, METADATA );

      // Now look for the object amongst the input objects:
      auto itr = m_inputObjects.find( name );
      if( itr == m_inputObjects.end() ) {
         return nullptr;
      }

      // It needs to be an object manager:
      const xAOD::TObjectManager* mgr =
         dynamic_cast< const xAOD::TObjectManager* >( itr->second );
      if( ! mgr ) {
         ::Error( "ASync::TEvent::type",
                  XAOD_MESSAGE( "Internal problem encountered" ) );
         return nullptr;
      }
      const xAOD::THolder* holder = mgr->holder();

      // Get the type info object from it:
      if( holder->getTypeInfo() ) {
         return holder->getTypeInfo();
      } else if( holder->getClass() ) {
         return holder->getClass()->GetTypeInfo();
      }

      // Something weird happened...
      ::Error( "ASync::TEvent::type",
               XAOD_MESSAGE( "Internal problem encountered..." ) );
      return nullptr;
   }

} // namespace ASync
