// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCEVENTLOOP_DATASTORELOADERALG_H
#define ASYNCEVENTLOOP_DATASTORELOADERALG_H

// Project include(s).
#include "AsyncBaseComps/Algorithm.h"

namespace ASync {

   /// Algorithm declaring objects to the scheduler that come from the input
   ///
   /// Note that the algorithm doesn't actually do anything. It is up to the
   /// scheduler to attach all unmet data dependencies of the algorithms in
   /// the job to this algorithm.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class DataStoreLoaderAlg : public Algorithm {

   public:
      /// Inherit the algorithm constructor(s).
      using Algorithm::Algorithm;

      /// Function executing the algorithm
      virtual StatusCode execute( const EventContext& ) const override;

   }; // class DataStoreLoaderAlg

} // namespace ASync

#endif // ASYNCEVENTLOOP_DATASTORELOADERALG_H
