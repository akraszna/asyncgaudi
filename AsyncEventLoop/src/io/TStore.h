// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCEVENTLOOP_TSTORE_H
#define ASYNCEVENTLOOP_TSTORE_H

// Athena include(s).
#include "xAODRootAccess/TStore.h"

namespace ASync {

   /// TStore object used internally by @c ASync::WhiteBoard
   ///
   /// @author Attila Kraznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class TStore : public xAOD::TStore {

   public:
      /// Inherit the xAOD::TStore constructor(s)
      using xAOD::TStore::TStore;

      /// @name Functions made public for the sake of @c AthCUDA::WhiteBoard
      /// @{

      /// Function recording an object that has a dictionary available
      xAOD::TReturnCode record( void* obj, const std::string& key,
                                const std::string& classname,
                                ::Bool_t isOwner = kTRUE );
      /// Function recording an object that has no dictionary
      xAOD::TReturnCode record( void* obj, const std::string& key,
                                const std::type_info& ti );

      /// @}

      /// Function getting the type of a given object
      const std::type_info* type( const std::string& name ) const;

   }; // class TStore

} // namespace ASync

#endif // ASYNCEVENTLOOP_TSTORE_H
