// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "DataStoreLoaderAlg.h"

// Declare the algorithm to the framework.
DECLARE_COMPONENT( ASync::DataStoreLoaderAlg )

namespace ASync {

   StatusCode DataStoreLoaderAlg::execute( const EventContext& ) const {

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

} // namespace ASync
