// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "EventStoreContent.h"

// Project include(s).
#include "AsyncBaseComps/MessagingMacros.h"

// Gaudi include(s).
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/DataObjID.h>
#include <GaudiKernel/MsgStream.h>

// System include(s).
#include <algorithm>
#include <cassert>

namespace {

   /// Helper function setting bits representing data object IDs
   void setBits( boost::dynamic_bitset<>& bitset,
                 const std::vector< DataObjID >& allObjectsVec,
                 const DataObjIDColl& objects ) {

      // A security check.
      assert( bitset.size() == allObjectsVec.size() );

      // Add all objects to the bitset.
      for( const DataObjID& obj : objects ) {
         // Find the index of this object.
         auto itr = std::find( allObjectsVec.begin(), allObjectsVec.end(),
                               obj );
         assert( itr != allObjectsVec.end() );
         const std::size_t index = itr - allObjectsVec.begin();
         assert( index < bitset.size() );
         // Set the index on the bitset.
         bitset.set( index );
      }
   }

}

namespace ASync {

   EventStoreContent::
   EventStoreContent( const std::vector< Gaudi::Algorithm* > algs,
                      MsgStream& msg )
   : m_algDependencies( algs.size() ), m_algOutputs( algs.size() ),
     m_storeContent(), m_storeContentMutex(), m_msg( &msg ) {

      // Collect all data objects produced / consumed by the algorithms.
      DataObjIDColl allObjectsSet;
      for( Gaudi::Algorithm* alg : algs ) {
         allObjectsSet.insert( alg->inputDataObjs().begin(),
                               alg->inputDataObjs().end() );
         allObjectsSet.insert( alg->extraInputDeps().begin(),
                               alg->extraInputDeps().end() );
         allObjectsSet.insert( alg->outputDataObjs().begin(),
                               alg->outputDataObjs().end() );
         allObjectsSet.insert( alg->extraOutputDeps().begin(),
                               alg->extraOutputDeps().end() );
      }
      // Assign an index to each of them.
      std::vector< DataObjID > allObjectsVec;
      allObjectsVec.reserve( allObjectsSet.size() );
      for( const DataObjID& obj : allObjectsSet ) {
         allObjectsVec.push_back( obj );
      }

      // Now go through the algorithms again, and set up the bitsets
      // representing their inputs and outputs.
      for( std::size_t i = 0; i < algs.size(); ++i ) {
         m_algDependencies[ i ].resize( allObjectsVec.size() );
         m_algOutputs[ i ].resize( allObjectsVec.size() );
         ::setBits( m_algDependencies[ i ], allObjectsVec,
                    algs[ i ]->inputDataObjs() );
         ::setBits( m_algDependencies[ i ], allObjectsVec,
                    algs[ i ]->extraInputDeps() );
         ::setBits( m_algOutputs[ i ], allObjectsVec,
                    algs[ i ]->outputDataObjs() );
         ::setBits( m_algOutputs[ i ], allObjectsVec,
                    algs[ i ]->extraOutputDeps() );
      }

      // Finally, set the event content bitset to the right size.
      m_storeContent.resize( allObjectsVec.size() );
   }

   EventStoreContent::EventStoreContent( const EventStoreContent& parent )
   : m_algDependencies( parent.m_algDependencies ),
     m_algOutputs( parent.m_algOutputs ),
     m_storeContent( parent.m_storeContent ),
     m_storeContentMutex(),
     m_msg( parent.m_msg ) {

   }

   EventStoreContent&
   EventStoreContent::operator=( const EventStoreContent& rhs ) {

      // Check for self-assignment.
      if( this == &rhs ) {
         return *this;
      }

      // Do the assignment.
      m_algDependencies = rhs.m_algDependencies;
      m_algOutputs = rhs.m_algOutputs;
      m_storeContent = rhs.m_storeContent;
      m_msg = rhs.m_msg;

      // Return this object.
      return *this;
   }

   StatusCode EventStoreContent::setAlgExecuted( std::size_t alg ) {

      // Check that the algorithm index is valid.
      if( alg >= m_algOutputs.size() ) {
         auto msgStream = [ this ]( MSG::Level level ) -> MsgStream& {
            return ( *( this->m_msg ) << level );
         };
         ATH_MSG_ERROR( "Invalid algorithm index (" << alg << " >= "
                        << m_algOutputs.size() << ") received" );
         return StatusCode::FAILURE;
      }

      // Do the deed.
      std::lock_guard< std::mutex > guard( m_storeContentMutex );
      m_storeContent |= m_algOutputs[ alg ];
      return StatusCode::SUCCESS;
   }

   bool EventStoreContent::isAlgExecutable( std::size_t alg ) const {

      // Check that the algorithm index is valid.
      if( alg >= m_algDependencies.size() ) {
         auto msgStream = [ this ]( MSG::Level level ) -> MsgStream& {
            return ( *( this->m_msg ) << level );
         };
         ATH_MSG_ERROR( "Invalid algorithm index (" << alg << " >= "
                        << m_algDependencies.size() << ") received" );
         return false;
      }

      // Make the check.
      std::lock_guard< std::mutex > guard( m_storeContentMutex );
      return m_algDependencies[ alg ].is_subset_of( m_storeContent );
   }

   void EventStoreContent::reset() {

      m_storeContent.reset();
   }

} // namespace ASync
