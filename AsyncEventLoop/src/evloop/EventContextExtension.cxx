// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s):
#include "EventContextExtension.h"

namespace ASync {

   EventContextExtension::EventContextExtension( run_number_t run,
                                                 event_number_t event )
   : m_run( run ), m_event( event ) {

   }

   EventContextExtension::run_number_t EventContextExtension::run() const {

      return m_run;
   }

   EventContextExtension::event_number_t EventContextExtension::event() const {

      return m_event;
   }

   bool EventContextExtension::heartbeat() const {

      return m_heartbeat;
   }

   void EventContextExtension::setHeartbeat( bool value ) {

      m_heartbeat = value;
      return;
   }

} // namespace ASync
