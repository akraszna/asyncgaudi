// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "SchedulerSvc.h"
#include "AlgExecutionTask.h"

// Project include(s).
#include "AsyncBaseComps/Algorithm.h"

// Gaudi include(s).
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/Bootstrap.h>
#include <GaudiKernel/ThreadLocalContext.h>

// TBB include(s).
#include <tbb/task.h>

// System include(s).
#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <chrono>
#include <fstream>
#include <sstream>

DECLARE_COMPONENT( ASync::SchedulerSvc )

namespace {

   struct DataObjIDSorter {
      bool operator()( const DataObjID* a, const DataObjID* b ) {
         return a->fullKey() < b->fullKey();
      }
   };

   // Sort a DataObjIDColl in a well-defined, reproducible manner.
   // Used for making debugging dumps.
   std::vector< const DataObjID* >
   sortedDataObjIDColl( const DataObjIDColl& coll ) {
      std::vector<const DataObjID*> v;
      v.reserve( coll.size() );
      for( const DataObjID& id : coll ) {
         v.push_back( &id );
      }
      std::sort( v.begin(), v.end(), DataObjIDSorter() );
      return v;
   }

} // namespace

namespace ASync {

   /// Define a useful shortcut for the algorithm execution state enumeration
   using AState = AlgExecutionStates::State;

   StatusCode SchedulerSvc::initialize() {

      // Initialise the base class.
      ATH_CHECK( Service::initialize() );

      // Get hold of the thread pool svc.
      ATH_CHECK( m_threadPoolSvc.retrieve() );

      // Activate the scheduler in another thread.
      ATH_MSG_INFO( "Activating scheduler in a separate thread" );
      m_thread = std::thread( [ this ]() { this->execute(); } );

      // Wait for the service thread to have finished with its own
      // initialisation.
      while( m_isActive != ACTIVE ) {
         if( m_isActive == FAILURE ) {
            ATH_MSG_FATAL( "Terminating initialisation" );
            return StatusCode::FAILURE;
         } else {
            ATH_MSG_DEBUG( "Waiting for AvalancheSchedulerSvc to activate" );
            std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
         }
      }

      // Retrieve the rest of the necessary services.
      ATH_CHECK( m_algResourcePool.retrieve() );
      ATH_CHECK( m_algExecStateSvc.retrieve() );
      ATH_CHECK( m_whiteboard.retrieve() );

      // Get the MaxEventsInFlight parameters from the number of WB stores
      const std::size_t maxEventsInFlight = m_whiteboard->getNumberOfStores();
      // Set an initial value for the number of free slots
      m_freeSlots = maxEventsInFlight;

      // Get the list of algorithms
      const std::list< IAlgorithm* >& algos = m_algResourcePool->getFlatAlgList();
      if( algos.size() > 0 ) {
         ATH_MSG_INFO( "Found " << algos.size() << " algorithm(s)" );
      } else {
         ATH_MSG_ERROR( "No algorithms found" );
         return StatusCode::FAILURE;
      }

      // Make sure that all algorithms are of type ASync::Algorithm. As this
      // scheduler only works with those.
      std::vector< Gaudi::Algorithm* > gaudiAlgs;
      gaudiAlgs.reserve( algos.size() );
      for( IAlgorithm* alg : algos ) {
         Gaudi::Algorithm* gaudiAlg =
            dynamic_cast< Gaudi::Algorithm* >( alg );
         if( gaudiAlg == nullptr ) {
            ATH_MSG_ERROR( "Algorithm \"" << alg->name()
                           << " does not inherit from Gaudi::Algorithm" );
            return StatusCode::FAILURE;
         }
         gaudiAlgs.push_back( gaudiAlg );
      }

      // Dependencies...
      // 1) Look for handles in algo, if none
      // 2) Assume none are required

      DataObjIDColl globalInp, globalOutp;

      // Figure out the full list of outputs.
      for( Gaudi::Algorithm* alg : gaudiAlgs ) {
         for( auto id : alg->outputDataObjs() ) {
            auto r = globalOutp.insert( id );
            if( r.second == false ) {
               ATH_MSG_WARNING( "Multiple algorithms declare " << id
                                << " as output! Could be a single instance in "
                                   "multiple paths though, or control flow "
                                   "may guarantee only one runs...!" );
            }
         }
      }

      std::ostringstream ostdd;
      ostdd << "Data dependencies for algorithm(s):";

      std::map< std::string, DataObjIDColl > algosDependenciesMap;
      for( Gaudi::Algorithm* alg : gaudiAlgs ) {

         ostdd << "\n  " << alg->name();

         DataObjIDColl algoDependencies;
         if( ( ! alg->inputDataObjs().empty() ) ||
             ( ! alg->outputDataObjs().empty() ) ) {

            for( const DataObjID* id :
                 sortedDataObjIDColl( alg->inputDataObjs() ) ) {

               ostdd << "\n    o INPUT  " << *id;
               algoDependencies.insert( *id );
               globalInp.insert( *id );
            }
            for( const DataObjID* id :
                 sortedDataObjIDColl( alg->outputDataObjs() ) ) {

               ostdd << "\n    o OUTPUT " << *id;
            }
         } else {
            ostdd << "\n      none";
         }
         algosDependenciesMap[ alg->name() ] = algoDependencies;
      }

      if( m_showDataDeps ) {
         ATH_MSG_INFO( ostdd.str() );
      }

      // If the user requested a .dot graph with the data-flow dependencies,
      // let's deliver it.
      if( m_dataFlowOutput.value() != "" ) {

         // Open the output text file.
         std::ofstream dotFile( m_dataFlowOutput.value(), std::ios_base::trunc );

         // Put a header into it.
         dotFile << "digraph AsyncGaudiJob {" << std::endl;

         // Data objects that have already been declared in the .dot file.
         std::set< std::string > declaredObjects;

         // Dummy algorithm object used to check which properties come from the
         // base class, and which from the specific algorithms. So that only
         // the specific properties would be printed.
         ASync::Algorithm dummyAlg( "DummyAlg", Gaudi::svcLocator() );

         // Loop over all algorithms, and declare them, and all the data that
         // they use and produce in the .dot file.
         for( Gaudi::Algorithm* alg : gaudiAlgs ) {

            // Construct the full algorithm name/description.
            std::ostringstream algNameDesc;
            algNameDesc << "\"";
            algNameDesc << alg->type() << "/" << alg->name() << "\\n\\n";
            for( const Gaudi::Details::PropertyBase* prop :
                 alg->getProperties() ) {
               if( ( ! Gaudi::Utils::hasProperty( &dummyAlg,
                                                  prop->name() ) ) &&
                   ( prop->name() != "InputKeys" ) &&
                   ( prop->name() != "OutputKeys" ) ) {
                  algNameDesc << prop->name() << ": " << prop->toString()
                              << "\\n";
               }
            }
            algNameDesc << "\"";

            // Set the properties of the algorithm's node.
            dotFile << "    " << algNameDesc.str()
                    << " [shape=\"box\"];" << std::endl;

            // Loop over the input dependencies of the algorithm.
            for( const DataObjID& id : alg->inputDataObjs() ) {
               // Set the properties of this data object if it has not been
               // declared yet.
               if( declaredObjects.insert( id.fullKey() ).second ) {
                  dotFile << "   \"" << id.fullKey() << "\" [shape=\"oval\"];"
                          << std::endl;
               }
               // Declare the data-flow dependency.
               dotFile << "   \"" << id.fullKey() << "\" -> "
                       << algNameDesc.str() << ";" << std::endl;
            }

            // Loop over the output object(s) of the algorithm.
            for( const DataObjID& id : alg->outputDataObjs() ) {
               // Set the properties of this data object if it has not been
               // declared yet.
               if( declaredObjects.insert( id.fullKey() ).second ) {
                  dotFile << "   \"" << id.fullKey() << "\" [shape=\"oval\"];"
                          << std::endl;
               }
               // Declare the data-flow dependency.
               dotFile << "   " << algNameDesc.str()
                       << " -> \"" << id.fullKey() << "\";" << std::endl;
            }
         }

         // Put a footer at the end of the file.
         dotFile << "}" << std::endl;

         // Tell the user what happened.
         ATH_MSG_INFO( "Wrote the data-flow graph to: "
                       << m_dataFlowOutput.value() );
      }

      // Check if we have unmet global input dependencies, and, optionally,
      // heal them by assigning them to the "data loader algorithm".
      if( m_checkDeps ) {

         DataObjIDColl unmetDep;
         for( auto i : globalInp ) {
            if( globalOutp.find( i ) == globalOutp.end() ) {
               unmetDep.insert( i );
            }
         }

         if( unmetDep.size() > 0 ) {
            std::ostringstream ost;
            for ( const DataObjID* o : sortedDataObjIDColl( unmetDep ) ) {
               ost << "\n   o " << *o << "    required by algorithm: ";
               for( const auto& p : algosDependenciesMap ) {
                  if( p.second.find( *o ) != p.second.end() ) {
                     ost << "\n       * " << p.first;
                  }
               }
            }

            if( ! m_dataLoaderAlgName.empty() ) {

               // Find the DataLoader algorithm
               auto itr =
                  std::find_if( gaudiAlgs.begin(), gaudiAlgs.end(),
                                [ this ]( Gaudi::Algorithm* alg ) {
                                   return ( alg->name() ==
                                            this->m_dataLoaderAlgName );
                                } );
               if( itr == gaudiAlgs.end() ) {
                  ATH_MSG_FATAL( "No DataLoader algorithm \""
                                 << m_dataLoaderAlgName.value()
                                 << "\" found, and unmet INPUT dependencies "
                                 << "were detected:\n"
                                 << ost.str() );
                  return StatusCode::FAILURE;
               }

               ATH_MSG_INFO( "Will attribute the following unmet INPUT "
                             "dependencies to \"" << ( *itr )->type() << "/"
                             << ( *itr )->name() << "\" Algorithm"
                             << ost.str() );

               for( auto& id : unmetDep ) {
                  ATH_MSG_DEBUG( "Adding OUTPUT dependency \"" << id
                                 << "\" to " << ( *itr )->type() << "/"
                                 << ( *itr )->name() );
                  ( *itr )->addDependency( id, Gaudi::DataHandle::Writer );
               }

            } else {
               ATH_MSG_FATAL( "Auto DataLoading not requested, and the "
                              "following unmet INPUT dependencies were found:"
                              << ost.str() );
               return StatusCode::FAILURE;
            }

         } else {
            ATH_MSG_INFO( "No unmet INPUT data dependencies were found" );
         }
      }

      // Set up the algorithm index -> name associations.
      m_algNames.reserve( gaudiAlgs.size() );
      for( const Gaudi::Algorithm* alg : gaudiAlgs ) {
         m_algNames.push_back( alg->name() );
      }

      // Set up the EventSlot and EventStoreContent helper objects.
      m_eventSlots.reserve( maxEventsInFlight );
      m_eventStoreContents.reserve( maxEventsInFlight );
      for( std::size_t i = 0; i < maxEventsInFlight; ++i ) {
         m_eventSlots.emplace_back( gaudiAlgs.size(), msgStream() );
         m_eventSlots.back().m_complete = true;
         m_eventStoreContents.emplace_back( gaudiAlgs, msgStream() );
      }

      // Clearly inform the user about the level of concurrency
      ATH_MSG_INFO( " o Number of events in flight: " << maxEventsInFlight );
      ATH_MSG_INFO( " o TBB thread pool size: " << m_threadPoolSize.value() );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode SchedulerSvc::finalize() {

      ATH_CHECK( Service::finalize() );
      stopExecute();

      ATH_MSG_INFO( "Joining Scheduler thread" );
      m_thread.join();

      // Final error check after thread pool termination
      if( m_isActive == FAILURE ) {
         ATH_MSG_ERROR( "Problems in the scheduler thread" );
         return StatusCode::FAILURE;
      }

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode SchedulerSvc::pushNewEvent( EventContext* eventContext ) {

      // Some security checks.
      if( eventContext == nullptr ) {
         ATH_MSG_FATAL( "Event context is nullptr" );
         return StatusCode::FAILURE;
      }
      if( m_freeSlots.load() <= 0 ) {
         ATH_MSG_DEBUG( "A free processing slot could not be found." );
         return StatusCode::FAILURE;
      }

      // Decrease the number of free slots right away.
      --m_freeSlots;

      // Set up an "action" that will add this event context to the ones
      // managed by the scheduler.
      auto action = [ this, eventContext ]() -> StatusCode {

         // Event processing slot forced to be the same as the wb slot
         EventSlot& slot = m_eventSlots[ eventContext->slot() ];
         if( ! slot.m_complete ) {
            ATH_MSG_FATAL( "The slot " << eventContext->slot()
                           << " is supposed to be a finished event but it's "
                              "not" );
            return StatusCode::FAILURE;
         }

         // Assign the event context to that event slot.
         ATH_MSG_DEBUG( "Executing event " << eventContext->evt()
                        << " on slot " << eventContext->slot() );
         slot.reset( std::unique_ptr< EventContext >( eventContext ) );

         // Clear the event store content helper.
         m_eventStoreContents[ eventContext->slot() ].reset();

         // Perform all possible algorithm state transitions.
         ATH_CHECK( updateStates() );

         // Return gracefully.
         return StatusCode::SUCCESS;
      };

      // Kick off the scheduling.
      ATH_MSG_VERBOSE( "Pushing the action to update the scheduler for slot "
                       << eventContext->slot() );
      ATH_MSG_VERBOSE( "Free slots available " << m_freeSlots.load() );
      m_actionsQueue.push( action );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode
   SchedulerSvc::pushNewEvents( std::vector< EventContext* >& eventContexts ) {

      for( EventContext* context : eventContexts ) {
         ATH_CHECK( pushNewEvent( context ) );
      }
      return StatusCode::SUCCESS;
   }

   StatusCode SchedulerSvc::popFinishedEvent( EventContext*& eventContext ) {

      // A security check, to detect if an event should be waited for.
      if( ( m_freeSlots.load() == ( int ) m_whiteboard->getNumberOfStores() )
          || ( m_isActive == INACTIVE ) ) {
         return StatusCode::FAILURE;
      }

      m_finishedEvents.pop( eventContext );
      ++m_freeSlots;
      ATH_MSG_DEBUG( "Popped slot " << eventContext->slot() << " (event "
                     << eventContext->evt() << ")" );
      return StatusCode::SUCCESS;
   }

   StatusCode SchedulerSvc::tryPopFinishedEvent( EventContext*& ctx ) {

      if( m_finishedEvents.try_pop( ctx ) ) {
         ATH_MSG_DEBUG( "Try Pop successful slot " << ctx->slot()
                        << "(event " << ctx->evt() << ")" );
         ++m_freeSlots;
         return StatusCode::SUCCESS;
      }
      return StatusCode::FAILURE;
   }

   unsigned int SchedulerSvc::freeSlots() {

      return std::max( m_freeSlots.load(), 0 );
   }

   StatusCode
   SchedulerSvc::scheduleEventView( const EventContext*,
                                    const std::string&,
                                    std::unique_ptr< EventContext > ) {

      ATH_MSG_FATAL( "This scheduler doesn't support event views" );
      return StatusCode::FAILURE;
   }


   void SchedulerSvc::execute() {

      // Initialise the thread pool.
      if( ! m_threadPoolSvc->initPool( m_threadPoolSize ).isSuccess() ) {
         ATH_MSG_ERROR( "Problems initializing ThreadPoolSvc" );
         m_isActive = FAILURE;
         return;
      }

      // Set the scheduler into an active state.
      m_isActive = ACTIVE;

      // Wait for actions to execute as long as the scheduler is in the
      // active state.
      Action_t action;
      while( ( m_isActive == ACTIVE ) || ( ! m_actionsQueue.empty() ) ) {

         m_actionsQueue.pop( action );
         --m_actionsQueueCount;
         action().ignore();
      }

      // Terminate the thread pool.
      if( ! m_threadPoolSvc->terminatePool().isSuccess() ) {
         ATH_MSG_ERROR( "Problems terminating the thread pool" );
         m_isActive = FAILURE;
      }

      // Tell the user what happened.
      ATH_MSG_INFO( "Deactivated the scheduler" );
      return;
   }

   void SchedulerSvc::stopExecute() {

      if( m_isActive == ACTIVE ) {

         // Set the number of slots available to zero
         m_freeSlots.store( 0 );

         // Empty the action queue
         Action_t action;
         while( m_actionsQueue.try_pop( action ) ) {}
         m_actionsQueueCount = 0;

         // This would be the last action
         m_actionsQueue.push( [ this ]() -> StatusCode {
            ATH_MSG_VERBOSE( "Deactivating scheduler" );
            m_isActive = INACTIVE;
            return StatusCode::SUCCESS;
         } );
      }
      return;
   }

   StatusCode SchedulerSvc::updateStates() {

      // Sort from the oldest to the newest event
      // Prepare a vector of pointers to the slots to avoid copies
      std::vector< EventSlot* > eventSlotsPtrs;
      eventSlotsPtrs.reserve( m_eventSlots.size() );
      for( EventSlot& eSlot : m_eventSlots ) {
         if( eSlot.m_complete == false ) {
            eventSlotsPtrs.push_back( &eSlot );
         }
      }
      std::sort( eventSlotsPtrs.begin(), eventSlotsPtrs.end(),
                 []( EventSlot* a, EventSlot* b ) {
                    return ( a->m_ctx->evt() <
                             b->m_ctx->evt() );
                 } );

      // Iterate over the slots in the right order.
      for( EventSlot* slot : eventSlotsPtrs ) {

         // The current slot's index.
         const std::size_t islot = slot->m_ctx->slot();

         // Perform the CONTROLREADY -> DATAREADY transitions. In the
         // simplest way possible. Whatever already has its data ready, is
         // allowed to go forward...
         for( auto it = slot->m_algStates.begin( AState::CONTROLREADY );
              it != slot->m_algStates.end( AState::CONTROLREADY ); ++it ) {

            if( m_eventStoreContents[ islot ].isAlgExecutable( *it ) ) {
               ATH_MSG_VERBOSE( "Bumping algorithm " << *it << " ("
                                << m_algNames[ *it ]
                                << ") to the DATAREADY state in event "
                                << slot->m_ctx->evt() << " slot " << islot );
               ATH_CHECK( slot->m_algStates.set( *it, AState::DATAREADY ) );
            }
         }

         // Enqueue TBB tasks for the dataready algorithms.
         for( auto it = slot->m_algStates.begin( AState::DATAREADY );
              it != slot->m_algStates.end( AState::DATAREADY ); ++it ) {

            ATH_CHECK( enqueue( *it, *( slot->m_ctx ) ) );
         }

         // Complete the event slot if possible.
         if( ( ! slot->m_algStates.containsAny(
                    { AState::CONTROLREADY, AState::DATAREADY,
                      AState::SCHEDULED, AState::RESOURCELESS } ) ) &&
             ( ! slot->m_complete ) ) {

            // The event is now completed.
            slot->m_complete = true;

            // if the event did not fail, add it to the finished events
            // otherwise it is taken care of in the error handling
            if( m_algExecStateSvc->eventStatus( *( slot->m_ctx ) ) ==
                EventStatus::Success ) {
               ATH_MSG_DEBUG( "Event " << slot->m_ctx->evt()
                              << " finished (slot "
                              << slot->m_ctx->slot() << ")." );
               m_finishedEvents.push( slot->m_ctx.release() );
            } else {
               ATH_MSG_ERROR( "Event " << slot->m_ctx->evt()
                              << " failed on slot " << slot->m_ctx->slot() );
               std::abort();
            }

            // Whether or not the event context was added to the list of
            // finished events, remove it from the event slot.
            slot->m_ctx.reset( nullptr );
         }

         // Check for a stall, in case some coding error is still in here somewhere.
         if( ( ! slot->m_algStates.containsAny( { AState::DATAREADY,
                                                  AState::SCHEDULED } ) ) &&
             ( ! slot->m_complete ) && ( m_actionsQueueCount.load() == 0 ) ) {
            ATH_MSG_ERROR( "Stall detected in: " << *slot );
            m_algExecStateSvc->setEventStatus( EventStatus::AlgStall,
                                               *( slot->m_ctx ) );
            std::ostringstream tmp;
            m_algExecStateSvc->dump( tmp, *( slot->m_ctx ) );
            ATH_MSG_ERROR( "Algorithm status: " << tmp.str() );
            m_finishedEvents.push( slot->m_ctx.release() );
         }
      }

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode SchedulerSvc::enqueue( std::size_t iAlgo, EventContext& ctx ) {

      // Use the algorithm rank to sort the queue
      const std::string& algName( m_algNames[ iAlgo ] );

      // Get algorithm pointer
      IAlgorithm* iAlgoPtr = nullptr;
      ATH_CHECK( m_algResourcePool->acquireAlgorithm( algName, iAlgoPtr ) );

      // Update the algorithm's state.
      ATH_CHECK( m_eventSlots[ ctx.slot() ].m_algStates.set( iAlgo,
                                                         AState::SCHEDULED ) );
      ATH_MSG_VERBOSE( "Bumping algorithm " << iAlgo << " ("
                       << m_algNames[ iAlgo ]
                       << ") to the SCHEDULED state on event " << ctx.evt()
                       << ", slot " << ctx.slot() );

      // Add the algorithm to the scheduled queue
      m_scheduledQueue.push( { iAlgo, &ctx, iAlgoPtr } );

      // Set up a TBB task that executes the algorithm
      tbb::task* algoTask =
         new ( tbb::task::allocate_root() )
            AlgExecutionTask( *this, *m_algExecStateSvc, *m_threadPoolSvc );
      tbb::task::enqueue( *algoTask
#if __TBB_TASK_PRIORITY
                          , tbb::priority_low
#endif // __TBB_TASK_PRIORITY
                          );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode SchedulerSvc::promoteToExecuted( IAlgorithm& alg,
                                               std::size_t algIndex,
                                               EventContext& ctx ) {

      // Set the thread-specific global context.
      Gaudi::Hive::setCurrentContext( &ctx );

      // First of all, release the algorithm.
      IAlgorithm* algPtr = &alg;
      ATH_CHECK( m_algResourcePool->releaseAlgorithm( m_algNames[ algIndex ],
                                                      algPtr ) );

      // Select the final state for the algorithm.
      const AlgExecState& algstate =
         m_algExecStateSvc->algExecState( m_algNames[ algIndex ], ctx );
      AState state = ( algstate.execStatus().isSuccess() ?
                       ( algstate.filterPassed() ? AState::EVTACCEPTED :
                         AState::EVTREJECTED )
                       : AState::ERROR );

      // Update the algorithm's and the scheduler's state.
      ATH_CHECK( m_eventSlots[ ctx.slot() ].m_algStates.set( algIndex,
                                                             state ) );
      ++m_actionsQueueCount;
      ATH_CHECK( m_eventStoreContents[ ctx.slot() ].setAlgExecuted( algIndex ) );

      // Schedule an update of the status of the algorithms
      m_actionsQueue.push( [ this ]() -> StatusCode {
         ATH_CHECK( this->updateStates() );
         return StatusCode::SUCCESS;
      } );

      // Tell the user what happened.
      ATH_MSG_VERBOSE( "Bumping algorithm " << algIndex << " ("
                       << m_algNames[ algIndex ] << ") to the " << state
                       << " state on event " << ctx.evt() << ", slot "
                       << ctx.slot() );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

} // namespace ASync
