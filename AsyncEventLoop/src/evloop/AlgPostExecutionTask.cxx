// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AlgPostExecutionTask.h"
#include "SchedulerSvc.h"

// Project include(s).
#include "AsyncBaseComps/Algorithm.h"
#include "AsyncBaseComps/MessagingMacros.h"

// Gaudi include(s).
#include <GaudiKernel/IAlgExecStateSvc.h>

/// Helper macro for checking status codes in
/// @c ASync::AlgPostExecutionTask::execute
#define TBB_CHECK( EXP )                                                  \
   do {                                                                   \
      if( ! EXP.isSuccess() ) {                                           \
         ATH_MSG_FATAL( "Failed to execute: " << #EXP );                  \
         std::abort();                                                    \
      }                                                                   \
   } while( false )

namespace ASync {

   AlgPostExecutionTask::AlgPostExecutionTask( Algorithm& alg,
                                               std::size_t algIndex,
                                               EventContext& ctx,
                                               IAlgExecStateSvc& aessvc,
                                               SchedulerSvc& schedulerSvc )
   : m_alg( &alg ), m_algIndex( algIndex ), m_ctx( &ctx ), m_aesSvc( &aessvc ),
     m_schedulerSvc( &schedulerSvc ) {

   }

   tbb::task* AlgPostExecutionTask::execute() {

      // Lambda used by the messaging macros.
      auto msgLevel = [ this ]( MSG::Level lvl ) -> bool {
         return this->m_schedulerSvc->msgLevel( lvl );
      };
      auto msgStream = [ this ]( MSG::Level lvl ) -> MsgStream& {
         return this->m_schedulerSvc->msgStream( lvl );
      };

      // Set up the algorithm for post-execution.
      TBB_CHECK( m_alg->whiteboard()->selectStore( m_ctx->slot() ) );

      // Post-execute the algorithm.
      try {
         if( ! m_alg->postExecute( *m_ctx ).isSuccess() ) {
            ATH_MSG_ERROR( "Execution of algorithm \"" << m_alg->name()
                           << "\" failed" );
            setFailed();
            return nullptr;
         }
      } catch(...) {
         ATH_MSG_FATAL( "Exception thrown by algorithm \"" << m_alg->name()
                        << "\"" );
         setFailed();
         return nullptr;
      }

      // Tell the user what's happening.
      ATH_MSG_VERBOSE( "Finishing the post-execution of algorithm \""
                       << m_alg->name() << "\" on event " << m_ctx->evt()
                       << ", slot " << m_ctx->slot() );

      // Notify everybody that the algorithm is done.
      m_aesSvc->updateEventStatus( false, *m_ctx );
      m_aesSvc->algExecState( m_alg, *m_ctx ).setState( AlgExecState::Done,
                                                        StatusCode::SUCCESS );
      TBB_CHECK( m_schedulerSvc->promoteToExecuted( *m_alg, m_algIndex,
                                                    *m_ctx ) );

      // No hint given to TBB about the next task to execute...
      return nullptr;
   }

   void AlgPostExecutionTask::setFailed() {

      // Lambda used by the messaging macros.
      auto msgStream = [ this ]( MSG::Level lvl ) -> MsgStream& {
         return this->m_schedulerSvc->msgStream( lvl );
      };

      // Start by marking the event as failed.
      m_aesSvc->updateEventStatus( true, *m_ctx );
      // Set the algorithm's state to failed.
      m_aesSvc->algExecState( m_alg, *m_ctx ).setState( AlgExecState::Done,
                                                        StatusCode::FAILURE );
      // Finally, mark the algorithm as executed in the scheduler.
      TBB_CHECK( m_schedulerSvc->promoteToExecuted( *m_alg, m_algIndex,
                                                    *m_ctx ) );

      return;
   }

} // namespace ASync
