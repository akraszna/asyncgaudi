// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCEVENTLOOP_ALGPOSTEXECUTIONTASK_H
#define ASYNCEVENTLOOP_ALGPOSTEXECUTIONTASK_H

// TBB include(s).
#include <tbb/task.h>

// System include(s).
#include <cstddef>

// Forward declaration(s).
class EventContext;
class IAlgExecStateSvc;

namespace ASync {

   // Forward declaration(s).
   class Algorithm;
   class SchedulerSvc;

   /// Task calling @c postExecute on an algorithm
   ///
   /// Such task objects are given to the user code to schedule the
   /// post execution of that algorithm in an appropriate way. Either
   /// right away, or after some asynchronous calculation has finished.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class AlgPostExecutionTask : public tbb::task {

   public:
      /// Constructor with all the required arguments
      AlgPostExecutionTask( Algorithm& alg, std::size_t algIndex,
                            EventContext& ctx, IAlgExecStateSvc& aessvc,
                            SchedulerSvc& schedulerSvc );

      /// Function calling @c postExecute(...) on the algorithm
      virtual tbb::task* execute() override;

   private:
      /// Function called when something goes wrong with the algorithm
      /// post-executiion
      void setFailed();

      /// The algorithm the task is operating on
      Algorithm* m_alg;
      /// Index of the algorithm inside the scheduler
      std::size_t m_algIndex;
      /// The event context to post-execute the algorithm for
      EventContext* m_ctx;
      /// The algorithm execution state service of the job
      IAlgExecStateSvc* m_aesSvc;
      /// Pointer to the scheduler that this task belongs to
      SchedulerSvc* m_schedulerSvc;

   }; // class AlgPostExecutionTask

} // namespace ASync

#endif // ASYNCEVENTLOOP_ALGPOSTEXECUTIONTASK_H
