// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCEVENTLOOP_EVENTSLOT_H
#define ASYNCEVENTLOOP_EVENTSLOT_H

// Local include(s).
#include "AlgExecutionStates.h"

// Gaudi include(s).
#include <GaudiKernel/EventContext.h>

// System include(s).
#include <cstddef>
#include <iosfwd>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

// Forward declaration(s).
class MsgStream;

namespace ASync {

   /// Class representing an event slot in the scheduler
   class EventSlot {

   public:
      /// Construct a (top-level) slot
      EventSlot( std::size_t numberOfAlgorithms,
                 MsgStream& msgStream );

      /// Copy constructor (deleted)
      EventSlot( const EventSlot& ) = delete;
      /// Assignment operator (deleted)
      EventSlot& operator=( const EventSlot& ) = delete;
      /// Move constructor (default)
      EventSlot( EventSlot&& ) = default;
      /// Move assignment (default)
      EventSlot& operator=( EventSlot&& ) = default;

      /// Reset all resources in order to reuse the slot (thread-unsafe)
      ///
      /// @param ctx Event context to use from now on
      ///
      void reset( std::unique_ptr< EventContext > ctx );

      /// Context for this event slot
      std::unique_ptr< EventContext > m_ctx;
      /// Vector of algorithms states
      AlgExecutionStates m_algStates;
      /// Flag for the completion of the event
      bool m_complete = false;

   }; // class EventSlot

} // namespace ASync

namespace std {

   /// Print operator for an @c ASync::EventSlot object
   std::ostream& operator<<( std::ostream& out, const ASync::EventSlot& slot );

} // namespace std

#endif // ASYNCEVENTLOOP_EVENTSLOT_H
