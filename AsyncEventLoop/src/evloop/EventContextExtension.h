// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCEVENTLOOP_EVENTCONTEXTEXTENSION_H
#define ASYNCEVENTLOOP_EVENTCONTEXTEXTENSION_H

namespace ASync {

   /// Event context extension class for @c ASync::EventLoopMgr
   ///
   /// This class is used to extend the information held by Gaudi's
   /// @c EventContext class. With information needed by the event loop
   /// manager used by the asynchronous code.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class EventContextExtension {

   public:
      /// Type for the run number
      typedef unsigned long run_number_t;
      /// Type for the event number
      typedef unsigned long long event_number_t;

      /// Constructor with a run and an event number
      EventContextExtension( run_number_t run, event_number_t event );

      /// Get the run number
      run_number_t run() const;
      /// Get the event number
      event_number_t event() const;

      /// Get whether there should be a heartbeat from the event loop manager
      bool heartbeat() const;
      /// Set whether there should be a heartbeat from the event loop manager
      void setHeartbeat( bool value );

   private:
      /// The run number of the processed event
      run_number_t m_run;
      /// The event number of the processed event
      event_number_t m_event;

      /// Flag storing whether there should be a heartbeat printed by the event
      /// loop manager
      bool m_heartbeat;

   }; // class EventContextExtension

} // namespace ASync

#endif // ASYNCEVENTLOOP_EVENTCONTEXTEXTENSION_H
