// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCEVENTLOOP_SCHEDULERSVC_H
#define ASYNCEVENTLOOP_SCHEDULERSVC_H

// Local include(s).
#include "AlgExecutionStates.h"
#include "EventSlot.h"
#include "EventStoreContent.h"

// Gaudi include(s).
#include <GaudiKernel/IAlgExecStateSvc.h>
#include <GaudiKernel/IAlgResourcePool.h>
#include <GaudiKernel/IHiveWhiteBoard.h>
#include <GaudiKernel/IScheduler.h>
#include <GaudiKernel/IThreadPoolSvc.h>
#include <GaudiKernel/Service.h>
#include <GaudiKernel/ServiceHandle.h>

// TBB include(s).
#include <tbb/concurrent_queue.h>

// System include(s).
#include <functional>
#include <string>
#include <thread>
#include <vector>

namespace ASync {

   /// A modification of Gaudi's @c AvalancheSchedulerSvc
   ///
   /// @author Illya Shapoval
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class SchedulerSvc : public extends< Service, IScheduler > {

      /// Make the TBB tasks friends of the scheduler.
      friend class AlgExecutionTask;
      friend class AlgPostExecutionTask;

   public:
      /// Constructor(s)
      using extends::extends;

      /// @name Interface inherited from @c IService
      /// @{

      /// Function initialising the service
      virtual StatusCode initialize() override;
      /// Function finalising the service
      virtual StatusCode finalize() override;

      /// @}

      /// @name Interface inherited from @c IScheduler
      /// @{

      /// Make an event available to the scheduler
      virtual StatusCode pushNewEvent( EventContext* eventContext ) override;

      /// Make multiple events available to the scheduler
      virtual StatusCode
      pushNewEvents( std::vector< EventContext* >& eventContexts ) override;

      /// Block until an event is available
      virtual StatusCode
      popFinishedEvent( EventContext*& eventContext ) override;

      /// Try to fetch an event from the scheduler
      virtual StatusCode
      tryPopFinishedEvent( EventContext*& eventContext ) override;

      /// Get the number of free slots
      virtual unsigned int freeSlots() override;

      /// Method to inform the scheduler about event views
      virtual StatusCode
      scheduleEventView( const EventContext* sourceContext,
                         const std::string& nodeName,
                         std::unique_ptr< EventContext > viewContext ) override;

      /// @}

   private:
      /// @name Functions orchestrating the scheduler's service thread
      /// @{

      /// Function executing the scheduler in a dedicated thread
      void execute();
      /// Stop the scheduler thread
      void stopExecute();

      /// @}

      /// @name Functions orchestrating the management of the algorithm states
      /// @{

      /// Update the states of all algorithms in all active slots
      StatusCode updateStates();
      /// Enqueue an algorithm for execution
      StatusCode enqueue( std::size_t algo, EventContext& ctx );
      /// Promote an algorithm to the executed state
      StatusCode promoteToExecuted( IAlgorithm& alg, std::size_t algIndex,
                                    EventContext& ctx );

      /// @}

      /// @name Service properties
      /// @{

      Gaudi::Property< int > m_threadPoolSize {
         this, "ThreadPoolSize", -1,
         "Size of the threadpool initialised by TBB; a value of -1 gives "
         "TBB the freedom to choose" };

      Gaudi::Property< bool > m_checkDeps {
         this, "CheckDependencies", true,
         "Runtime check of Algorithm Data Dependencies" };

      Gaudi::Property< std::string > m_dataLoaderAlgName {
         this, "DataLoaderAlg", "",
         "Attribute unmet input dependencies to this DataLoader algorithm" };

      Gaudi::Property< bool > m_showDataDeps {
         this, "ShowDataDependencies", true,
         "Show the INPUT and OUTPUT data dependencies of the Algorithms" };
      Gaudi::Property< std::string > m_dataFlowOutput {
         this, "DataFlowOutput", "",
         "Output .dot file with the data-flow information" };

      ServiceHandle< IHiveWhiteBoard > m_whiteboard {
         this, "WhiteboardSvc", "ASync::WhiteBoard",
         "The Hive whiteboard service to use" };
      ServiceHandle< IAlgExecStateSvc > m_algExecStateSvc {
         this, "AlgExecStateSvc", "AlgExecStateSvc",
         "Algorithm execution state service to use" };
      ServiceHandle< IAlgResourcePool > m_algResourcePool {
         this, "AlgResourcePool", "AlgResourcePool",
         "Pool of algorithms / algorithm resources to use" };
      ServiceHandle< IThreadPoolSvc > m_threadPoolSvc {
         this, "ThreadPoolSvc", "ThreadPoolSvc",
         "Service managing the pool of threads used by the job" };

      /// @}

      /// Activation state of the scheduler
      enum ActivationState {
         INACTIVE = 0,
         ACTIVE = 1,
         FAILURE = 2
      };

      /// Names of all the algorithms used in the job
      std::vector< std::string > m_algNames;

      /// Flag to track if the scheduler is active or not
      std::atomic< ActivationState > m_isActive{ INACTIVE };

      /// The thread the scheduler runs in
      std::thread m_thread;

      /// Vector of events slots
      std::vector< EventSlot > m_eventSlots;
      /// Vector of event store content handlers
      std::vector< EventStoreContent > m_eventStoreContents;

      /// Number of current free slots in the scheduler
      std::atomic_int m_freeSlots = 0;

      /// Queue of finished events
      tbb::concurrent_bounded_queue< EventContext* > m_finishedEvents;

      /// Type for the "actions" executed by the scheduler
      typedef std::function< StatusCode() > Action_t;
      /// Queue where actions are stored and picked for execution
      tbb::concurrent_bounded_queue< Action_t > m_actionsQueue;
      /// Number of actions currently in, or about to get into the queue
      std::atomic_uint m_actionsQueueCount;

      /// Struct to hold entries in the algorithm queue
      struct AlgQueueEntry {
         /// The index of the algorithm in the scheduler's bookkeeping
         std::size_t m_algIndex;
         /// Event context to run the algorithm on
         EventContext* m_ctx;
         /// Pointer to the algorithm that is to be run
         IAlgorithm* m_algorithm;
      };

      /// Queue for the scheduled algorithms
      tbb::concurrent_bounded_queue< AlgQueueEntry > m_scheduledQueue;

   }; // class SchedulerSvc

} // namespace ASync

#endif // ASYNCEVENTLOOP_SCHEDULERSVC_H
