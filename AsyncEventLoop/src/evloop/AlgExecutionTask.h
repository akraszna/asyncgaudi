// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCEVENTLOOP_ALGEXECUTIONTASK_H
#define ASYNCEVENTLOOP_ALGEXECUTIONTASK_H

// TBB include(s).
#include <tbb/task.h>

// Forward declaration(s).
class EventContext;
class IAlgorithm;
class IAlgExecStateSvc;
class ISvcLocator;
class IThreadPoolSvc;

namespace ASync {

   // Forward declaration(s).
   class SchedulerSvc;

   /// TBB task used to execute algorithm objects
   ///
   /// Depending on the algorithm's type and setup, the task will either call
   /// @c IAlgorithm::execute, or @c ASync::Algorithm::mainExecute on the
   /// algorithm object that it receives.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class AlgExecutionTask : public tbb::task {

   public:
      /// Constructor with all necessary arguments
      AlgExecutionTask( SchedulerSvc& schedulerSvc, IAlgExecStateSvc& aesSvc,
                        IThreadPoolSvc& tpoolSvc );

      /// Function executing the task/algorithm
      virtual tbb::task* execute() override;

   private:
      /// Function called when something goes wrong with the algorithm
      /// executiion
      void setFailed( EventContext& ctx, IAlgorithm& alg, std::size_t iAlg );

      /// Pointer to the scheduler that created this task object
      SchedulerSvc* m_schedulerSvc;
      /// Pointer to the algorithm execute state service
      IAlgExecStateSvc* m_aesSvc;
      /// Pointer to the thread pool service
      IThreadPoolSvc* m_tpoolSvc;

   }; // class AlgExecutionTask

} // namespace ASync

#endif // ASYNCEVENTLOOP_ALGEXECUTIONTASK_H
