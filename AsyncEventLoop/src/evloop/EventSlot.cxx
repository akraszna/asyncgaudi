// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "EventSlot.h"

// System include(s).
#include <iostream>

namespace ASync {

   EventSlot::EventSlot( std::size_t numberOfAlgorithms,
                         MsgStream& msgStream )
   : m_algStates( numberOfAlgorithms, msgStream ) {

   }

   void EventSlot::reset( std::unique_ptr< EventContext > ctx ) {

      m_ctx = std::move( ctx );
      m_algStates.reset();
      m_complete = false;
      return;
   }

} // namespace ASync

namespace std {

   std::ostream& operator<<( std::ostream& out,
                             const ASync::EventSlot& slot ) {

      out << "event: " << slot.m_ctx->evt() << std::endl;
      out << "state: " << slot.m_algStates;
      return out;
   }

} // namespace std
