// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AlgExecutionStates.h"

// Project include(s).
#include "AsyncBaseComps/MessagingMacros.h"

// System include(s).
#include <algorithm>

namespace {
   
   /// Transition identifier between two states
   constexpr uint16_t
   transitionId( ASync::AlgExecutionStates::State first,
                 ASync::AlgExecutionStates::State second ) {

      return ( static_cast< uint16_t >( first ) * 256 +
               static_cast< uint16_t >( second ) );
   }

} // private namespace

namespace ASync {

   ////////////////////////////////////////////////////////////////////////////
   //
   //             Implementation of ASync::AlgExecutionStates
   //

   AlgExecutionStates::AlgExecutionStates( std::size_t algsNumber,
                                           MsgStream& msgStream )
   : m_states( algsNumber ), m_msgStream( &msgStream ) {

      std::fill( m_states.begin(), m_states.end(), CONTROLREADY );
   }

   AlgExecutionStates::Iterator AlgExecutionStates::begin( State kind ) const {

      return Iterator( kind, m_states, m_states.begin() );
   }

   AlgExecutionStates::Iterator AlgExecutionStates::end( State kind ) const {

      return Iterator( kind, m_states, m_states.end() );
   }

   StatusCode AlgExecutionStates::set( std::size_t iAlgo, State newState ) {

      // Check that the specified index is valid.
      if( iAlgo >= m_states.size() ) {
         ATH_MSG_ERROR( "Index (" << iAlgo << ") is out of bound ("
                        << m_states.size() << ")" );
         return StatusCode::FAILURE;
      }

      // If no state switch is happening, that is of course allowed.
      if( m_states[ iAlgo ] == newState ) {
         return StatusCode::SUCCESS;
      }

      // Only a certain number of state changes.
      switch( transitionId( m_states[ iAlgo ], newState ) ) {

      case transitionId( CONTROLREADY, DATAREADY ):
      case transitionId( DATAREADY, SCHEDULED ):
      case transitionId( DATAREADY, RESOURCELESS ):
      case transitionId( RESOURCELESS, SCHEDULED ):
      case transitionId( SCHEDULED, ERROR ):
      case transitionId( SCHEDULED, EVTACCEPTED ):
      case transitionId( SCHEDULED, EVTREJECTED ):
         m_states[ iAlgo ] = newState;
         return StatusCode::SUCCESS;
         break;
      default:
         ATH_MSG_ERROR( "[AlgIndex " << iAlgo << "] Transition from "
                        << m_states[ iAlgo ] << " to " << newState
                        << " is not allowed" );
         m_states[ iAlgo ] = ERROR;
         return StatusCode::FAILURE;
      }
   }

   void AlgExecutionStates::reset() {
     
      std::fill( m_states.begin(), m_states.end(), CONTROLREADY );
   }

   bool AlgExecutionStates::contains( State state ) const {
     
      return ( std::find( m_states.begin(), m_states.end(), state ) !=
               m_states.end() );
   }

   bool AlgExecutionStates::
   containsAny( std::initializer_list< State > states ) const {
   
      return ( std::find_first_of( m_states.begin(), m_states.end(),
                                   states.begin(), states.end() ) !=
               m_states.end() );
   }

   bool AlgExecutionStates::
   containsOnly( std::initializer_list< State > states ) const {
   
      return std::all_of( m_states.begin(), m_states.end(),
                          [ &states ]( State s ) {
                             return ( std::find( states.begin(), states.end(),
                                                 s ) != states.end() );
                          } );
   }

   AlgExecutionStates::State
   AlgExecutionStates::operator[]( unsigned int iAlgo ) const {

      return m_states.at( iAlgo );
   }

   std::size_t AlgExecutionStates::size() const {
     
      return m_states.size();
   }

   std::size_t AlgExecutionStates::sizeOfSubset( State state ) const {

      return std::count_if( m_states.begin(), m_states.end(),
                            [ state ]( State s ) { return s == state; } );
   }

   void AlgExecutionStates::dump( std::ostream& out ) const {

      for( std::size_t i = 0; i < m_states.size(); ++i ) {
         out << "Algorithm " << i << " state: " << m_states[ i ].load();
         if( i + 1 < m_states.size() ) {
            out << std::endl;
         }
      }
      return;
   }

   MsgStream& AlgExecutionStates::msgStream( MSG::Level level ) const {

      return ( *m_msgStream << level );
   }

   //
   ////////////////////////////////////////////////////////////////////////////

   ////////////////////////////////////////////////////////////////////////////
   //
   //        Implementation of ASync::AlgExecutionStates::Iterator
   //

   AlgExecutionStates::Iterator::
   Iterator( State state, const std::vector< std::atomic< State > >& vec,
             std::vector< std::atomic< State > >::const_iterator pos )
   : m_state( state ), m_vec( &vec ),
     m_pos( std::find( pos, vec.end(), state ) ) {

   }

   bool AlgExecutionStates::Iterator::operator==( const Iterator& rhs ) const {

      return ( ( m_state == rhs.m_state ) && ( m_vec == rhs.m_vec ) &&
               ( m_pos == rhs.m_pos ) );
   }

   bool AlgExecutionStates::Iterator::operator!=( const Iterator& rhs ) const {

      return ( ( m_state != rhs.m_state ) || ( m_vec != rhs.m_vec ) ||
               ( m_pos != rhs.m_pos ) );
   }

   AlgExecutionStates::Iterator& AlgExecutionStates::Iterator::operator++() {

      if( m_pos != m_vec->end() ) {
         m_pos = std::find( std::next( m_pos ), m_vec->end(), m_state );
      }
      return *this;
   }

   AlgExecutionStates::Iterator&
   AlgExecutionStates::Iterator::operator++( int ) {

      return ++( *this );
   }

   std::size_t AlgExecutionStates::Iterator::operator*() const {
     
      return std::distance( m_vec->begin(), m_pos );
   }

   //
   ////////////////////////////////////////////////////////////////////////////

} // namespace ASync

namespace std {

   std::ostream& operator<<( std::ostream& out,
                             ASync::AlgExecutionStates::State state ) {

// Helper macro for printing the names of the states to an output stream
#define PRINT_STATE(NAME)                                                \
   case ASync::AlgExecutionStates::State::NAME:                        \
      out << #NAME;                                                      \
      break

      // Print the name of the state to the output stream.
      switch( state ) {
         PRINT_STATE( CONTROLREADY );
         PRINT_STATE( DATAREADY );
         PRINT_STATE( RESOURCELESS );
         PRINT_STATE( SCHEDULED );
         PRINT_STATE( EVTACCEPTED );
         PRINT_STATE( EVTREJECTED );
         PRINT_STATE( ERROR );
      default:
         out << "<UNKNOWN>";
         break;
      }

// Clean up.
#undef PRINT_STATE

      // Return the output stream.
      return out;
   }

   std::ostream& operator<<( std::ostream& out,
                             const ASync::AlgExecutionStates& states ) {

      states.dump( out );
      return out;
   }

} // namespace std
