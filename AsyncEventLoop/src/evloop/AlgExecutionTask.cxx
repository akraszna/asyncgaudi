// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AlgExecutionTask.h"
#include "AlgPostExecutionTask.h"
#include "SchedulerSvc.h"

// Project include(s).
#include "AsyncBaseComps/Algorithm.h"
#include "AsyncBaseComps/AlgTaskPtr.h"
#include "AsyncBaseComps/MessagingMacros.h"

// Gaudi include(s).
#include <GaudiKernel/IAlgExecStateSvc.h>
#include <GaudiKernel/IThreadPoolSvc.h>
#include <GaudiKernel/ThreadLocalContext.h>
#include <Gaudi/Algorithm.h>

// System include(s).
#include <cstdlib>
#include <thread>

/// Helper macro for checking status codes in
/// @c ASync::AlgExecutionTask::execute
#define TBB_CHECK( EXP )                                                  \
   do {                                                                   \
      if( ! EXP.isSuccess() ) {                                           \
         ATH_MSG_FATAL( "Failed to execute: " << #EXP );                  \
         std::abort();                                                    \
      }                                                                   \
   } while( false )

namespace ASync {

   AlgExecutionTask::AlgExecutionTask( SchedulerSvc& schedulerSvc,
                                       IAlgExecStateSvc& aesSvc,
                                       IThreadPoolSvc& tpoolSvc )
   : m_schedulerSvc( &schedulerSvc ), m_aesSvc( &aesSvc ),
     m_tpoolSvc( &tpoolSvc ) {

   }

   tbb::task* AlgExecutionTask::execute() {

      // Get all information necessary for an algorithm's execution.
      SchedulerSvc::AlgQueueEntry algEntry;
      if( ! m_schedulerSvc->m_scheduledQueue.try_pop( algEntry ) ) {
         return nullptr;
      }

      // Create some convenience variables for the rest of the function.
      EventContext& ctx = *( algEntry.m_ctx );
      Gaudi::Algorithm* gaudiAlg =
         dynamic_cast< Gaudi::Algorithm* >( algEntry.m_algorithm );
      assert( gaudiAlg != nullptr );
      ASync::Algorithm* asyncAlg =
         dynamic_cast< ASync::Algorithm* >( algEntry.m_algorithm );

      // Set the event context for the current thread.
      Gaudi::Hive::setCurrentContext( ctx );

      // Lambdas used by the messaging macros.
      auto msgLevel = [ this ]( MSG::Level lvl ) -> bool {
         return this->m_schedulerSvc->msgLevel( lvl );
      };
      auto msgStream = [ this ]( MSG::Level lvl ) -> MsgStream& {
         return this->m_schedulerSvc->msgStream( lvl );
      };

      // Make sure that this thread was already initialised.
      if( ! IThreadPoolSvc::threadInitDone() ) {
         ATH_MSG_DEBUG( "New thread detected (" << std::this_thread::get_id()
                        << "). Performing the thread local initialisation." );
         m_tpoolSvc->initThisThread();
      }

      // Select the appropriate store for the algorithm.
      TBB_CHECK( gaudiAlg->whiteboard()->selectStore( ctx.valid() ?
                                                      ctx.slot() : 0 ) );
      ATH_MSG_VERBOSE( "Whiteboard selected on algorithm" );

      // Put the algorithm into the "executing" state.
      m_aesSvc->algExecState( gaudiAlg,
                              ctx ).setState( AlgExecState::Executing );
      ATH_MSG_VERBOSE( "State of algorithm \"" << gaudiAlg->name()
                       << "\" set to \"Executing\" on event " << ctx.evt() );

      // Execute the algorithm.
      try {
         if( ( asyncAlg != nullptr ) && asyncAlg->isAsynchronous() ) {
            // Create the task that will be used to post-execute the algorithm
            // (eventually...).
            AlgTaskPtr_t pet(
               new ( tbb::task::allocate_root() )
                  AlgPostExecutionTask( *asyncAlg, algEntry.m_algIndex, ctx,
                                        *m_aesSvc, *m_schedulerSvc ) );
            // Call the asynchronous execute function.
            if( ! asyncAlg->mainExecute( ctx,
                                         std::move( pet ) ).isSuccess() ) {
               ATH_MSG_ERROR( "Asynchronous execution of algorithm \""
                              << asyncAlg->name() << "\" has failed" );
               setFailed( ctx, *asyncAlg, algEntry.m_algIndex );
            }
         } else {
            // Call the synchronous execute function. Note that I don't call
            // @sysExecute here, as all the (necessary) padding done by that
            // function is executed by this task function already.
            if( ! gaudiAlg->execute( ctx ).isSuccess() ) {
               ATH_MSG_ERROR( "Synchronous execution of algorithm \""
                              << gaudiAlg->name() << "\" has failed" );
               setFailed( ctx, *gaudiAlg, algEntry.m_algIndex );
            }
            // If the synchronous execution succeeded, then finish the
            // execution gracefully.
            static const bool EVENTFAILED = false;
            m_aesSvc->updateEventStatus( EVENTFAILED, ctx );
            m_aesSvc->algExecState( gaudiAlg,
                                    ctx ).setState( AlgExecState::Done,
                                                    StatusCode::SUCCESS );
            TBB_CHECK( m_schedulerSvc->promoteToExecuted( *gaudiAlg,
                                                          algEntry.m_algIndex,
                                                          ctx ) );
         }
      } catch(...) {
         ATH_MSG_FATAL( "Exception thrown by algorithm \"" << gaudiAlg->name()
                        << "\"" );
         setFailed( ctx, *gaudiAlg, algEntry.m_algIndex );
      }

      // Reset the event context of the current thread to an invalid value.
      Gaudi::Hive::setCurrentContextEvt( -1 );

      // Tell the user what happened.
      ATH_MSG_VERBOSE( "Finished (main-)executing algorithm \""
                       << gaudiAlg->name() << "\" in event " << ctx.evt()
                       << ", slot " << ctx.slot()  );

      // Do not give a hint to TBB about the next task that it should execute.
      return nullptr;
   }

   void AlgExecutionTask::setFailed( EventContext& ctx, IAlgorithm& alg,
                                     std::size_t algIndex ) {

      // Lambda used by the messaging macros.
      auto msgStream = [ this ]( MSG::Level lvl ) -> MsgStream& {
         return this->m_schedulerSvc->msgStream( lvl );
      };

      // Start by marking the event as failed.
      static const bool EVENTFAILED = true;
      m_aesSvc->updateEventStatus( EVENTFAILED, ctx );
      // Set the algorithm's state to failed.
      m_aesSvc->algExecState( &alg, ctx ).setState( AlgExecState::Done,
                                                    StatusCode::FAILURE );
      // Finally, mark the algorithm as executed in the scheduler.
      TBB_CHECK( m_schedulerSvc->promoteToExecuted( alg, algIndex, ctx ) );

      return;
   }

} // namespace ASync
