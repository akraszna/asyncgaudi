// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "EventLoopMgr.h"
#include "EventContextExtension.h"

// Project include(s).
#include "AsyncBaseComps/CheckMacros.h"
#include "AsyncBaseComps/MessagingMacros.h"

// Gaudi include(s).
#include <GaudiKernel/GaudiException.h>
#include <GaudiKernel/MsgStream.h>
#include <GaudiKernel/Property.h>
#include <GaudiKernel/AppReturnCode.h>
#include <GaudiKernel/DataObject.h>
#include <GaudiKernel/IDataManagerSvc.h>
#include <GaudiKernel/IHiveWhiteBoard.h>
#include <GaudiKernel/IAlgResourcePool.h>
#include <GaudiKernel/IAlgExecStateSvc.h>
#include <GaudiKernel/IScheduler.h>

// System include(s).
#include <chrono>

DECLARE_COMPONENT( ASync::EventLoopMgr )

namespace ASync {

   StatusCode EventLoopMgr::initialize() {

      // Let the user know what's happening.
      ATH_MSG_INFO( "Initialising the asynchronous event loop manager" );

      // Initialise the base class.
      ATH_CHECK( MinimalEventLoopMgr::initialize() );

      //------------------------------------------------------------------------
      // Setup stuff for hive
      //------------------------------------------------------------------------

      m_whiteboard = serviceLocator()->service( m_whiteboardName );
      if( ! m_whiteboard.isValid() ) {
         ATH_MSG_ERROR( "Error retrieving " << m_whiteboardName
                        << " with interface IHiveWhiteBoard" );
         return StatusCode::FAILURE;
      }

      m_dataStore = serviceLocator()->service( m_whiteboardName );
      if( ! m_dataStore.isValid() ) {
         ATH_MSG_ERROR( "Error retrieving " << m_whiteboardName
                        << " with interface ASync::IDataStore" );
         return StatusCode::FAILURE;
      }

      m_schedulerSvc = serviceLocator()->service( m_schedulerName );
      if ( ! m_schedulerSvc.isValid() ) {
         ATH_MSG_ERROR( "Error retrieving " << m_schedulerName
                        << " with interface ISchedulerSvc" );
         return StatusCode::FAILURE;
      }

      m_algResourcePool = serviceLocator()->service( "AlgResourcePool" );
      if( ! m_algResourcePool.isValid() ) {
         ATH_MSG_ERROR( "Error retrieving AlgResourcePool" );
         return StatusCode::FAILURE;
      }

      m_algExecStateSvc = serviceLocator()->service( "AlgExecStateSvc" );
      if( ! m_algExecStateSvc.isValid() ) {
         ATH_MSG_ERROR( "Error retrieving AlgExecStateSvc" );
         return StatusCode::FAILURE;
      }

      m_appMgrProperty = serviceLocator();
      if( ! m_appMgrProperty.isValid() ) {
         ATH_MSG_ERROR( "IProperty interface not found in ApplicationMgr" );
         return StatusCode::FAILURE;
      }

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode EventLoopMgr::finalize() {

      // Finalise the base class.
      ATH_CHECK( MinimalEventLoopMgr::finalize() );

      // Clear the member pointers.
      m_whiteboard = nullptr;
      m_algResourcePool = nullptr;
      m_algExecStateSvc = nullptr;
      m_schedulerSvc = nullptr;

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   EventContext EventLoopMgr::createEventContext() {

      // Create the event context object.
      const unsigned int currentEvent = m_nextEvent++;
      EventContext result( currentEvent,
                           m_whiteboard->allocateStore( currentEvent ) );

      // Tell the whiteboard to use this event's slot.
      if( ! m_whiteboard->selectStore( result.slot() ).isSuccess() ) {
         ATH_MSG_ERROR( "Failed to select the appropriate slot in the "
                        "whiteboard" );
         return EventContext();
      }

      // Run and event numbers for the event context.
      EventContextExtension::run_number_t run = 0;
      EventContextExtension::event_number_t event = currentEvent;

      // Decide whether to have a heartbeat for this event.
      const bool doEvtHeartbeat =
         ( ( m_eventPrintoutInterval.value() > 0 ) &&
           ( ! ( currentEvent % m_eventPrintoutInterval.value() ) ) );

      // Set up the event context extension.
      EventContextExtension ext( run, event );
      ext.setHeartbeat( doEvtHeartbeat );
      result.setExtension( ext );

      // Reset the algorithm execution state for this slot.
      m_algExecStateSvc->reset( result );

      // Tell the user what happened.
      ATH_MSG_DEBUG( "created EventContext, num: " << result.evt()
                     << " in slot: " << result.slot() );

      // Return the created object.
      return result;
   }

   StatusCode EventLoopMgr::executeEvent( EventContext&& ctx ) {

      // Get the event context extension.
      const EventContextExtension& ext =
         ctx.getExtension< EventContextExtension >();

      // Print an info message when starting the first run.
      if( m_firstRun ) {
         // Update the member.
         m_firstRun = false;
         // Tell the user what's happening.
         ATH_MSG_INFO( "  ===>>>  start of run " << ext.run()
                       << " <<<===" );
      }

      // Print a begin event message if we need to.
      if( ext.heartbeat() ) {
         ATH_MSG_INFO( "  ===>>>  start processing event #" << ext.event()
                       << ", run #" << ext.run() << " on slot "
                       << ctx.slot() << ",  " << m_eventsProcessed
                       << " events processed so far  <<<===" );
      }

      // Now add the event to the scheduler.
      ATH_MSG_DEBUG( "Adding event " << ctx.evt()
                     << ", slot " << ctx.slot()
                     << " to the scheduler" );
      ATH_CHECK( m_schedulerSvc->pushNewEvent(
                                      new EventContext( std::move( ctx ) ) ) );

      // Return gracefully:
      return StatusCode::SUCCESS;
   }

   StatusCode EventLoopMgr::executeRun( int maxEvt ) {

      // Execute all events.
      ATH_CHECK( nextEvent( maxEvt ) );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode EventLoopMgr::nextEvent( int maxEvt ) {

      // Make nextEvent(0) a no-op call.
      if( maxEvt == 0 ) {
         return StatusCode::SUCCESS;
      }

      // Reset the application return code.
      ATH_CHECK( Gaudi::setAppReturnCode( m_appMgrProperty,
                                          Gaudi::ReturnCode::Success, true ) );

      // Set up some internal counters.
      int finishedEvts = 0;
      int createdEvts = 0;
      ATH_MSG_INFO( "Starting loop on " << maxEvt << " event(s)" );

      // Loop over events if the maxEvt (received as input) is different from -1.
      // If evtmax is -1 it means infinite loop (until we hit the time limit).
      bool loop_ended = false;
      StatusCode sc( StatusCode::SUCCESS, true );

      // Take the start time.
      auto startTime = std::chrono::high_resolution_clock::now();

      // Iterate over the events.
      while( ( ! loop_ended ) && ( ( maxEvt < 0 ) ||
                                   ( finishedEvts < maxEvt ) ) ) {

         ATH_MSG_DEBUG( " -> createdEvts: " << createdEvts );

         if( // The events are not finished with an unlimited number of events
            ( createdEvts >= 0 ) &&
            // The events are not finished with a limited number of events
            ( ( createdEvts < maxEvt ) || ( maxEvt < 0 ) ) &&
            // There are still free slots in the scheduler
            ( m_schedulerSvc->freeSlots() > 0 ) ) {

            ATH_MSG_DEBUG( "createdEvts: " << createdEvts << ", freeslots: "
                           << m_schedulerSvc->freeSlots() );

            sc = executeEvent( createEventContext() );
            ++createdEvts;
            if( sc.isFailure() ) {
               ATH_MSG_ERROR( "Terminating event processing loop due to "
                              "errors" );
               loop_ended = true;
            }

         } else {
            // All the events were created but not all finished or the slots
            // were all busy: the scheduler should finish its job.

            ATH_MSG_DEBUG( "Draining the scheduler" );

            // Pull out of the scheduler the finished events
            const int ir = drainScheduler( finishedEvts );
            if( ir < 0 ) {
               // There was some sort of error in draining the scheduler.
               loop_ended = true;
               sc = StatusCode::FAILURE;
            } else if( ir == 0 ) {
               // No more events in scheduler. We're done.
               loop_ended = true;
               sc = StatusCode::SUCCESS;
            } else {
               // Keep going!
            }
         }
      } // end main loop on finished events

      // Measure the event loop time.
      auto stopTime = std::chrono::high_resolution_clock::now();
      auto eventLoopTime =
         std::chrono::duration_cast< std::chrono::duration< double > >(
            stopTime - startTime );

      // Print a final message.
      ATH_MSG_INFO( "Loop Finished in " << eventLoopTime.count()
                    << " seconds" );

      // Return the right code, based on what happened:
      return sc;
   }

   StatusCode EventLoopMgr::stopRun() {

      // Set the application return code.
      ATH_CHECK( Gaudi::setAppReturnCode( m_appMgrProperty,
                                          Gaudi::ReturnCode::ScheduledStop ) );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   int EventLoopMgr::drainScheduler( int& finishedEvts ) {

      /// List of finished event contexts.
      std::vector< std::unique_ptr< EventContext > > finishedEvtContexts;
      /// Helper variable to extract finished event contexts with.
      EventContext* context = nullptr;

      // Wait for at least one event context.
      ATH_MSG_DEBUG( "drainScheduler: [" << finishedEvts
                     << "] Waiting for a context" );
      if( ! m_schedulerSvc->popFinishedEvent( context ).isSuccess() ) {
         // No more events left in scheduler to be drained
         ATH_MSG_DEBUG( "drainScheduler: scheduler empty" );
         return 0;
      }

      // Remember the context.
      finishedEvtContexts.emplace_back( context );

      // Now let's remove as many contexts as we can.
      while( m_schedulerSvc->tryPopFinishedEvent( context ).isSuccess() ) {
         finishedEvtContexts.emplace_back( context );
      }

      // Now we flush them.
      bool fail( false );
      for( auto& ctx : finishedEvtContexts ) {

         // This should never happen...
         if( ctx.get() == nullptr ) {
            ATH_MSG_FATAL( "Detected nullptr ctxt while clearing WB" );
            fail = true;
            continue;
         }

         // Check that the event finished without errors.
         if( m_algExecStateSvc->eventStatus( *ctx ) != EventStatus::Success ) {
            ATH_MSG_FATAL( "Failed event detected on " << *ctx
                           << " w/ fail mode: "
                           << m_algExecStateSvc->eventStatus( *ctx ) );
            fail = true;
            continue;
         }

         // Select the store of this context for the finalisation.
         if( ! m_whiteboard->selectStore( ctx->slot() ).isSuccess() ) {
            ATH_MSG_FATAL( "Unable to select store " << ctx->slot() );
            fail = true;
            continue;
         }

         // Tell the user what's happening.
         ATH_MSG_DEBUG( "Clearing slot " << ctx->slot()
                        << " (event " << ctx->evt()
                        << ") of the whiteboard" );

         // Clear the whiteboard.
         if( ! clearWBSlot( ctx->slot() ).isSuccess() ) {
            ATH_MSG_ERROR( "Whiteboard slot " << ctx->slot()
                           << " could not be properly cleared" );
            fail = true;
            continue;
         }

         // Update the event counters:
         ++finishedEvts;
         ++m_eventsProcessed;

         // Get the event context extension:
         EventContextExtension& ext =
            ctx->getExtension< EventContextExtension >();

         // Have a heartbeat if necessary:
         if( ext.heartbeat() ) {
            ATH_MSG_INFO( "  ===>>>  done processing event #" << ext.event()
                          << ", run #" << ext.run() << " on slot "
                          << ctx->slot() << ",  "
                          << m_eventsProcessed
                          << " events processed so far  <<<===" );
         }
      }

      // Return -1 or 1 based on whether there was an error during the cleanup:
      return ( fail ? -1 : 1 );
   }

   StatusCode EventLoopMgr::clearWBSlot( int evtSlot ) {

      // Clear the store in this slot:
      ATH_CHECK( m_whiteboard->clearStore( evtSlot ) );
      // Free up the slot to process another event:
      ATH_CHECK( m_whiteboard->freeStore( evtSlot ) );

      // Return gracefully:
      return StatusCode::SUCCESS;
   }

} // namespace ASync
