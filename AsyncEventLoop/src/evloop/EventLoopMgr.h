// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCEVENTLOOP_EVENTLOOPMGR_H
#define ASYNCEVENTLOOP_EVENTLOOPMGR_H

// Project include(s).
#include "AsyncEventLoopInterfaces/IDataStore.h"

// Gaudi include(s).
#include <GaudiKernel/SmartIF.h>
#include <GaudiKernel/MinimalEventLoopMgr.h>
#include <GaudiKernel/ServiceHandle.h>
#include <GaudiKernel/IEvtSelector.h>
#include <GaudiKernel/IScheduler.h>

// System includes:
#include <string>
#include <vector>

class IProperty;
class IAlgResourcePool;
class IDataManagerSvc;

namespace ASync {

   /// Asynchronous event loop manager
   ///
   /// This event loop manager takes care of executing Gaudi test jobs.
   /// It is based on the Athena event loop manager, simplifying it wherever
   /// possible.
   ///
   /// @author Martin Errenst <Martin.Errenst@cern.ch>
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class EventLoopMgr : public MinimalEventLoopMgr {

   public:
      /// Inherit the @c MinimalEventLoopMgr constructor(s)
      using MinimalEventLoopMgr::MinimalEventLoopMgr;

      /// @name Function(s) inherited from @c IService
      /// @{

      /// Initialise the service
      virtual StatusCode initialize() override;
      /// Finalise the service
      virtual StatusCode finalize() override;

      /// @}

      /// @name Function(s) inherited from @c IEventProcessor
      /// @{

      /// Create a new event context
      virtual EventContext createEventContext() override;
      /// Process a single event
      virtual StatusCode executeEvent( EventContext&& ctx ) override;
      /// Process the specified number of events as a run
      virtual StatusCode executeRun( int maxEvt ) override;
      /// Process the next @c maxEvt count events
      virtual StatusCode nextEvent( int maxEvt ) override;
      /// Schedule stopping the event processing
      virtual StatusCode stopRun() override;

      /// @}

   protected:
      /// @name Event loop manager properties
      /// @{

      /// The name of the whiteboard service to use
      Gaudi::Property< std::string > m_whiteboardName { this,
         "WhiteboardSvc",
         "ASync::WhiteBoard",
         "Name of the Whiteboard to be used" };
      /// The name of the scheduler service to use
      Gaudi::Property< std::string > m_schedulerName { this,
         "SchedulerSvc",
         "ASync::SchedulerSvc",
         "Name of the scheduler to be used" };
      /// The event printout interval during the analysis
      Gaudi::Property< unsigned int > m_eventPrintoutInterval { this,
         "EventPrintoutInterval",
         1,
         "Print event heartbeat printouts every N events" };

      /// @}

      /// Reference to the Whiteboard interface
      SmartIF< IHiveWhiteBoard > m_whiteboard;
      /// Reference to the whiteboard's data store interface
      SmartIF< IDataStore > m_dataStore;
      /// Reference to the Algorithm resource pool
      SmartIF< IAlgResourcePool > m_algResourcePool;
      /// Property interface of ApplicationMgr
      SmartIF< IProperty > m_appMgrProperty;
      /// Reference to the AlgExecStateSvc
      SmartIF< IAlgExecStateSvc > m_algExecStateSvc;
      /// A shortcut for the scheduler
      SmartIF< IScheduler > m_schedulerSvc;

      /// Create event context
      StatusCode createEventContext( EventContext*& eventContext,
                                     int createdEvents );
      /// Drain the scheduler from all actions that may be queued
      int drainScheduler( int& finishedEvents );
      /// Clear a slot in the WB
      StatusCode clearWBSlot( int evtSlot );

   private:
      /// Number of events that finished processing
      unsigned int m_eventsProcessed = 0;
      /// The next event to start processing
      unsigned int m_nextEvent = 0;
      /// Status flag, set to @c true for the first event of the execution
      bool m_firstRun = true;

   }; // class EventLoopMgr

} // namespace ASync

#endif // ASYNCEVENTLOOP_EVENTLOOPMGR_H
