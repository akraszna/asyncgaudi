// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCEVENTLOOP_ALGEXECUTIONSTATES_H
#define ASYNCEVENTLOOP_ALGEXECUTIONSTATES_H

// Gaudi include(s).
#include <GaudiKernel/StatusCode.h>
#include <GaudiKernel/MsgStream.h>

// System include(s).
#include <atomic>
#include <cstddef>
#include <cstdint>
#include <initializer_list>
#include <iosfwd>
#include <iterator>
#include <vector>

namespace ASync {

   /// Algorithm execution state
   ///
   /// This class encodes the state machine for the execution of algorithms
   /// within a single event.
   ///
   /// @author Benedikt Hegner
   /// @author Danilo Piparo
   ///
   class AlgExecutionStates final {

   public:
      /// Execution states of the algorithms
      enum State : uint8_t {
         CONTROLREADY = 0,
         DATAREADY    = 1,
         RESOURCELESS = 2,
         SCHEDULED    = 3,
         EVTACCEPTED  = 4,
         EVTREJECTED  = 5,
         ERROR        = 6
      };

      /// Constructor with all required arguments
      ///
      /// @param algsNumber The number of algorithms to track the states of
      /// @param msgStream Object to print messages with
      ///
      AlgExecutionStates( std::size_t algsNumber, MsgStream& msgStream );

      /// Iterator for visiting all algorithms that have a certain state
      ///
      /// This iterator is used in the code to loop over all algorithms that
      /// have a given state, and get their index in a C++-ish way.
      ///
      class Iterator final : public std::iterator< std::forward_iterator_tag,
                                                   std::size_t > {

      public:
         /// Constructor with all required arguments
         ///
         /// @param state The state that all algorithms being iterated over
         ///              have to have
         /// @param vec The underlying vector that the iterator uses
         /// @param pos The position in the underlying vector that this
         ///            iterator represents
         ///
         Iterator( State state, const std::vector< std::atomic< State > >& vec,
                   std::vector< std::atomic< State > >::const_iterator pos );

         /// Check for the equality of two objects
         ///
         /// @param rhs The iterator object being compared to this one
         /// @return @c true if the object on the right hand side is the same
         ///         as this one, @c false otherwise
         ///
         bool operator==( const Iterator& rhs ) const;

         /// Check for the inequality of two objects
         ///
         /// @param rhs The iterator object being compared to this one
         /// @return @c true if the object on the right hand side is different
         ///         from this one, @c false if they are the same
         ///
         bool operator!=( const Iterator& rhs ) const;

         /// Step to the next valid element of the underlying vector
         Iterator& operator++();

         /// Step to the next valid element of the underlying vector
         Iterator& operator++( int );

         /// Get the current algorithm's index
         std::size_t operator*() const;

      private:
         /// The state that all algorithms being iterated over have to have
         State                                               m_state;
         /// The underlying vector that the iterator uses
         const std::vector< std::atomic< State > >*          m_vec;
         /// The position in the underlying vector that this iterator
         /// represents
         std::vector< std::atomic< State > >::const_iterator m_pos;

      }; // class Iterator

      /// Get the begin iterator for a given algorithm state
      ///
      /// @param kind The algorithm state that the iterator is to be for
      /// @return The begin iterator for the specified state
      ///
      Iterator begin( State kind ) const;

      /// Get the end iterator for a given algorithm state
      ///
      /// @param kind The algorithm state that the iterator is to be for
      /// @return The end iterator for the specified state
      ///
      Iterator end( State kind ) const;

      /// Set one agorithm to a specific state
      ///
      /// @param iAlgo The index of the algorithm being updated
      /// @paran newState The new state to set for the algorithm
      /// @return The usual @c StatusCode values
      ///
      StatusCode set( std::size_t iAlgo, State newState );

      /// Reset the state of every algorithm to @c INITIAL
      void reset();

      /// Check if the collection contains at least one state of the requested
      /// type
      ///
      /// @param state The state to look for
      /// @return @c true if at least one algorithm has the state in question,
      ///         @c false otherwise
      ///
      bool contains( State state ) const;

      /// Check if the collection contains at least one state of any of the
      /// listed types
      ///
      /// @param states The list of states to look for
      /// @return @c true if at least one algorithm has one of the states in
      ///         question, @c false otherwise
      ///
      bool containsAny( std::initializer_list< State > states ) const;

      /// Check if the collection contains only states of the listed types
      ///
      /// @param states The list of states to look for
      /// @return @c true if all algorithms have one of the states listed,
      ///         @c false otherwise
      ///
      bool containsOnly( std::initializer_list< State > states ) const;

      /// Get the state of one of the algorithms
      ///
      /// @param iAlgo The index of the algorithm being investigated
      /// @return The state of the algorithm in question
      ///
      State operator[]( unsigned int iAlgo ) const;

      /// The number of algorithms being tracked
      std::size_t size() const;

      /// The number of algorithms having a specific state
      ///
      /// @param state The state to count algorithms for
      /// @return The number of algorithms that have the requested state
      ///
      std::size_t sizeOfSubset( State state ) const;

      /// Dump the state of the object to the specified stream
      ///
      /// @param out The stream to print the state of the object to
      ///
      void dump( std::ostream& out ) const;

   private:
      /// Function used by the messaging macro(s)
      MsgStream& msgStream( MSG::Level level ) const;

      /// Vector keeping track of the state of each algorithm
      std::vector< std::atomic< State > > m_states;
      /// Message stream object used for logging messages
      MsgStream* m_msgStream;

   }; // class AlgExecutuionStates

} // namespace ASync

namespace std {

   /// Print operator for the @c ASync::AlgExecutionStates::State enumeration
   std::ostream& operator<<( std::ostream& out,
                             ASync::AlgExecutionStates::State state );
   /// Print operator for @c ASync::AlgExecutionStates
   std::ostream& operator<<( std::ostream& out,
                             const ASync::AlgExecutionStates& states );

} // namespace std

#endif // ASYNCEVENTLOOP_ALGEXECUTIONSTATES_H
