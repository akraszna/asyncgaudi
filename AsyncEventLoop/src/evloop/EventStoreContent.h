// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCEVENTLOOP_EVENTSTORECONTENT_H
#define ASYNCEVENTLOOP_EVENTSTORECONTENT_H

// Gaudi include(s).
#include <GaudiKernel/StatusCode.h>

// Boost include(s).
#include <boost/dynamic_bitset.hpp>

// System include(s).
#include <cstddef>
#include <mutex>
#include <vector>

// Forward declaration(s).
class MsgStream;
namespace Gaudi {
   class Algorithm;
}

namespace ASync {

   /// Class used for keeping track of which objects are available in the store
   ///
   /// The logic of the scheduler is that it just trusts that whatever an
   /// algorithm promised that it will produce, it will produce. So when an
   /// algorithm's execution is finished, the code assumes that the output
   /// objects that the algorithm declared are now ready for consumtion.
   ///
   /// One objects of this type per processing / event slot is used used by
   /// @c ASync::SchedulerSvc to select which algorithms are ready to run
   /// at any given time.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class EventStoreContent {

   public:
      /// Constructor with the vector of algorithms in the job
      EventStoreContent( const std::vector< Gaudi::Algorithm* > algs,
                         MsgStream& msg );
      /// Copy constructor
      EventStoreContent( const EventStoreContent& parent );

      /// Assignment operator
      EventStoreContent& operator=( const EventStoreContent& rhs );

      /// Set one of the algorithms as having finished its execution
      StatusCode setAlgExecuted( std::size_t alg );

      /// Check if an algorithm is ready to be run
      bool isAlgExecutable( std::size_t alg ) const;

      /// Reset the event store content
      void reset();

   private:
      /// Type representing object availability / requirements
      typedef boost::dynamic_bitset<> DataObjColl_t;

      /// The dependencies of all of the algorithms of the job
      std::vector< DataObjColl_t > m_algDependencies;
      /// The outputs of all of the algorithms of the job
      std::vector< DataObjColl_t > m_algOutputs;
      /// The current content of the event store
      DataObjColl_t m_storeContent;
      /// Mutex for the event store content
      mutable std::mutex m_storeContentMutex;

      /// Pointer to the parent's message stream object
      MsgStream* m_msg;

   }; // class EventStoreContent

} // namespace ASync

#endif // ASYNCEVENTLOOP_EVENTSTORECONTENT_H
