# Components implementing an asynchronous Gaudi event loop

This package collects all the code necessary to run an asynchronous event
loop using Gaudi. It provides 2 libraries:
  - `AsyncEventLoopLib`: A shared library that one can use to set up an
    asynchronous job with. (Using the `ASync::Job` class.)
  - `AsyncEventLoop`: A component library that provides all the Gaudi
    components necessary to run an asynchronous job through `ASync::Job`.
