# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

# Customise the behaviour of CMakeGraphVizOptions.cmake.
set( GRAPHVIZ_EXTERNAL_LIBS OFF )
set( GRAPHVIZ_EXECUTABLES OFF )
set( GRAPHVIZ_GENERATE_DEPENDERS OFF )
set( GRAPHVIZ_IGNORE_TARGETS ".*Pkg$" ".*PkgPrivate$" )
