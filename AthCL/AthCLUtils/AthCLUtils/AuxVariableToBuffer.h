// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCLUTILS_AUXVARIABLETOBUFFER_H
#define ATHCLUTILS_AUXVARIABLETOBUFFER_H

// Project include(s).
#include "AthCLInterfaces/Definitions.h"

// Athena include(s).
#include "AthContainersInterfaces/AuxTypes.h"

// System include(s).
#include <string>
#include <type_traits>

// Forward declaration(s).
namespace SG {
   class IConstAuxStore;
}

namespace AthCL {

   // Forward declaration(s).
   class Device;

   /// Functor creating an OpenCL buffer from a fundamental xAOD variable
   ///
   /// This class is used internally by @c AuxStoreProcessor to prepare the
   /// input buffers of the OpenCL kernel.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   template< typename Type >
   class AuxVariableToBuffer {

      // Some sanity check(s).
      static_assert( std::is_fundamental< Type >::value,
                     "Only fundamental types can be provided to OpenCL" );

   public:
      /// Constructor with the device and the name of the variable to extract
      AuxVariableToBuffer( const Device& device, const std::string& varname );
      /// Destructor
      ~AuxVariableToBuffer();

      /// Function creating a buffer with the requested variable's payload
      cl_mem operator()( cl_command_queue queue,
                         const SG::IConstAuxStore& store,
                         cl_event* event = nullptr );

   private:
      /// The auxiliary ID of the requested variable
      SG::auxid_t m_auxid;
      /// The buffer allocated on the GPU for the variable
      cl_mem m_deviceBuffer;

   }; // class AuxVariableToBuffer

} // namespace AthCL

#endif // ATHCLUTILS_AUXVARIABLETOBUFFER_H
