// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCLUTILS_BUFFERTOAUXVARIABLE_H
#define ATHCLUTILS_BUFFERTOAUXVARIABLE_H

// Project include(s).
#include "AthCLInterfaces/Definitions.h"

// EDM include(s).
#include "AthContainersInterfaces/AuxTypes.h"

// System include(s).
#include <string>
#include <type_traits>

// Forward declaration(s).
namespace SG {
   class IAuxStore;
}

namespace AthCL {

   /// Functor copying an OpenCL buffer back into an auxiliary variable
   ///
   /// This class is used internally by @c AuxStoreProcessor to collect the
   /// output buffers of the OpenCL kernel.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   template< typename Type >
   class BufferToAuxVariable {

      // Some sanity check(s).
      static_assert( std::is_fundamental< Type >::value,
                     "Only fundamental types can be provided to OpenCL" );

   public:
      /// Constructor with the device and the name of the variable to update
      BufferToAuxVariable( const std::string& varname );

      /// Function scheduling the copy of the buffer back into host memory
      void operator()( cl_command_queue queue,
                       cl_mem buffer,
                       SG::IAuxStore& store,
                       const cl_event& kernelEvent,
                       cl_event* event = nullptr ) const;

   private:
      /// The auxiliary ID of the requested variable
      SG::auxid_t m_auxid;

   }; // class BufferToAuxVariable

} // namespace AthCL

#endif // ATHCLUTILS_BUFFERTOAUXVARIABLE_H
