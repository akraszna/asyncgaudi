// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCLUTILS_DEVICES_H
#define ATHCLUTILS_DEVICES_H

// Project include(s).
#include "AthCLInterfaces/Device.h"

// System include(s).
#include <vector>

namespace AthCL {

   /// Helper class for accessing the OpenCL devices at runtime
   ///
   /// This class should only be used directly when not inside of a
   /// Gaudi/Athena job. As there one needs to use @c AthCL::IDeviceSvc
   /// for this purpose instead.
   ///
   /// This class is mainly meant for setting up unit tests with various
   /// code pieces.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class Devices {

   public:
      /// Destructor
      ~Devices();

      /// The singleton object accessor function
      static Devices& instance();

      /// Return a list of all available devices on the running platform
      const std::vector< Device >& devices() const;

   private:
      /// Private constructor, to implement the singleton design
      Devices();

      /// All the devices that are available at runtime
      std::vector< Device > m_devices;

   }; // class Devices

} // namespace AthCL

#endif // ATHCLUTILS_DEVICES_H
