// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCLUTILS_PROGRAM_H
#define ATHCLUTILS_PROGRAM_H

// Project include(s).
#include "AthCLInterfaces/Definitions.h"

// System include(s).
#include <string>
#include <vector>

namespace AthCL {

   // Forward declaration(s).
   class Device;

   /// Wrapper around the OpenCL program handling functions
   ///
   /// This is very similar to @c cl::Program in the end, I only opted for
   /// implementing my own take on it is to:
   ///   - Make it work also when OpenCL is not available during the build;
   ///   - Make it easier to find/load files in an ATLAS environment setup.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class Program {

   public:
      /// Constructor with the OpenCL file name(s) to compile into a program
      Program( const Device& device,
               const std::vector< std::string >& clFiles );
      /// Destructor
      ~Program();

      /// Return the OpenCL program identifier
      cl_program operator()() const;

   private:
      /// Identifier for the compiled OpenCL program in memory
      cl_program m_program;

   }; // class Program

} // namespace AthCL

#endif // ATHCLUTILS_PROGRAM_H
