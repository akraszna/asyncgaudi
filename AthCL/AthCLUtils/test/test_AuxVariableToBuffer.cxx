// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AthCLUtils/AuxVariableToBuffer.h"
#include "AthCLUtils/Devices.h"

// Project include(s).
#include "AthCLInterfaces/Definitions.h"
#include "AthCLInterfaces/Device.h"

// Athena include(s).
#include "AthContainers/DataVector.h"
#include "AthContainers/AuxElement.h"
#include "AthContainers/AuxStoreInternal.h"

// System include(s).
#include <cmath>
#include <iostream>

int main() {

   // Create a test container.
   DataVector< SG::AuxElement > interface;
   SG::AuxStoreInternal aux;
   interface.setStore( &aux );

   // Accessors used in interacting with the variables.
   static const SG::AuxElement::Accessor< float >
      etaAcc( "eta" ), phiAcc( "phi" ), ptAcc( "pt" );

   // Fill it with some variables.
   for( std::size_t i = 0; i < 10; ++i ) {
      SG::AuxElement* e = new SG::AuxElement();
      interface.push_back( e );
      etaAcc( *e ) = -2.0f + i * 0.3f;
      phiAcc( *e ) = -M_PI + i * 0.4f;
      ptAcc( *e ) = 10000.0f + i * 1000.0f;
   }

   // Loop over all available devices.
   for( const AthCL::Device& device : AthCL::Devices::instance().devices() ) {

      // Create buffer converters.
      AthCL::AuxVariableToBuffer< float > etaBuff( device, "eta" );
      AthCL::AuxVariableToBuffer< float > phiBuff( device, "phi" );
      AthCL::AuxVariableToBuffer< float > ptBuff ( device, "pt" );

      // Create a command queue.
      cl_command_queue queue = 0;
      cl_int queueErr = 0;
      queue = clCreateCommandQueueWithProperties( device.ctx(),
                                                  device.deviceID(),
                                                  0, &queueErr );
      CL_EXP_CHECK( queueErr );

      // Exercise the buffer converters.
      /*auto eta =*/ etaBuff( queue, aux );
      /*auto phi =*/ phiBuff( queue, aux );
      /*auto pt  =*/ ptBuff ( queue, aux );

      // Let the user know that we succeeded.
      std::cout << "Created buffers on device: " << device << std::endl;
   }

   // Return gracefully.
   return 0;
}
