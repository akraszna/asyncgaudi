// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AthCLUtils/Program.h"
#include "AthCLUtils/Devices.h"

// System include(s).
#undef NDEBUG
#include <cassert>
#include <exception>
#include <iostream>

int main() {

   // Access the helper object.
   static const AthCL::Devices& devs = AthCL::Devices::instance();

   // If no OpenCL devices are available, then return right away. As the test
   // won't be able to do anything constructive anyway...
   if( devs.devices().size() == 0 ) {
      std::cout << "No OpenCL devices are available" << std::endl;
      return 0;
   }

   // Loop over all available devices.
   for( const AthCL::Device& dev : devs.devices() ) {

      // Create a program object that exists.
      AthCL::Program validProg( dev, { "test_Program.cl" } );   
      std::cout << "Test program created for device: " << dev << std::endl;

      // Check that creating a non-existing test program actually fails.
      bool exceptionCaught = false;
      try {
         AthCL::Program invalidProg( dev, { "foo.cl" } );
      } catch( std::exception& ) {
         exceptionCaught = true;
      }
      assert( exceptionCaught == true );
   }

   return 0;
}
