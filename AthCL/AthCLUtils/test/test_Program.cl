// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

/// Dummy kernel function to test the OpenCL code compilation with.
__kernel void foo( int a, __global float* b ) {

   // Get the global ID, and make sure -- similar to how it's done in CUDA --
   // that it's a valid ID.
   const int id = get_global_id( 0 );
   if( id >= a ) {
      return;
   }

   // Perform a trivial operation.
   b[ id ] *= 1.5f;
   return;
}
