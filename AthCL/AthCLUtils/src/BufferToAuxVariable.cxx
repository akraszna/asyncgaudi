// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AthCLUtils/BufferToAuxVariable.h"

// Project include(s).
#include "AthCLInterfaces/Device.h"

// Athena include(s).
#include "AthContainersInterfaces/IAuxStore.h"
#include "AthContainers/AuxTypeRegistry.h"

// System include(s).
#include <stdexcept>

namespace AthCL {

   template< typename Type >
   BufferToAuxVariable< Type >::
   BufferToAuxVariable( const std::string& varname )
   : m_auxid( SG::AuxTypeRegistry::instance().getAuxID< Type >( varname ) ) {

   }

   template< typename Type >
   void BufferToAuxVariable< Type >::operator()( cl_command_queue queue,
                                                 cl_mem buffer,
                                                 SG::IAuxStore& store,
                                                 const cl_event& kernelEvent,
                                                 cl_event* event ) const {

      // Set up the copy from the OpenCL buffer.
      CL_EXP_CHECK( clEnqueueReadBuffer( queue, buffer, ( event == nullptr ),
                                         0, store.size() * sizeof( Type ),
                                         store.getData( m_auxid, store.size(),
                                                        store.size() ),
                                         1, &kernelEvent, event ) );

      // This is to avoid compilation warnings when OpenCL is not available.
      if( queue ) {}
      if( buffer ) {}
      store.size();
      if( kernelEvent ) {}
      if( event ) {}

      // Return gracefully.
      return;
   }

} // namespace AthCL

/// Macro helping to instantiate the class for all POD types.
#define INST_CONV( TYPE )                           \
   template class AthCL::BufferToAuxVariable< TYPE >

// Instantiate the type for all POD types.
INST_CONV( char );
INST_CONV( unsigned char );
INST_CONV( short );
INST_CONV( unsigned short );
INST_CONV( int );
INST_CONV( unsigned int );
INST_CONV( long );
INST_CONV( unsigned long );
INST_CONV( long long );
INST_CONV( unsigned long long );
INST_CONV( float );
INST_CONV( double );

// Clean up.
#undef INST_CONV
