// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AthCLUtils/Program.h"

// Project include(s).
#include "AthCLInterfaces/Device.h"

// Boost include(s).
#include <boost/filesystem.hpp>

// System include(s).
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>

namespace {

   /// Heleper function returning the search paths for OpenCL files
   std::vector< std::string > clSearchPaths() {

      // The result vector. Start by adding the local directory into it.
      std::vector< std::string > result( { "" } );

      // Get the ROOT_INCLUDE_PATH environment variable.
      const char* envvar = getenv( "CL_SEARCH_PATH" );
      if( ! envvar ) {
         // If the environment variable is not available, check only in the
         // current directory.
         return result;
      }
      // Create a helper string variable around the bare character array.
      const std::string envvarString( envvar );

      // If the string doesn't contain ':' anywhwere, then assume that it's a
      // single path.
      if( envvarString.find( ':' ) == std::string::npos ) {
         result.push_back( envvarString );
         return result;
      }

      // Tokenize the environment variable.
      std::string::size_type pos = 0, prevPos = 0;
      while( ( pos = envvarString.find( ':', prevPos ) ) !=
              std::string::npos ) {
         result.push_back( envvarString.substr( prevPos, pos - prevPos ) );
         prevPos = pos + 1;
      }

      // Return whatever we got out of that...
      return result;
   }

   /// Helper function for finding OpenCL files at runtime
   ///
   /// @param name The "logical" name of the file
   /// @return The absolute path of the file on disk
   ///
   std::string findCLFile( const std::string& name ) {

      // Figure out the list of paths that need to be checked for the file.
      static const std::vector< std::string > paths = clSearchPaths();

      // Loop over the search paths, and try to find the file in them.
      for( const std::string& path : paths ) {
         // Construct a path object.
         boost::filesystem::path filePath( ( path == "" ? std::string( "" ) :
                                             path + "/" ) + name );
         if( boost::filesystem::is_regular_file( filePath ) ) {
            return filePath.native();
         }
      }

      // If the file was not found, return an empty string.
      return "";
   }

}

namespace AthCL {

   Program::Program( const Device& device,
                     const std::vector< std::string >& clFiles )
   : m_program( 0 ) {

      // Read in the contents of all of the files into memory.
      std::vector< std::string > clFileContents;
      for( const std::string& fname : clFiles ) {

         // Try to open the file.
         std::ifstream clFile( findCLFile( fname ) );
         if( ! clFile.is_open() ) {
            std::cerr << "Couldn't open file: " << fname << std::endl;
            throw std::runtime_error( "Couldn't open file: " + fname );
         }

         // Read in all lines of text from the file.
         std::ostringstream fileContents;
         std::string line;
         while( std::getline( clFile, line ) ) {
            fileContents << line << "\n";
         }

         // Add the read contents to the list.
         clFileContents.push_back( fileContents.str() );

         // Explicitly close the file.
         clFile.close();
      }

      // Create variables that can be passed to the OpenCL function(s).
      std::vector< const char* > strings;
      std::vector< size_t > stringSizes;
      strings.reserve( clFileContents.size() );
      stringSizes.reserve( clFileContents.size() );
      for( const std::string& clFileContent : clFileContents ) {
         strings.push_back( clFileContent.c_str() );
         stringSizes.push_back( clFileContent.length() );
      }

      // Create the program object in memory.
      CL_IGNORE( cl_int errCode = 0 );
      CL_IGNORE( m_program = clCreateProgramWithSource( device.ctx(),
                                                        strings.size(),
                                                        strings.data(),
                                                        stringSizes.data(),
                                                        &errCode ) );
      CL_EXP_CHECK( errCode );

      // Now build it.
      const cl_device_id did = device.deviceID();
#ifdef OPENCL_AVAILABLE
      if( clBuildProgram( m_program, 1, &did, "",
                          nullptr, nullptr ) != CL_SUCCESS ) {

        // Give the user a detailed explanation of what happened.
        char buffer[ 10240 ];
        CL_EXP_CHECK( clGetProgramBuildInfo( m_program, did,
                                             CL_PROGRAM_BUILD_LOG,
                                             sizeof( buffer ), buffer,
                                             nullptr ) );
        std::cerr << "OpenCL build failed with:\n" << buffer << std::endl;
        throw std::runtime_error( "OpenCL program build failed" );
      }
#endif // OPENCL_AVAILABLE

      // If everything succeeded, make sure that the program remains in
      // memory as long as this object is alive.
      CL_EXP_CHECK( clRetainProgram( m_program ) );
   }

   Program::~Program() {

      // Release the program.
      CL_IGNORE( clReleaseProgram( m_program ) );
   }

   cl_program Program::operator()() const {

      return m_program;
   }

} // namespace AthCL
