// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AthCLUtils/AuxVariableToBuffer.h"

// Project include(s).
#include "AthCLInterfaces/Device.h"

// Athena include(s).
#include "AthContainersInterfaces/IConstAuxStore.h"
#include "AthContainers/AuxTypeRegistry.h"

// System include(s).
#include <stdexcept>

namespace AthCL {

   /// Maximal size of the arrays handled by @c AthCL::AuxVariableToBuffer
   static const std::size_t MAX_ARRAY_SIZE = 10000;

   template< typename Type >
   AuxVariableToBuffer< Type >::
   AuxVariableToBuffer( const Device& device, const std::string& varname )
   : m_auxid( SG::AuxTypeRegistry::instance().getAuxID< Type >( varname ) ) {

      // Create the buffer on the device, and make it big enough to hold
      // 10k elements of this primitive variable.
      CL_IGNORE( cl_int retCode = 0 );
      CL_IGNORE( m_deviceBuffer =
                 clCreateBuffer( device.ctx(), CL_MEM_READ_WRITE,
                                 MAX_ARRAY_SIZE * sizeof( Type ), nullptr,
                                 &retCode ) );
      CL_EXP_CHECK( retCode );
      CL_EXP_CHECK( clRetainMemObject( m_deviceBuffer ) );

      // This is to avoid compilation warnings when OpenCL is not available.
      device.deviceID();
   }

   template< typename Type >
   AuxVariableToBuffer< Type >::
   ~AuxVariableToBuffer() {

      // Release the device memory.
      CL_IGNORE( clReleaseMemObject( m_deviceBuffer ) );
   }

   template< typename Type >
   cl_mem AuxVariableToBuffer< Type >::
   operator()( cl_command_queue queue, const SG::IConstAuxStore& store,
               cl_event* event ) {

      // Make sure that the container is not too large.
      if( store.size() > MAX_ARRAY_SIZE ) {
         throw std::runtime_error( "Received array is too large" );
      }

      // Set up the copy into the previously allocated buffer.
      CL_EXP_CHECK( clEnqueueWriteBuffer( queue, m_deviceBuffer,
                                          ( event == nullptr ), 0,
                                          store.size() * sizeof( Type ),
                                          store.getData( m_auxid ), 0, nullptr,
                                          event ) );

      // This is to avoid compilation warnings when OpenCL is not available.
      if( queue ) {}
      if( event ) {}

      // Return the buffer that the variable will eventually end up in.
      return m_deviceBuffer;
   }

} // namespace AthCL

/// Macro helping to instantiate the class for all POD types.
#define INST_CONV( TYPE )                           \
   template class AthCL::AuxVariableToBuffer< TYPE >

// Instantiate the type for all POD types.
INST_CONV( char );
INST_CONV( unsigned char );
INST_CONV( short );
INST_CONV( unsigned short );
INST_CONV( int );
INST_CONV( unsigned int );
INST_CONV( long );
INST_CONV( unsigned long );
INST_CONV( long long );
INST_CONV( unsigned long long );
INST_CONV( float );
INST_CONV( double );

// Clean up.
#undef INST_CONV
