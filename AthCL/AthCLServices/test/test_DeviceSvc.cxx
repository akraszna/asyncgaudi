// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Project include(s).
#include "AsyncEventLoop/Job.h"

// System include(s).
#include <iostream>

/// Helper macro for checking StatusCode return codes
#define SC_CHECK( EXP )                                                   \
   do {                                                                   \
      if( ! EXP.isSuccess() ) {                                           \
         std::cerr << "Failed to execute: " << #EXP << std::endl;         \
         return 1;                                                        \
      }                                                                   \
   } while( false )

int main() {

   // Set up a job that just instantiates AthCL::DeviceSvc, and runs just a
   // dummy algorithm.
   ASync::Job job( 1 );
   SC_CHECK( job.configure() );
   SC_CHECK( job.setProperty( "CreateSvc", "[\"AthCL::DeviceSvc\"]" ) );
   SC_CHECK( job.addAlgorithm( "TestAlgorithm1", "TestAlgorithm1" ) );

   // Run the job.
   SC_CHECK( job.run() );

   // Return gracefully.
   return 0;
}
