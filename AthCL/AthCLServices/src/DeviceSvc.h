// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCLSERVICES_DEVICESVC_H
#define ATHCLSERVICES_DEVICESVC_H

// Project include(s).
#include "AthCLInterfaces/IDeviceSvc.h"

// Gaudi include(s).
#include <GaudiKernel/extends.h>
#include <GaudiKernel/Property.h>
#include <GaudiKernel/Service.h>

// System include(s).
#include <vector>

namespace AthCL {

   /// Service providing information about OpenCL devices
   ///
   /// This service is meant to configure in one central place how the
   /// OpenCL devices should be used in a job.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class DeviceSvc : public extends< Service, IDeviceSvc > {

   public:
      /// Inherit the base class's constructor
      using extends::extends;

      /// @name Function(s) inherited from @c Service
      /// @{

      /// Function initialising the service
      virtual StatusCode initialize() override;

      /// Function finalising the service
      virtual StatusCode finalize() override;

      /// @}

      /// @name Function(s) inherited from @c AthCL::IDeviceSvc
      /// @{

      /// Get all the OpenCL devices available for the job
      virtual const std::vector< Device >& devices() const override;

      /// @}

   private:
      /// A list of all available OpenCL devices
      std::vector< Device > m_devices;

   }; // class DeviceSvc

} // namespace AthCL

#endif // ATHCLSERVICES_DEVICESVC_H
