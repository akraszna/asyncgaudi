// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "DeviceSvc.h"

// Project include(s).
#include "AsyncBaseComps/CheckMacros.h"
#include "AsyncBaseComps/MessagingMacros.h"
#include "AthCLInterfaces/Definitions.h"

// System include(s).
#include <cstddef>

// Declare the service to Gaudi.
DECLARE_COMPONENT( AthCL::DeviceSvc )

namespace AthCL {

   StatusCode DeviceSvc::initialize() {

      // Initialise the base class.
      ATH_CHECK( Service::initialize() );

      // Get the available platforms
      static const cl_uint MAX_PLATFORMS = 100;
      cl_uint nPlatforms = 0;
      cl_platform_id platforms[ MAX_PLATFORMS ];
      CL_IGNORE( clGetPlatformIDs( MAX_PLATFORMS, platforms, &nPlatforms ) );

      // Loop over all the platforms.
      for( cl_uint iPlatform = 0; iPlatform < nPlatforms; ++iPlatform ) {

         // Get the name of the platform.
         static const std::size_t MAX_STRING_LENGTH = 200;
         char platformName[ MAX_STRING_LENGTH ] = "";
         CL_SC_CHECK( clGetPlatformInfo( platforms[ iPlatform ],
                                         CL_PLATFORM_NAME, MAX_STRING_LENGTH,
                                         platformName, nullptr ) );

         // Get all the devices provided by this platform.
         static const cl_uint MAX_DEVICES = 100;
         cl_uint nDevices = 0;
         cl_device_id devices[ MAX_DEVICES ];
         CL_SC_CHECK( clGetDeviceIDs( platforms[ iPlatform ],
                                      CL_DEVICE_TYPE_ALL, MAX_DEVICES,
                                      devices, &nDevices ) );

         // Create AthCL::Device objects for all found devices.
         for( cl_uint iDevice = 0; iDevice < nDevices; ++iDevice ) {

            // Get the name of the device.
            char deviceName[ MAX_STRING_LENGTH ] = "";
            CL_SC_CHECK( clGetDeviceInfo( devices[ iDevice ], CL_DEVICE_NAME,
                                          MAX_STRING_LENGTH, deviceName,
                                          nullptr ) );

            // Create a context for this device.
            cl_context ctx;
            CL_IGNORE( cl_int contextErr = 0 );
            CL_IGNORE( ctx = clCreateContext( nullptr, 1, devices + iDevice,
                                              nullptr, nullptr,
                                              &contextErr ) );
            CL_SC_CHECK( contextErr );

            // Make sure that the device is not released while the service is
            // running.
            CL_SC_CHECK( clRetainDevice( devices[ iDevice ] ) );
            // Make sure that the context is not release while the service is
            // running.
            CL_SC_CHECK( clRetainContext( ctx ) );

            // Finally, create the AthCL::Device object.
            m_devices.emplace_back( platformName, deviceName,
                                    platforms[ iPlatform ],
                                    devices[ iDevice ], ctx );
         }
      }

      // Print them for information.
      ATH_MSG_INFO( "Found OpenCL device(s):" );
      if( m_devices.empty() ) {
         ATH_MSG_INFO( "  NONE" );
      } else {
         for( const Device& device : m_devices ) {
            ATH_MSG_INFO( "  - " << device );
         }
      }

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode DeviceSvc::finalize() {

      // Release all the devices.
      for( const Device& device : m_devices ) {
         // This is just to silence a warning when OpenCL is not available
         // for the compilation.
         device.deviceID();
         // Release the context.
         CL_SC_CHECK( clReleaseContext( device.ctx() ) );
         // Release this device.
         CL_SC_CHECK( clReleaseDevice( device.deviceID() ) );
      }

      // Finalise the base class.
      ATH_CHECK( Service::finalize() );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   const std::vector< Device >& DeviceSvc::devices() const {

      return m_devices;
   }

} // namespace AthCL
