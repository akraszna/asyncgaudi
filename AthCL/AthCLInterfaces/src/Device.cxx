// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AthCLInterfaces/Device.h"

// System include(s).
#include <iostream>

namespace AthCL {

   Device::Device( const std::string& platformName,
                   const std::string& deviceName,
                   cl_platform_id platformId, cl_device_id deviceId,
                   cl_context ctx )
   : m_platformName( platformName ), m_deviceName( deviceName ),
     m_platformId( platformId ), m_deviceId( deviceId ), m_ctx( ctx ) {

   }

   bool Device::operator==( const Device& rhs ) const {

      return ( ( m_platformName == rhs.m_platformName ) &&
               ( m_deviceName == rhs.m_deviceName ) );
   }

   bool Device::operator!=( const Device& rhs ) const {

      return !( this->operator==( rhs ) );
   }

   const std::string& Device::platformName() const {

      return m_platformName;
   }

   void Device::setPlatformName( const std::string& platformName ) {

      m_platformName = platformName;
      return;
   }

   const std::string& Device::deviceName() const {

      return m_deviceName;
   }

   void Device::setDeviceName( const std::string& deviceName ) {

      m_deviceName = deviceName;
      return;
   }

   cl_platform_id Device::platformID() const {

      return m_platformId;
   }

   void Device::setPlatformID( cl_platform_id id ) {

      m_platformId = id;
      return;
   }

   cl_device_id Device::deviceID() const {

      return m_deviceId;
   }

   void Device::setDeviceID( cl_device_id id ) {

      m_deviceId = id;
      return;
   }

   cl_context Device::ctx() const {

      return m_ctx;
   }

} // namespace AthCL

namespace std {

   std::ostream& operator<<( std::ostream& out, const AthCL::Device& dev ) {

      out << dev.deviceName() << " (platform: " << dev.platformName() << ")";
      return out;
   }

} // namespace std
