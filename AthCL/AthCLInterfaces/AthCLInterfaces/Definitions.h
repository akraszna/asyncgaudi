// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCLINTERFACES_DEFINITIONS_H
#define ATHCLINTERFACES_DEFINITIONS_H

// Project include(s).
#include "AsyncBaseComps/MessagingMacros.h"

// Gaudi include(s).
#include <GaudiKernel/StatusCode.h>

// System include(s).
#include <iostream>
#include <stdexcept>

/// Until all references to this variable are removed, hardcode it to 1
#define OPENCL_AVAILABLE 1

// OpenCL include(s).
#define CL_TARGET_OPENCL_VERSION 220
#ifdef __APPLE__
#   include <OpenCL/cl.h>
#else
#   include <CL/cl.h>
#endif // __APPLE__

/// Helper function for checking OpenCL return codes in Athena components
#define CL_SC_CHECK( EXP )                                               \
   do {                                                                  \
      const cl_int result = EXP;                                         \
      if( result != CL_SUCCESS ) {                                       \
         ATH_MSG_ERROR( "Failed to execute: " << #EXP );                 \
         return StatusCode::FAILURE;                                     \
      }                                                                  \
   } while( false )

/// Helper function for checking OpenCL return codes where we don't return
/// StatusCode
#define CL_EXP_CHECK( EXP )                                              \
   do {                                                                  \
      const cl_int result = EXP;                                         \
      if( result != CL_SUCCESS ) {                                       \
         std::cerr << "Failed to execute: " << #EXP << std::endl;        \
         throw std::runtime_error( "Failed to execute: " #EXP );         \
      }                                                                  \
   } while( false )

/// Helper function for running OpenCL functions without checking their return
/// values
#define CL_IGNORE( EXP ) EXP;

#endif // ATHCLINTERFACES_DEFINITIONS_H
