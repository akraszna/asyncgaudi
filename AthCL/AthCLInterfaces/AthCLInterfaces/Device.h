// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCLINTERFACES_DEVICE_H
#define ATHCLINTERFACES_DEVICE_H

// Local include(s).
#include "AthCLInterfaces/Definitions.h"

// System include(s).
#include <iosfwd>
#include <string>

/// Namespace for the OpenCL specific code
namespace AthCL {

   /// Trivial representation for an OpenCL device
   ///
   /// This is meant to be an Athena-specific representation for an OpenCL
   /// device that's available at runtime.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class Device {

   public:
      /// Constructor with a platform and device name
      Device( const std::string& platformName, const std::string& deviceName,
              cl_platform_id platformId, cl_device_id deviceId,
              cl_context ctx );
      /// Use the default copy constructor
      Device( const Device& parent ) = default;

      /// Use the default assignment operator
      Device& operator=( const Device& rhs ) = default;

      /// Check the equality of two devices
      bool operator==( const Device& rhs ) const;
      /// Check the non-equality of two devices
      bool operator!=( const Device& rhs ) const;

      /// Get the name of the platform that provides the device
      const std::string& platformName() const;
      /// Set the name of the platform that provides the device
      void setPlatformName( const std::string& platformName );

      /// Get the name of the device
      const std::string& deviceName() const;
      /// Set the name of the device
      void setDeviceName( const std::string& deviceName );

      /// Get the platform identifier of the device
      cl_platform_id platformID() const;
      /// Set the platform identifier of the device
      void setPlatformID( cl_platform_id id );

      /// Get the device identifier of the device
      cl_device_id deviceID() const;
      /// Set the device identifier of the device
      void setDeviceID( cl_device_id id );

      /// Get the context created for the device
      cl_context ctx() const;

   private:
      /// The name of the platform that provides the device
      std::string m_platformName;
      /// The name of the device
      std::string m_deviceName;
      /// The platform identifier
      cl_platform_id m_platformId;
      /// The device identifier
      cl_device_id m_deviceId;
      /// Context created for the device
      cl_context m_ctx;

   }; // class Device

} // namespace AthCL

namespace std {

   /// Output/print operator for an @c AthOpenCL::Device object
   std::ostream& operator<<( std::ostream& out, const AthCL::Device& dev );

} // namespace std

#endif // ATHCLINTERFACES_DEVICE_H
