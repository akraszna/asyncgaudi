// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHCLINTERFACES_IDEVICESVC_H
#define ATHCLINTERFACES_IDEVICESVC_H

// Local include(s).
#include "AthCLInterfaces/Device.h"

// Gaudi include(s).
#include <GaudiKernel/IService.h>

// System include(s).
#include <vector>

namespace AthCL {

   /// Interface for interacting with OpenCL devices
   ///
   /// The underlying service is meant to provide code that's not OpenCL
   /// aware, with information about the OpenCL devices available to the job.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class IDeviceSvc : public virtual IService {

   public:
      /// Declare the ID of this interface
      DeclareInterfaceID( AthCL::IDeviceSvc, 1, 0 );

      /// Get all the OpenCL devices available for the job
      virtual const std::vector< Device >& devices() const = 0;

   }; // class IDeviceSvc

} // namespace AthCL

#endif // ATHCLINTERFACES_IDEVICESVC_H
