// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHSYCLSERVICES_QUEUEPOOLSVC_H
#define ATHSYCLSERVICES_QUEUEPOOLSVC_H

// Project include(s).
#include "AthSYCLInterfaces/IQueuePoolSvc.h"

// Gaudi include(s).
#include <GaudiKernel/Service.h>
#include <GaudiKernel/Property.h>

// SYCL include(s).
#include <CL/sycl.hpp>

// TBB include(s).
#include <tbb/concurrent_queue.h>

// System include(s).
#include <atomic>

namespace AthSYCL {

   /// Service managing the SYCL queue objects during a job
   ///
   /// This service is responsible to manage all of the @c cl::sycl::queue
   /// objects needed by a job in memory. Making sure that queue objects are
   /// re-used as much as possible during the job.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class QueuePoolSvc : public extends< Service, IQueuePoolSvc > {

   public:
      /// Inherit the base class's constructor(s)
      using extends::extends;

      /// @name Interface inherited from @c IService
      /// @{

      /// Initialise the service
      virtual StatusCode initialize() override;

      /// Finalise the service
      virtual StatusCode finalize() override;

      /// @}

      /// @name Interface inherited from @c AthSYCL::IQueuePoolSvc
      /// @{

      /// Function for accessing a @c cl::sycl::queue object during the job
      ///
      /// This function should be used in the execute function of algorithms
      /// and tools to access a SYCL queue to which they may send calculations.
      ///
      /// @return A smart object wrapping a @c cl::sycl::queue object
      ///
      virtual QueueHolder getQueue() override;

      /// Function to call after an asynchronous calculation has finished
      ///
      /// This is meant to allow the service to keep track of how many
      /// calculations are in flight at any given moment, and return queue
      /// objects to the callers accordingly.
      ///
      virtual void asyncCalcFinished() override;

      /// Put a given queue back into the pool
      ///
      /// This function is used by @c AthSYCL::QueueHolder to return its
      /// @c cl::sycl::queue object into the service's pool once the user no
      /// longer needs it.
      ///
      /// @param holder The @c cl::sycl::queue holder object
      ///
      virtual void yieldQueue( QueueHolder& holder ) override;

      /// @}

   private:
      /// @name Service properties
      /// @{

      /// The number of "accelerator" queues to set up
      Gaudi::Property< unsigned int > m_nAccQueues{ this, "NAccQueues", 2,
         "The number of accelerator queues to set up" };
      /// The number of host queues to set up
      Gaudi::Property< unsigned int > m_nHostQueues{ this, "NHostQueues", 4,
         "The number of host queues to set up" };
      /// The maximum number of kernels in flight in a job
      Gaudi::Property< unsigned int > m_maxKernelsInFlight{ this,
         "MaxKernelsInFlight", 2, "The maximum number of kernels in flight" };

      /// @}

      /// @name Implementation variable(s)
      /// @{

      /// Accelerator queues managed by the service
      tbb::concurrent_bounded_queue< cl::sycl::queue* > m_accQueues;
      /// Host queues managed by the service
      tbb::concurrent_bounded_queue< cl::sycl::queue* > m_hostQueues;
      /// The number of kernels in flight right now
      std::atomic_uint m_kernelsInFlight = 0;

      /// @}

   }; // class QueuePoolSvc

} // namespace AthSYCL

#endif // ATHSYCLSERVICES_QUEUEPOOLSVC_H
