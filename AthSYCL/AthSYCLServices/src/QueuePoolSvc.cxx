// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "QueuePoolSvc.h"
#include "AcceleratorSelector.h"

// Project include(s).
#include "AsyncBaseComps/CheckMacros.h"
#include "AsyncBaseComps/MessagingMacros.h"

// System include(s).
#include <cassert>
#include <memory>

// Declare the component to the Gaudi component factory
DECLARE_COMPONENT( AthSYCL::QueuePoolSvc )

namespace AthSYCL {

   StatusCode QueuePoolSvc::initialize() {

      // Helper device selector for setting up the accelerator queues.
      AcceleratorSelector accSelector;

      // Set up the requested number of accelerator queues.
      for( unsigned int i = 0; i < m_nAccQueues.value(); ++i ) {

         // Create a new queue.
         auto queue = std::make_unique< cl::sycl::queue >( accSelector );
         if( ! queue->is_host() ) {
            m_accQueues.push( queue.release() );
         }
      }

      // Helper device selector for setting up the host queues.
      cl::sycl::host_selector hostSelector;

      // Set up the requested number of host queues.
      for( unsigned int i = 0; i < m_nHostQueues.value(); ++i ) {

         // Create a new queue.
         auto queue = std::make_unique< cl::sycl::queue >( hostSelector );
         m_hostQueues.push( queue.release() );
      }

      // Reset the internal counter(s).
      m_kernelsInFlight = 0;

      // Tell the user what happened.
      ATH_MSG_INFO( "Created " << m_accQueues.size() << " accelerator and "
                    << m_hostQueues.size() << " host queue(s)" );
#ifndef TRISYCL_CL_SYCL_HPP
      if( msgLevel( MSG::DEBUG ) ) {
         ATH_MSG_DEBUG( "Accelerated queue(s) is/are using device(s):" );
         const std::size_t nAccQueues = m_accQueues.size();
         if( nAccQueues > 0 ) {
            for( std::size_t i = 0; i < nAccQueues; ++i ) {
               cl::sycl::queue* queue = nullptr;
               m_accQueues.pop( queue );
               auto devName =
                  queue->get_device().get_info< cl::sycl::info::device::name >();
               ATH_MSG_DEBUG( "  o " << devName );
               m_accQueues.push( queue );
            }
         } else {
            ATH_MSG_DEBUG( "   N/A" );
         }
      }
#endif // not TRISYCL_CL_SYCL_HPP

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode QueuePoolSvc::finalize() {

      // Delete all of the accelerator queues.
      const std::size_t nAccQueues = m_accQueues.size();
      while( ! m_accQueues.empty() ) {
         cl::sycl::queue* queue = nullptr;
         m_accQueues.pop( queue );
         assert( queue != nullptr );
         delete queue;
      }

      // Delete all of the host queues.
      const std::size_t nHostQueues = m_hostQueues.size();
      while( ! m_hostQueues.empty() ) {
         cl::sycl::queue* queue = nullptr;
         m_hostQueues.pop( queue );
         assert( queue != nullptr );
         delete queue;
      }

      // Tell the user what happened.
      ATH_MSG_INFO( "Deleted " << nAccQueues << " accelerator and "
                    << nHostQueues << " host queue(s)" );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   QueueHolder QueuePoolSvc::getQueue() {

      // Try to get an accelerator queue at first.
      cl::sycl::queue* queue = nullptr;
      if( ( m_kernelsInFlight.load() < m_maxKernelsInFlight.value() ) &&
          m_accQueues.try_pop( queue ) ) {
         assert( queue != nullptr );
         ++m_kernelsInFlight;
         return QueueHolder( queue, *this );
      }

      // If that's not available (right now), fall back to a host queue.
      if( m_hostQueues.try_pop( queue ) ) {
         assert( queue != nullptr );
         return QueueHolder( queue, *this );
      }

      // If we got this far, apparently we underestimated in the job
      // configuration how many host queues should be created. So let's
      // create a new queue now.
      ATH_MSG_INFO( "Creating an additional host queue" );
      cl::sycl::host_selector hostSelector;
      auto hostQueue = std::make_unique< cl::sycl::queue >( hostSelector );
      return QueueHolder( hostQueue.release(), *this );
   }

   void QueuePoolSvc::asyncCalcFinished() {

      --m_kernelsInFlight;
      return;
   }

   void QueuePoolSvc::yieldQueue( QueueHolder& holder ) {

      // Add back the queue to the right list.
      if( holder.get().is_host() ) {
         m_hostQueues.push( &( holder.get() ) );
      } else {
         m_accQueues.push( &( holder.get() ) );
      }
      return;
   }

} // namespace AthSYCL
