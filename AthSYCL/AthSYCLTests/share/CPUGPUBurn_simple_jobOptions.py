# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

# Set up the SYCL service(s).
from AsyncEventLoop.ServiceMgr import svcMgr
from AthSYCLServices.AthSYCLServicesConf import AthSYCL__QueuePoolSvc
queueSvc = AthSYCL__QueuePoolSvc( 'AthSYCL::QueuePoolSvc',
                                  NAccQueues = 5, NHostQueues = 2,
                                  MaxKernelsInFlight = 5,
                                  OutputLevel = 2 )
svcMgr += queueSvc

# Import the algorithm sequence object.
from AsyncEventLoop.AlgSequence import AlgSequence
algSeq = AlgSequence()

# Add all algorithms.
from AthSYCLTests.AthSYCLTestsConf import *
algSeq += AthSYCLTests__ParticleCreatorAlg( 'ParticleCreator',
                                            MaxParticles = 1000,
                                            OutputKey = 'TestParticles' )
algSeq += AthSYCLTests__CPUGPUBurnerAlg( 'CPUGPUBurner',
                                         CalcIterations = 100,
                                         InputKey = 'TestParticles',
                                         QueuePoolSvc = queueSvc )

# Set some global job properties.
from AsyncEventLoop.AsyncEventLoopFlags import asyncEventLoopFlags
asyncEventLoopFlags.EvtMax.set_Value_and_Lock( 1000 )
asyncEventLoopFlags.EventPrintoutInterval.set_Value_and_Lock( 100 )

# Set additional properties on the algorithms/services based on the global job
# properties.
if asyncEventLoopFlags.SynchronousScheduler():
   from GaudiHive.GaudiHiveConf import AvalancheSchedulerSvc
   svcMgr += AvalancheSchedulerSvc( 'AvalancheSchedulerSvc',
               PreemptiveIOBoundTasks = True,
               MaxIOBoundAlgosInFlight = asyncEventLoopFlags.NAccTasks() )
   for alg in [ algSeq.CPUGPUBurner ]:
      alg.IsAsynchronous = False
      alg.IsIOBound = True
      pass
else:
   from AsyncEventLoop.AsyncEventLoopConf import ASync__SchedulerSvc
   svcMgr += ASync__SchedulerSvc( 'ASync::SchedulerSvc',
               DataFlowOutput = 'sycl_data_flot.dot' )
   for alg in [ algSeq.CPUGPUBurner ]:
      alg.IsAsynchronous = True
      pass
   pass
