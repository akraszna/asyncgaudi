// Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "CPUGPUCruncherAlg.h"

// SYCL/OpenCL include(s).
#include <CL/sycl.hpp>
#include <CL/cl.h>

// System include(s).
#include <memory>
#include <vector>

DECLARE_COMPONENT( AthSYCL::CPUGPUCruncherAlg )

namespace {

   /// Function called by OpenCL when a particular calculation has finished
   void callbackFunction( cl_event /*event*/, cl_int /*execStatus*/,
                          void* userData ) {

      ASync::AlgTaskPtr_t::element_type* taskPtr =
         reinterpret_cast< ASync::AlgTaskPtr_t::element_type* >( userData );
      ASync::AlgTaskPtr_t task( taskPtr );
      ASync::enqueue( std::move( task ) ).ignore();

      return;
   }

} // private namespace

namespace AthSYCL {

   StatusCode CPUGPUCruncherAlg::initialize() {

      // Retrieve/initialise the necessary object(s).
      ATH_CHECK( m_calibSvc.retrieve() );
      ATH_CHECK( m_queuePool.retrieve() );

      // Make sure that at least one output key is set up, otherwise
      // the algorithm won't be able to work.
      if( m_outputKeys.value().size() == 0 ) {
         const std::string outputName = name() + "_Output";
         ATH_MSG_WARNING( "No output key was specified, using: "
                          << outputName );
         m_outputKeys.value().emplace_back( outputName, *this );
      }

      // Get "safe" values for the times.
      const double avgTime = std::max( m_avgRuntime.value(), 0.0 );
      const double varTime = std::max( m_varRuntime.value(), 0.0 );

      // Calculate how many FP operations to execute.
      const unsigned int milliseconds_count = avgTime * 1000;
      const std::chrono::milliseconds milliseconds( milliseconds_count );
      m_fpOps = ( m_calibSvc->getFPOperationsFor( m_floatArraySize.value(),
                                                  milliseconds ) *
                  m_fopsMultiplier.value() );

      // Tell the user what is about to happen.
      ATH_MSG_INFO( "Will execute for " << avgTime << " +- " << varTime
                    << " second(s)" );
      ATH_MSG_INFO( "For this, will execute " << m_fpOps
                    << " floating point operation(s) on "
                    << m_floatArraySize.value() << " float values" );
      ATH_MSG_INFO( "Running in "
                    << ( isIOBound() ? "I/O-bound " : "non-I/O-bound " )
                    << ( isAsynchronous() ? "asynchronous" : "synchronous" )
                    << " mode" );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode CPUGPUCruncherAlg::execute( const EventContext& ) const {

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode CPUGPUCruncherAlg::mainExecute( const EventContext&,
                                              ASync::AlgTaskPtr_t pet ) const {

      // Create the buffer that the calculation will be run on.
      const std::vector< float > helper( m_floatArraySize.value(), 1.34f );
      typedef cl::sycl::buffer< cl::sycl::cl_float > Buffer_t;
      auto buffer = std::make_unique< Buffer_t >( helper.begin(),
                                                  helper.end() );
      Buffer_t* bufferPtr = buffer.get();

      // Record the buffer into the event store.
      ATH_CHECK( evtStore()->record( std::move( buffer ),
                                     m_outputKeys.value()[ 0 ].objKey() ) );

      // Get the queue to send the calculation to.
      auto queue = m_queuePool->getQueue();
      if( ! queue ) {
         ATH_MSG_FATAL( "Logic error detected in the code!" );
         return StatusCode::FAILURE;
      }

      // Launch the calculation.
      const unsigned int fpOps = m_fpOps;
      cl::sycl::event event =
         queue.get().submit( [ fpOps, bufferPtr ]( cl::sycl::handler& hdlr ) {
            // Get an update accessor to the buffer.
            auto bufferAcc =
               bufferPtr->get_access< cl::sycl::access::mode::read_write >( hdlr );
            // Set up the calculation.
            const cl::sycl::range< 1 > range( bufferPtr->get_count() );
            const unsigned int arraySize = bufferPtr->get_count();
            hdlr.parallel_for< class CpuGpuCrunch >(
               range, [=]( cl::sycl::id< 1 > id ) {
                  // Perform the requested number of floating point operations.
                  const std::size_t i = id.get( 0 );
                  for( unsigned int j = 0; j < fpOps; ++j ) {
                     bufferAcc[ id ] *= bufferAcc[ ( i + j ) % arraySize ];
                  }
               } );
         } );

      // Decide what to do.
      if( event.is_host() ) {
         // Tell the user what happened.
         ATH_MSG_DEBUG( "The calculation ran on the host" );
         // Schedule the post-execution task right away.
         if( pet ) {
            ATH_CHECK( enqueue( std::move( pet ) ) );
         }
      } else {
         // Set up a callback that will notify us when the calculation is done.
         ATH_MSG_DEBUG( "The calculation was dispatched to: "
                        << queue.get().get_device().get_info< cl::sycl::info::device::name >() );
         if( clSetEventCallback( event.get(), CL_COMPLETE, callbackFunction,
                                 pet.release() ) != CL_SUCCESS ) {
            ATH_MSG_ERROR( "Couldn't set up the callback function!" );
            return StatusCode::FAILURE;
         }
         // Allow the queue pool to reuse the queue right away.
         m_queuePool->asyncCalcFinished();
      }

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode CPUGPUCruncherAlg::postExecute( const EventContext& ) const {

      // Just print a debug message.
      ATH_MSG_DEBUG( "CPU / GPU crunching task finished" );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

} // namespace AthSYCL
