// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "CPUGPUBurnerAlg.h"

// Project include(s).
#include "AsyncBaseComps/ReadHandle.h"

// Athena include(s).
#include "AthContainers/AuxElement.h"
#include "AthContainers/DataVector.h"

// SYCL/OpenCL include(s).
#include <CL/sycl.hpp>
#include <CL/cl.h>

// System include(s).
#include <memory>

DECLARE_COMPONENT( AthSYCLTests::CPUGPUBurnerAlg )

namespace {

   /// Type holding on to the SYCL objects while the calculation is running
   class SYCLObjects {
   public:
      /// Type of the SYCL buffers used
      typedef cl::sycl::buffer< cl::sycl::cl_float > Buffer_t;

      /// Constructor with the size of the container, and the pointers to the
      /// host variables
      SYCLObjects( std::size_t size, float* ptPtr, float* etaPtr,
                   float* phiPtr )
      : m_pt( std::make_unique< Buffer_t >( ptPtr, ptPtr + size ) ),
        m_eta( std::make_unique< Buffer_t >( etaPtr, etaPtr + size ) ),
        m_phi( std::make_unique< Buffer_t >( phiPtr, phiPtr + size ) ),
        m_workItems( size ) {}

      /// Reset/delete the buffer objects.
      void resetBuffers() {
         m_pt.reset();
         m_eta.reset();
         m_phi.reset();
         return;
      }

      /// The buffer variables
      std::unique_ptr< Buffer_t > m_pt, m_eta, m_phi;
      /// The size of the work
      cl::sycl::range< 1 > m_workItems;
      /// Flag showing whether the calculation was launched asynchronously
      bool m_async = false;

   }; // struct SYCLObjects

   /// Function called by OpenCL when a particular calculation has finished
   void callbackFunction( cl_event /*event*/, cl_int /*execStatus*/,
                          void* userData ) {

      ASync::AlgTaskPtr_t::element_type* taskPtr =
         reinterpret_cast< ASync::AlgTaskPtr_t::element_type* >( userData );
      ASync::AlgTaskPtr_t task( taskPtr );
      ASync::enqueue( std::move( task ) ).ignore();

      return;
   }

} // private namespace

namespace AthSYCLTests {

   StatusCode CPUGPUBurnerAlg::initialize() {

      // Tell the user what will happen.
      ATH_MSG_INFO( "Will perform " << m_calcIterations.value()
                    << " calculation interations on the " << m_inputKey.value()
                    << " container" );
      ATH_MSG_INFO( "Running in "
                    << ( isIOBound() ? "I/O-bound " : "non-I/O-bound " )
                    << ( isAsynchronous() ? "asynchronous" : "synchronous" )
                    << " mode" );

      // Retrieve all used components.
      ATH_CHECK( m_queuePool.retrieve() );

      // Reset the internal counters.
      m_allCalculations = 0;
      m_accCalculations = 0;

      // Return gracefully:
      return StatusCode::SUCCESS;
   }

   StatusCode CPUGPUBurnerAlg::finalize() {

      // Tell the user what happened.
      ATH_MSG_INFO( "o Number of all calculations        : "
                    << m_allCalculations.load() );
      ATH_MSG_INFO( "o Number of accelerated calculations: "
                    << m_accCalculations.load() << " ("
                    << ( m_accCalculations.load() * 100.0 /
                         m_allCalculations.load() )
                    << "%)" );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode CPUGPUBurnerAlg::execute( const EventContext& ctx ) const {

      // Launch the calculation.
      ATH_CHECK( launch( ctx, ASync::AlgTaskPtr_t() ) );
      // Run the post-execute step. Note that the post-execute step acts as
      // an implicit synchronisation point.
      ATH_CHECK( postExecute( ctx ) );

      // Return gracefully:
      return StatusCode::SUCCESS;
   }

   StatusCode CPUGPUBurnerAlg::mainExecute( const EventContext& ctx,
                                            ASync::AlgTaskPtr_t pet ) const {

      // Launch the calculation.
      ATH_CHECK( launch( ctx, std::move( pet ) ) );

      // Return gracefully:
      return StatusCode::SUCCESS;
   }

   StatusCode CPUGPUBurnerAlg::postExecute( const EventContext& ctx ) const {

      ATH_MSG_VERBOSE( "In postExecute for event " << ctx.evt() );

      // Retrieve and reset the helper object, if it exists.
      if( evtStore()->contains< const SYCLObjects >( m_inputKey.objKey() +
                                                     "_SYCLObjects" ) ) {
         const SYCLObjects* syclObjects = nullptr;
         ATH_CHECK( evtStore()->retrieve( syclObjects,
                                          m_inputKey.objKey() +
                                          "_SYCLObjects" ) );
         const_cast< SYCLObjects* >( syclObjects )->resetBuffers();
         if( syclObjects->m_async ) {
            m_queuePool->asyncCalcFinished();
         }
      }

      // Return gracefully:
      return StatusCode::SUCCESS;
   }

   StatusCode CPUGPUBurnerAlg::launch( const EventContext&,
                                       ASync::AlgTaskPtr_t pet ) const {

      // Create the read handle.
      ASync::ReadHandle< DataVector< SG::AuxElement > >
         container( m_inputKey );

      // If the container is empty, just schedule the post-exec task right
      // away, and don't bother setting up any SYCL calculations.
      if( container->size() == 0 ) {
         if( pet ) {
            ATH_CHECK( enqueue( std::move( pet ) ) );
         }
         return StatusCode::SUCCESS;
      }

      // Get a non-const pointer to the container. This is ugly, but for now
      // I just want to make things work "somehow".
      DataVector< SG::AuxElement >* nc_container =
         const_cast< DataVector< SG::AuxElement >* >( container.get() );

      // Helper objects
      static const SG::AuxElement::Accessor< float >
         ptAcc( "pt" ), etaAcc( "eta" ), phiAcc( "phi" );

      // Set up the SYCL objects.
      auto syclObjects =
         std::make_unique< SYCLObjects >( container->size(),
                                          ptAcc.getDataArray( *nc_container ),
                                          etaAcc.getDataArray( *nc_container ),
                                          phiAcc.getDataArray( *nc_container ) );
      SYCLObjects* syclObjectsPtr = syclObjects.get();

      // Store the object into the event store, without ever having declared
      // it as an output of the algorithm.
      ATH_CHECK( evtStore()->record( std::move( syclObjects ),
                                     m_inputKey.objKey() + "_SYCLObjects" ) );

      // Lambda for performing the CPU/GPU burning.
      auto cpuGpuBurn = []( std::size_t nIterations,
                            float eta, float phi, float pt ) -> float {
         for( std::size_t i = 0; i < nIterations; ++i ) {
            if( eta < 0.0 ) {
               pt += 1.05 * cl::sycl::cos( cl::sycl::pow( cl::sycl::abs(
                               cl::sycl::sin( phi + pt ) ), eta ) );
            } else {
               pt += 0.95 * cl::sycl::sin( cl::sycl::pow( cl::sycl::abs(
                               cl::sycl::cos( phi + pt ) ), eta ) );
            }
         }
         return pt;
      };

      // Get the queue to send the calculation to.
      auto queue = m_queuePool->getQueue();
      if( ! queue ) {
         ATH_MSG_FATAL( "Logic error detected in the code!" );
         return StatusCode::FAILURE;
      }

      // Launch the calculation.
      std::size_t calcIterations = m_calcIterations.value();
      cl::sycl::event event =
         queue.get().submit( [ syclObjectsPtr, calcIterations,
                               cpuGpuBurn ]( cl::sycl::handler& hdlr ) {

            // Get accessors to all the variables/buffers.
            auto etaAcc =
               syclObjectsPtr->m_eta->get_access< cl::sycl::access::mode::read >( hdlr );
            auto phiAcc =
               syclObjectsPtr->m_phi->get_access< cl::sycl::access::mode::read >( hdlr );
            auto ptAcc =
               syclObjectsPtr->m_pt->get_access< cl::sycl::access::mode::read_write >( hdlr );

            // Set up the calculation.
            hdlr.parallel_for< class CpuGpuBurn >( syclObjectsPtr->m_workItems,
                                                   [=]( cl::sycl::id< 1 > id ) {
                  ptAcc[ id ] = cpuGpuBurn( calcIterations, etaAcc[ id ],
                                            phiAcc[ id ], ptAcc[ id ] );
               } );
         } );

      // Decide what to do.
      if( event.is_host() ) {
         // Tell the user what happened.
         ATH_MSG_DEBUG( "The calculation ran on the host" );
         // Schedule the post-execution task right away.
         if( pet ) {
            ATH_CHECK( enqueue( std::move( pet ) ) );
         }
      } else {
#ifndef TRISYCL_CL_SYCL_HPP
         // Tell the user what happened.
         ATH_MSG_DEBUG( "The calculation was dispatched to: "
                        << queue.get().get_device().get_info< cl::sycl::info::device::name >() );
         ++m_accCalculations;
         syclObjectsPtr->m_async = true;
         // Set up a callback that will notify us when the calculation is done.
         if( clSetEventCallback( event.get(), CL_COMPLETE, callbackFunction,
                                 pet.release() ) != CL_SUCCESS ) {
            ATH_MSG_ERROR( "Couldn't set up the callback function!" );
            return StatusCode::FAILURE;
         }
#else
         ATH_MSG_FATAL( "triSYCL should not offload calculations!" );
         return StatusCode::FAILURE;
#endif // not TRISYCL_CL_SYCL_HPP
      }

      // Increment the number of all calculations.
      ++m_allCalculations;

      // Return gracefully:
      return StatusCode::SUCCESS;
   }

} // namespace AthSYCLTests
