// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#ifndef ATHSYCLTESTS_CPUGPUCRUNCHERALG_H
#define ATHSYCLTESTS_CPUGPUCRUNCHERALG_H

// Project include(s).
#include "AsyncBaseComps/Algorithm.h"
#include "AsyncBaseComps/ReadHandleKeyArray.h"
#include "AsyncBaseComps/WriteHandleKeyArray.h"
#include "AsyncComps/ICPUGPUCruncherCalibSvc.h"
#include "AthSYCLInterfaces/IQueuePoolSvc.h"

// Gaudi include(s).
#include <GaudiKernel/Property.h>
#include <GaudiKernel/ServiceHandle.h>

namespace AthSYCL {

   /// A generic CPU/GPU cruncher algorithm to use for performance tests
   ///
   /// This algorithm can be used to run a calculation that copies a set
   /// amount of memory back and forth between the host and the device, and
   /// runs a set number of floating point operations on each element of
   /// that memory.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class CPUGPUCruncherAlg : public ASync::Algorithm {

   public:
      // Inherit the base class's constructor(s).
      using ASync::Algorithm::Algorithm;

      /// @name Basic algorithm function(s).
      /// @{

      /// Function initialising the algorithm
      virtual StatusCode initialize() override;

      /// Execute the CPU / GPU burning synchronously
      virtual StatusCode execute( const EventContext& ) const override;

      /// @}

      /// @name Asynchronous algorithm function(s).
      /// @{

      /// Execute the CPU / GPU burning asynchronously
      virtual StatusCode mainExecute( const EventContext&,
                                      ASync::AlgTaskPtr_t pet ) const override;
      /// Post-execute step for the CPU / GPU burning
      virtual StatusCode postExecute( const EventContext& ) const override;

      /// @}

   private:
      /// @name Algorithm properties
      /// @{

      /// Key(s) of the input objects
      Gaudi::Property< ASync::ReadHandleKeyArray > m_inputKeys{ this,
         "InputKeys", ASync::ReadHandleKeyArray( *this ),
         "Keys of input objects that the algorithm needs to wait for" };
      Gaudi::Property< ASync::WriteHandleKeyArray > m_outputKeys{ this,
         "OutputKeys", ASync::WriteHandleKeyArray( *this ),
         "Keys of output objects that the algorithm produces" };

      /// The number of floating point variables to handle
      Gaudi::Property< unsigned int > m_floatArraySize{ this,
         "FloatArraySize", 1000, "Size of the float array to use" };
      /// Target runtime for the algorithm
      Gaudi::Property< double > m_avgRuntime{ this, "AverageRuntime", 0.1,
         "Average runtime for the algorithm, in seconds" };
      /// Variation in the algorithm's runtime
      Gaudi::Property< double > m_varRuntime{ this, "RuntimeVariation", 0.0,
         "Variation in the runtime, in seconds" };
      /// Multiplier to use for the number of floating point operations
      Gaudi::Property< unsigned int > m_fopsMultiplier{ this,
         "FloatOpsMultiplier", 1,
         "Multiplier for the idealised number of FPops" };

      /// @}

      /// Service to get a calibration from
      ServiceHandle< ASync::ICPUGPUCruncherCalibSvc > m_calibSvc{ this,
         "CalibSvc", "ASync::CPUGPUCruncherCalibSvc" };
      /// Service providing SYCL queue objects during the run
      ServiceHandle< AthSYCL::IQueuePoolSvc > m_queuePool{ this, "QueuePoolSvc",
         "AthSYCL::QueuePoolSvc" };

      /// Floating point operations to execute for the target runtime
      unsigned int m_fpOps = 0;

   }; // CPUGPUCruncherAlg

} // namespace AthSYCL

#endif // ATHSYCLTESTS_CPUGPUCRUNCHERALG_H
