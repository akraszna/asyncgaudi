// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Project include(s).
#include "AsyncEventLoop/Job.h"

// Boost include(s).
#include <boost/program_options.hpp>

// System include(s).
#include <iostream>
#include <string>

/// Helper macro checking status codes inside main(...)
#define SC_CHECK( EXP )                                           \
   do {                                                           \
      const auto sc = EXP;                                        \
      if( ! sc.isSuccess() ) {                                    \
         std::cerr << "Failed to execute: " << #EXP << std::endl; \
         return 1;                                                \
      }                                                           \
   } while( false )

/// A helper definition
namespace po = boost::program_options;

namespace {

   /// Helper function used to extract unsigned integer command line arguments
   /// as strings.
   std::string getOption( po::variables_map& vm, const std::string& name ) {

      return std::to_string( vm[ name ].as< int >() );
   }

} // private namespace

int main( int argc, char* argv[] ) {

   // Parse all the command line options.
   po::options_description desc( "CUDA based CPU / GPU burning test" );
   desc.add_options()
      ( "help,h", "Produce this help message" )
      ( "events,e", po::value< int >()->default_value( 1000 ),
        "The number of events to process" )
      ( "max-particles,p", po::value< int >()->default_value( 200 ),
        "Maximum number of particles to generate in each event" )
      ( "calc-iterations,c", po::value< int >()->default_value( 10 ),
        "Number of calculation iterations to execute" )
      ( "synchronous,y",
        "Run the code in synchronous mode (the default is asynchronous)" )
      ( "cpu-threads,t", po::value< int >()->default_value( 2 ),
        "The number of CPU threads to use" )
      ( "gpu-queues,q", po::value< int >()->default_value( 2 ),
        "The number of accelerated SYCL queues to use" )
      ( "gpu-tasks,g", po::value< int >()->default_value( 2 ),
        "The number of parallel accelerated SYCL tasks to use" );
   po::variables_map vm;
   po::store( po::command_line_parser( argc, argv ).options( desc ).run(),
              vm );
   po::notify( vm );

   // Print the help / command line options if requested.
   if( vm.count( "help" ) ) {
      std::cout << desc << std::endl;
      return 0;
   }

   // Set up the main job object.
   ASync::Job job( vm[ "cpu-threads" ].as< int >() );
   if( vm.count( "synchronous" ) ) {
      job.setSchedulerName( "AvalancheSchedulerSvc" );
   }
   SC_CHECK( job.configure() );
   SC_CHECK( job.setProperty( "ASync::EventLoopMgr", "EventPrintoutInterval",
                              "100" ) );
   SC_CHECK( job.setProperty( "EvtMax", getOption( vm, "events" ) ) );
   SC_CHECK( job.setProperty( "AthSYCL::QueuePoolSvc", "NAccQueues",
                              getOption( vm, "gpu-queues" ) ) );
   SC_CHECK( job.setProperty( "AthSYCL::QueuePoolSvc", "NHostQueues",
                              getOption( vm, "cpu-threads" ) ) );
   SC_CHECK( job.setProperty( "AthSYCL::QueueRunnerSvc", "MaxKernelsInFlight",
                              getOption( vm, "gpu-tasks" ) ) );

   // Add the algorithms of the job.
   SC_CHECK( job.addAlgorithm( "AthSYCLTests::ParticleCreatorAlg",
                               "ParticleCreator",
                               { { "MaxParticles",
                                   getOption( vm, "max-particles" ) } } ) );
   SC_CHECK( job.addAlgorithm( "AthSYCLTests::CPUGPUBurnerAlg", "CPUGPUBurner",
                               { { "CalcIterations",
                                   getOption( vm, "calc-iterations" ) } } ) );

   // Specific options for (a)synchronous execution.
   if( vm.count( "synchronous" ) ) {
      SC_CHECK( job.setProperty( "AvalancheSchedulerSvc",
                                 "PreemptiveIOBoundTasks", "1" ) );
      SC_CHECK( job.setProperty( "AvalancheSchedulerSvc",
                                 "MaxIOBoundAlgosInFlight",
                                 getOption( vm, "gpu-tasks" ) ) );
      SC_CHECK( job.setProperty( "CPUGPUBurner", "IsAsynchronous", "0" ) );
      SC_CHECK( job.setProperty( "CPUGPUBurner", "IsIOBound", "1" ) );
   } else {
      SC_CHECK( job.setProperty( "CPUGPUBurner", "IsAsynchronous", "1" ) );
   }

   // Set up a few more events in flight than how many threads we use.
   // Otherwise the scheduler has a hard time keeping the TBB threads busy.
   if( vm[ "gpu-tasks" ].as< int >() > 0 ) {
      SC_CHECK( job.setProperty( "ASync::WhiteBoard", "EventSlots",
                std::to_string( vm[ "cpu-threads" ].as< int >() +
                                vm[ "gpu-tasks" ].as< int >() ) ) );
   }

   // And now run it.
   SC_CHECK( job.run() );

   // Return gracefully.
   return 0;
}
