// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// SYCL include(s).
#include <CL/sycl.hpp>

// OpenCL include(s).
#include <CL/cl.h>

// System include(s).
#include <chrono>
#include <iostream>
#include <thread>

/// Device selector disfavouring all non-functional devices on my laptop
///
/// Unfortunately 2 out of the 3 available OpenCL devices on my laptop don't
/// work correctly with the current LLVM SYCL implementation. :-( This selector
/// makes sure that the SYCL runtime would not use those devices.
///
/// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
///
class DeviceSelector : public cl::sycl::device_selector {

public:
   /// Function used for scoring the different devices
   virtual int operator()( const cl::sycl::device& device ) const override {

#ifndef TRISYCL_CL_SYCL_HPP
      // Disfavour all NVidia / CUDA devices.
      const std::string vendor =
         device.get_info< cl::sycl::info::device::vendor >();
      if( vendor.find( "NVIDIA" ) != std::string::npos ) {
         return -1;
      }
#endif // not TRISYCL_CL_SYCL_HPP

      // For anything else let's inherit the behaviour of
      // cl::sycl::device_selector::operator().
      if( device.is_gpu() ) {
         return 500;
      }
      if( device.is_accelerator() ) {
         return 400;
      }
      if( device.is_cpu() ) {
         return 300;
      }
      if( device.is_host() ) {
         return 100;
      }
      return -1;
   }

}; // class DeviceSelector

/// Function called by OpenCL when the SYCL calculation has finished
void callbackFunction( cl_event /*event*/, cl_int execStatus,
                       void* userData ) {

   // Tell the user what happened.
   std::cout << "Calculation has finished!" << std::endl;
   std::cout << "  execStatus = " << execStatus << std::endl;

   // Signal that the calculation has finished.
   *( reinterpret_cast< bool* >( userData ) ) = true;
   return;
}

int main() {

   // Create a large buffer of floating point numbers.
   static const std::size_t BUFFER_SIZE = 10000000;
   cl::sycl::buffer< cl::sycl::cl_float > buffer( BUFFER_SIZE );
   auto bufferWriter = buffer.get_access< cl::sycl::access::mode::write >();
   for( std::size_t i = 0; i < buffer.get_count(); ++i ) {
      bufferWriter[ i ] = 1.5f * i;
   }

   // Set up the SYCL objects necessary for the offloaded calculation.
   DeviceSelector deviceSelector;
   cl::sycl::queue queue( deviceSelector );
   cl::sycl::range< 1 > workItems( buffer.get_count() );
   const cl::sycl::device& device = queue.get_device();

   // Set up a lambda for performing some GPU burning operation that would
   // take a little bit.
   auto gpuBurn = []( float value ) -> float {
      float result = 0.0;
      for( int i = 0; i < 100; ++i ) {
         result +=
            cl::sycl::cos( cl::sycl::pow( cl::sycl::abs( cl::sycl::sin( result +
                                                                        value ) ),
                                          result + value ) );
      }
      return result;
   };

   // Set up a synchronisation object that will be used to wait for the
   // offloaded calculation to finish.
   bool isFinished = false;

   // Take the start time.
   auto startTime = std::chrono::high_resolution_clock::now();

   // Set up an offloaded calculation through SYCL, grabbing the event object
   // produced by the calculation.
   std::cout << "Launching the calculation..." << std::endl;
   cl::sycl::event event = queue.submit( [&]( cl::sycl::handler& handler ) {
      auto accessor =
         buffer.get_access< cl::sycl::access::mode::read_write >( handler );
      handler.parallel_for< class GPUBurn >( workItems,
                                             [=]( cl::sycl::id< 1 > id ) {
                                                accessor[ id ] =
                                                   gpuBurn( accessor[ id ] );
                                             } );
   } );

   // Decide what to do.
   if( event.is_host() ) {
      // Tell the user what happened.
      std::cout << "Calculation ran on the host." << std::endl;
   } else {
#ifndef TRISYCL_CL_SYCL_HPP
      // Tell the user what happened.
      std::cout << "Calculation was dispatched to: "
                << device.get_info< cl::sycl::info::device::name >()
                << std::endl;
      // Set up a callback that will notify us when the calculation is done.
      if( clSetEventCallback( event.get(), CL_COMPLETE, callbackFunction,
                              &isFinished ) != CL_SUCCESS ) {
         std::cerr << "Couldn't set up the callback function!" << std::endl;
         return 1;
      }
      // Wait for the calculation to finish.
      while( isFinished == false ) {
         std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
      }
#else
      std::cerr << "What?!? triSYCL launched a calculation asynchronously?!?"
                << std::endl;
      return 1;
#endif // not TRISYCL_CL_SYCL_HPP
   }

   // Calculate the time used for the calculation.
   auto stopTime = std::chrono::high_resolution_clock::now();
   auto calcTime =
      std::chrono::duration_cast< std::chrono::duration< double > >(
         stopTime - startTime );
   std::cout << "The calculation took " << calcTime.count() << " second(s)"
             << std::endl;

   // Return gracefully.
   return 0;
}
