// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// SYCL include(s).
#include <CL/sycl.hpp>

// System include(s).
#include <iostream>
#include <vector>

/// Function performing a linear transformation on a specific device
int testDevice( const cl::sycl::device& device,
                const std::vector< float >& values ) {

   // Create and fill a small buffer on which a linear transformation will be
   // done.
   cl::sycl::buffer< cl::sycl::cl_float, 1 > buffer( values.begin(),
                                                     values.end() );

   // Tell the user what's happening.
   std::cout << "Testing device:" << std::endl;
#ifndef TRISYCL_CL_SYCL_HPP
   std::cout << "  - name    = "
             << device.get_info< cl::sycl::info::device::name >()
             << std::endl;
   std::cout << "  - vendor  = "
             << device.get_info< cl::sycl::info::device::vendor >()
             << std::endl;
#endif // not TRISYCL_CL_SYCL_HPP
   std::cout << "  - is_host = " << device.is_host() << std::endl;
   std::cout << "  - is_cpu  = " << device.is_cpu() << std::endl;
   std::cout << "  - is_gpu  = " << device.is_gpu() << std::endl;
   std::cout << "  - is_accelerator = " << device.is_accelerator()
             << std::endl;

   // Set up the SYCL objects necessary for the linear transformation.
   cl::sycl::queue queue( device );
   cl::sycl::range< 1 > workItems( buffer.get_count() );

   // Set up a lambda for performing a linear transformation on a value.
   auto linearTransform = []( float value ) -> float {
      return 3.141592f * value + 3.456789f;
   };

   // Set up the linear transformation through SYCL.
   queue.submit( [&]( cl::sycl::handler& handler ) {
      auto deviceAccessor =
         buffer.get_access< cl::sycl::access::mode::read_write >( handler );
      handler.parallel_for< class LinearTransform1 >(
         workItems, [=]( cl::sycl::id< 1 > id ) {
            deviceAccessor[ id ] = linearTransform( deviceAccessor[ id ] );
         } );
   } );

   // Check the results of the linear transformation.
   float maxError = 0.0f;
   auto hostReadAccessor =
      buffer.get_access< cl::sycl::access::mode::read >();
   for( std::size_t i = 0; i < buffer.get_count(); ++i ) {
      maxError = std::max( maxError,
                           std::abs( hostReadAccessor[ i ] -
                                     linearTransform( values[ i ] ) ) );
   }
   std::cout << "Max. error is: " << maxError << std::endl;

   // Base the return code on the maximum error of the calculation.
   return ( maxError > 0.01 ? 1 : 0 );
}

int main() {

   // Fill a small vector with the input variables that will be used in the
   // test(s).
   static const std::size_t BUFFER_SIZE = 100;
   std::vector< float > buffer_input;
   buffer_input.reserve( BUFFER_SIZE );
   for( std::size_t i = 0; i < BUFFER_SIZE; ++i ) {
      buffer_input.push_back( 1.23f * i );
   }

   // Loop over all available SYCL platforms.
   for( const cl::sycl::platform& platform :
        cl::sycl::platform::get_platforms() ) {

      // Loop over all devices available from this platform.
      for( const cl::sycl::device& device : platform.get_devices() ) {

         // Skip NVidia devices.
#ifndef TRISYCL_CL_SYCL_HPP
         const std::string vendor =
            device.get_info< cl::sycl::info::device::vendor >();
         if( vendor.find( "NVIDIA" ) != std::string::npos ) {
            continue;
         }
#endif // not TRISYCL_CL_SYCL_HPP

         // Test this device.
         if( testDevice( device, buffer_input ) != 0 ) {
            return 1;
         }
      }
   }

   // Return gracefully.
   return 0;
}
