// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "AthSYCLInterfaces/QueueHolder.h"
#include "AthSYCLInterfaces/IQueuePoolSvc.h"

// System include(s).
#include <stdexcept>

namespace AthSYCL {

   QueueHolder::QueueHolder()
   : m_queue( nullptr ), m_pool( nullptr ) {

   }

   QueueHolder::QueueHolder( cl::sycl::queue* queue, IQueuePoolSvc& svc )
   : m_queue( queue ), m_pool( &svc ) {

   }

   QueueHolder::~QueueHolder() {

      if( m_queue && m_pool ) {
         m_pool->yieldQueue( *this );
      }
   }

   QueueHolder& QueueHolder::operator=( QueueHolder&& rhs ) {

      // Check if anything needs to be done.
      if( this == &rhs ) {
         return *this;
      }

      // Move the contents of the object.
      m_queue = rhs.m_queue;
      m_pool  = rhs.m_pool;

      // Make the received object forget about its contents.
      rhs.m_queue = nullptr;
      rhs.m_pool  = nullptr;

      // Return this object.
      return *this;
   }

   QueueHolder::operator bool() const {

      return ( m_queue != nullptr );
   }

   cl::sycl::queue& QueueHolder::get() {

      // Check if we can return a queue or not.
      if( m_queue == nullptr ) {
         throw std::logic_error( "No cl::sycl::queue object available" );
      }

      // Return the queue.
      return *m_queue;
   }

   const cl::sycl::queue& QueueHolder::get() const {

      // Check if we can return a queue or not.
      if( m_queue == nullptr ) {
         throw std::logic_error( "No cl::sycl::queue object available" );
      }

      // Return the queue.
      return *m_queue;
   }

} // namespace AthSYCL
