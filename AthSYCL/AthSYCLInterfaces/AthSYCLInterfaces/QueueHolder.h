// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHSYCLINTERFACES_QUEUEHOLDER_H
#define ATHSYCLINTERFACES_QUEUEHOLDER_H

// SYCL include(s).
#include <CL/sycl.hpp>

namespace AthSYCL {

   /// Forward declaration(s).
   class IQueuePoolSvc;

   /// Convenience type for passing @c cl::sycl::queue objects around
   ///
   /// In the jobs all @c cl::sycl::queue objects should be created by an
   /// instance of @c AthSYCL::IQueuePoolSvc. Users can only lend queues from
   /// that pool for temporary usage. This type helps with implementing this
   /// behaviour.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class QueueHolder {

   public:
      /// Default constructor
      QueueHolder();
      /// Constructor with the queue and the service owning the queue
      QueueHolder( cl::sycl::queue* queue, IQueuePoolSvc& svc );
      /// Disallow copying the object
      QueueHolder( const QueueHolder& ) = delete;
      /// Destructor
      ~QueueHolder();

      /// Copy assignment operator
      QueueHolder& operator=( const QueueHolder& ) = delete;
      /// Move assignment operator
      QueueHolder& operator=( QueueHolder&& rhs );

      /// Convenience operator for checking if the holder has a valid object
      operator bool() const;

      /// Get the queue held by the object (non-const)
      cl::sycl::queue& get();
      /// Get the queue held by the object (const)
      const cl::sycl::queue& get() const;

   private:
      /// The stream that the object holds
      cl::sycl::queue* m_queue;
      /// The service that created this holder object
      IQueuePoolSvc* m_pool;

   }; // QueueHolder

} // namespace AthSYCL

#endif // ATHSYCLINTERFACES_QUEUEHOLDER_H
