// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ATHSYCLINTERFACES_IQUEUEPOOLSVC_H
#define ATHSYCLINTERFACES_IQUEUEPOOLSVC_H

// Local include(s).
#include "AthSYCLInterfaces/QueueHolder.h"

// Gaudi include(s).
#include <GaudiKernel/IService.h>

namespace AthSYCL {

   /// Interface for using @c cl::sycl::queue objects in Gaudi/Athena
   ///
   /// This interface can be used to access shared or unique
   /// @c std::sycl::queue instances in algorithms/tools during an Athena
   /// job.
   ///
   /// The reason that algorithms shouldn't hold on to these themselves is
   /// because queue objects are non-const, so reentrant algorithms can't
   /// easily hold them as member variables.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class IQueuePoolSvc : public virtual IService {

      /// Make @c AthSYCL::QueueHolder a friend of this interface
      friend class QueueHolder;

   public:
      /// Declare the interface ID
      DeclareInterfaceID( AthSYCL::IQueuePoolSvc, 1, 0 );

      /// Function for accessing a @c cl::sycl::queue object during the job
      ///
      /// This function should be used in the execute function of algorithms
      /// and tools to access a SYCL queue to which they may send calculations.
      ///
      /// @return A smart object wrapping a @c cl::sycl::queue object
      ///
      virtual QueueHolder getQueue() = 0;

      /// Function to call after an asynchronous calculation has finished
      ///
      /// This is meant to allow the service to keep track of how many
      /// calculations are in flight at any given moment, and return queue
      /// objects to the callers accordingly.
      ///
      virtual void asyncCalcFinished() = 0;

   protected:
      /// Put a given queue back into the pool
      ///
      /// This function is used by @c AthSYCL::QueueHolder to return its
      /// @c cl::sycl::queue object into the service's pool once the user no
      /// longer needs it.
      ///
      /// @param holder The @c cl::sycl::queue holder object
      ///
      virtual void yieldQueue( QueueHolder& holder ) = 0;

   }; // class IQueuePoolSvc

} // namespace AthSYCL

#endif // ATHSYCLINTERFACES_IQUEUEPOOLSVC_H
