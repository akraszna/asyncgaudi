// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCEVENTLOOPCOMPS_CPUCRUNCHERALG_H
#define ASYNCEVENTLOOPCOMPS_CPUCRUNCHERALG_H

// Project include(s).
#include "AsyncBaseComps/Algorithm.h"
#include "AsyncBaseComps/ReadHandleKeyArray.h"
#include "AsyncBaseComps/WriteHandleKeyArray.h"

// Gaudi include(s).
#include <GaudiKernel/ICPUCrunchSvc.h>
#include <GaudiKernel/Property.h>
#include <GaudiKernel/ServiceHandle.h>

// System include(s).
#include <memory>
#include <random>
#include <string>
#include <vector>

namespace ASync {

   /// A reentrant version of Gaudi's @c CPUCruncher algorithm
   ///
   /// Unfortunately the Gaudi algorithm wants to use the event store in a
   /// way that I just didn't want to support with the asynchronous code. :-(
   /// So it seemed simplest to just provide a separate implementation of this
   /// algorithm.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class CPUCruncherAlg : public Algorithm {

   public:
      // Inherit the base class's constructor(s).
      using ASync::Algorithm::Algorithm;

      /// @name Basic algorithm function(s).
      /// @{

      /// Function initialising the algorithm
      virtual StatusCode initialize() override;
      /// Function executing the algorithm
      virtual StatusCode execute( const EventContext& ctx ) const override;

      /// @}

   private:
      /// @name Algorithm properties
      /// @{

      /// Key(s) of the input objects
      Gaudi::Property< ReadHandleKeyArray > m_inputKeys{ this,
         "InputKeys", ReadHandleKeyArray( *this ),
         "Keys of input objects that the algorithm needs to wait for" };
      Gaudi::Property< WriteHandleKeyArray > m_outputKeys{ this,
         "OutputKeys", WriteHandleKeyArray( *this ),
         "Keys of output objects that the algorithm produces" };

      Gaudi::Property< double > m_avgRuntime{ this, "AverageRuntime", 0.1,
         "Average runtime for the algorithm, in seconds" };
      Gaudi::Property< double > m_varRuntime{ this, "RuntimeVariation", 0.05,
         "Variation in the runtime, in seconds" };

      /// @}

      /// @name Implementation variable(s)
      /// @{

      /// Service to use for crunching the CPU
      ServiceHandle< ICPUCrunchSvc > m_svc{ this, "CPUCrunchSvc",
         "CPUCrunchSvc", "The service to use for crunching the CPU" };

      /// Object to generate the per-event CPU crunch times with
      std::unique_ptr< std::normal_distribution< double > > m_cpuCrunchTime;

      /// @}

   }; // CPUCruncherAlg

} // namespace ASync

#endif // ASYNCEVENTLOOPCOMPS_CPUCRUNCHERALG_H
