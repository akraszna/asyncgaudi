// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "CPUGPUCruncherCalibSvc.h"

// Project include(s).
#include "AsyncBaseComps/MessagingMacros.h"

// ROOT include(s).
#include <TF2.h>
#include <TGraph2D.h>

// System include(s).
#include <cmath>

DECLARE_COMPONENT( ASync::CPUGPUCruncherCalibSvc )

namespace ASync {

   /// @name Measurement points on the FPOps - ArraySize grid
   /// @{

   static const std::size_t
   FPOPS_VALUES[ CPUGPUCruncherCalibSvc::N_FPOPS_VALUES ] = {
      10, 100, 500, 1000, 5000, 10000, 17500, 25000
   };

   static const std::size_t
   ARSIZ_VALUES[ CPUGPUCruncherCalibSvc::N_ARSIZ_VALUES ] = {
      10, 100, 500, 1000, 5000, 10000, 17500, 25000
   };

   /// @}

   StatusCode CPUGPUCruncherCalibSvc::initialize() {

      // Tell the user what's happening.
      ATH_MSG_INFO( "Calibrating the host CPU's speed... "
                    "(this may take a bit)" );

      // Perform the measurements.
      for( std::size_t i = 0; i < N_ARSIZ_VALUES; ++i ) {
         for( std::size_t j = 0; j < N_FPOPS_VALUES; ++j ) {
            m_measurements[ i ][ j ] = getTimeNeededFor( ARSIZ_VALUES[ i ],
                                                         FPOPS_VALUES[ j ] );
            ATH_MSG_DEBUG( "Time needed for " << FPOPS_VALUES[ j ]
                           << " FP operations on " << ARSIZ_VALUES[ i ]
                           << " numbers: "
                           << m_measurements[ i ][ j ].count() << " ns" );
         }
      }

      // Set up a 2-dimensional ROOT graph with these measurements.
      TGraph2D measGraph( N_ARSIZ_VALUES * N_FPOPS_VALUES );
      for( std::size_t i = 0; i < N_ARSIZ_VALUES; ++i ) {
         for( std::size_t j = 0; j < N_FPOPS_VALUES; ++j ) {
            measGraph.SetPoint( i * N_FPOPS_VALUES + j,
                                ARSIZ_VALUES[ i ], FPOPS_VALUES[ j ],
                                m_measurements[ i ][ j ].count() * 1e-6 );
         }
      }

      // Set up a function that we'll fit the measurements with.
      TF2 fitFunc( "CPUGPUCalibFunc", "[0]*x*y",
                   0, ARSIZ_VALUES[ N_ARSIZ_VALUES - 1 ] + 10.0,
                   0, FPOPS_VALUES[ N_FPOPS_VALUES - 1 ] + 10.0 );
      static const char* COEFF_NAME = "coeff";
      fitFunc.SetParNames( COEFF_NAME );

      // Fit the graph with the function.
      measGraph.Fit( &fitFunc, "WQ" );

      // Remember the result of the fit.
      m_coefficient = fitFunc.GetParameter( COEFF_NAME );
      ATH_MSG_DEBUG( COEFF_NAME << " = " << m_coefficient );

      // Tell the user that we're finished.
      ATH_MSG_INFO( "Calibrating the host CPU's speed... done" );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   std::size_t CPUGPUCruncherCalibSvc::
   getFPOperationsFor( std::size_t arraySize,
                       const std::chrono::milliseconds& duration ) const {

      // std::ceil is used to make sure that we would never return 0 here. :-P
      return std::ceil( duration.count() / ( m_coefficient * arraySize ) );
   }

   std::chrono::nanoseconds CPUGPUCruncherCalibSvc::
   getTimeNeededFor( std::size_t arraySize, std::size_t fpOps ) const {

      // Create a dummy array that makes sure that calculations are not
      // optimised out by the compiler.
      std::vector< float > array( arraySize, 1.234f );

      // Remember the start time.
      auto start = std::chrono::high_resolution_clock::now();

      // Perform the requested operations. In a way that stop compilers
      // from simplifying these simple operations.
      for( std::size_t i = 0; i < arraySize; ++i ) {
         for( std::size_t j = 0; j < fpOps; ++j ) {
            array[ i ] *= array[ ( i + j ) % arraySize ];
         }
      }

      // Get the finish time.
      auto finish = std::chrono::high_resolution_clock::now();

      // Return the time needed for the calculation.
      return std::chrono::duration_cast< std::chrono::nanoseconds >( finish -
                                                                     start );
   }

} // namespace ASync
