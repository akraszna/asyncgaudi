// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "CPUCruncherAlg.h"

// System include(s).
#include <algorithm>

DECLARE_COMPONENT( ASync::CPUCruncherAlg )

namespace ASync {

   StatusCode CPUCruncherAlg::initialize() {

      // Retrieve the CPU cruncher service.
      ATH_CHECK( m_svc.retrieve() );

      // Set up the random distribution generator object.
      const double avgTime = std::max( m_avgRuntime.value(), 0.0 );
      const double varTime = std::max( m_varRuntime.value(), 0.0 );
      m_cpuCrunchTime =
         std::make_unique< std::normal_distribution< double > >( avgTime,
                                                                 varTime );

      // Tell the user what is about to happen.
      ATH_MSG_INFO( "Will execute for " << avgTime << " +- " << varTime
                    << " second(s)" );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode CPUCruncherAlg::execute( const EventContext& ctx ) const {

      // Set up the random number generator.
      std::default_random_engine engine( ctx.evt() );

      // Figure out how long we want to spend with crunching the CPU.
      const double seconds = std::max( ( *m_cpuCrunchTime )( engine ), 0.0 );
      const unsigned int milliseconds = seconds * 1000;

      // Crunch the CPU.
      m_svc->crunch_for( std::chrono::milliseconds( milliseconds ) );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

} // namespace ASync
