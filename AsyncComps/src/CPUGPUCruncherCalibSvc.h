// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCCOMPS_CPUGPUCRUNCHERCALIBSVC_H
#define ASYNCCOMPS_CPUGPUCRUNCHERCALIBSVC_H

// Local include(s).
#include "AsyncComps/ICPUGPUCruncherCalibSvc.h"

// Gaudi include(s).
#include <GaudiKernel/Service.h>

// System include(s).
#include <array>

namespace ASync {

   /// CPU/GPU crunch calibration service
   ///
   /// This service fulfills a very similar role to @c CPUCruncherSvc in
   /// Gaudi. It can figure out how many CPU operations correspond to a certain
   /// time duration on the runner host.
   ///
   /// Unlike @c CPUCruncherSvc though it doesn't perform any calculations
   /// during runtime. That is left up to the algorithms, to perform in
   /// whichever way that they want. (Allowing us to write different CPU/GPU
   /// offloading/sharing codes.)
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class CPUGPUCruncherCalibSvc : public extends< Service,
                                                  ICPUGPUCruncherCalibSvc > {

   public:
      /// Inherit the base class's constructor(s)
      using extends::extends;

      /// @name Interface inherited from @c IService
      /// @{

      /// Initialise the service
      virtual StatusCode initialize() override;

      /// @}

      /// @name Interface inherited from @c ASync::ICPUGPUCruncherCalibSvc
      /// @{

      /// CPU "calibration" function
      virtual std::size_t
      getFPOperationsFor( std::size_t arraySize,
                          const std::chrono::milliseconds& duration ) const override;

      /// @}

      /// Measure the time needed to run a calculation on the CPU
      ///
      /// @param arraySize The size of the array to process
      /// @param fpOps The number of floating point operations to execute
      /// @return The elapsed time in nanoseconds
      ///
      std::chrono::nanoseconds getTimeNeededFor( std::size_t arraySize,
                                                  std::size_t fpOps ) const;

      /// The number of floating point operation measurement points
      static const std::size_t N_FPOPS_VALUES = 8;
      /// The number of array size measurement points
      static const std::size_t N_ARSIZ_VALUES = 8;

      /// Array holding the measured values
      std::array< std::array< std::chrono::nanoseconds,
                     N_FPOPS_VALUES >, N_ARSIZ_VALUES >
         m_measurements;

      /// Calibrated coefficient used in the main function of the service
      double m_coefficient = 1.0;

   }; // class CPUGPUCruncherCalibSvc

} // namespace ASync

#endif // ASYNCCOMPS_CPUGPUCRUNCHERCALIBSVC_H
