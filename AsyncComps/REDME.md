# Generic components used in asynchronous tests

This package collects components (algorithms) that don't use any GPU technology
themselves, but can come in handy in tests with GPU code.
