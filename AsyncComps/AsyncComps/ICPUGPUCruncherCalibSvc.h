// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#ifndef ASYNCCOMPS_ICPUGPUCRUNCHERCALIBSVC_H
#define ASYNCCOMPS_ICPUGPUCRUNCHERCALIBSVC_H

// Gaudi include(s).
#include <GaudiKernel/IService.h>

// System include(s).
#include <chrono>

namespace ASync {

   /// Interface for the CPU/GPU crunch calibration service
   ///
   /// The service behind this interface is meant to figure out during
   /// initialisation how many floating point operations equal a certain
   /// amount of calculation time on the runner host. This information can
   /// then be used to execute the same number of floating point operations
   /// on the/a CPU or the/a GPU during the job.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class ICPUGPUCruncherCalibSvc : public virtual IService {

   public:
      /// Declare the interface ID
      DeclareInterfaceID( ASync::ICPUGPUCruncherCalibSvc, 1, 0 );

      /// CPU "calibration" function
      ///
      /// Calculate the number of floating point operations needed on a given
      /// array size (per element) to use up a specific amount of runtime for
      /// the calculation.
      ///
      /// @param arraySize The size of the floating point array to use
      /// @param duration  The intended duration of the calculation on the CPU
      /// @return The number of floating point operations for this calculation
      ///
      virtual std::size_t
      getFPOperationsFor( std::size_t arraySize,
                          const std::chrono::milliseconds& duration ) const = 0;

   }; // class ICPUGPUCruncherCalibSvc

} // namespace ASync

#endif // ASYNCCOMPS_ICPUGPUCRUNCHERCALIBSVC_H
